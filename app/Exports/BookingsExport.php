<?php

namespace App\Exports;

use App\Booking;
use App\BookingStatus;
use App\Customer;
use App\Doctor;
use App\Office;
use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Request;

class BookingsExport implements FromQuery, WithStrictNullComparison, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize
{
    public $request;
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct($request)
    {
        $this->request = $request;
    }
       
    public function query()
    {
        $request = $this->request;
        $bookings = Booking::with('office', 'office.doctor', 'time','procedure', 'bookingStatus', 'visitStatus');
        if ($request->filled('appoinrange')) {
            $arr = explode(' - ', request('appoinrange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();
            $bookings->whereBetween('appointment_date', [$startDate, $endDate]);
        }
        if ($request->filled('recordrange')) {
            $arr = explode(' - ', request('recordrange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $bookings->whereBetween('created_at', [$startDate, $endDate]);
        }

        if ($request->filled('user'))
        {
            $userIds = User::searchUser(trim($request->user), 'id');
            $doctorIds = Doctor::whereIn('user_id', $userIds)->pluck('id')->toArray();
            $customerIds = Customer::whereIn('user_id', $userIds)->pluck('id')->toArray();
            $officeIds = Office::whereIn('doctor_id', $doctorIds)->pluck('id')->toArray();
            $bookings->whereIn('customer_id', $customerIds)->orWhereIn('office_id', $officeIds);
        }
        if ($request->filled('patient')) {
            $bookings->where('patient_name', 'LIKE', '%' . $request->patient . '%');
        }
        if ($request->filled('patient_phone')) {
            $bookings->where('mobile', 'LIKE', '%' . $request->patient_phone . '%');
        }
        if ($request->filled('time')) {
            $bookings->whereHas('time', function ($query) use ($request) {
                $query->where('key', 'like', $request->time);
            });
        }
        if ($request->filled('region')) {
            $bookings->whereHas('office', function ($query) use ($request) {
                $query->whereHas('region', function ($query2) use ($request) {
                    $query2->where('key', 'like', $request->region);
                });
            });
        }
        if ($request->filled('bookingstatus')) {
            $bookings->whereHas('bookingStatus', function ($query) use ($request) {
                $query->where('key', 'like', $request->bookingstatus);
            });
        }

        if ($request->filled('visitstatus')) {
            $bookings->whereHas('visitStatus', function ($query) use ($request) {
                $query->where('key', 'like', $request->visitstatus);
            });
        }

        if ($request->filled('office')) {
            $bookings->whereHas('office', function ($query) use ($request) {
                $query->where('name_ar', 'LIKE', '%' . $request->office . '%');
            });
        }

        if ($request->filled('feestart')) {
            $bookings->whereHas('office', function ($query) use ($request) {
                $query->where('fees', '>=', $request->feestart);
            });
        }
        if ($request->filled('feeend')) {
            $bookings->whereHas('office', function ($query) use ($request) {
                $query->where('fees', '<=', $request->feeend);
            });
        }

        if ($request->filled('when')) {
            if ($request->when == 'upcoming'){
                $bookings->upcoming();
            }
            elseif ($request->when == 'past'){
                $bookings->past();
            }
        }

        if ($request->filled('invoice')) {
            $bookings->where('is_invoiced', $request->invoice);
        }

        return $bookings;
    }

    public function map($booking): array
    {
        $office = $booking->office;
        $doctor = optional($office)->doctor;
        $time = $booking->time;
        $bookingStatus = $booking->bookingStatus;
        $visitStatus = $booking->visitStatus;
        $procedure = $booking->procedure;

        return [
            $booking->id,
            $booking->patient_name,
            optional($doctor)->getName(),
            $booking->mobile,
            $booking->email,
            Date::dateTimeToExcel($booking->appointment_date),
            $booking->appointment_date->englishDayOfWeek,
            $time->name_ar,
            optional($bookingStatus)->name_ar,
            optional($visitStatus)->name_ar,
            $booking->is_invoiced ? "نعم" : "لا",
            optional($procedure)->name_ar,
            $booking->cost,
            $booking->patient_review,
        ];
    }

    public function headings(): array
    {
        return [
            "#",
            "Patient's Name",
            "Doctor's Name",
            "Patient's Number",
            "Patient's Email",
            "Date of Reservation",
            "The Day",
            "Time",
            "Booking Status",
            "Visit Status",
            "Invoice",
            "Procedure",
            "Cost",
            "Patient Review",
        ];
    }

    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}
