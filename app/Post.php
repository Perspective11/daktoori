<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Session;

class Post extends Model
{
    use StatisticsFunctions;
    protected $with = ['category'];
    protected $fillable = [
        "title_en",
        "title_ar",
        "body_en",
        "body_ar",
        "status",
        "type",
        "picture"
    ];
    public static $TYPE_NORMAL_POST = 0;
    public static $TYPE_FEATURED_POST = 1;
    public static $LIMIT_FEATURED_POSTS = 4;

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function getCategoryName()
    {
        if ($category = $this->category){
            return $category->name_ar;
        }
        return '';
    }

    public function path($admin = null){
        if($admin){
            return '/admin/posts/' . $this->id;
        }
        return '/blog/' . $this->id;
    }
    public function togglePublished(){
        if ($this->status){
            $this->status = 0;
            Session::flash('toast','Post is unpublished');
        }
        else{
            $this->status = 1;
            Session::flash('toast','Post is published');
        }
        $this->save();
    }
    public function toggleFeatured(){
        if ($this->type == static::$TYPE_FEATURED_POST){// if post is featured post
            $this->type = static::$TYPE_NORMAL_POST;
            Session::flash('toast', ['Post is not featured anymore', 'success']);
        }
        else if ($this->type == static::$TYPE_NORMAL_POST){
            if (static::where('type', static::$TYPE_FEATURED_POST)->count() >= static::$LIMIT_FEATURED_POSTS){ // if there are more than 4 featured posts
                Session::flash('toast', ['You cant have more than ' . static::$LIMIT_FEATURED_POSTS . ' featured posts', 'error']);
                return;
            }
            if (! $this->picture) {
                Session::flash('toast', ['A featured post must have an image', 'error']);
                return;
            }
            $this->type = static::$TYPE_FEATURED_POST;
            Session::flash('toast', ['Post will now appear in the home page blog', 'success']);
        }
        $this->save();
    }

    public function getPicture(){
        if ($this->picture){
            return $this->picture;
        }
        else return '/images/posts/default.png';
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }
    public function scopeFeatured($query)
    {
        return $query->where('type', static::$TYPE_FEATURED_POST);
    }

    public function scopeSearch($query, $keywords)
    {
        return $query
            ->where('title_en', 'LIKE', '%' . $keywords . '%')
            ->orWhere('title_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('body_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('body_en', 'LIKE', '%' . $keywords . '%');
    }

    public static function publishedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);
        $publishedCount = static::where('created_at', '>=', $date->toDateString())->where('status', 1)->count();
        $count = static::where('created_at', '>=', $date->toDateString())->count();

        return compact('publishedCount', 'count');
    }
}
