<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $fillable = [
        'id',
        'name_ar',
        'name_en',
        'company_ar',
        'company_en',
        'address_ar',
        'address_en',
        'phone',
        'description_ar',
        'description_en',
        'longitude',
        'latitude',
    ];


    public function offices()
    {
        return $this->belongsToMany('App\Office');
    }
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }
    public function getDoctors()
    {
        $doctors = collect([]);
        foreach ($this->offices as $office){
            $doctors[] = $office->doctor;
        }
        return $doctors;
    }

    public function path($admin = false){
        if($admin)
            return '/admin/insurances/' . $this->id;

        return '/insurances/' . $this->id;
    }
}
