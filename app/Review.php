<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public static $REVIEW_THRESHOLD = 1;
    protected $fillable = [
        'doctor_id',
        'customer_id',
        'patient_name',
        'rating',
        'review',
        'description',
        'rating_approved',
        'review_approved',
        'review_hidden',
        'featured',
    ];

    public static $LIMIT_FEATURED_REVIEWS = 4;

    public function doctor()
    {
        return $this->belongsTo('App\Doctor');
    }
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
    public function setApproved()
    {
        $this->rating_approved = 1;
        $this->save();
    }
    public function setDisapproved()
    {
        $this->rating_approved = 0;
        $this->save();
    }
    public function setFeatured()
    {
        $this->featured = 1;
        $this->save();
    }
    public function setUnfeatured()
    {
        $this->featured = 0;
        $this->save();
    }
    public static function applyRatings(){
        Doctor::all()->each(function ($doctor){
            $doctor->updateRatings();
        });
    }
    public function scopeApproved($query)
    {
        return $query->where('rating_approved', 1);
    }
    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }
    public function scopeHasComment($query)
    {
        return $query->whereNotNull('review')->where('review', '!=', '');
    }
    public function getRating($percentage = false){
        if ($percentage){
            return $this->rating / 5 * 100;
        }
        return round($this->rating, 1);
    }

    public function path($admin = null){
        if($admin){
            return '/admin/reviews/' . $this->id;
        }
        return '/reviews/' . $this->id;
    }
}
