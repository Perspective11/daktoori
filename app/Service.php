<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name_ar',
        'name_en',
    ];
    public function offices()
    {
        return $this->belongsToMany('App\Office');
    }


    public function path($admin = false){
        if($admin)
            return '/admin/services/' . $this->id;

        return '/services/' . $this->id;
    }
}
