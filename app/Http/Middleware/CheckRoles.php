<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $allow = false;
        foreach ($roles as $role){
            if (Auth::user()->isRole($role)){
                $allow = true;
            }
        }
        if ($allow){
            return $next($request);
        }
        else return redirect('/');
    }
}
