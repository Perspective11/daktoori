<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use View;


class PostsController extends Controller
{
    function __construct()
    {
        Carbon::setLocale('ar');
        $allPosts = Post::published()->latest()->get();
        $groupedPosts = $allPosts->groupBy(function($e) {
            return Carbon::parse($e->created_at)->format('Y - m');
        });
        $categories = Category::whereHas('posts')->withCount('posts')->get();
        $featuredPosts = Post::featured()->published()->latest()->get();
        View::share(compact('allPosts', 'groupedPosts', 'featuredPosts', 'categories'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::published();

        if ($request->filled('category')) {
            $posts->whereHas('category', function ($query) use ($request) {
                $query->where('name_ar', 'like', $request->category);
            });
        }
        if ($request->filled('date')) {
            $parsedDate = Carbon::parse($request->date);
            $startDate = $parsedDate->toDateString();
            $afterMonth = $parsedDate->addMonth(1)->addDays(-1);
            $endDate = $afterMonth->toDateString();
            $posts->whereBetween('created_at', [$startDate, $endDate]);
        }
        if ($request->filled('search')) {
            $posts->search($request->search);
        }
        $posts = $posts->latest()->paginate(10);

        return view('posts.index', compact('posts'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $singlePost = Post::findOrFail($id);

        if ($singlePost->status != 1)
            abort(404);

        $prevPost = '';
        $nextPost = '';
        try{
            $previousId = Post::where('id', '<', $singlePost->id)->max('id');
            $prevPost = Post::find($previousId);
        }
        catch (Exception $e){

        }
        try{
            $nextId = Post::where('id', '>', $singlePost->id)->min('id');
            $nextPost = Post::find($nextId);
        }
        catch (Exception $e){

        }
        return view('posts.show', compact('singlePost', 'prevPost', 'nextPost'));
    }

}
