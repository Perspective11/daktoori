<?php

namespace App\Http\Controllers\admin;

use App\Booking;
use App\Doctor;
use App\Http\Controllers\Controller;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statuses = \App\BookingStatus::where('is_approved', '>=', 1)->pluck('id', 'key')->toArray(); //  select those not waiting for approval

        $recentPosts = Post::latest()->take(6)->get();
        $unapprovedDoctors = Doctor::where('is_approved', 0)->with('offices.region', 'offices.city')->latest()->take(6)->get();
        $unapprovedBookings = Booking::whereNotIn('booking_status_id', $statuses)->upcoming()->with('office.doctor', 'customer')->latest()->take(6)->get();
        return view('admin.home', compact('recentEvents', 'unapprovedDoctors', 'unapprovedBookings'));
    }
}
