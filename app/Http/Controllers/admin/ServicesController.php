<?php

namespace App\Http\Controllers\admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServicesRequest;
use App\Service;
use DataTables;
use Illuminate\Http\Request;
use Session;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service = new Service;

        return view('admin.services.create', compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServicesRequest $request)
    {

        $service = Service::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ]);

        Session::flash('toast', ['Service is Successfully Created', 'success']);

        return redirect("/admin/services");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);

        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServicesRequest $request, Service $service)
    {
        $service->update([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ]);
        Session::flash('toast', 'Service is Successfully Updated');

        return redirect("/admin/services");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if ($service->offices()->count()) {
            if (request()->wantsJson()) {
                return response()->json(['msg' => 'You can\'t delete this service unless you have deleted all the doctors associated with it'], 501);
            }
            Session::flash('toast', ['Unable to delete the service!', 'error']);

            return redirect('admin/services');
        }

        $service->delete();
        if (request()->wantsJson()) {
            return response()->json([], 200);
        }
        Session::flash('toast', 'Service Deleted successfully');

        return redirect('admin/services');
    }

    public function getServicesData(Request $request)
    {
        $services = Service::query();

        return DataTables::of($services)->editColumn('created_at', function ($service) {
            return $service->created_at->toFormattedDateString();
        })->addColumn('offices_count', function ($service) {
            $count = $service->offices()->count();

            return "<div class='text-center'><span class='label label-primary label-lg'>{$count}</span></div>";
        })->addColumn('actions', function ($service) {
            $buttonGroupStart = '<div class="btn-group btn-group-xs">';
            $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $service->path(true) . '/edit' . '" class="btn btn-xs service-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
            $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $service->path(true) . '" class="btn btn-xs service-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
            $buttonGroupEnd = '</div>';

            return $edit . $delete;
        })->rawColumns(['actions', 'offices_count'])->make(true);
    }
}
