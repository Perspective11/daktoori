<?php

namespace App\Http\Controllers\admin;

use App\ImageContent;
use App\Http\Controllers\Controller;
use App\Picture;
use DB;
use Illuminate\Http\Request;
use Session;

class ImageContentsController extends Controller
{
    public function show()
    {
        $imageContents = ImageContent::all();
        return view('admin.image-contents.show' , compact('imageContents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $imageContents = ImageContent::all();
        return view('admin.image-contents.edit' , compact('imageContents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $paths = $request->updated_path;
        $keys = $request->key;
        $updatedCounter = 0;
        for ($i = 0; $i <= count($keys); $i++) {
            if (! isset($paths[$i])){
                continue;
            }
            $image = $paths[$i];
            $picture_name = ImageContent::getSanitizedName($image);
            $picture_path = 'media/image_contents/';
            $image->move($picture_path, $picture_name);
            $picture_path = '/' . $picture_path;
            ImageContent::where('key', $keys[$i])->first()->update([
                "image_path" => $picture_path . $picture_name,
            ]);
            $updatedCounter ++;
        }
        if (! $updatedCounter){
            Session::flash('toast', ['No Image Contents Were Updated', 'info']);
        }
        else
            Session::flash('toast', [$updatedCounter . ' Image Contents Successfully Updated', 'success']);
        return redirect()->back();
    }


}
