<?php

namespace App\Http\Controllers\admin;

use App\Booking;
use App\BookingStatus;
use App\VisitStatus;
use App\Customer;
use App\Doctor;
use App\Exports\BookingsExport;
use App\Mail\PatientBookingConfirmed;
use App\Office;
use App\User;
use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Procedure;
use Excel;
use Session;
use Validator;

class BookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.bookings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        $booking->load('office', 'office.doctor', 'time', 'bookingStatus', 'visitStatus','procedure');
        $office = $booking->office;
        $time = $booking->time;
        $bookingStatus = $booking->bookingStatus;
        $bookingStatuses = BookingStatus::all();
        $visitStatuses = VisitStatus::all();
        $visitStatus = $booking->visitStatus;
        $bookingProcedure = $booking->procedure;
        $procedures = Procedure::all();
        return view('admin.bookings.show', compact('booking', 'office', 'time', 'bookingStatus', 'bookingStatuses', 'bookingProcedure', 'procedures', 'visitStatus', 'visitStatuses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        $validator = Validator::make($request->all(), [
            "patient_confirmed"  => 'in:0,1',
            "is_invoiced"        => 'in:0,1',
            "note"               => 'max:255',
            "booking_status_id"  => 'exists:booking_statuses,id',
            "visit_status_id"  => 'exists:visit_statuses,id',
            "procedure_id"  => 'exists:procedures,id',
            "cost" => 'integer|between:0,1000000',
            "patient_review" => 'numeric|between:0,5.0'
        ]);
        $validator->validate();

        $booking->patient_confirmed = $request->patient_confirmed;
        $booking->is_invoiced = $request->is_invoiced;
        $booking->scheduled_datetime = Carbon::parse($request->scheduled_datetime);
        $booking->actual_datetime = Carbon::parse($request->actual_datetime);
        $booking->note = $request->note;
        $booking->booking_status_id = $request->bookingStatus;
        $booking->visit_status_id = $request->visitStatus;
        $booking->procedure_id = $request->procedure;
        $booking->cost = $request->cost;
        $booking->patient_review = $request->patient_review;
        $booking->save();
        Session::flash('toast', 'Booking is Successfully Updated');
        return redirect($booking->path(true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $booking->delete();
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', 'Booking is Successfully Deleted');
        return redirect('admin/bookings');
    }

    public function changeStatus(Booking $booking, $change_to)
    {
        $booking->booking_status_id = $change_to;
        $booking->save();
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', 'Booking Status Successfully Changed');
        return redirect('admin/bookings');
    }
    public function changeVisitStatus(Booking $booking, $change_to)
    {
        $booking->visit_status_id = $change_to;
        $booking->save();
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', 'Visit Status Successfully Changed');
        return redirect('admin/bookings');
    }
    public function changeProcedure(Booking $booking, $change_to)
    {
        $booking->procedure_id = $change_to;
        $booking->save();
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', 'Booking Procedure Successfully Changed');
        return redirect('admin/bookings');
    }

    public function confirmPatient(Booking $booking, $change_to)
    {
        $booking->patient_confirmed = $change_to;
        $booking->save();
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', 'Patient Confirmation Successfully Changed');
        return redirect('admin/bookings');
    }

    public function confirmInvoiced(Booking $booking, $change_to)
    {
        $booking->is_invoiced = $change_to;
        $booking->save();
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', 'Patient Confirmation Successfully Changed');
        return redirect('admin/bookings');
    }
    public function approved(Booking $booking, $action = null)
    {
        if ($action == 'approve'){
            $booking->setApproved();
            \Mail::to($booking->email)->send(new PatientBookingConfirmed($booking));
            if (request()->wantsJson()){
                return response()
                    ->json(['msg' => 'Booking Successfully Approved'], 200);
            }
            return back();
        }

        if ($action == 'reject'){
            $booking->setRejected();
            if (request()->wantsJson()){
                return response()
                    ->json(['msg' => 'Booking Successfully Rejected'], 200);
            }
            return back();
        }

        $booking->toggleApproved();
        return back();
    }

    public function export(Request $request){
        ini_set('memory_limit', '1024M');
        return Excel::download(new BookingsExport($request), "bookings-" . Carbon::now()->toDateTimeString() . ".xlsx");
        ini_set('memory_limit', '256M');

    }

    public function getBookingsData(Request $request)
    {
        $statuses = \App\BookingStatus::select(['id','name_ar'])->get()->toArray();
        $visitStatuses = \App\VisitStatus::select(['id','name_ar'])->get()->toArray();
        $procedures = \App\Procedure::select(['id','name_ar'])->get()->toArray();
        $bookings = Booking::with('office', 'customer', 'time', 'bookingStatus', 'procedure', 'visitStatus');
        if ($request->filled('appoinrange')) {
            $arr = explode(' - ', request('appoinrange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();
            $bookings->whereBetween('appointment_date', [$startDate, $endDate]);
        }
        if ($request->filled('recordrange')) {
            $arr = explode(' - ', request('recordrange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $bookings->whereBetween('created_at', [$startDate, $endDate]);
        }

        if ($request->filled('user'))
        {
            $userIds = User::searchUser(trim($request->user), 'id');
            $doctorIds = Doctor::whereIn('user_id', $userIds)->pluck('id')->toArray();
            $customerIds = Customer::whereIn('user_id', $userIds)->pluck('id')->toArray();
            $officeIds = Office::whereIn('doctor_id', $doctorIds)->pluck('id')->toArray();
            $bookings->whereIn('customer_id', $customerIds)->orWhereIn('office_id', $officeIds);
        }
        if ($request->filled('patient')) {
            $bookings->where('patient_name', 'LIKE', '%' . $request->patient . '%');
        }
        if ($request->filled('patient_phone')) {
            $bookings->where('mobile', 'LIKE', '%' . $request->patient_phone . '%');
        }
        if ($request->filled('time')) {
            $bookings->whereHas('time', function ($query) use ($request) {
                $query->where('key', 'like', $request->time);
            });
        }
        if ($request->filled('region')) {
            $bookings->whereHas('office', function ($query) use ($request) {
                $query->whereHas('region', function ($query2) use ($request) {
                    $query2->where('key', 'like', $request->region);
                });
            });
        }
        if ($request->filled('bookingstatus')) {
            $bookings->whereHas('bookingStatus', function ($query) use ($request) {
                $query->where('key', 'like', $request->bookingstatus);
            });
        }

        if ($request->filled('visitstatus')) {
            $bookings->whereHas('visitStatus', function ($query) use ($request) {
                $query->where('key', 'like', $request->visitstatus);
            });
        }

        if ($request->filled('office')) {
            $bookings->whereHas('office', function ($query) use ($request) {
                $query->where('name_ar', 'LIKE', '%' . $request->office . '%');
            });
        }

        if ($request->filled('feestart')) {
            $bookings->whereHas('office', function ($query) use ($request) {
                $query->where('fees', '>=', $request->feestart);
            });
        }
        if ($request->filled('feeend')) {
            $bookings->whereHas('office', function ($query) use ($request) {
                $query->where('fees', '<=', $request->feeend);
            });
        }

        if ($request->filled('when')) {
            if ($request->when == 'upcoming'){
                $bookings->upcoming();
            }
            elseif ($request->when == 'past'){
                $bookings->past();
            }
        }

        if ($request->filled('invoice')) {
            $bookings->where('is_invoiced', $request->invoice);
        }

        return DataTables::of($bookings)
            ->editColumn('created_at', function ($booking)
            {
                return $booking->created_at->toDateTimeString();
            })
            ->addColumn('doctor', function ($booking)
            {
                if (! $booking->office){
                    return "تم حذف الدكتور";
                }
                $doctor = $booking->office->doctor;
                return '<a data-toggle="tooltip" title="Click to View Doctor" href="' . $doctor->userPath(true) . '" class="">' . $doctor->getName() . '</a>';
            })
            ->addColumn('customer', function ($booking)
            {
                if ($customer = $booking->customer)
                {
                    return '<a data-toggle="tooltip" title="Click to View Customer Account" href="' . $customer->userPath(true) . '" class="">' . $customer->getName() . '</a>';
                }
                return '';
            })
            ->addColumn('patient_name', function ($booking)
            {
                return $booking->patient_name;
            })
            ->addColumn('doctor_phone', function ($booking)
            {
                if (! $booking->office){
                    return "تم حذف الدكتور";
                }
                $doctor = $booking->office->doctor;
                return $doctor->mobile;
            })
            ->addColumn('office', function ($booking)
            {
                if (! $booking->office){
                    return "تم حذف الدكتور";
                }
                $office = $booking->office;
                return '<a data-toggle="tooltip" title="Click to View Office" href="' . $office->path(true) . '" class="">' . $office->name_ar . '</a>';
            })
            ->addColumn('insurance', function ($booking)
            {
                if ($insurance = $booking->insurance)
                    return '<a data-toggle="tooltip" title="Click to View Insurance" href="' . $insurance->path(true) . '" class="">' . $insurance->name_ar . '</a>';
                return '';
            })
            ->addColumn('time', function ($booking)
            {
                $time = $booking->time->name_ar;
                return "<div class='text-center'><span class='label label-warning label-lg'>{$time}</span></div>";
            })
            ->addColumn('when', function ($booking)
            {
                return $booking->getWhen();
            })
            ->addColumn('fees', function ($booking)
            {
                if (! $booking->office){
                    return "تم حذف الدكتور";
                }
                if ($booking->office->fees){
                    return $booking->office->fees . ' ريال';
                }
                return '';
            })
            ->editColumn('appointment_date', function ($booking)
            {
                return $booking->appointment_date->format('l Y/m/d');
            })
            ->addColumn('region', function ($booking) {
                if (! $booking->office){
                    return "تم حذف الدكتور";
                }
                return $booking->office->region->name_ar;
            })
            ->addColumn('actions', function ($booking) use ($statuses, $procedures, $visitStatuses) {
                $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $booking->path(true) . '" class="btn btn-xs booking-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
                $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $booking->path(true) . '" class="btn btn-xs booking-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
                $statusOptions = '';
                $visitOptions = '';
                $procedureOptions = '';
                foreach($statuses as $status){
                    $statusOptions .= '<li><a class="change-status" href="' . $booking->path(true) . '/changeStatus/' . $status['id'] . '">' . $status['name_ar'] . '</a></li>';
                }
                $dropdownStatus = '<div class="btn-group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ' .$booking->bookingStatus->name_ar . '<span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                ' . $statusOptions . '
                              </ul>
                            </div>';
                foreach($visitStatuses as $visitStatus){
                    $visitOptions .= '<li><a class="change-visit-status" href="' . $booking->path(true) . '/changeVisitStatus/' . $visitStatus['id'] . '">' . $visitStatus['name_ar'] . '</a></li>';
                }
                $dropdownVisitStatus = '<div class="btn-group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ' . (optional($booking->visitStatus)->name_ar ?: "غير محدد") . '<span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                ' . $visitOptions . '
                              </ul>
                            </div>';
                foreach($procedures as $procedure){
                    $procedureOptions .= '<li><a class="change-procedure" href="' . $booking->path(true) . '/changeProcedure/' . $procedure['id'] . '">' . $procedure['name_ar'] . '</a></li>';
                }
                $dropdownProcedure = '<div class="btn-group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ' . (optional($booking->procedure)->name_ar ?: "غير محدد") . '<span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                ' . $procedureOptions . '
                              </ul>
                            </div>';
                $dropdownInvoiced = '<div class="btn-group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ' . ($booking->is_invoiced == '1' ? 'تمت الفوترة' : 'لم تتم الفوترة') . '<span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a class="change-status" href="' . $booking->path(true) . '/confirmInvoiced/1">تمت الفوترة</a></li>
                                <li><a class="change-status" href="' . $booking->path(true) . '/confirmInvoiced/0">لم تتم الفوترة</a></li>
                              </ul>
                            </div>';

                return $dropdownStatus . $dropdownProcedure . $dropdownVisitStatus . $dropdownInvoiced . $edit . $delete;
            })
            ->rawColumns(['doctor', 'customer', 'office', 'time', 'actions', 'insurance', 'procedure'])
            ->make(true);
    }
}
