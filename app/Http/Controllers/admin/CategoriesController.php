<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriesRequest;
use App\Category;
use DataTables;
use Illuminate\Http\Request;
use Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category;

        return view('admin.categories.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
    {

        $category = Category::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ]);

        Session::flash('toast', ['Category is Successfully Created', 'success']);

        return redirect("/admin/categories");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);

        return view('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesRequest $request, Category $category)
    {
        $category->update([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ]);
        Session::flash('toast', 'Category is Successfully Updated');

        return redirect("/admin/categories");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->posts()->count()) {
            if (request()->wantsJson()) {
                return response()->json(['msg' => 'You can\'t delete this category unless you have deleted all the posts associated with it'], 501);
            }
            Session::flash('toast', ['Unable to delete the category!', 'error']);

            return redirect('admin/categories');
        }

        $category->delete();
        if (request()->wantsJson()) {
            return response()->json([], 200);
        }
        Session::flash('toast', 'Category Deleted successfully');

        return redirect('admin/categories');
    }

    public function getCategoriesData(Request $request)
    {
        $categories = Category::query();

        return DataTables::of($categories)->editColumn('created_at', function ($category) {
            return $category->created_at->toFormattedDateString();
        })->addColumn('posts_count', function ($category) {
            $count = $category->posts()->count();

            return "<div class='text-center'><span class='label label-primary label-lg'>{$count}</span></div>";
        })->addColumn('actions', function ($category) {
            $buttonGroupStart = '<div class="btn-group btn-group-xs">';
            $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $category->path(true) . '/edit' . '" class="btn btn-xs category-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
            $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $category->path(true) . '" class="btn btn-xs category-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
            $buttonGroupEnd = '</div>';

            return $edit . $delete;
        })->rawColumns(['actions', 'posts_count'])->make(true);
    }
}
