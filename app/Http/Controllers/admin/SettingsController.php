<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingsRequest;
use App\Setting;
use Illuminate\Http\Request;
use Psy\Util\Json;
use Session;
use Validator;
use View;

class SettingsController extends Controller
{
    /**
     * SettingsController constructor.
     */
    public function __construct()
    {
        $contents = \App\Content::all()->keyBy('key')->toArray();
        View::share(compact('contents'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $settings = Setting::all();
        $groupedSettings = $settings->groupBy(function($e) {
            return $e->category;
        });
        return view('admin.settings.edit' , compact('settings', 'groupedSettings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingsRequest $request)
      {
        foreach($request->except('_token') as $key => $input){
            Setting::where('key', $key)->firstOrFail()->update(['value' => $input]);
        }
        $appSettings = Setting::pluck('value', 'key')->toArray();
        \Cache::forever('appSettings', $appSettings);

        Session::flash('toast', ['Values Updated Successfully', 'success']);


        return redirect('/admin/settings');
    }

}
