<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegionsRequest;
use App\Region;
use DataTables;
use Illuminate\Http\Request;
use Session;

class RegionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.regions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.regions.create', compact('region'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\RegionsRequest $request
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(RegionsRequest $request)
    {
        $region = Region::create([
            'name_ar'  => $request->name_ar,
            'key'      => $request->key,
            'is_shown' => $request->is_shown,
        ]);

        Session::flash('toast', ['Region is Successfully Created', 'success']);

        return redirect("/admin/regions");
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        return view('admin.regions.show', compact('region'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Region $region)
    {
        return view('admin.regions.edit', compact('region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RegionsRequest $request, Region $region)
    {
        $region->update([
            'name_ar'  => $request->name_ar,
            'key'      => $request->key,
            'is_shown' => $request->is_shown,
        ]);

        Session::flash('toast', ['Region is Successfully Updated', 'success']);

        return redirect("/admin/regions");
    }

    public function toggleShown(Region $region)
    {
        $region->toggleShown();

        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        if ($region->offices()->count()) {
            if (request()->wantsJson()) {
                return response()->json(['msg' => 'You can\'t delete this region unless you have deleted all the offices associated with it'], 501);
            }
            Session::flash('toast', ['Unable to delete the region!', 'error']);

            return redirect('admin/regions');
        }

        $region->delete();
        if (request()->wantsJson()) {
            return response()->json([], 200);
        }
        Session::flash('toast', 'Region Deleted successfully');

        return redirect('admin/regions');
    }

    public function getRegionsData(Request $request)
    {
        $regions = Region::with('offices');

        return DataTables::of($regions)
            ->editColumn('created_at', function ($region) {
            return $region->created_at->toFormattedDateString();
        })->addColumn('doctors_count', function ($region) {
            $count = $region->getDoctors()->count();

            return "<div class='text-center'><span class='label label-primary label-lg'>{$count}</span></div>";
        })->editColumn('is_shown', function ($region) {
                $class = $region->is_shown ? 'success' : 'danger';
                $is_shown = $region->is_shown ? 'Yes' : 'No';
                return '<div class="text-center">' .
                    '<a data-toggle="tooltip" href="' . $region->path(true) . '/toggleShown" class="btn btn-xs region-is_shown btn-' . $class . '"><i class="fa fa-eye"></i> ' . $is_shown . '</a>' .
                    '</div>';

            })->addColumn('actions', function ($region) {
            $buttonGroupStart = '<div class="btn-group btn-group-xs">';
            $edit = '<a data-toggle="tooltip" title="Click to Edit" href="'.$region->path(true).'/edit'.'" class="btn btn-xs region-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
            $delete = '<a data-toggle="tooltip" title="Click to Delete" href="'.$region->path(true).'" class="btn btn-xs region-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
            $buttonGroupEnd = '</div>';

            return $edit.$delete;
        })->rawColumns(['doctors_count', 'is_shown', 'actions'])->make(true);
    }
}
