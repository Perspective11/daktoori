<?php

namespace App\Http\Controllers\admin;

use App\Customer;
use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.customers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response([], 200);
    }

    public function getCustomersData(Request $request)
    {
        $customers = Customer::with('city', 'user', 'region');

        if ($request->filled('name')) {
            $customers->search($request->name);
        }
        if ($request->filled('city')) {
            $customers->whereHas('city', function ($query) use ($request) {
                $query->where('key', 'like', $request->city);
            });
        }
        if ($request->filled('region')) {
            $customers->whereHas('region', function ($query) use ($request) {
                $query->where('key', 'like', $request->region);
            });
        }
        if ($request->filled('recordrange')) {
            $arr = explode(' - ', request('recordrange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $customers->whereBetween('created_at', [$startDate, $endDate]);
        }

        return DataTables::of($customers)
            ->editColumn('created_at', function ($customer)
            {
                return $customer->created_at->toFormattedDateString();
            })
            ->addColumn('full_name', function ($customer)
            {
                return $customer->getName();
            })
            ->addColumn('age', function ($customer)
            {
                if($age = $customer->age()){
                    return "<div class='text-center'><span class='label label-warning label-lg'>{$age}</span></div>";
                }
                else return '';
            })
            ->editColumn('gender', function ($customer)
            {
                $color = '';
                $gender = '';
                if (! $customer->gender){
                    return '';
                }
                if ($customer->gender === 1){
                    $gender = 'male';
                    $color = 'lightblue';
                }
                if ($customer->gender === 2){
                    $gender = 'female';
                    $color = 'pink';
                }
                return '<div class="text-center"><span style="color:'. $color .'" class="fa-2x fa fa-'. $gender .'"></span></div>';


            })

            ->addColumn('city', function ($customer) {
                return $customer->city->name_ar;
            })
            ->addColumn('region', function ($customer) {
                return $customer->region->name_ar;
            })
            ->addColumn('actions', function ($customer) {
                $buttonGroupStart = '<div class="btn-group btn-group-xs">';
                $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $customer->userPath(true) . '/edit' . '" class="btn btn-xs customer-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
                $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $customer->userPath(true) . '" class="btn btn-xs customer-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
                $view = '<a data-toggle="tooltip" title="Click to View" href="' . $customer->userPath(true) . '" class="btn btn-xs customer-view btn-info"><i class="fa fa-eye"></i></a>';
                $buttonGroupEnd = '</div>';

                return $edit . $view . $delete;
            })
            ->addColumn('bookings', function ($customer) {
                $bookingsCount = $customer->bookings()->count();
                return "<div class='text-center'><span class='label label-info label-lg'>{$bookingsCount}</span></div>";
            })
            ->rawColumns(['age', 'gender', 'bookings', 'actions'])
            ->make(true);
    }
}
