<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FaqsRequest;
use App\Faq;
use Illuminate\Http\Request;
use DataTables;
use Session;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.faqs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faq = new Faq;

        return view('admin.faqs.create', compact('faq'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqsRequest $request)
    {
        $faq = Faq::create([
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'body_en'  => $request->body_en,
            'body_ar'  => $request->body_ar,
        ]);

        Session::flash('toast', ['Faq is Successfully Created', 'success']);
        return redirect("/admin/faqs");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $faq = Faq::findOrFail($id);
        return view('admin.faqs.show', compact('faq'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        return view('admin.faqs.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqsRequest $request, Faq $faq)
    {
        $faq->update([
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'body_en'  => $request->body_en,
            'body_ar'  => $request->body_ar,
        ]);
        Session::flash('toast','Faq is Successfully Updated');
        return redirect("/admin/faqs");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        $faq->delete();
        Session::flash('toast','Faq is Successfully Deleted');
        return redirect('admin/faqs');
    }

    public function getFaqsData(Request $request)
    {
        $faqs = Faq::query();

        return DataTables::of($faqs)->editColumn('created_at', function ($faq) {
            return $faq->created_at->toFormattedDateString();
        })->make(true);
    }
}
