<?php

namespace App\Http\Controllers\admin;

use App\Content;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Session;

class ContentsController extends Controller
{
    public function show()
    {
        $contents = Content::all()->sortBy('id');
        return view('admin.contents.show' , compact('contents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $contents = Content::all()->sortBy('id');
        return view('admin.contents.edit' , compact('contents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        foreach ($request->except('_token') as $key => $value){
            Content::where('key', $key)->first()->update([
                "value_ar" => isset($value[0])? $value[0] : null
            ]);
        }
        Session::flash('toast', ['Text Contents Successfully Updated', 'success']);
        return redirect()->back();
    }


}
