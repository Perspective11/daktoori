<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Doctor;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewsRequest;
use App\Picture;
use App\Review;
use Carbon\Carbon;
use DataTables;
use File;
use Illuminate\Http\Request;
use Session;

class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.reviews.index');
    }
    public function create($doctor_id = null)
    {
        $doctor_id = 3;
        $doctors = Doctor::all();
        return view('admin.reviews.create', compact('doctors','doctor_id'));
    }
    public function store(ReviewsRequest $request)
    {
        Review::create([
            'doctor_id'       => $request->doctor,
            'patient_name'    => $request->patient_name,
            'rating'          => $request->rating,
            'review'          => $request->review,
            'rating_approved' => 1,
            'review_approved' => 1,
            'review_hidden'   => 0,
            'featured'        => 0,
        ]);
        $doctor = Doctor::find($request->doctor);
        $doctor->updateRatings();
        return redirect('/admin/reviews');
    }

    public function destroy(Review $review)
    {
        $review->delete();
        Session::flash('toast', 'Review is Successfully Deleted');

        return redirect('admin/reviews');
    }

    public function changeStatus(Review $review, $action = null, $value = null)
    {
        //dd($action, $value);
        if ($action == 'approval'){
            if ($value == 0){
                $review->setApproved();
                if (request()->wantsJson()){
                    return response()
                        ->json(['msg' => 'Review Approved Successfully'], 200);
                }
            }
            if ($value == 1){
                $review->setDisapproved();
                if (request()->wantsJson()){
                    return response()
                        ->json(['msg' => 'Review Disapproved Successfully'], 200);
                }
            }
        }

        if ($action == 'featuring'){
            if ($value == 0){
                $review->setFeatured();
                if (request()->wantsJson()){
                    return response()
                        ->json(['msg' => 'Review Featured Successfully'], 200);
                }
            }
            if ($value == 1){
                $review->setUnfeatured();
                if (request()->wantsJson()){
                    return response()
                        ->json(['msg' => 'Review Unfeatured Successfully'], 200);
                }
            }
        }
        return back();
    }

    public function getReviewsData(Request $request)
    {
        $reviews = Review::query();

        if ($request->has('approval')) {
            if ($request->approval == 'approved') {
                $reviews->where('rating_approved', 1);
            } else {
                if ($request->approval == 'disapproved') {
                    $reviews->where('rating_approved', 0);
                }
            }
        }
        if ($request->filled('user'))
        {
            $userIds = \App\User::searchUser(trim($request->user), 'id');
            $doctorIds = \App\Doctor::whereIn('user_id', $userIds)->pluck('id')->toArray();
            $customerIds = \App\Customer::whereIn('user_id', $userIds)->pluck('id')->toArray();
            $reviews->whereIn('customer_id', $customerIds)->orWhereIn('doctor_id', $doctorIds);
        }
        if ($request->has('type')) {
            if ($request->type == 'featured') {
                $reviews->featured();
            } else {
                if ($request->type == 'unfeatured') {
                    $reviews->where('featured', '!=', 1);
                }
            }
        }
        if ($request->filled('daterange')) {
            $arr = explode(' - ', request('daterange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $reviews->whereBetween('created_at', [$startDate, $endDate]);
        }

        return DataTables::of($reviews)

            ->editColumn('created_at', function ($review) {
                return $review->created_at->toFormattedDateString();
            })

            ->addColumn('doctor', function ($review) {
                if (! $review->doctor) {
                    return "تم حذف الدكتور";
                }
                $doctor = $review->doctor;
                return '<a data-toggle="tooltip" title="Click to View Doctor" href="'.$doctor->userPath(true).'" class="">'.$doctor->getName().'</a>';
            })

            ->addColumn('customer', function ($review) {
                if ($customer = $review->customer) {
                    return '<a data-toggle="tooltip" title="Click to View Customer Account" href="'.$customer->userPath(true).'" class="">'.$customer->getName().'</a>';
                }
                if ($name = $review->patient_name){
                    return $name;
                }
                return '';
            })
            ->addColumn('actions', function ($review) {
                $is_approved = $review->rating_approved == 1;
                $is_featured = $review->featured == 1;
                $classApprove = $is_approved ? 'check' : 'times';
                $classFeature = $is_featured ? 'bell' : 'bell-slash';
                $colorApprove = $is_approved ? 'lightgreen': 'lightsalmon';
                $colorFeature = $is_featured ? 'orange': 'purple';
                $approving = $is_approved ? 'Disapprove': 'Approve';
                $featuring = $is_featured ? 'Unfeature': 'Feature';
                $approved = '<a data-toggle="tooltip" title="Click to ' . $approving . '" href="' . $review->path(true) . '/change-status/approval/' . $is_approved . '" style="color: ' . $colorApprove . '" class="review-activation"><i class="fa fa-2x fa-' . $classApprove . '"></i></a>';
                $featured = '<a data-toggle="tooltip" title="Click to ' . $featuring . '" href="' . $review->path(true) . '/change-status/featuring/' . $is_featured . '" style="color: ' . $colorFeature . '" class="review-activation"><i class="fa fa-2x fa-' . $classFeature . '"></i></a>';
                $buttonGroupStart = '<div class="btn-group btn-group-xs">';
                $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $review->path(true) . '" class="btn btn-xs review-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
                $buttonGroupEnd = '</div>';

                return  $approved . $featured . $delete;
            })
            ->rawColumns(['doctor', 'customer','actions'])->make(true);
    }
}
