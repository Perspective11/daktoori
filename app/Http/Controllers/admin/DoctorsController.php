<?php

namespace App\Http\Controllers\admin;

use App\City;
use App\Doctor;
use App\Http\Controllers\Controller;
use App\Major;
use App\Picture;
use App\Region;
use App\User;
use Carbon\Carbon;
use DataTables;
use File;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Session;

class DoctorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.doctors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
    }

    public function approved(Doctor $doctor, $action = null)
    {
        if ($action == 'approve'){
            $doctor->setApproved();
            if (request()->wantsJson()){
                return response()
                    ->json(['msg' => 'Doctor Successfully Approved'], 200);
            }
            return back();
        }

        if ($action == 'reject'){
            $doctor->setRejected();
            if (request()->wantsJson()){
                return response()
                    ->json(['msg' => 'Doctor Successfully Rejected'], 200);
            }
            return back();
        }
        $doctor->toggleApproved();
        return back();
    }

    public function getDoctorsData(Request $request)
    {
        $doctors = Doctor::with('offices', 'majors', 'city', 'region', 'user');

        if ($request->filled('name')) {
            $doctors->search($request->name);
        }
        if ($request->filled('status')) {
            if ($request->status == 'approved') {
                $doctors->where('is_approved', 1);
            } else {
                if ($request->status == 'unapproved') {
                    $doctors->where('is_approved', '!=', 1);
                }
            }
        }
        if ($request->filled('availability')) {
            if ($request->availability == 'available') {
                $doctors->where('is_available', 1);
            }
            elseif ($request->availability == 'unavailable') {
                    $doctors->where('is_available', 0);
            }
        }
        if ($request->filled('city')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->whereHas('city', function ($query2) use ($request) {
                    $query2->where('key', 'like', $request->city);
                });
            });
        }
        if ($request->filled('region')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->whereHas('region', function ($query2) use ($request) {
                    $query2->where('key', 'like', $request->region);
                });
            });
        }
        if ($request->filled('insurance')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->whereHas('insurances', function ($query2) use ($request) {
                    $query2->where('name_ar', 'like', $request->insurance);
                });
            });
        }
        if ($request->filled('major')) {
            $doctors->whereHas('majors', function ($query) use ($request) {
                $query->where('name_ar', 'like', $request->major);
            });
        }
        if ($request->filled('service')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->whereHas('services', function ($query2) use ($request) {
                    $query2->where('name_ar', 'like', $request->service);
                });
            });
        }
        if ($request->filled('office')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->where('name_ar', 'LIKE', '%' . $request->office . '%');
            });
        }
        if ($request->filled('feestart')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->where('fees', '>=', $request->feestart);
            });
        }
        if ($request->filled('feeend')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->where('fees', '<=', $request->feeend);
            });
        }
        if ($request->filled('ratingstart')) {
            $doctors->where('rating_percentage', '>=', $request->ratingstart);
        }
        if ($request->filled('ratingend')) {
            $doctors->where('rating_percentage', '<', $request->ratingend);
        }

        if ($request->filled('recordrange')) {
            $arr = explode(' - ', request('recordrange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $doctors->whereBetween('created_at', [$startDate, $endDate]);
        }

        return DataTables::of($doctors)->editColumn('created_at', function ($doctor) {
            return $doctor->created_at->toFormattedDateString();
        })->editColumn('picture', function ($doctor) {
            return "<img class='table-img img-responsive' src='" . $doctor->getPicture() . "' alt='$doctor->lname_ar'>";
        })->editColumn('gender', function ($doctor) {
            $color = '';
            $gender = '';
            if (! $doctor->gender) {
                return '';
            }
            if ($doctor->gender == 1) {
                $gender = 'male';
                $color = 'lightblue';
            }
            if ($doctor->gender == 2) {
                $gender = 'female';
                $color = 'pink';
            }

            return '<div class="text-center"><span style="color:' . $color . '" class="fa-2x fa fa-' . $gender . '"></span></div>';
        })->editColumn('average_waiting_time', function ($doctor) {
            if (! $doctor->average_waiting_time) {
                return '';
            }
            $date = Carbon::now()->addMinute($doctor->average_waiting_time);

            return $date->diffInMinutes() . ' دقائق';
        })->editColumn('fees', function ($doctor) {
            return $doctor->feesString() . ' ريال';
        })->addColumn('majors', function ($doctor) {
            return $doctor->majorsString();
        })->addColumn('services', function ($doctor) {
            return $doctor->getOffice()->servicesString();
        })->
        addColumn('offices', function ($doctor) {
            return $doctor->officesString();
        })->addColumn('insurances', function ($doctor) {
            return $doctor->getOffice()->insurancesString();
        })->addColumn('city', function ($doctor) {
            return $doctor->getOffice()->city->name_ar;
        })->addColumn('region', function ($doctor) {
            return $doctor->getOffice()->region->name_ar;
        })->editColumn('is_available', function ($doctor) {
            $check = '';
            $available = $doctor->is_available;
            if ($available === 0) {
                $check = 'times';
            } elseif ($available === 1) {
                $check = 'check';
            } else {
                return '';
            }
            $color = $doctor->is_available ? 'lightgreen' : 'lightsalmon';

            return '<div class="text-center"><span style="color:' . $color . '" class="fa-2x fa fa-' . $check . '"></span></div>';
        })->addColumn('bookings', function ($doctor) {
            $bookingsCount = $doctor->getBookings()->count();

            return "<div class='text-center'><span class='label label-info label-lg'>{$bookingsCount}</span></div>";
        })->addColumn('rating', function ($doctor) {
            $rating = $doctor->getRating();
            return "<div class='text-center'><span class='label label-primary label-lg'>{$rating}</span></div>";

        })->addColumn('actions', function ($doctor) {
            $is_approved = $doctor->is_approved;
            $class = $is_approved ? 'unlock' : 'lock';
            $color = $is_approved ? 'lightgreen' : 'lightsalmon';
            $approving = $is_approved ? 'Lock' : 'Unlock';
            $approved = '<a data-toggle="tooltip" title="Click to ' . $approving . '" href="' . $doctor->path(true) . '/activation' . '"style="color: ' . $color . '" class="doctor-activation"><i class="fa fa-2x fa-' . $class . '"></i></a>';
            $buttonGroupStart = '<div class="btn-group btn-group-xs">';
            $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $doctor->userPath(true) . '/edit' . '" class="btn btn-xs doctor-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
            $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $doctor->userPath(true) . '" class="btn btn-xs doctor-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
            $view = '<a data-toggle="tooltip" title="Click to View" href="' . $doctor->userPath(true) . '" class="btn btn-xs doctor-view btn-info"><i class="fa fa-eye"></i></a>';
            $buttonGroupEnd = '</div>';

            return $approved . $edit . $view . $delete;
        })->rawColumns(['picture', 'is_available', 'age', 'gender', 'bookings', 'rating', 'actions'])->make(true);
    }
}
