<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostsRequest;
use App\Picture;
use App\Post;
use Carbon\Carbon;
use DataTables;
use File;
use Illuminate\Http\Request;
use Session;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = new Post;
        $categories = Category::all();
        return view('admin.posts.create', compact('post', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {

        $image_path = '';
        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/posts/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $image_path = '/' . $image_path;
        }

        $post = Post::create([
            'title_ar'    => $request->title_ar,
            'type'        => 0,
            'body_ar'     => $request->body_ar,
            'picture'     => $image_path,
            'status'      => 0,
            'category_id' => $request->category,
        ]);

        Session::flash('toast', ['Post is Successfully Created', 'success']);
        return redirect("/admin/posts");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        return view('admin.posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostsRequest $request, Post $post)
    {
        if ($request->hasFile('picture')) {
            File::delete(public_path(str_replace('/', '\\', $post->picture)));
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/posts/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $post->update([
                'picture' => '/'.$image_path,
            ]);
        }
        $post->update([
            'title_ar'    => $request->title_ar,
            'body_ar'     => $request->body_ar,
            'category_id' => $request->category,
        ]);
        Session::flash('toast','Post is Successfully Updated');
        return redirect("/admin/posts");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        File::delete(public_path(str_replace('/', '\\', $post->picture)));
        $post->delete();
        Session::flash('toast','Post is Successfully Deleted');
        return redirect('admin/posts');
    }
    public function activation(Post $post)
    {
        $post->togglePublished();

        return back();
    }

    public function featured(Post $post)
    {
        $post->toggleFeatured();

        return back();
    }
    public function slider(Post $post)
    {
        $post->toggleSlider();

        return back();
    }
    public function getPostsData(Request $request)
    {
        $posts = Post::query();

        if ($request->has('status'))
        {
            if ($request->status == 'published')
                $posts->where('status', 1);
            else if ($request->status == 'unpublished')
                $posts->where('status', 0);
        }
        if ($request->has('type'))
        {
            if ($request->type == 'featured')
                $posts->featured();
            else if ($request->type == 'unfeatured')
                $posts->where('type', '!=' , Post::$TYPE_FEATURED_POST);
        }
        if ($request->filled('daterange')) {
            $arr = explode(' - ', request('daterange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $posts->whereBetween('created_at', [$startDate, $endDate]);
        }
        if ($request->filled('category')) {
            $posts->whereHas('category', function ($query) use ($request) {
                $query->where('name_ar', 'like', $request->category);
            });
        }
        return DataTables::of($posts)->editColumn('created_at', function ($post) {
            return $post->created_at->toFormattedDateString();
        })->editColumn('picture', function ($post) {
            return "<img class='table-img img-responsive' src='" . $post->getPicture() . "' alt='$post->title'>";
        })->editColumn('category', function ($post) {
            return $post->getCategoryName();
        })
            ->editColumn('status', function ($post) {
            $class = $post->status ? 'success' : 'danger';
            $status = $post->status ? 'Unpublish' : 'Publish';
            $featuredPostHTML = '';
            if ($post->type == Post::$TYPE_FEATURED_POST){
                $featuredPostHTML = '<a data-toggle="tooltip" title="Click to Unfeature the post" href="' . $post->path(true) . '/featured" class="btn btn-xs post-featured btn-warning"><i class="fa fa-star"></i> Unfeature</a>';
            }
            return '<div class="text-center">' .
                '<a data-toggle="tooltip" title="Click to ' . $status . ' the post" href="' . $post->path(true) . '/activation" class="btn btn-xs post-status btn-' . $class . '"><i class="fa fa-rss"></i> ' . $status . '</a>' .
                $featuredPostHTML .
                '</div>';

        })
            ->rawColumns(['body_ar', 'picture', 'status'])
            ->make(true);
    }
}
