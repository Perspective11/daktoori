<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProceduresRequest;
use App\Procedure;
use DataTables;
use Illuminate\Http\Request;
use Session;

class ProceduresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.procedures.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.procedures.create', compact('procedure'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProceduresRequest $request)
    {
        $procedure = Procedure::create([
            'name_ar'     => $request->name_ar,
        ]);

        Session::flash('toast', ['Procedure is Successfully Created', 'success']);

        return redirect("/admin/procedures");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Procedure $procedure)
    {
        return view('admin.procedures.show', compact('procedure'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Procedure $procedure)
    {
        return view('admin.procedures.edit', compact('procedure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProceduresRequest $request, Procedure $procedure)
    {
        $procedure->update([
            'name_ar'     => $request->name_ar,
        ]);

        Session::flash('toast', ['Procedure is Successfully Updated', 'success']);

        return redirect("/admin/procedures");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Procedure $procedure)
    {
        if ($procedure->bookings()->count()) {
            if (request()->wantsJson()) {
                return response()->json(['msg' => 'You can\'t delete this procedure unless you have deleted all the bookings associated with it'], 501);
            }
            Session::flash('toast', ['Unable to delete the procedure!', 'error']);

            return redirect('admin/procedures');
        }

        $procedure->delete();
        if (request()->wantsJson()) {
            return response()->json([], 200);
        }
        Session::flash('toast', 'Procedure Deleted successfully');

        return redirect('admin/procedures');
    }

    public function getProceduresData(Request $request)
    {
        $procedures = Procedure::with('bookings');

        return DataTables::of($procedures)->editColumn('created_at', function ($procedure) {
            return $procedure->created_at->toFormattedDateString();
        })->addColumn('bookings_count', function ($procedure) {
            $count = $procedure->bookings()->count();

            return "<div class='text-center'><span class='label label-primary label-lg'>{$count}</span></div>";
        })->addColumn('actions', function ($procedure) {
            $buttonGroupStart = '<div class="btn-group btn-group-xs">';
            $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $procedure->path(true) . '/edit' . '" class="btn btn-xs procedure-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
            $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $procedure->path(true) . '" class="btn btn-xs procedure-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
            $view = '<a data-toggle="tooltip" title="Click to View" href="' . $procedure->path(true) . '" class="btn btn-xs procedure-view btn-info"><i class="fa fa-eye"></i></a>';
            $buttonGroupEnd = '</div>';

            return $edit . $view . $delete;
        })->rawColumns(['bookings_count', 'actions'])->make(true);
    }
}
