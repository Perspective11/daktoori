<?php

namespace App\Http\Controllers\admin;

use App\Office;
use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfficesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.offices.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response([], 200);
    }

    public function getOfficesData(Request $request)
    {
        $offices = Office::with('doctor', 'city');

        return DataTables::of($offices)
            ->editColumn('created_at', function ($office)
            {
                return $office->created_at->toFormattedDateString();
            })
            ->addColumn('doctor', function ($office)
            {
                $doctor = $office->doctor;
                return '<a data-toggle="tooltip" title="Click to View Doctor" href="' . $doctor->path(true) . '" class="">' . $doctor->getName() . '</a>';
            })
            ->editColumn('fees', function ($office)
            {
                if ($office->fees){
                    return $office->fees . ' ريال';
                }
                return '';
            })
            ->addColumn('city', function ($office) {
                return $office->city->name_ar;
            })
            ->addColumn('actions', function ($office) {
                $buttonGroupStart = '<div class="btn-group btn-group-xs">';
                $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $office->path(true) . '/edit' . '" class="btn btn-xs office-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
                $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $office->path(true) . '" class="btn btn-xs office-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
                $view = '<a data-toggle="tooltip" title="Click to View" href="' . $office->path(true) . '" class="btn btn-xs office-view btn-info"><i class="fa fa-eye"></i></a>';
                $buttonGroupEnd = '</div>';

                return  $edit . $view . $delete;
            })
            ->rawColumns(['doctor', 'actions'])
            ->make(true);
    }
}
