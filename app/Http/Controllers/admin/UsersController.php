<?php

namespace App\Http\Controllers\admin;

use App\City;
use App\Doctor;
use App\Http\Controllers\Controller;
use App\Http\Requests\UsersRequest;
use App\Insurance;
use App\Major;
use App\Picture;
use App\Region;
use App\Role;
use App\Service;
use App\User;
use DataTables;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Response;
use Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uuid = Uuid::uuid4();
        $temp_token = $uuid->toString();
        $majors = Major::where('level', 1)->get();
        $cities = City::where('is_shown', 1)->get();
        $regions = Region::where('is_shown', 1)->get();

        $secondaryMajors = Major::where('level', 2)->get();
        $insurances = Insurance::all();
        $services = Service::all();
        $user = new User;

        return view('admin.users.create', compact('temp_token', 'majors', 'secondaryMajors', 'cities', 'user', 'regions', 'insurances', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(UsersRequest $request)
    {
        $user = new User;
        $roles = Role::pluck('id', 'key')->toArray();

        if (isset($request->sname_ar)){
            $fullName = $request->fname_ar . ' ' . $request->sname_ar . ' ' . $request->lname_ar;
        }
        else
            $fullName = $request->fname_ar . ' ' . $request->lname_ar;

        DB::transaction(function () use ($request, $user, $roles, $fullName) {

            $user = User::create([
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => bcrypt($request->password),
                'status'   => $request->user_status,
                'approved' => 1,
            ]);
            $user->roles()->attach(request('user_type'));

            if ($request->user_type == $roles['doctor']) {
                $city = $request->city;
                $region = $request->region;
                $major = $request->major;
                $secondaryMajors = $request->secondaryMajors;
                $secondaryMajors[] = $major;
                $insurances = $request->insurances;
                $services = $request->services;
                $rank = $request->rank ?: 0;
                $doctor = $user->doctor()->create([
                    'fname_ar'             => $request->fname_ar,
                    'sname_ar'             => $request->sname_ar,
                    'lname_ar'             => $request->lname_ar,
                    'title_ar'             => $request->title_ar,
                    'prefix'               => $request->prefix,
                    'mobile'               => $request->mobile,
                    'whatsapp'             => $request->whatsapp,
                    'grad_year'            => $request->grad_year,
                    'grad_university'      => $request->grad_university,
                    'grad_country'         => $request->grad_country,
                    'is_approved'          => $request->is_approved,
                    'is_available'         => $request->is_available,
                    'average_waiting_time' => $request->average_waiting_time,
                    'dob'                  => $request->dob,
                    'gender'               => $request->gender,
                    'about_ar'             => $request->about_ar,
                    'address_ar'           => $request->address_ar,
                    'city_id'              => $city,
                    'region_id'            => $region,
                    'full_name_ar'         => $fullName,
                    'rank'                 => $rank,

                ]);
                $office = $doctor->offices()->create([
                    'name_ar'    => $request->office_name,
                    'address_ar' => $request->address_ar,
                    'fees'       => $request->fees,
                    'phone'      => $request->phone,
                    'city_id'    => $city,
                    'region_id'  => $region,
                ]);
                $doctor->majors()->attach($secondaryMajors);
                $office->insurances()->attach($insurances);
                $keysServices = [];
                foreach($services as $service) {
                    if (is_numeric($service)) {
                        $keysServices [] = $service;
                    } else {
                        $newService = Service::create([
                            'name_ar' => $service,
                        ]);
                        $keysServices[] = $newService->id;
                    }
                }
                $office->services()->attach($keysServices);
                $temp_token = '';
                if ($request->has('temp')) {
                    $temp_token = $request->temp;
                }

                $pictures = Picture::where('temp_token', $temp_token)->get();
                for ($i = 0; $i < $pictures->count(); $i++) {
                    if ($i >= Doctor::$image_limit) {
                        break;
                    }
                    $doctor->pictures()->save($pictures[$i]);
                    $pictures[$i]->update(['temp_token' => null]);
                }
                Picture::deleteTemp();
            }
            if ($request->user_type == $roles['customer']) {
                $city = $request->customer_city;
                $region = $request->customer_region;
                $customer = $user->customer()->create([
                    'fname_ar'   => $request->customer_fname_ar,
                    'sname_ar'   => $request->customer_sname_ar,
                    'lname_ar'   => $request->customer_lname_ar,
                    'mobile'     => $request->customer_mobile,
                    'whatsapp'   => $request->customer_whatsapp,
                    'dob'        => $request->customer_dob,
                    'gender'     => $request->customer_gender,
                    'note'       => $request->note,
                    'address_ar' => $request->customer_address_ar,
                    'city_id'    => $city,
                    'region_id'  => $region,
                ]);
            }
        });
        Session::flash('toast', 'User Created Successfully');

        return redirect($user->path(true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $doctor = $user->doctor;
        $customer = $user->customer;
        if ($user->isDoctor()){
            $doctor->load('pictures');
            $office = $doctor->getOffice();
        }
        return view('admin.users.show', compact('user', 'doctor', 'customer', 'office'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if ($user->isSuperAdmin() && ! Auth::user()->isSuperAdmin()){
            Session::flash('toast', ['You dont have permission to edit user', 'error', 'Error!']);
            return redirect()->back();
        }
        $majors = Major::where('level', 1)->get();
        $cities = City::where('is_shown', 1)->get();
        $regions = Region::where('is_shown', 1)->get();
        $secondaryMajors = Major::where('level', 2)->get();
        $insurances = Insurance::all();
        $services = Service::all();
        $doctor = $user->doctor;
        $customer = $user->customer;
        if ($user->isDoctor()){
            $doctor->load('pictures');
            $doctor_id = $doctor->id;
            $office = $doctor->getOffice();
        }
        return view('admin.users.edit', compact('user', 'doctor', 'customer', 'majors', 'secondaryMajors', 'cities', 'doctor_id', 'office', 'regions', 'insurances', 'services'));
    }

    /**
     * Update the specified[; resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(UsersRequest $request, User $user)
    {
        if ($user->isSuperAdmin() && ! Auth::user()->isSuperAdmin()){
            if (request()->wantsJson()){
                return response()
                    ->json(['msg' => 'Super admin user may not be updated'], 401);
            }
            Session::flash('toast', ['You dont have permission to update user', 'error', 'Error!']);
            return redirect()->back();
        }
        $roles = Role::pluck('id', 'key')->toArray();

        $doctor = $user->doctor;
        $customer = $user->customer;
        $userArray = [
            'name'   => $request->name,
            'email'  => $request->email,
            'status' => $request->user_status,
        ];
        if ($request['password']) // because nullable rule doesnt fucking work
        {
            if ($user->isAdmin() && ($user->id != Auth::user()->id)){
                if (request()->wantsJson()){
                    return response()
                        ->json(['msg' => 'You may not change the password of another admin user'], 401);
                }
                Session::flash('toast', ['You may not change the password of another admin user', 'error', 'Error!']);
                return redirect()->back();
            }
            $userArray['password'] = bcrypt($request->password);
        }

        if (isset($request->sname_ar)){
            $fullName = $request->fname_ar . ' ' . $request->sname_ar . ' ' . $request->lname_ar;
        }
        else
            $fullName = $request->fname_ar . ' ' . $request->lname_ar;


        DB::transaction(function () use ($request, $user, $doctor, $customer, $userArray, $roles, $fullName) {

            $user->update($userArray);

            if ($request->user_type == $roles['doctor'])
            { //doctor

                $city = $request->city;
                $region = $request->region;
                $major = $request->major;
                $secondaryMajors = $request->secondaryMajors;
                $secondaryMajors[] = $major;
                $insurances = $request->insurances;
                $services = $request->services;
                $office = $doctor->getOffice();
                $doctor->update([
                    'fname_ar'             => $request->fname_ar,
                    'sname_ar'             => $request->sname_ar,
                    'lname_ar'             => $request->lname_ar,
                    'title_ar'             => $request->title_ar,
                    'prefix'               => $request->prefix,
                    'mobile'               => $request->mobile,
                    'whatsapp'             => $request->whatsapp,
                    'grad_year'            => $request->grad_year,
                    'grad_university'      => $request->grad_university,
                    'grad_country'         => $request->grad_country,
                    'is_approved'          => $request->is_approved,
                    'is_available'         => $request->is_available,
                    'average_waiting_time' => $request->average_waiting_time,
                    'dob'                  => $request->dob,
                    'gender'               => $request->gender,
                    'about_ar'             => $request->about_ar,
                    'address_ar'           => $request->address_ar,
                    'city_id'              => $city,
                    'region_id'            => $region,
                    'full_name_ar'         => $fullName,
                    'rank'                 => $request->rank,
                ]);
                $office->update([
                    'name_ar'    => $request->office_name,
                    'address_ar' => $request->address_ar,
                    'fees'       => $request->fees,
                    'phone'      => $request->phone,
                    'city_id'    => $city,
                    'region_id'  => $region,

                ]);
                $doctor->majors()->sync($secondaryMajors);
                $office->insurances()->sync($insurances);
                $keysServices = [];
                foreach($services as $service) {
                    if (is_numeric($service)) {
                        $keysServices [] = $service;
                    } else {
                        $newService = Service::create([
                            'name_ar' => $service,
                        ]);
                        $keysServices[] = $newService->id;
                    }
                }
                $office->services()->sync($keysServices);
            }
            if ($request->user_type == $roles['customer'])
            { // customer
                $city = $request->customer_city;
                $region = $request->customer_region;
                $user->customer->update([
                    'fname_ar'   => $request->customer_fname_ar,
                    'sname_ar'   => $request->customer_sname_ar,
                    'lname_ar'   => $request->customer_lname_ar,
                    'mobile'     => $request->customer_mobile,
                    'whatsapp'   => $request->customer_whatsapp,
                    'dob'        => $request->customer_dob,
                    'gender'     => $request->customer_gender,
                    'address_ar' => $request->customer_address_ar,
                    'note'       => $request->note,
                    'city_id'    => $city,
                    'region_id'  => $region,
                ]);
            }
        });
        Session::flash('toast', 'User Updated Successfully');

        return redirect($user->getIndexPath());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function destroy(User $user)
    {
        if ($user->isSuperAdmin() && ! Auth::user()->isSuperAdmin()){
            if (request()->wantsJson()){
                return response()
                    ->json(['msg' => 'Super admin user may not be deleted'], 401);
            }
            Session::flash('toast', ['You dont have permission to delete user', 'error', 'Error!']);
            return redirect()->back();
        }

        DB::transaction(function () use ($user) {

            if ($user->isDoctor()){
                $doctor = $user->doctor;
                foreach ($doctor->pictures as $picture) {
                    File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
                    File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
                }
                $doctor->pictures()->delete();
                $doctor->offices()->delete();
                $doctor->delete();
            }
            if ($user->isCustomer())
                $user->customer->delete();

            $user->delete();

        });
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }

        Session::flash('toast', 'User Deleted successfully');
        return redirect('admin/users');
    }

    public function changePassword()
    {
        $user = Auth::user();

        return view('admin.users.change-password', compact('user'));
    }

    public function changePasswordStore(Request $request)
    {
        $user = Auth::user();
        Validator::make($request->all(), [
            'old_password' => 'required',
            'password'     => 'min:6|confirmed|required',
        ])->validate();

        if (! Hash::check($request->old_password, Auth::user()->password)) {
            return redirect()->back()->withErrors(['old_password' => 'Old password is invalid.']);
        }

        $user->update([
            'password' => bcrypt($request->password),
        ]);

        return redirect('/admin/users');
    }

    public function uploadImagesStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image"  => 'required|mimes:jpg,png,jpeg|max:2000',
            "temp"   => 'required',
            "qquuid" => 'required',
        ]);
        $temp_token = '';
        if ($request->has('temp')) {
            $temp_token = $request->temp;
        }
        $temp_uuid = '';
        if ($request->has('qquuid')) {
            $temp_uuid = $request->qquuid;
        }
        if ($temp_token) {
            $validator->after(function ($validator) use ($temp_token) {
                if (Picture::where('temp_token', $temp_token)->count() >= Doctor::$image_limit) {
                    $validator->errors()->add('image', 'You cant upload more than '.Doctor::$image_limit.' images!');
                }
            });
        }

        if ($validator->fails()) {
            return response()->json(implode('
            ', array_flatten($validator->messages()->toArray())), 500);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_'.Picture::getSanitizedName($image);
            $picture_path = 'images/doctors/'.$picture_name;
            $thumbnail_path = 'images/doctors/'.$thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/'.$picture_path;
            $thumbnail_path = '/'.$thumbnail_path;
            Picture::create([
                'name'       => $picture_name,
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_token' => $temp_token,
                'temp_uuid'  => $temp_uuid,
            ]);
        }

        return Response::json(["success" => true, "class" => "success", "message" => "Upload Successful"], 201);
    }

    public function uploadImagesUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image"     => 'required|mimes:jpg,png,jpeg|max:2000',
            "qquuid"    => 'required',
            "doctor_id" => 'required',
        ]);

        $temp_uuid = '';
        if ($request->has('qquuid')) {
            $temp_uuid = $request->qquuid;
        }

        $doctor = Doctor::findOrFail($request->doctor_id);

        if (isset($doctor)) {
            $validator->after(function ($validator) use ($doctor) {
                if ($doctor->pictures()->count() > Doctor::$image_limit) {
                    $validator->errors()->add('image', 'You cant upload more than '.Doctor::$image_limit.' images!');
                }
            });
        }

        if ($validator->fails()) {
            return response()->json(implode('
            ', array_flatten($validator->messages()->toArray())), 500);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_'.Picture::getSanitizedName($image);
            $picture_path = 'images/doctors/'.$picture_name;
            $thumbnail_path = 'images/doctors/'.$thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/'.$picture_path;
            $thumbnail_path = '/'.$thumbnail_path;
            $doctor->pictures()->create([
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_uuid'  => $temp_uuid,
                'name'    => $picture_name,
            ]);
        }

        return Response::json(["success" => true, "class" => "success", "message" => "Upload Successful"], 201);
    }

    public function listDoctorImages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "doctor_id" => 'required',
        ]);

        $doctor = Doctor::findOrFail($request->doctor_id);

        if ($validator->fails()) {
            return response()->json(implode('
            ', array_flatten($validator->messages()->toArray())), 500);
        }
        $returnedImages = [];
        $pictures = $doctor->pictures;
        foreach ($pictures as $picture) {
            $returnedImages[] = [
                "id"           => $picture->id,
                "name"         => $picture->name ? $picture->name : '',
                "uuid"         => $picture->temp_uuid,
                "thumbnailUrl" => $picture->thumb_path,
            ];
        }

        return Response::json($returnedImages);
    }

    public function DeleteImage(Request $request)
    {
        $temp_uuid = '';
        if ($request->has('qquuid')) {
            $temp_uuid = $request->qquuid;
        }
        $picture = Picture::where('temp_uuid', $temp_uuid)->get()->first();
        if ($picture->image_path) {
            File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
        }
        if ($picture->thumb_path) {
            File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
        }
        $picture->delete();

        return Response::json([
            "success" => true,
            "class"   => "success",
            "message" => "Image Deleted Successfully",
        ], 201);
    }

    public function activation(User $user)
    {
        $user->toggleActive();
        return back();
    }
    public function getUsersData(Request $request)
    {
        $users = User::with(['doctor', 'customer']);

        if ($request->filled('role')) {
            $users->whereHas('roles', function ($query) use ($request) {
                $query->where('key', $request->role);
            });
        }

        if ($request->filled('status')) {
            if ($request->status == 'active') {
                $users->where('status', 1);
            } else {
                if ($request->status == 'inactive') {
                    $users->where('status', 0);
                }
            }
        }

        if ($request->filled('user')) {
            $userIds = User::searchUser(trim($request->user), 'id');
            $users->whereIn('id', $userIds);
        }

        return DataTables::of($users)->addColumn('role', function ($user) {
            return $user->getRoleName(true);
        })->addColumn('child_name', function ($user) {
            $child = $user->child();
            if (isset($child)) {
                return '<a data-toggle="tooltip" title="Click to View" href="'.$user->path(true).'" class="">'.$child->getName().'</a>';
            }

            return '';
        })->editColumn('status', function ($user) {
            $class = $user->status ? 'success' : 'danger';
            $status = $user->status ? 'Deactivate' : 'Activate';

            return '<a href="'.$user->path(true).'/activation" class="btn btn-xs user-status btn-'.$class.'">'.$status.'</a>';
        })->editColumn('created_at', function ($user) {
            return $user->created_at->toFormattedDateString();
        })->addColumn('actions', function ($user) {
            $buttonGroupStart = '<div class="btn-group btn-group-xs">';
            $edit = '<a data-toggle="tooltip" title="Click to Edit" href="'.$user->path(true).'/edit'.'" class="btn btn-xs user-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
            $delete = '<a data-toggle="tooltip" title="Click to Delete" href="'.$user->path(true).'" class="btn btn-xs user-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
            $view = '<a data-toggle="tooltip" title="Click to View" href="'.$user->path(true).'" class="btn btn-xs user-view btn-info"><i class="fa fa-eye"></i></a>';
            $buttonGroupEnd = '</div>';

            return $edit.$view.$delete;
        })->rawColumns(['child_name', 'status', 'actions'])->make(true);
    }
}
