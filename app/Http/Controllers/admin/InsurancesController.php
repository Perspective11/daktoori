<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\InsurancesRequest;
use App\Insurance;
use DataTables;
use Illuminate\Http\Request;
use Session;

class InsurancesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.insurances.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.insurances.create', compact('insurance'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsurancesRequest $request)
    {
        $insurance = Insurance::create([
            'name_ar'     => $request->name_ar,
            'company_ar'  => $request->company_ar,
            'phone'       => $request->phone,
            'address_ar'  => $request->address_ar,
            'description' => $request->description,
        ]);

        Session::flash('toast', ['Insurance is Successfully Created', 'success']);

        return redirect("/admin/insurances");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Insurance $insurance)
    {
        return view('admin.insurances.show', compact('insurance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Insurance $insurance)
    {
        return view('admin.insurances.edit', compact('insurance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(InsurancesRequest $request, Insurance $insurance)
    {
        $insurance->update([
            'name_ar'     => $request->name_ar,
            'company_ar'  => $request->company_ar,
            'phone'       => $request->phone,
            'address_ar'  => $request->address_ar,
            'description' => $request->description,
        ]);

        Session::flash('toast', ['Insurance is Successfully Updated', 'success']);

        return redirect("/admin/insurances");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Insurance $insurance)
    {
        if ($insurance->offices()->count()) {
            if (request()->wantsJson()) {
                return response()->json(['msg' => 'You can\'t delete this insurance unless you have deleted all the offices associated with it'], 501);
            }
            Session::flash('toast', ['Unable to delete the insurance!', 'error']);

            return redirect('admin/insurances');
        }

        $insurance->delete();
        if (request()->wantsJson()) {
            return response()->json([], 200);
        }
        Session::flash('toast', 'Insurance Deleted successfully');

        return redirect('admin/insurances');
    }

    public function getInsurancesData(Request $request)
    {
        $insurances = Insurance::with('offices');

        return DataTables::of($insurances)->editColumn('created_at', function ($insurance) {
            return $insurance->created_at->toFormattedDateString();
        })->addColumn('doctors_count', function ($insurance) {
            $count = $insurance->getDoctors()->count();

            return "<div class='text-center'><span class='label label-primary label-lg'>{$count}</span></div>";
        })->addColumn('actions', function ($insurance) {
            $buttonGroupStart = '<div class="btn-group btn-group-xs">';
            $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $insurance->path(true) . '/edit' . '" class="btn btn-xs insurance-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
            $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $insurance->path(true) . '" class="btn btn-xs insurance-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
            $view = '<a data-toggle="tooltip" title="Click to View" href="' . $insurance->path(true) . '" class="btn btn-xs insurance-view btn-info"><i class="fa fa-eye"></i></a>';
            $buttonGroupEnd = '</div>';

            return $edit . $view . $delete;
        })->rawColumns(['doctors_count', 'actions'])->make(true);
    }
}
