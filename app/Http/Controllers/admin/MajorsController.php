<?php

namespace App\Http\Controllers\admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\MajorsRequest;
use App\Major;
use DataTables;
use Illuminate\Http\Request;
use Session;

class MajorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.majors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $major = new Major;
        $majors = Major::where('level', 1)->get();
        return view('admin.majors.create', compact('major', 'majors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MajorsRequest $request)
    {
        $major = Major::create([
            'name_ar'   => $request->name_ar,
            'name_en'   => $request->name_en,
            'level'     => $request->level,
            'is_hidden'    => $request->is_hidden,
            'rank'     => $request->rank,
            'parent_id' => $request->parent_major ?? null,
        ]);

        Session::flash('toast', ['Major is Successfully Created', 'success']);

        return redirect("/admin/majors");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $major = Major::findOrFail($id);

        return view('admin.majors.show', compact('major'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Major $major)
    {
        $majors = Major::where('level', 1)->get();
        return view('admin.majors.edit', compact('major', 'majors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MajorsRequest $request, Major $major)
    {
        $major->name_ar = $request->name_ar;
        $major->name_en = $request->name_en;
        $major->level = $request->level;
        $major->is_hidden = $request->is_hidden;
        $major->rank = $request->rank;

        if ($request->level == '2'){
            if ($major->level == '1'){
                if ($major->doctors->count()){
                    return back()->withErrors(['Primary major is already attached to doctor']);
                }
            }
            else{
                $major->parent_id = $request->parent_major;
            }
        }
        if ($request->level == '1'){
            $major->parent_id = null;
        }
        $major->save();

        Session::flash('toast', 'Major is Successfully Updated');

        return redirect("/admin/majors");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Major $major)
    {
        if ($major->doctors()->count()) {
            if (request()->wantsJson()) {
                return response()->json(['msg' => 'You can\'t delete this major unless you have deleted all the doctors associated with it'], 501);
            }
            Session::flash('toast', ['Unable to delete the major!', 'error']);

            return redirect('admin/majors');
        }

        $major->delete();
        if (request()->wantsJson()) {
            return response()->json([], 200);
        }
        Session::flash('toast', 'Major Deleted successfully');

        return redirect('admin/majors');
    }

    public function visibility(Major $major, $action = null)
    {
        if ($action == 'hide'){
            $major->setIsHidden();
            if (request()->wantsJson()){
                return response()
                    ->json(['msg' => 'Major Successfully Hidden'], 200);
            }
            return back();
        }

        if ($action == 'unhide'){
            $major->setNotHidden();
            if (request()->wantsJson()){
                return response()
                    ->json(['msg' => 'Major Successfully Unhidden'], 200);
            }
            return back();
        }

        $major->toggleVisibility();
        return back();
    }

    public function getMajorsData(Request $request)
    {
        $majors = Major::query();

        if ($request->filled('level')) {
            if ($request->level == '1') {
                $majors->where('level', 1);
            } else {
                if ($request->level == '2') {
                    $majors->where('level', 2);
                }
            }
        }

        return DataTables::of($majors)->editColumn('created_at', function ($major) {
            return $major->created_at->toFormattedDateString();
        })->addColumn('parent', function ($major) {
            $parent = $major->parent;
            if ($parent) {
                return $parent->name_ar;
            }

            return '';
        })->editColumn('level', function ($major) {
            $level = $major->level;
            $text = $level == 1 ? 'رئيسي' : 'فرعي';
            $color = $level == 1 ? 'primary' : 'info';

            return "<div class='text-center'><span class='label label-{$color} label-lg'>{$text}</span></div>";
        })->addColumn('doctors_count', function ($major) {
            $count = $major->doctors()->count();

            return "<div class='text-center'><span class='label label-primary label-lg'>{$count}</span></div>";
        })->addColumn('actions', function ($major) {
            $hidden = $major->is_hidden;
            $text = $hidden == 1 ? 'Click to unhide' : 'Click to hide';
            $icon = $hidden == 1 ? 'eye-slash' : 'eye';
            $color = $hidden == 1 ? 'danger' : 'primary';

            $buttonGroupStart = '<div class="btn-group btn-group-xs">';
            $edit = '<a data-toggle="tooltip" title="Click to Edit" href="' . $major->path(true) . '/edit' . '" class="btn btn-xs major-edit btn-warning"><i class="fa fa-pencil-square-o"></i></a>';
            $delete = '<a data-toggle="tooltip" title="Click to Delete" href="' . $major->path(true) . '" class="btn btn-xs major-delete btn-danger"><i class="fa fa-trash-o"></i></a>';
            $hide = '<a data-toggle="tooltip" title="' . $text . '" href="' . $major->path(true) . '/visibility" class="btn btn-xs major-hide btn-' . $color . '"><i class="fa fa-' . $icon . '"></i></a>';
            $buttonGroupEnd = '</div>';

            return $edit . $delete . $hide;
        })->rawColumns(['actions', 'doctors_count', 'level'])->make(true);
    }
}
