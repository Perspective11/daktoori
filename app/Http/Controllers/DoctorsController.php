<?php

namespace App\Http\Controllers;

use App\Day;
use App\Doctor;
use App\Http\Requests\DoctorsRequest;
use App\Insurance;
use App\Major;
use App\OfficeTime;
use App\Picture;
use App\Role;
use App\Service;
use App\Time;
use App\User;
use Auth;
use DB;
use File;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Response;
use Session;
use Validator;

class DoctorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uuid = Uuid::uuid4();
        $temp_token = $uuid->toString();
        $majors = Major::where('level', 1)->get();
        $secondaryMajors = Major::where('level', 2)->get();
        $insurances = Insurance::all();
        $services = Service::all();
        $contents = \App\Content::all()->keyBy('key')->toArray();
        return view('doctors.create', compact('majors', 'secondaryMajors', 'temp_token', 'insurances', 'services', 'contents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(DoctorsRequest $request)
    {
        $user = new User;
        $roles = Role::pluck('id', 'key')->toArray();

        if (isset($request->sname_ar)){
            $fullName = $request->fname_ar . ' ' . $request->sname_ar . ' ' . $request->lname_ar;
        }
        else
            $fullName = $request->fname_ar . ' ' . $request->lname_ar;

        DB::transaction(function () use ($request, $user, $roles, $fullName) {

            $user = User::create([
                'name'     => $request->username,
                'email'    => $request->email,
                'password' => bcrypt($request->password),
                'status'   => 1,
                'approved' => 1,
            ]);
            $user->roles()->attach($roles['doctor']);

            $city = $request->city;
            $region = $request->region;
            $major = $request->major;
            $secondaryMajors = $request->secondaryMajors;
            $insurances = $request->insurances;
            $secondaryMajors[] = $major;
            $doctor = $user->doctor()->create([
                'fname_ar'             => $request->fname_ar,
                'sname_ar'             => $request->sname_ar,
                'lname_ar'             => $request->lname_ar,
                'title_ar'             => $request->title_ar,
                'prefix'               => $request->prefix,
                'mobile'               => $request->mobile,
                'whatsapp'             => $request->whatsapp,
                'grad_year'            => $request->grad_year,
                'grad_university'      => $request->grad_university,
                'grad_country'         => $request->grad_country,
                'average_waiting_time' => $request->average_waiting_time,
                'dob'                  => $request->dob,
                'gender'               => $request->gender,
                'about_ar'             => $request->about_ar,
                'address_ar'           => $request->address_ar,
                'city_id'              => $city,
                'region_id'            => $region,
                'full_name_ar'         => $fullName,
            ]);
            $office = $doctor->offices()->create([
                'name_ar'    => $request->office_name,
                'address_ar' => $request->address_ar,
                'fees'       => $request->fees,
                'phone'      => $request->phone,
                'city_id'    => $city,
                'region_id'  => $region,
            ]);
            $doctor->majors()->attach($secondaryMajors);
            $office->insurances()->attach($insurances);

            $temp_token = '';
            if ($request->has('temp')) {
                $temp_token = $request->temp;
            }

            $pictures = Picture::where('temp_token', $temp_token)->get();
            for ($i = 0; $i < $pictures->count(); $i++) {
                if ($i >= Doctor::$image_limit) {
                    break;
                }
                $doctor->pictures()->save($pictures[$i]);
                $pictures[$i]->update(['temp_token' => null]);
            }
            Picture::deleteTemp();
        });
        Session::flash('toast', ['سوف تتمكن من الدخول بعد المراجعة والموافقة على حسابك', 'success', 'تم انشاء الحساب بنجاح!']);

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $doctor = Doctor::getDoctorBySlug($slug);
        if (! $doctor->is_approved){
            return redirect('/listings');
        }
        $user = $doctor->user;
        $office = $doctor->getOffice();
        $times = Time::pluck('id', 'key')->toArray();
        $officeTimes = $office->officeTimes()->get();
        $sortedDays = Day::getSortedDays();
        $reviews = $doctor->reviews()->approved()->hasComment()->orderBy("created_at", "desc")->get();
        $customer = optional(\Auth::user()) ->customer;
        $customer_can_rate = $customer && $customer->canRate($doctor->id);
        return view('doctors.show', compact('doctor', 'user', 'office', 'times', 'officeTimes', 'sortedDays', 'reviews', 'customer_can_rate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $doctor = $user->doctor;
        $userArray = [
            'name'   => $request->username,
            'email'  => $request->email,
        ];
        if ($request['password']) // because nullable rule doesnt fucking work
        {
            $userArray['password'] = bcrypt($request->password);
        }
        if (isset($request->sname_ar)){
            $fullName = $request->fname_ar . ' ' . $request->sname_ar . ' ' . $request->lname_ar;
        }
        else
            $fullName = $request->fname_ar . ' ' . $request->lname_ar;

        DB::transaction(function () use ($request, $user, $doctor, $userArray, $fullName) {

            $user->update($userArray);
            $city = $request->city;
            $region = $request->region;
            $major = $request->major;
            $secondaryMajors = $request->secondaryMajors;
            $insurances = $request->insurances;
            $secondaryMajors[] = $major;
            $office = $doctor->offices()->first();
            $doctor->update([
                'fname_ar'             => $request->fname_ar,
                'sname_ar'             => $request->sname_ar,
                'lname_ar'             => $request->lname_ar,
                'title_ar'             => $request->title_ar,
                'mobile'               => $request->mobile,
                'whatsapp'             => $request->whatsapp,
                'grad_year'            => $request->grad_year,
                'grad_university'      => $request->grad_university,
                'grad_country'         => $request->grad_country,
                'is_available'         => $request->is_available,
                'average_waiting_time' => $request->average_waiting_time,
                'dob'                  => $request->dob,
                'gender'               => $request->gender,
                'about_ar'             => $request->about_ar,
                'address_ar'           => $request->address_ar,
                'city_id'              => $city,
                'region_id'            => $region,
                'full_name_ar'         => $fullName,
            ]);
            $office->update([
                'name_ar'    => $request->office_name,
                'address_ar' => $request->address_ar,
                'fees'       => $request->fees,
                'phone'      => $request->phone,
                'city_id'    => $city,
                'region_id'  => $region,
            ]);
            $doctor->majors()->sync($secondaryMajors);
            $office->insurances()->sync($insurances);
        });
        Session::flash('toast', 'تم تعديل البيانات بنجاح');

        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function profile()
    {
        $uuid = Uuid::uuid4();
        $temp_token = $uuid->toString();
        $majors = Major::where('level', 1)->get();
        $secondaryMajors = Major::where('level', 2)->get();
        $insurances = Insurance::all();
        $user = Auth::user();
        $doctor = $user->doctor;
        $doctor_id = $doctor->id;
        $office = $doctor->getOffice();
        return view('doctors.profile', compact('majors', 'secondaryMajors', 'temp_token', 'user', 'doctor', 'doctor_id', 'office', 'insurances'));
    }

    public function schedule()
    {
        $user = Auth::user();
        $doctor = $user->doctor;
        $office = $doctor->getOffice();
        $times = Time::pluck('id', 'key')->toArray();
        $officeTimes = $office->officeTimes()->get();
        return view('doctors.schedule', compact('user', 'doctor', 'office', 'times', 'officeTimes'));
    }
    public function scheduleStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "days"       => 'array|max:7',
            "evenings"   => 'array|max:7',
            "days.*"     => 'in:1',
            "evenings.*" => 'in:1',
        ]);
        $validator->validate();

        $user = Auth::user();
        $doctor = $user->doctor;
        $office = $doctor->getOffice();
        $dayChecks = $request->days;
        $eveningChecks = $request->evenings;
        $times = Time::pluck('id', 'key')->toArray();
        $office->officeTimes()->delete();
        for ($i = 1; $i <= 7; $i++){
            if (isset($dayChecks[$i])){
                OfficeTime::create([
                   'office_id' => $office->id,
                   'day_id' => $i,
                   'time_id' => $times['day'],
                   'status' => 1,
                ]);
            }
            if (isset($eveningChecks[$i])){
                OfficeTime::create([
                    'office_id' => $office->id,
                    'day_id' => $i,
                    'time_id' => $times['evening'],
                    'status' => 1,
                ]);
            }
        }
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', ['تم تعديل الجدول بنجاح', 'success']);
        return back();
    }

    public function bookings(Request $request){
        $user = Auth::user();
        $doctor = $user->doctor;
        $office = $doctor->getOffice();
        $bookings = $office->bookings()->with('office', 'customer', 'time', 'bookingStatus', 'visitStatus', 'insurance')->paginate(10);
        return view('doctors.bookings', compact('user', 'doctor', 'office', 'bookings'));
    }

    public function cancel(Request $request, \App\Booking $booking){
        if ($booking->office->doctor->id != \Auth::user()->doctor->id){ // verify it's the same user who made the booking
            abort(501);
        }
        //dd($request->all(), $booking);
        $bookingStatuses = \App\BookingStatus::pluck('id', 'key')->toArray();
        $booking->update([
            'booking_status_id' => $bookingStatuses['canceled_by_doctor']
        ]);
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', ['تم الغاء الحجز بنجاح!', 'success', 'تمت العملية!']);
        return redirect('/user/bookings');

    }

    public function uploadImagesStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image"  => 'required|mimes:jpg,png,jpeg|max:2000',
            "temp"   => 'required',
            "qquuid" => 'required',
        ]);
        $temp_token = '';
        if ($request->has('temp')) {
            $temp_token = $request->temp;
        }
        $temp_uuid = '';
        if ($request->has('qquuid')) {
            $temp_uuid = $request->qquuid;
        }
        if ($temp_token) {
            $validator->after(function ($validator) use ($temp_token) {
                if (Picture::where('temp_token', $temp_token)->count() >= Doctor::$image_limit) {
                    $validator->errors()->add('image', 'You cant upload more than '.Doctor::$image_limit.' images!');
                }
            });
        }

        if ($validator->fails()) {
            return response()->json(implode('
            ', array_flatten($validator->messages()->toArray())), 500);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_'.Picture::getSanitizedName($image);
            $picture_path = 'images/doctors/'.$picture_name;
            $thumbnail_path = 'images/doctors/'.$thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/'.$picture_path;
            $thumbnail_path = '/'.$thumbnail_path;
            Picture::create([
                'name'       => $picture_name,
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_token' => $temp_token,
                'temp_uuid'  => $temp_uuid,
            ]);
        }

        return Response::json(["success" => true, "class" => "success", "message" => "Upload Successful"], 201);
    }

    public function uploadImagesUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image"     => 'required|mimes:jpg,png,jpeg|max:2000',
            "qquuid"    => 'required',
            "doctor_id" => 'required',
        ]);

        $temp_uuid = '';
        if ($request->has('qquuid')) {
            $temp_uuid = $request->qquuid;
        }

        $doctor = Doctor::findOrFail($request->doctor_id);

        if (isset($doctor)) {
            $validator->after(function ($validator) use ($doctor) {
                if ($doctor->pictures()->count() > Doctor::$image_limit) {
                    $validator->errors()->add('image', 'You cant upload more than '.Doctor::$image_limit.' images!');
                }
            });
        }

        if ($validator->fails()) {
            return response()->json(implode('
            ', array_flatten($validator->messages()->toArray())), 500);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_'.Picture::getSanitizedName($image);
            $picture_path = 'images/doctors/'.$picture_name;
            $thumbnail_path = 'images/doctors/'.$thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/'.$picture_path;
            $thumbnail_path = '/'.$thumbnail_path;
            $doctor->pictures()->create([
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_uuid'  => $temp_uuid,
                'name'    => $picture_name,
            ]);
        }

        return Response::json(["success" => true, "class" => "success", "message" => "Upload Successful"], 201);
    }

    public function listDoctorImages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "doctor_id" => 'required',
        ]);

        $doctor = Doctor::findOrFail($request->doctor_id);

        if ($validator->fails()) {
            return response()->json(implode('
            ', array_flatten($validator->messages()->toArray())), 500);
        }
        $returnedImages = [];
        $pictures = $doctor->pictures;
        foreach ($pictures as $picture) {
            $returnedImages[] = [
                "id"           => $picture->id,
                "name"         => $picture->name ? $picture->name : '',
                "uuid"         => $picture->temp_uuid,
                "thumbnailUrl" => $picture->thumb_path,
            ];
        }

        return Response::json($returnedImages);
    }

    public function DeleteImage(Request $request)
    {
        $temp_uuid = '';
        if ($request->has('qquuid')) {
            $temp_uuid = $request->qquuid;
        }
        $picture = Picture::where('temp_uuid', $temp_uuid)->get()->first();
        if ($picture->image_path) {
            File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
        }
        if ($picture->thumb_path) {
            File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
        }
        $picture->delete();

        return Response::json([
            "success" => true,
            "class"   => "success",
            "message" => "Image Deleted Successfully",
        ], 201);
    }
}
