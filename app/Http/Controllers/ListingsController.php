<?php

namespace App\Http\Controllers;

use App;
use App\Booking;
use App\Doctor;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Validator;

class ListingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = App\City::has('offices')->get();
        $regions = App\Region::has('offices')->get();
        $insurances = App\Insurance::has('offices')->get();
        $times = App\Time::where('is_shown', 1)->pluck('id', 'key')->toArray();
        $sortedDays = App\Day::getSortedDays();

        $doctors = Doctor::approved()->with('offices.officeTimes', 'majors', 'city', 'user', 'region');

        if ($request->filled('doctor')) {
            $doctors->search($request->doctor);
        }
        if ($request->filled('city')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->whereHas('city', function ($query2) use ($request) {
                    $query2->where('key', 'like', $request->city);
                });
            });
        }
        if ($request->filled('region')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->whereHas('region', function ($query2) use ($request) {
                    $query2->where('key', 'like', $request->region);
                });
            });
        }
        if ($request->filled('major')) {
            $doctors->whereHas('majors', function ($query) use ($request) {
                $query->where('name_ar', 'like', $request->major);
            });
        }
        if ($request->filled('office')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->where('name_ar', 'LIKE', '%' . $request->office . '%');
            });
        }
        if ($request->filled('insurance')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->whereHas('insurances', function ($query2) use ($request) {
                    $query2->where('name_ar', 'like', $request->insurance);
                });
            });
        }
        if ($request->filled('service')) {
            $doctors->whereHas('offices', function ($query) use ($request) {
                $query->whereHas('services', function ($query2) use ($request) {
                    $query2->where('name_ar', 'like', $request->service);
                });
            });
        }
        if ($request->filled('price')) {

            $arr = explode(' - ', request('price'));
            if (count($arr) == 2) {
                $feeStart = (int)$arr[0];
                $feeEnd = (int)$arr[1];

                $doctors->whereHas('offices', function ($query) use ($request, $feeStart, $feeEnd) {
                    $query->whereBetween('fees', [$feeStart, $feeEnd]);
                });
            }
            
        }
        $doctors->orderBy('rank', 'desc');
        $doctors = $doctors->paginate(10);
        return view('pages.listings', compact('doctors', 'majors', 'times', 'insurances', 'cities', 'sortedDays', 'regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function reservation(Request $request){
        $validator = Validator::make($request->all(), [
            "office" => 'required|integer|exists:offices,id',
            "day"    => 'required|integer|exists:days,id',
            "time"   => 'required|integer|exists:times,id',
            "date"   => 'required|date',
        ]);
        $validator->validate();

        $customer = new App\Customer;
        if (\Auth::check() && \Auth::user()->isCustomer()){
            $customer = \Auth::user()->customer;
        }
        $office = App\Office::findOrFail($request->office);
        $doctor = $office->doctor;
        if (! $doctor->is_approved){
            return redirect('/listings');
        }
        $day = App\Day::findOrFail($request->day);
        $time = App\Time::findOrFail($request->time);
        $user = $doctor->user;
        $date = Carbon::parse($request->date);
        $insurances = $office->insurances;
        return view('pages.reservation',  compact('customer', 'day', 'time', 'office', 'doctor', 'user', 'date', 'insurances'));
    }

    public function reserve(Request $request){
        $validator = Validator::make($request->all(), [
            "office"       => 'required|integer|exists:offices,id',
            "day"          => 'required|integer|exists:days,id',
            "time"         => 'required|integer|exists:times,id',
            "date"         => 'required|date',
            "patient_name" => 'required|between:5,255',
            "mobile"       => ['required', 'regex:/^[\x{0660}-\x{0669}|0-9]{6,20}$/u'],
            "email"        => 'nullable|email|max:50',
            "insurance"    => 'nullable|integer|exists:insurances,id',
        ]);
        $validator->validate();
        $bookingStatuses = App\BookingStatus::pluck('id', 'key')->toArray();
        $customer_id = null;
        if (\Auth::check() && \Auth::user()->isCustomer()){
            $customer_id = \Auth::user()->customer->id;
        }
        $insurance_id = null;
        if (filled($request->insurance)){
            $insurance_id = $request->insurance;
        }
        $doctor = App\Office::findOrFail($request->office)->doctor;
        $booking = Booking::updateOrCreate([
            'patient_name'      => $request->patient_name,
            'mobile'            => $request->mobile,
            'office_id'         => $request->office,
            'appointment_date'  => $request->date,
        ],
        [
            'time_id'           => $request->time,
            'booking_status_id' => $bookingStatuses['reviewing'],
            'email'             => $request->email,
            'customer_id'       => $customer_id,
            'insurance_id'      => $insurance_id,
        ]);

        \Mail::send(new App\Mail\AdminBooking($booking));
        $day = App\Day::findOrFail($request->day);
        $time = App\Time::findOrFail($request->time);
        $date = Carbon::parse($request->date);


        // App\Booking::clearDuplicates();

        Session::flash('toast', ['سيتم ارسال رسالة نصية فيها تفاصيل الحجز.', 'success', 'تم اتمام الحجز بنجاح!', true]);
        if (Auth::check() && Auth::user()->isRole("customer")){
            return redirect('/user/bookings');
        }
        else return redirect($doctor->path())
            ->with(['confirmed' => [
                    'day' => $day->name_ar,
                    'time' => $time->name_ar,
                    'date'=> $date->toDateString()
                    ]]);

    }
}
