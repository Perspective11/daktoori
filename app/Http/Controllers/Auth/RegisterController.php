<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'fname_ar' => 'required|between:2,20',
            'sname_ar' => 'required|between:2,20',
            'lname_ar' => 'required|between:2,20',
            'city'     => 'nullable|exists:cities,id',
            'region'   => 'nullable|exists:regions,id',
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $validator->setAttributeNames([
            "fname_ar" => 'الأسم الأول',
            "sname_ar" => 'أسم الأب',
            "lname_ar" => 'اللقب',
        ]);
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (isset($data['sname_ar'])){
            $name = $data['fname_ar'] . ' ' . $data['sname_ar'] . ' ' . $data['lname_ar'];
        }
        else
            $name = $this->fname_ar . ' ' . $this->lname_ar;

        return User::create([
            'name'     => $name,
            'email'    => $data['email'],
            'status'   => 1,
            'approved' => 1,
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function registered(Request $request, $user)
    {
        $customerRoleId = Role::where('key', 'customer')->first();
        $user->customer()->create([
            'fname_ar'   => $request->fname_ar,
            'sname_ar'   => $request->sname_ar,
            'lname_ar'   => $request->lname_ar,
            'city_id'    => $request->city,
            'region_id'  => $request->region,
        ]);
        $user->roles()->attach($customerRoleId);
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
    }
    public function showRegistrationForm()
    {
        return redirect('login');
    }

}
