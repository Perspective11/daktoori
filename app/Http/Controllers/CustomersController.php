<?php

namespace App\Http\Controllers;

use App\Booking;
use App\BookingStatus;
use App\Customer;
use App\Review;
use Illuminate\Http\Request;
use Session;
use Validator;

class CustomersController extends Controller
{
    public function bookings(Request $request){
        $customer = new Customer;
        if (\Auth::check() && \Auth::user()->isCustomer()){
            $customer = \Auth::user()->customer;
        }
        else
            abort(404);
        $bookingStatuses = BookingStatus::all();
        $bookings = $customer->bookings()->with('office.doctor', 'customer', 'time', 'bookingStatus', 'visitStatus')->paginate(3);
        return view('customers.bookings', compact('customer', 'bookings'));
    }
    public function cancel(Request $request, Booking $booking){
        if ($booking->customer_id != \Auth::user()->customer->id){ // verify it's the same user who made the booking
            abort(501);
        }
        //dd($request->all(), $booking);
        $bookingStatuses = \App\BookingStatus::pluck('id', 'key')->toArray();
        $booking->update([
            'booking_status_id' => $bookingStatuses['canceled_by_customer']
        ]);
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }
        Session::flash('toast', ['تم الغاء الحجز بنجاح!', 'success', 'تمت العملية!']);
        return redirect('/user/bookings');

    }
    public function review(Request $request, \App\Doctor $doctor){
        $validator = Validator::make($request->all(), [
            "rating" => 'required|integer|between:1,5',
            "review" => 'nullable|max:1000',
        ]);
        $validator->validate();
        if (! \Auth::user()->isCustomer()){
            abort(501, 'Unauthorized, user is not a customer');
        }
        $customer = \Auth::user()->customer;
        if (! $customer->canRate($doctor->id)){
            abort(501, 'Unauthorized, customer doesn\'t have a confirmed booking with this doctor');
        }

        Review::where('customer_id', $customer->id)->where('doctor_id', $doctor->id)->delete(); //delete previous review done by same customer to the same doctor

        $review = Review::create([
            'doctor_id'       => $doctor->id,
            'customer_id'     => $customer->id,
            'patient_name'    => $customer->getName(),
            'rating'          => $request->rating,
            'review'          => $request->review,
            'review_approved' => 1,
            'rating_approved' => 1,
        ]);
        $doctor->updateRatings();
        if (request()->wantsJson()){
            return response()
                ->json([], 200);
        }

    }
}
