<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Session;
use Validator;
use View;

class PagesController extends Controller
{
    function __construct()
    {
        $lang = "ar";
        $contents = \App\Content::select('key', 'value_'.$lang)->get()->mapWithKeys(function ($item) use ($lang) {
            return [$item['key'] => $item['value_'.$lang]];
        })->toArray();
        $images = \App\ImageContent::all()->keyBy('key');
        View::share(compact('images', 'contents'));
    }

    function index(){
        return view('index');
    }
    function contact(){
        return view('pages.contact');
    }
    function about(){

        return view('pages.about');
    }
    function privacyPolicy(){
        return view('pages.privacy-policy');
    }
    function credits(){
        return view('pages.credits');
    }
    function team(){
        return view('pages.team');
    }
    function faqs(){
        $faqs = App\Faq::all();
        return view('pages.faqs', compact('faqs'));
    }

    function services(){
        $services = App\Service::has('offices')->get();
        return view('pages.services', compact('services'));
    }
    function majors(){
        return view('pages.majors', compact('majors'));
    }


    function storeContact(Request $request){
        $validator = Validator::make($request->all(), [
            "name"    => 'nullable|string|between:2,50',
            "email"   => 'required|email|between:6,100',
            "subject" => 'nullable|between:2,15',
            "message" => 'required|between:3,2000',
        ]);

        if ($validator->fails())
        {
            Session::flash('toast', [__('contact.notif_not_sent'), 'error', __('toast.error')]);
            return redirect()->back()->withErrors($validator)->withInput();
        }

        App\Contact::create($request->all());
        Session::flash('toast', [__('contact.notif_sent'), 'success', __('toast.success')]);
        return redirect()->back();
    }
}
