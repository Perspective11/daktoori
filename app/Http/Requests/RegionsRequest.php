<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->region ? $this->region->id : 0;
        if ($this->method() == 'POST') {
            $uniqueKey = '|unique:regions,key';
        }
        return [
            'name_ar'  => 'required|between:2,20',
            'name_en'  => 'nullable|between:2,20',
            "key"      => "between:2,20|unique:regions,key, {$id}",
            "is_shown" => "in:0,1",
            "city_id"  => "nullable|exists:cities,id",
        ];
    }
}
