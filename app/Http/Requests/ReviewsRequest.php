<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doctor'       => 'required|exists:doctors,id',
            'patient_name' => 'required|between:3, 100',
            'rating'       => 'required|integer|between:1,5',
            'review'       => 'nullable|max:1000',
        ];
    }
}
