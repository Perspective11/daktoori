<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MajorsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'      => 'required|between:3, 100',
            'name_en'      => 'nullable|between:3, 100',
            'level'        => 'required|integer|in:1,2',
            'parent_major' => 'required_if:level,2',
            'is_hidden'    => 'required|integer|in:0,1',
            'rank'         => 'required|integer',
        ];
    }
}
