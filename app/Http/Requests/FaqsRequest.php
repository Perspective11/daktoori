<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'nullable|between:3, 200',
            'title_ar' => 'required|between:3, 200',
            'body_en'  => 'nullable|max:1000',
            'body_ar'  => 'required|max:1000',
        ];
    }
}
