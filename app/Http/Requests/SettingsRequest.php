<?php

namespace App\Http\Requests;
use App\Setting;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rulesArray = [];
        foreach(request()->except('_token') as $key => $input){
            $setting = Setting::where('key', $key)->firstOrFail();
            $rulesArray[$setting->key ] = $setting->rules;
        }
        return $rulesArray;
    }
}
