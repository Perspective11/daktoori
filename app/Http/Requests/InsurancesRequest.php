<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsurancesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'     => 'required|between:2,20',
            "company_ar"  => "nullable|between:2,20",
            "phone"       => "nullable|digits_between:6,20",
            "address_ar"  => "nullable|max:190",
            "description" => "nullable|max:255",
        ];
    }
}
