<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = \App\Role::pluck('id', 'key')->toArray();

        $requiredPassword = '|nullable';
        if ($this->method() == 'POST') {
            $requiredPassword = '|required';
        }
        $userRules = [
            'name'        => 'required|between:5,200',
            'email'       => 'required|email|max:50|unique:users,email,' . request('id'),
            'password'    => 'min:6|confirmed' . $requiredPassword,
            'user_type'   => 'required|exists:roles,id',
            'user_status' => 'boolean',
            'approved'    => 'boolean',
        ];
        $doctorRules = [
            'fname_ar'             => 'required|between:2,20',
            'sname_ar'             => 'nullable|between:2,20',
            'lname_ar'             => 'required|between:2,20',
            'title_ar'             => 'required|between:5,190',
            'office_name'          => 'required|between:5,190',
            'mobile'               => 'nullable|digits_between:6,20',
            'whatsapp'             => 'nullable|digits_between:6,20',
            'phone'                => 'nullable|digits_between:6,20',
            'grad_year'            => 'nullable|integer|between:1950,2018',
            'grad_university'      => 'nullable|between:3,100',
            'grad_country'         => 'nullable|between:3,100',
            'is_approved'          => 'required|boolean',
            'is_available'         => 'required|boolean',
            'average_waiting_time' => 'nullable|integer|max:240',
            'fees'                 => 'nullable|integer|max:100000',
            'dob'                  => 'nullable|date',
            'gender'               => 'nullable|integer|in:1,2',
            'address_ar'           => 'nullable|max:190',
            'about_ar'             => 'nullable|max:3000',
            'major'                => 'required|exists:majors,id',
            'city'                 => 'required|exists:cities,id',
            'region'               => 'required|exists:regions,id',
            'secondaryMajors'      => 'nullable|array|max:5',
            'secondaryMajors.*'    => 'exists:majors,id',
            'insurances'           => 'nullable|array|max:20',
            'insurances.*'         => 'exists:insurances,id',
        ];
        $customerRules = [
            'customer_fname_ar'   => 'required|between:2,20',
            "customer_sname_ar"   => "nullable|between:2,20",
            "customer_lname_ar"   => "required|between:2,20",
            "customer_mobile"     => "nullable|digits_between:6,20",
            "customer_whatsapp"   => "nullable|digits_between:6,20",
            "customer_dob"        => "nullable|date",
            "customer_gender"     => "nullable|integer|in:1,2",
            "customer_address_ar" => "nullable|max:190",
            "customer_city"       => "required|exists:cities,id",
            "customer_region"     => "required|exists:region,id",
            "note"                => "nullable|max:3000",
        ];

        switch (request('user_type')) {
            case $roles['admin']:
                return $userRules;
                break;
            case $roles['doctor']:
                return array_merge($userRules, $doctorRules);
                break;
            case $roles['customer']:
                return array_merge($userRules, $customerRules);
                break;
            default:
                return [
                    'user_type' => 'required|exists:roles,id',
                ];
                break;
        }
    }
}
