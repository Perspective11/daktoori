<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoctorsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requiredPassword = '|nullable';
        if ($this->method() == 'POST') {
            $requiredPassword = '|required';
        }

        return [
            'username'             => 'required|between:5,200',
            'email'                => 'required|email|max:50|unique:users,email,' . request('id'),
            'password'             => 'min:6|confirmed' . $requiredPassword,
            'fname_ar'             => 'required|between:2,20',
            'sname_ar'             => 'nullable|between:2,20',
            'lname_ar'             => 'required|between:2,20',
            'title_ar'             => 'required|between:5,190',
            'prefix'               => 'nullable|max:10',
            'office_name'          => 'required|between:5,190',
            'mobile'               => 'nullable',
            'whatsapp'             => 'nullable',
            'phone'                => 'nullable',
            'grad_year'            => 'nullable|integer|between:1950,2018',
            'grad_university'      => 'nullable|between:3,100',
            'grad_country'         => 'nullable|between:3,100',
            'average_waiting_time' => 'nullable|integer|max:240',
            'fees'                 => 'nullable|integer|max:100000',
            'dob'                  => 'nullable|date',
            'gender'               => 'nullable|integer|in:1,2',
            'address_ar'           => 'nullable|max:190',
            'about_ar'             => 'nullable|max:3000',
            'major'                => 'required|exists:majors,id',
            'city'                 => 'required|exists:cities,id',
            'region'               => 'required|exists:regions,id',
            'notes_ar'             => 'nullable|max:3000',
            'secondaryMajors'      => 'nullable|array',
            'secondaryMajors.*'    => 'exists:majors,id',
            'insurances'           => 'nullable|array|max:20',
            'insurances.*'         => 'exists:insurances,id',
            'services'             => 'nullable|array|max:20',
            'services.*'           => 'exists:services,id',
        ];
    }
}
