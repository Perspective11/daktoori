<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'id',
        'key',
        'name_ar',
        'name_en',
    ];
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
