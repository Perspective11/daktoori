<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BookingStatus
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Booking[] $bookings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Doctor[] $doctors
 * @mixin \Eloquent
 */
class BookingStatus extends Model
{
    public static $APPROVED = 1;
    public static $REJECTED = 2;
    public static $WAITING = 0;
    protected $fillable = [
        'key',
        'name_ar',
        'name_en',
    ];

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function offices()
    {
        return $this->belongsToMany('App\Office', 'bookings')->using('App\Booking')->withTimestamps();
    }
    public function customers()
    {
        return $this->belongsToMany('App\Customer', 'bookings')->using('App\Booking')->withTimestamps();
    }
}
