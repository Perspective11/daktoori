<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $global_majors = \App\Major::has('doctors')->withCount('doctors')->visible()->ranked()->get();
        $global_services = \App\Service::has('offices')->limit(10)->withCount('offices')->orderBy('offices_count')->get();
        view()->share('global_majors', $global_majors);
        view()->share('global_services', $global_services);
        if($this->app->environment() === 'production'){
            $this->app['request']->server->set('HTTPS', true);        
            \URL::forceScheme('https');
         }
         $this->loadContent();
         $this->loadAppSettings();

    }

    public function loadContent(){
        $lang = "ar";
        $contents = \App\Content::select('key', 'value_'.$lang)->get()->mapWithKeys(function ($item) use ($lang) {
            return [$item['key'] => $item['value_'.$lang]];
        })->toArray();
        \View::share(compact('contents'));
    }

    public function loadAppSettings(){
        try{
            if (! \Cache::has('appSettings')){
                $appSettings = \App\Setting::pluck('value', 'key')->toArray();
                \Cache::forever('appSettings', $appSettings);
            }
        }
        catch (\Exception $e){

        }

        $appSettings = \Cache::get('appSettings');

        \View::share(compact('appSettings'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
