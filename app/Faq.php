<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = [
        "title_en",
        "title_ar",
        "body_en",
        "body_ar",
    ];

    public function path($admin = false){
        if($admin)
            return '/admin/faqs/' . $this->id;

        return '/faqs/' . $this->id;
    }
}
