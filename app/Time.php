<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $fillable = [
        'id',
        'key',
        'name_ar',
        'name_en',
        'is_shown',
    ];


    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function path($admin = false){
        if($admin)
            return '/admin/times/' . $this->id;

        return '/times/' . $this->id;
    }
    public function scopeShown($query)
    {
        return $query->where('is_shown', 1);
    }
}
