<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'status',
        'approved',
        'dev_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function doctor()
    {
        return $this->hasOne('App\Doctor');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer');
    }

    public function path($admin = null)
    {
        if ($admin)
        {
            return '/admin/users/' . $this->id;
        }

        return '/users/' . $this->id;
    }
    public function hasRoles()
    {
        return (bool)count($this->roles);
    }

    public function isCustomer()
    {
        return $this->roles()->where('key', 'customer')->exists();
    }

    public function isDoctor()
    {
        return $this->roles()->where('key', 'doctor')->exists();
    }


    public function child()
    {
        if ((bool)isset($this->customer))
        {
            return $this->customer;
        }
        if ((bool)isset($this->doctor))
        {
            return $this->doctor;
        }
    }
    public function childName()
    {
        if ((bool)isset($this->customer))
        {
            return $this->customer->getName();
        }
        if ((bool)isset($this->doctor))
        {
            return $this->doctor->getName();
        }

        return '';
    }

    public function childOrUserName()
    {
        if ((bool)isset($this->customer))
        {
            return $this->customer->getName();
        }
        if ((bool)isset($this->doctor))
        {
            return $this->doctor->getName();
        }

        return $this->name;
    }
    public function childOrUserFirstName()
    {
        if ((bool)isset($this->customer))
        {
            return $this->customer->fname_ar;
        }
        if ((bool)isset($this->doctor))
        {
            return $this->doctor->fname_ar;
        }

        return $this->name;
    }
    public function isAdmin()
    {
        return $this->isRole('admin') || $this->isRole('superadmin');
    }
    public function isSuperAdmin()
    {
        return $this->isRole('superadmin');
    }
    public function isRole($role)
    {
        return $this->roles()->where('key', $role)->exists();
    }
    public function getRoleName($english = false)
    {
        if ($english)
            return $this->roles()->first()->name_en;
        else
            return $this->roles()->first()->name_ar;
    }
    public function toggleActive()
    {
        if ($this->status)
        {
            $this->status = 0;
            Session::flash('toast', 'User successfully deactivated');
        } else
        {
            $this->status = 1;
            Session::flash('toast', 'User successfully activated');
        }
        $this->save();
    }
    public function getIndexPath(){
        if ($this->isRole('doctor')){
            return '/admin/doctors';
        }
        if ($this->isRole('customer')){
            return '/admin/customers';
        }
        return '/admin/users';
    }

    public static function searchUser($string, $returnType = null, $onlyDoctors = true)
    {
        $c = collect([]);
        $users = User::where('name', 'like', '%' . $string . '%')->with('customer', 'doctor')->get();
        foreach ($users as $user)
        {
            $c->push($user);
        }
        $doctors = Doctor::where('fname_ar', 'like', '%' . $string . '%')->orWhere('lname_ar', 'like', '%' . $string . '%')->orWhere('sname_ar', 'like', '%' . $string . '%')->with('user')->get();
        foreach ($doctors as $doctor)
        {
            $c->push($doctor->user->load('doctor'));
        }
        if(!$onlyDoctors){
            $customers = Customer::where('fname_ar', 'like', '%' . $string . '%')->orWhere('lname_ar', 'like', '%' . $string . '%')->orWhere('sname_ar', 'like', '%' . $string . '%')->with('user')->get();
            foreach ($customers as $customer)
            {
                $c->push($customer->user->load('customer'));
            }
        }
        
        if ($returnType == 'id')
        {
            return $c->pluck('id')->toArray();
        }

        return $c;
    }

    public static function activatedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $activatedCount = static::where('created_at', '>=', $date->toDateString())->where('status', 1)->count();

        return compact('activatedCount', 'count');
    }
}
