<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Contact
 *
 * @mixin \Eloquent
 */
class Contact extends Model
{
    use StatisticsFunctions;

    protected $fillable = [
        'name',
        'email',
        'subject',
        'message'
    ];
    public function path()
    {
        return '/admin/contacts/' . $this->id;
    }
}
