<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Region extends Model
{
    protected $fillable = [
        'id',
        'city',
        'key',
        'name_ar',
        'name_en',
        'is_shown',
    ];
    public function doctors()
    {
        return $this->hasMany('App\Doctor');
    }
    public function offices()
    {
        return $this->hasMany('App\Office');
    }
    public function customers()
    {
        return $this->hasMany('App\Customer');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function path($admin = false){
        if($admin)
            return '/admin/regions/' . $this->id;

        return '/regions/' . $this->id;
    }
    public function getDoctors()
    {
        $doctors = collect([]);
        foreach ($this->offices as $office){
            $doctors[] = $office->doctor;
        }
        return $doctors;
    }
    public function toggleShown(){
        if ($this->is_shown){
            $this->is_shown = 0;
            Session::flash('toast','Region is now hidden');
        }
        else{
            $this->is_shown = 1;
            Session::flash('toast','Region is now shown');
        }
        $this->save();
    }
    public function scopeShown($query)
    {
        return $query->where('is_shown', 1);
    }
}
