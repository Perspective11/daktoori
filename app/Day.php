<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Day
 *
 * @mixin \Eloquent
 */
class Day extends Model
{
    protected $fillable = [
        'id',
        'name_ar',
        'name_en',
    ];
    public static function getSortedDays(){
        $days = static::all()->keyBy('id');
        $currentDate = \Carbon\Carbon::today();
        $dayString = $currentDate->englishDayOfWeek;
        $todayId = 0;
        foreach ($days as $key => $day){
            if ($day->name_en == $dayString){
                $todayId = $day->id;
            }
        }
        $sliceId = $todayId - 1;
        $days1 = $days->slice(0, $sliceId);
        $days2 = $days->slice($sliceId);
        $merged = $days2->concat($days1);

        foreach($merged as $day){
            $day['string'] = $day->name_ar;
            $day['date'] = $currentDate->toDateString();
            $currentDate = $currentDate->addDays(1);
        }
        $merged[$todayId]['string'] = 'اليوم';
        $merged[$todayId + 1]['string'] = 'غداً';

        return $merged;
    }
}
