<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;


class MyHelper
{
    /**
     * @param $targetUrls
     * @param string $urlPathInfo
     * @param bool $strict
     * @return string
     */
    public static function urlActive($targetUrls, string $urlPathInfo, $strict = false)
    {
        if ($strict){
            if (ends_with($urlPathInfo, $targetUrls))
                return 'active';
            return '';
        }
        if (str_contains($urlPathInfo, $targetUrls)){
            return 'active';
        }
        return '';
    }

    public static function slugify($string, $separator = '-')
    {
        if (is_null($string)) {
            return "";
        }

        // Remove spaces from the beginning and from the end of the string
        $string = trim($string);

        // Lower case everything
        // using mb_strtolower() function is important for non-Latin UTF-8 string | more info: http://goo.gl/QL2tzK
        $string = mb_strtolower($string, "UTF-8");;

        // Make alphanumeric (removes all other characters)
        // this makes the string safe especially when used as a part of a URL
        // this keeps latin characters and arabic charactrs as well
        $string = preg_replace("/[^a-z0-9_\s-ءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]#u/", "", $string);

        // Remove multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);

        // Convert whitespaces and underscore to the given separator
        $string = preg_replace("/[\s_]/", $separator, $string);

        $string = filter_var($string, FILTER_SANITIZE_ENCODED);

        return $string;
    }
    public static function mb_str_split($string, $split_length = 1){
        if ($split_length == 1) {
            return preg_split("//u", $string, -1, PREG_SPLIT_NO_EMPTY);
        } else if ($split_length > 1) {
            $return_value = [];
            $string_length = mb_strlen($string, "UTF-8");
            for ($i = 0; $i < $string_length; $i += $split_length) {
                $return_value[] = mb_substr($string, $i, $split_length, "UTF-8");
            }
            return $return_value;
        } else {
            return false;
        }
    }
}