<?php

namespace App\Mail;

use App\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminBooking extends Mailable
{
    use Queueable, SerializesModels;
    public $booking;
    public $doctor;
    public $day;
    public $time;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->doctor = $booking->doctor;
        $this->day = $booking->day;
        $this->time = $booking->time;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to(['Breake16@gmail.com', 'Daktooriinfo@gmail.com'])
            ->subject('إشعار بالحجز - دكتوري')
            ->markdown('emails.admin-booking');
    }
}
