<?php

namespace App\Mail;

use App\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PatientBookingConfirmed extends Mailable
{
    use Queueable, SerializesModels;
    public $booking;
    public $doctor;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->doctor = $booking->doctor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('تأكيد الحجز مع دكتوري')
            ->markdown('emails.patient-booking-confirmed');
    }
}
