<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Major extends Model
{
    protected $fillable = [
        'name_ar',
        'name_en',
        'level',
        'is_hidden',
        'rank',
    ];
    protected $withCount = ['doctors'];

    public function doctors()
    {
        return $this->belongsToMany('App\Doctor');
    }
    public function parent()
    {
        return $this->belongsTo('App\Major', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Major', 'parent_id');
    }

    public function scopePrimary($query){
        return $query->where('level', 1);
    }
    public function scopeSecondary($query){
        return $query->where('level', 2);
    }
    public function scopeVisible($query){
        return $query->where('is_hidden', 0);
    }
    public function scopeRanked($query){
        return $query->orderBy('rank', 'desc');
    }

    public function path($admin = false){
        if($admin)
            return '/admin/majors/' . $this->id;

        return '/majors/' . $this->id;
    }
    public function toggleVisibility()
    {
        if ($this->is_hidden)
        {
            $this->is_hidden = 0;
            Session::flash('toast', 'Major is now visible');
        } else
        {
            $this->is_hidden = 1;
            Session::flash('toast', 'Major is now hidden');
        }
        $this->save();
    }

    public function setIsHidden()
    {
        $this->is_hidden = 1;
        $this->save();
    }
    public function setNotHidden()
    {
        $this->is_hidden = 0;
        $this->save();
    }
}
