<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $with = ['doctor'];
    protected $fillable = [
        'doctor_id',
        'city_id',
        'region_id',
        'name_ar',
        'name_en',
        'address_ar',
        'address_en',
        'phone',
        'mobile',
        'fees',
        'longitude',
        'latitude',
    ];
    public function doctor()
    {
        return $this->belongsTo('App\Doctor');
    }
    public function services()
    {
        return $this->belongsToMany('App\Service');
    }
    public function servicesString()
    {

        $services = $this->services()->pluck('name_ar')->toArray();
        $servicesString = '';
        if ($services){
            $servicesString = implode('، ', $services);
        }
        return $servicesString;
    }
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }
    public function officeTimes()
    {
        return $this->hasMany('App\OfficeTime');
    }
    public function insurances()
    {
        return $this->belongsToMany('App\Insurance');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function region()
    {
        return $this->belongsTo('App\Region');
    }
    public function path($admin = false){
        if($admin)
            return '/admin/offices/' . $this->id;

        return '/offices/' . $this->id;
    }
    public function customers()
    {
        return $this->belongsToMany('App\Customer', 'bookings')->using('App\Booking')->withTimestamps();
    }
    public function insurancesString()
    {
        $insurances = $this->insurances()->pluck('name_ar')->toArray();
        return implode('، ', $insurances);
    }
    public function scopeSearch($query, $keywords)
    {
        return $query
            ->where('name_en', 'LIKE', '%' . $keywords . '%')
            ->orWhere('name_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('company_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('company_en', 'LIKE', '%' . $keywords . '%');
    }
}
