<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ImageContent extends Model
{
    protected $guarded = [];
    
  public static function getSanitizedName($image){
    $extension = $image->guessExtension();
    $fileName = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
    $sanitizedName = Str::limit( preg_replace( '/[^a-z0-9]+/', '-', strtolower( $fileName)), 20 , '');
    $imageName = Str::random(5) . "_" . $sanitizedName . '.' . $extension;
    return $imageName;
}


public function getBackgroundCssString(){
    $color = 'white';
    $image = 'url(' . $this->image_path . ')';
    $position = $this->position ? $this->position : 'center';
    $size = $this->size ? $this->size : 'cover';
    $repeat = $this->repeat ? $this->repeat : 'no-repeat';
    $attachment = $this->attachment ? $this->attachment : 'scroll';

    return $color . ' ' . $image . ' ' . $position . '/' . $size . ' ' . $repeat . ' ' . $attachment;
}
}
