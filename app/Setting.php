<?php

namespace App;

use Cache;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    
      protected $guarded = [];
    

    public function path()
    {
        return '/admin/settings/' . $this->id;
    }

    public static function getLinks()
    {
        return Setting::where('type', 'social_links')->get();
    }

}
