<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    protected $fillable = [
        'id',
        'name_ar',
        'name_en',
    ];

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function path($admin = false){
        if($admin)
            return '/admin/procedures/' . $this->id;

        return '/procedures/' . $this->id;
    }
}
