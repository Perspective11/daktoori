<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Session;

/**
 * App\Booking
 *
 * @property-read \App\BookingStatus $bookingStatus
 * @property-read \App\Customer $customer
 * @property-read \App\Office $office
 * @property-read \App\Time $time
 * @mixin \Eloquent
 */
class OfficeTime extends Pivot
{
    protected $table = 'office_time';
    protected $fillable = [
        'office_id',
        'month_id',
        'day_id',
        'status',
        'waiting_minutes',
        'time_id',
    ];

    public function office()
    {
        return $this->belongsTo('App\Office');
    }
    public function time()
    {
        return $this->belongsTo('App\Time');
    }
    public function doctor()
    {
        return $this->office->doctor();
    }

}
