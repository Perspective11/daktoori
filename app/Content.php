<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Content
 *
 * @mixin \Eloquent
 */
class Content extends Model
{
    const TYPE_NORMAL = 0;
    const TYPE_EDITOR = 1;

    protected $guarded = [];
    
    public function path()
    {
        return '/admin/contents/'.$this->id;
    }
}
