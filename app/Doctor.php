<?php

namespace App;

use App\Helpers\MyHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Session;

class Doctor extends Model
{
    //
    use StatisticsFunctions;

    public static $image_limit = 1;
    protected $with = ['user'];
    protected $fillable = [
        'fname_ar',
        'sname_ar',
        'lname_ar',
        'fname_en',
        'sname_en',
        'lname_en',
        'title_ar',
        'title_en',
        'prefix',
        'dob',
        'mobile',
        'phone',
        'whatsapp',
        'grad_year',
        'grad_university',
        'grad_country',
        'fees',
        'about_ar',
        'about_en',
        'address_ar',
        'address_en',
        'gender',
        'is_available',
        'is_approved',
        'average_waiting_time',
        'user_id',
        'city_id',
        'region_id',
        'total_ratings',
        'rating_percentage',
        'links',
        'full_name_ar',
        'rank'
    ];
    protected $dates = [
        'dob'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pictures()
    {
        return $this->morphMany('App\Picture', 'imageable');
    }

    public function getPicture($large = false){
        if ($this->pictures()->count()){
            if ($large){
                return $this->pictures()->first()->image_path;
            }
            return $this->pictures()->first()->thumb_path;
        }
        else return '/images/doctors/default.png';
    }

    public function majors()
    {
        return $this->belongsToMany('App\Major');
    }

    public function majorsString()
    {
        if (! $this->majors->count()){
            return '';
        }
        $major = $this->majors()->where('level', 1)->first();
        $secondaryMajors = $this->majors()->where('level', 2)->pluck('name_ar')->toArray();
        if ($major !== null){
            $majorName = $major->name_ar;
        }
        else{
            $majorName = $secondaryMajors[0];
            array_shift($secondaryMajors);
        }
        if ($secondaryMajors){
            $secondaryMajorsString = implode('، ', $secondaryMajors);
        }

        $string = $majorName;
        if (count($secondaryMajors))
        {
            $string .= ' متخصص في ' . $secondaryMajorsString;
        }
        return $string;
    }
    public function getMajorName(){
        $major = optional($this->majors()->orderBy('level')->first())->name_ar;
        return $major;
    }
    public function getMajor(){
        $major = $this->majors()->orderBy('level')->first();
        return $major;
    }
    public function getOffice(){
        $office = $this->offices()->first();
        return $office;
    }
    public function officesString()
    {
        return implode('، ', $this->offices->pluck('name_ar')->toArray());
    }
    public function feesString()
    {
        return implode(', ', $this->offices->pluck('fees')->toArray());
    }
    public function getPrefix()
    {
        return $this->prefix;
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function region()
    {
        return $this->belongsTo('App\Region');
    }
    public function offices()
    {
        return $this->hasMany('App\Office');
    }
    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function path($admin = false){
        if($admin)
            return '/admin/doctors/' . $this->id;

        return '/doctors/' . $this->createSlug();
    }
    public function userPath($admin = false){
        $user_id = $this->user->id;
        if($admin)
            return '/admin/users/' . $user_id;

        return '/users/' . $user_id;
    }

    public function age()
    {
        if ($this->dob)
        {
            return $this->dob->age;
        }
    }
    public function getName($with_middle_name = true){
        if ($with_middle_name && $this->sname_ar){
            return $this->fname_ar . ' ' . $this->sname_ar . ' ' . $this->lname_ar;
        }
        return $this->fname_ar . ' ' . $this->lname_ar;
    }

    public function customers()
    {
        return $this->hasManyThrough('App\Customer', 'App\Office');
    }
    public function getCustomers()
    {
        $customers = collect([]);
        foreach ($this->offices as $office){
            $customers[] = $office->customers;
        }
        return $customers->flatten();
    }
    public function scopeSearch($query, $keywords)
    {
        $fuzzySearch = '';
        if (! empty(trim($keywords))) {
            $fuzzySearch = implode("%", MyHelper::mb_str_split(str_replace(" ", "", $keywords)));  // e.g. test -> t%e%s%t
        }
        $fuzzySearch = "%$fuzzySearch%"; // test -> %t%e%s%t%s%
        return $query
            ->where('full_name_ar', 'LIKE', $fuzzySearch)
            ->orWhere('full_name_en', 'LIKE', $fuzzySearch);
    }
    public function scopeApproved($query)
    {
        return $query->where('is_approved', '1');
    }
    public function getBookings()
    {
        $bookings = collect([]);
        foreach ($this->offices as $office){
            $bookings[] = $office->bookings;
        }
        return $bookings->flatten();
    }
    public function toggleApproved()
    {
        if ($this->is_approved == 1)
        {
            $this->is_approved = 0;
            Session::flash('toast', 'Doctor successfully disapproved');
        } else
        {
            $this->is_approved = 1;
            Session::flash('toast', 'Doctor successfully approved');
        }
        $this->save();
    }
    public function setApproved()
    {
        $this->is_approved = 1;
        $this->save();
    }
    public function setRejected()
    {
        $this->is_approved = 0;
        $this->save();
    }
    public function getFreshRating(){
        $ratings = Review::where('doctor_id', $this->id)->pluck('rating')->toArray();
        if ($ratings){
            $average = array_sum($ratings) / count($ratings);
            return round($average, 1);
        }
        else
            return null;
    }
    public function getFreshRatingCount(){
        return Review::where('doctor_id', $this->id)->count();
    }
    public function getRating($percentage = false){
        if ($percentage){
            return $this->rating_percentage / 5 * 100;
        }
        return round($this->rating_percentage, 1);
    }
    public function getRatingsCount(){
        return $this->total_ratings;
    }
    public function getRatingsCountHasComment(){
        return $this->reviews()->hasComment()->count();
    }
    public function updateRatings(){
        $this->total_ratings = $this->getFreshRatingCount();
        $this->rating_percentage = $this->getFreshRating();
        $this->save();
    }

    public function createSlug(){
        $name = $this->getName(true);
        $sluged_name = MyHelper::slugify($name);
        //$sluged_name = str_slug($name);
        return $this->id . '-' . $sluged_name;
    }
    public static function getDoctorBySlug($slug){
        preg_match('/^\d+/',$slug, $matches);
        if (isset($matches[0]))
            $id = $matches[0];
        else
            abort(404);
        $doctor = static::findOrFail($id);
        return $doctor;
    }

    public static function approvedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $approvedCount = static::where('created_at', '>=', $date->toDateString())->where('is_approved', 1)->count();

        return compact('approvedCount', 'count');
    }
    public static function availablePercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $availableCount = static::where('created_at', '>=', $date->toDateString())->where('is_available', 1)->count();

        return compact('availableCount', 'count');
    }

}
