<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Customer
 *
 * @property-read \App\City $city
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Office[] $offices
 * @property-read \App\User $user
 * @mixin \Eloquent
 */
class Customer extends Model
{

    use StatisticsFunctions;

    protected $with = ['user'];

    protected $fillable = [
        'id',
        'fname_ar',
        'sname_ar',
        'lname_ar',
        'fname_en',
        'sname_en',
        'lname_en',
        'dob',
        'mobile',
        'phone',
        'whatsapp',
        'address_ar',
        'address_en',
        'note',
        'gender',
        'user_id',
        'city_id',
        'region_id',
    ];
    protected $dates = [
      'dob'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function region()
    {
        return $this->belongsTo('App\Region');
    }
    public function offices()
    {
        return $this->belongsToMany('App\Office', 'bookings')->using('App\Booking')->withTimestamps();
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }
    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
    public function getPicture($large = false){
        if ($this->pictures()->count()){
            if ($large){
                return $this->pictures()->first()->image_path;
            }
            return $this->pictures()->first()->thumb_path;
        }
        else return '/images/customers/default.jpg';
    }

    public function age()
    {
        if ($this->dob)
        {
            return $this->dob->age;
        }
    }
    public function getName($with_middle_name = true){
        if ($with_middle_name && $this->sname_ar){
            return $this->fname_ar . ' ' . $this->sname_ar . ' ' . $this->lname_ar;
        }
        return $this->fname_ar . ' ' . $this->lname_ar;
    }

    public function path($admin = false){
        if($admin)
            return '/admin/customers/' . $this->id;

        return '/customers/' . $this->id;
    }
    public function userPath($admin = false){
        $user_id = $this->user->id;
        if($admin)
            return '/admin/users/' . $user_id;

        return '/users/' . $user_id;
    }

    public function getDoctors()
    {
        $doctors = collect([]);
        foreach ($this->offices as $office){
            $doctors[] = $office->doctor;
        }
        return $doctors;
    }
    public function canRate($doctor_id){  // check if customer has a booking with doctor
        $doctors_ids = null;
        $bookingStatuses = BookingStatus::where('is_approved', BookingStatus::$APPROVED)->pluck('id')->toArray();
        $bookings = $this->bookings;
        foreach ($bookings as $booking){
            if (in_array($booking->booking_status_id, $bookingStatuses) && ! $booking->isUpcoming()){
                $doctors_ids[] = $booking->doctor->id;
            }
        }
        if (! $doctors_ids)
            return false;
        if (! in_array($doctor_id, $doctors_ids)){
            return false;
        }
        return true;
    }
    public function scopeSearch($query, $keywords)
    {
        return $query
            ->where('fname_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('fname_en', 'LIKE', '%' . $keywords . '%')
            ->orWhere('lname_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('lname_en', 'LIKE', '%' . $keywords . '%')
            ->orWhere('sname_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('sname_en', 'LIKE', '%' . $keywords . '%');
    }
}
