<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\City
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Doctor[] $doctors
 * @mixin \Eloquent
 */
class City extends Model
{
    protected $fillable = [
        'id',
        'country_id',
        'key',
        'name_ar',
        'name_en',
        'is_shown',
    ];
    public function regions()
    {
        return $this->hasMany('App\Region');
    }
    public function doctors()
    {
        return $this->hasMany('App\Doctor');
    }
    public function offices()
    {
        return $this->hasMany('App\Office');
    }
    public function customers()
    {
        return $this->hasMany('App\Customer');
    }
    public function path($admin = false){
        if($admin)
            return '/admin/cities/' . $this->id;

        return '/cities/' . $this->id;
    }
    public function scopeShown($query)
    {
        return $query->where('is_shown', 1);
    }
}
