<?php

namespace App;

use Carbon\Carbon;
use File;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Picture extends Model
{
    //
    protected $fillable = [
        "name",
        "image_path",
        "thumb_path",
        "temp_token",
        "temp_uuid"
    ];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function path($admin = null){
        if($admin){
            return '/admin/pictures/' . $this->id;
        }
        return '/pictures/' . $this->id;
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    public function getTag($lang = null)
    {
        $tag = $this->tags()->first();
        if (! $tag)
            return '';
        if ($lang == 'en')
            return $tag->name_en;
        if ($lang == 'ar')
            return $tag->name_ar;
        return $tag->name_en . ' - ' . $tag->name_ar;
    }
    public function getTagId()
    {
        $tag = $this->tags()->first();
        if (! $tag)
            return '';
        return $tag->id;
    }
    public function tagsString()
    {
        return implode(', ', $this->tags->pluck('name_en')->toArray());
    }
    public function tagsWithLinks()
    {
        $tags = $this->tags->map(function ($tag) {
            $url = '/pictures/tag/' . $tag->id;
            return  "<a href='{$url}'>{$tag->name}</a>";
        })->toArray();
        return implode(', ', $tags);
    }
    public static function modifyImage($image, $width = 1000, $square = false){
        $picture = Image::make($image->getRealPath());
        $picture->resize(null, $width, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // TODO:optimize image here
        if ($square){
            $picture->fit($width, $width);
        }
        return $picture;
    }
    public static function makeThumbnail($image, $width = 200){
        $picture = Image::make($image->getRealPath());
        $picture->resize(null, $width, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // TODO:optimize image here
        return $picture;
    }
    public static function getSanitizedName($image){
        $extension = $image->guessExtension();
        $fileName = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
        $sanitizedName = str_limit( preg_replace( '/[^a-z0-9]+/', '-', strtolower( $fileName)), 20 , '');
        $imageName = Carbon::now()->timestamp . '_' . str_random(5) . "_" . $sanitizedName . '.' . $extension;
        return $imageName;
    }

    public static function deleteTemp(){
        $pictures = static::whereNotNull('temp_token')->get();
        foreach ($pictures as $picture){
            File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
            File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
            $picture->delete();
        }
    }
}
