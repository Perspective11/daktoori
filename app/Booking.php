<?php

namespace App;

use App\Mail\PatientBookingConfirmed;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Session;

/**
 * App\Booking
 *
 * @property-read \App\BookingStatus $bookingStatus
 * @property-read \App\Customer $customer
 * @property-read \App\Office $office
 * @property-read \App\Time $time
 * @mixin \Eloquent
 */
class Booking extends Pivot
{

    use StatisticsFunctions;
    protected $table = 'bookings';
    protected static $statuses;
    protected static $visitStatuses;
    protected $fillable = [
        'note',
        'actual_datetime',
        'scheduled_datetime',
        'appointment_date',
        'office_id',
        'customer_id',
        'insurance_id',
        'procedure_id',
        'time_id',
        'booking_status_id',
        'visit_status_id',
        'patient_name',
        'mobile',
        'email',
        'is_invoiced',
        'patient_confirmed',
        'patient_rating',
        'cost',
    ];

    protected $dates = [
        'actual_datetime',
        'scheduled_datetime',
        'appointment_date'
    ];

    /**
     * Booking constructor.
     *
     * @param $bookingsStatuses
     */
    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
        static::$statuses = \App\BookingStatus::pluck('id', 'key')->toArray();
        static::$visitStatuses = \App\VisitStatus::pluck('id', 'key')->toArray();
    }

    public function office()
    {
        return $this->belongsTo('App\Office');
    }
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
    public function insurance()
    {
        return $this->belongsTo('App\Insurance');
    }
    public function procedure()
    {
        return $this->belongsTo('App\Procedure');
    }
    public function time()
    {
        return $this->belongsTo('App\Time');
    }
    public function bookingStatus()
    {
        return $this->belongsTo('App\BookingStatus');
    }
    public function visitStatus()
    {
        return $this->belongsTo('App\VisitStatus');
    }
    public function city()
    {
        return $this->office->city();
    }
    public function doctor()
    {
        return $this->office->doctor();
    }
    public function path($admin = false){
        if($admin)
            return '/admin/bookings/' . $this->id;

        return '/bookings/' . $this->id;
    }
    public function toggleApproved()
    {
        if ($this->booking_status_id === static::$statuses['confirmed'])
        {
            $this->booking_status_id = static::$statuses['rejected'];
            Session::flash('toast', 'Booking successfully rejected');
        }
        else
        {
            $this->booking_status_id = static::$statuses['confirmed'];
            Session::flash('toast', 'Booking successfully approved');
            if($this->email){
                \Mail::to($this->email)->send(new PatientBookingConfirmed($this));
            }
        }
        $this->save();
    }
    public function setApproved()
    {
        $this->booking_status_id = static::$statuses['confirmed'];
        $this->save();
    }
    public function setApproving()
    {
        $this->booking_status_id = static::$statuses['approving'];
        $this->save();
    }
    public function setRejected()
    {
        $this->booking_status_id = static::$statuses['rejected'];
        $this->save();
    }
    public function scopeUpcoming($query)
    {
        return $query->where('appointment_date', '>=', Carbon::today()->toDateString());
    }
    public function scopePast($query)
    {
        return $query->where('appointment_date', '<', Carbon::today()->toDateString());
    }
    public function getWhen($en = false)
    {
        if ($this->appointment_date < Carbon::today()){
            if ($en){
                return 'Past';
            }
            return 'مضى';
        }

        if ($this->appointment_date >= Carbon::today()){
            if($en){
                return 'Upcoming';
            }
            return 'قادم';
        }

    }
    public function isUpcoming()
    {
        if ($this->appointment_date > Carbon::today()){
            return true;
        }
        return false;

    }

    public static function clearDuplicates()
    {
        $uniqueBookings = static::select('id', 'office_id', 'patient_name', 'appointment_date')->get();
        $uniqueBookings = $uniqueBookings->unique(function ($item) {
            return $item['office_id'].$item['patient_name'].$item['appointment_date'];
        });
        $uniqueBookingsId = array_column($uniqueBookings ->toArray(), 'id');
        static::whereNotIn('id', $uniqueBookingsId )->delete();
    }

    public static function approvedPercentage($days = 30)
    {

        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $approvedCount = static::where('created_at', '>=', $date->toDateString())->where('booking_status_id', static::$statuses['confirmed'])->count();

        return compact('approvedCount', 'count');
    }
}
