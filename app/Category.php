<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name_ar'
    ];
    public function posts()
    {
        return $this->hasMany('App\Post');
    }
    public function path()
    {
        return '/admin/categories/' . $this->id;
    }
}
