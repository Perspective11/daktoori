<?php

return [
    'blog'         => 'المدونة',
    'search-here'  => 'ابحث هنا',
    'recent-posts' => 'اخر المنشورات',
    'archive'      => 'ارشيف',
    'filters'      => 'بحث بحسب',
    'date'         => 'التاريخ',
    'search'       => 'النص',
    'search-btn'   => 'ابحث!',


];
