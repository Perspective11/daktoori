$(function() {
    $.fn.select2.defaults.set("theme", "bootstrap");
    $.fn.select2.defaults.set("dir", "rtl");
    $.fn.select2.defaults.set("width", "100%");
	$('#select-city').select2({
        dir: 'rtl',
	});
    $('#select-region').select2({
        dir: 'rtl',
    });
    $('#select-major').select2({
        dir: 'rtl',
    });

    $('.rate-button').on('click', function (e) {
        e.preventDefault();
        var buttonData = $(this)[0].dataset;
        var doctorid = buttonData.doctorId;
        iziToast.show({
            rtl: true,
            layout: 1,
            drag: false,
            timeout: false,
            close: true,
            closeOnEscapeKey: true,
            balloon: true,
            icon: 'fal fa-star',
            overlay: true,
            color: 'blue',
            displayMode: 'replace',
            id: 'question',
            progressBar: false,
            title: 'قيّم',
            message: 'كيف كانت تجربتك مع الطبيب؟',
            position: 'center',
            inputs: [
                ['<select name="rating" class="rating-select"><option value="">اختر</option><option value="1">سيئة جداً</option><option value="2">سيئة</option><option value="3">لا بأس</option><option value="4">جيدة</option><option value="5">ممتازة</option></select>', 'change', function (instance, toast, select, e) {

                }],
                ['<textarea name="review" class="review-input" placeholder="ضع تعليقك هنا إن وُجد"></textarea>', 'keyup', function (instance, toast, input, e) {

                }, true],
            ],
            buttons: [
                ['<button><b>ادراج</b></button>', function (instance, toast, button, e, inputs) {
                    var rating = inputs[0].options[inputs[0].selectedIndex].value;
                    var review = inputs[1].value;
                    console.info(rating, review);
                    if (! rating){
                        iziToast.error({
                            id: 'success',
                            zindex: 9999,
                            timeout: 5000,
                            title: 'خطأ',
                            overlay: true,
                            message: 'يجب ان تختر قيمة لتقييم الطبيب',
                            position: 'center',
                            rtl: true
                        });
                        return;
                    }

                    $.ajax(
                        {
                            type: "post",
                            url: "/user/review/" + doctorid,
                            data: {
                                _token : $('meta[name="csrf-token"]').attr('content'),
                                doctorId : doctorid,
                                rating : rating,
                                review : review,
                            },
                            dataType: 'json',
                            success: function(data){
                                iziToast.success({
                                    id: 'success',
                                    zindex: 9999,
                                    timeout: 5000,
                                    title: 'شكرا',
                                    overlay: true,
                                    message: 'سيتم اعتبار تقييمك',
                                    position: 'center',
                                    rtl: true
                                });
                            },
                            error: function (data) {
                                iziToast.error({
                                    id: 'error',
                                    zindex: 9999,
                                    timeout: 5000,
                                    title: 'خطأ',
                                    overlay: true,
                                    message: 'لم تتم العملية بنجاح',
                                    position: 'center',
                                    rtl: true
                                });
                            }
                        }
                    );
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }, false], // true to focus
            ],
            onClosing: function(instance, toast, closedBy){
                // console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                // console.info('Closed | closedBy: ' + closedBy);
            }
        });
    })
});