@extends('layouts.app')
@section('content')
    <section class="middle-padding">
        <div class="container">
            <!--dasboard-wrap-->
            <div class="col-md-8 col-md-offset-2">
                <!-- dashboard-content-->
                <div class="dashboard-content fl-wrap">
                    <div class="dashboard-list-box fl-wrap">
                        <div class="dashboard-header fl-wrap">
                            <h3>حجوزاتي</h3>
                        </div>
                    @forelse ($bookings as $booking)
                        <!-- dashboard-list end-->
                            <div class="dashboard-list">
                                <div class="dashboard-message">
                                    <span class="new-dashboard-item booking-status hidden-xs">{{ $booking->bookingStatus->name_ar }}</span>
                                    <span class="new-dashboard-item booking-when hidden-xs">{{ $booking->appointment_date->diffForHumans() }}</span>
                                    <div class="dashboard-message-avatar">
                                        <img src="{{ $booking->doctor->getPicture() }}" alt="">
                                    </div>
                                    <div class="dashboard-message-text">
                                        <h4><a href="{{ $booking->doctor->path() }}">{{ $booking->doctor->getName() }}</a> - <span><strong>{{ $booking->created_at->format('Y/m/d - h:iA') }}</strong></span></h4>
                                        <div class="booking-details fl-wrap">
                                            <span class="booking-title">تاريخ الموعد :</span>
                                            <span class="booking-text">{{ $booking->appointment_date->format('l Y/m/d') }}</span>
                                        </div>
                                        <div class="booking-details fl-wrap">
                                            <span class="booking-title">وقت الموعد :</span>
                                            <span class="booking-text">{{ $booking->time->name_ar }}</span>
                                        </div>
                                        @if ($booking->office->address_ar)
                                            <div class="booking-details fl-wrap">
                                                <span class="booking-title">العنوان :</span>
                                                <span class="booking-text">{{ $booking->office->address_ar }}</span>
                                            </div>
                                        @endif
                                        @if ($booking->office->phone)
                                            <div class="booking-details fl-wrap">
                                                <span class="booking-title">هاتف المكتب :</span>
                                                <span class="booking-text">{{ $booking->office->phone }}</span>
                                            </div>
                                        @endif

                                        <div class="booking-details fl-wrap">
                                            <span class="booking-title">حالة الحجز :</span>
                                            <span class="booking-text">{{ $booking->bookingStatus->name_ar }}</span>
                                        </div>
                                        <div class="booking-details fl-wrap">
                                            <span class="booking-title">موعد الكشف :</span>
                                            <span class="booking-text">{{ $booking->appointment_date->diffForHumans() }}</span>
                                        </div>
                                        <span class="fw-separator"></span>
                                        <div class="booking-details booking-buttons fl-wrap">
                                            {{-- if the booking has not been rejected and the appointment hasnt taken place --}}
                                            @if ($booking->bookingStatus->is_approved !== \App\BookingStatus::$REJECTED && $booking->isUpcoming())
                                                <button class="btn booking-cancel color2-bg float-btn" data-booking-id="{{ $booking->id }}">الغاء الحجز<i class="fal fa-times"></i></button>
                                            @endif
                                            {{-- if the booking has been approved, and the appointment has already taken place --}}
                                            @if ($booking->customer->canRate($booking->doctor->id))
                                                <button class="btn rate-button color-bg float-btn" data-doctor-id="{{ $booking->doctor->id }}">تقييم الطبيب<i class="fal fa-star"></i></button>
                                            @endif
                                        </div>
                                        @if ($booking->note)
                                            <span class="fw-separator"></span>
                                            <p>{{ $booking->note }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- dashboard-list end-->
                        @empty
                            <div class="dashboard-message-text">
                                <h4>لا توجد حجوزات</h4>
                            </div>
                        @endforelse
                    </div>
                    <div class="pagination-wrapper row text-center">
                        <div class="col-xs-12">
                            {{ $bookings->links() }}
                        </div>
                    </div>
                </div>
                <!-- dashboard-list-box end-->
            </div>
            <!-- dasboard-wrap end-->
        </div>
    </section>
@endsection
@section('bottom-ex')
    <script>
        $(function() {
            $('.booking-cancel').on('click', function (e) {
                e.preventDefault();
                var buttonData = $(this)[0].dataset;
                var bookingid = buttonData.bookingId;
                swal({
                    title: "تأكيد",
                    text: "هل أنت متأكد من الغاء الحجز مع الطبيب؟",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "نعم",
                    confirmButtonColor: "#ec6c62",
                    cancelButtonText: "لا",
                    cancelButtonColor: "#cbccce"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: "/user/bookings/cancel/" + bookingid,
                            data: {_token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("تم الألغاء!", "الحجز الغي بنجاح", "success");
                                window.location.reload();
                            },
                            error: function (data) {
                                swal("خطأ!", "لم تتم العملية بنجاح", "error");
                            }
                        }
                    )
                });
            });
        });
    </script>
@endsection