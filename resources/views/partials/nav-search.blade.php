<div class="header-search vis-search">
    <div class="container">
        <div class="row">
            <!-- header-search-input-item -->
            <div class="col-sm-4">
                <div class="header-search-input-item fl-wrap location autocomplete-container">
                    <label>Destination or Doctor Name</label>
                    <span class="header-search-input-item-icon"><i class="fal fa-map-marker-alt"></i></span>
                    <input placeholder="Location" class="autocomplete-input" id="autocompleteid" value="" type="text">
                    <a href="#"><i class="fal fa-dot-circle"></i></a>
                </div>
            </div>
            <!-- header-search-input-item end -->
            <!-- header-search-input-item -->
            <div class="col-sm-3">
                <div class="header-search-input-item fl-wrap date-parent">
                    <label>Date In-Out </label>
                    <span class="header-search-input-item-icon"><i class="fal fa-calendar-check"></i></span>
                    <input placeholder="When" name="header-search" value="" type="text">
                </div>
            </div>
            <!-- header-search-input-item end -->
            <!-- header-search-input-item -->
            <div class="col-sm-3">
                <div class="header-search-input-item fl-wrap">
                    <div class="quantity-item">
                        <label>Rooms</label>
                        <div class="quantity">
                            <input min="1" max="3" step="1" value="1" type="number">
                        </div>
                    </div>
                    <div class="quantity-item">
                        <label>Adults</label>
                        <div class="quantity">
                            <input min="1" max="3" step="1" value="1" type="number">
                        </div>
                    </div>
                    <div class="quantity-item">
                        <label>Children</label>
                        <div class="quantity">
                            <input min="0" max="3" step="1" value="0" type="number">
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-search-input-item end -->
            <!-- header-search-input-item -->
            <div class="col-sm-2">
                <div class="header-search-input-item fl-wrap">
                    <button class="header-search-button" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                </div>
            </div>
            <!-- header-search-input-item end -->
        </div>
    </div>
    <div class="close-header-search"><i class="fal fa-angle-double-up"></i></div>
</div>