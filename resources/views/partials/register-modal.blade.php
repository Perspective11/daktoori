<div class="main-register-wrap modal">
    <div class="reg-overlay"></div>
    <div class="main-register-holder">
        <div class="main-register rtl fl-wrap">
            <div class="close-reg color-bg"><i class="fal fa-times"></i></div>
            <ul class="tabs-menu">
                <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> تسجيل الدخول</a></li>
                <li><a href="#tab-2"><i class="fal fa-user-plus"></i> اشتراك</a></li>
            </ul>
            <!--tabs -->
            <div id="tabs-container">
                <div class="tab">
                    <!--tab -->
                    <div id="tab-1" class="tab-content">
                        <h3>سجل الدخول
                            <span class="logo-style pull-left">
                                <span>Dak<strong>toori</strong></span>
                            </span>
                        </h3>
                        <div class="custom-form">
                            <form name="registerform" id="login-form" method="POST">
                                {{ csrf_field() }}
                                <label>البريد الالكتروني <i class="fal fa-envelope"></i><span>*</span> </label>
                                <input name="email" type="email" required onClick="this.select()" value="">
                                <label>كلمة المرور <i class="fal fa-lock"></i><span>*</span> </label>
                                <input name="password" type="password" required onClick="this.select()" value="" >
                                <button type="submit" id="login-button" class="log-submit-btn color-bg"><span>دخـول</span></button>
                                <div class="clearfix"></div>
                                <div class="filter-tags">
                                    <input id="check-a" type="checkbox" name="remember">
                                    <label for="check-a">تذكرني</label>
                                </div>
                            </form>
                            <div class="lost_password">
                                <a href="{{ url('/password/reset') }}">نسيت كلمة المرور؟</a>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                    <!--tab -->
                    <div class="tab">
                        <div id="tab-2" class="tab-content">
                            <h3>اشترك
                                <span class="logo-style pull-left">
                                    <span>Dak<strong>toori</strong></span>
                                </span>
                            </h3>
                            <p>اذا كنت طبيب ادخل رابط التسجيل من  <a href="/doctors/create">هنا</a></p>
                            <div class="custom-form">
                                <form name="registerform" class="main-register-form" id="register-form" method="POST">
                                    {{ csrf_field() }}
                                    <label>الاسم الأول <i class="fal fa-user"></i><span>*</span></label>
                                    <input name="fname_ar" type="text" onClick="this.select()" value="" required>
                                    <label>اسم الأب <i class="fal fa-user"></i></label>
                                    <input name="sname_ar" type="text" onClick="this.select()" value="">
                                    <label>اللقب <i class="fal fa-user"></i><span>*</span></label>
                                    <input name="lname_ar" type="text" onClick="this.select()" value="" required>
                                    <label>المدينة <i class="fal fa-marker"></i></label>
                                    <div class="listsearch-input-item">
                                        <select data-placeholder="اختر المدينة..." data-display="اختر المدينة..." class="chosen-select" name="city">
                                            @foreach(\App\City::shown()->get() as $city)
                                                <option value="{{ $city->id }}">{{ $city->name_ar }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label>المنطقة <i class="fal fa-marker"></i></label>
                                    <div class="listsearch-input-item">
                                        <select data-placeholder="اختر المنطقة..." data-display="اختر المنطقة..." class="chosen-select" name="region">
                                            @foreach(\App\Region::shown()->get() as $region)
                                                <option value="{{ $region->id }}">{{ $region->name_ar }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label>البريد الالكتروني <i class="fal fa-envelope"></i><span>*</span></label>
                                    <input name="email" type="email" onClick="this.select()" value="" required>
                                    <label>كلمة المرور <i class="fal fa-lock"></i><span>*</span></label>
                                    <input name="password" type="password" onClick="this.select()" value="" required>
                                    <label>تأكيد كلمة المرور <i class="fal fa-lock"></i><span>*</span></label>
                                    <input name="password_confirmation" type="password" onClick="this.select()" value="" required>
                                    <button type="submit" id="register-button"  class="log-submit-btn color-bg"><span>تسجيل</span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                </div>
                <!--tabs end -->
            </div>
        </div>
    </div>
</div>

@push('bottom-stack')
    <script>
        $(function() {
            var flatten = function(a, shallow,r){
                if(!r){ r = []}

                if (shallow) {
                    return r.concat.apply(r,a);
                }

                for(var i=0; i<a.length; i++){
                    if(a[i].constructor == Array){
                        flatten(a[i],shallow,r);
                    }else{
                        r.push(a[i]);
                    }
                }
                return r;
            }
            $loginForm = $('#login-form');
            $loginForm.on('submit', function (e) {
                e.preventDefault();
                var formData = $loginForm.serialize();

                $.ajax({
                    url: '/login',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    success:function(data){
                        location.reload();
                    },
                    error: function (data) {
                        response = data.responseJSON;
                        var arr = response.errors;
                        var string = '';
                        for (var key in arr) {
                            string += arr[key] + '\n'
                        }
                        swal("خطأ", string, "error")
                    }
                });
            });
            $registerForm = $('#register-form');
            $registerForm.on('submit', function (e) {
                e.preventDefault();
                var formData = $registerForm.serialize();

                $.ajax({
                    url: '/register',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    success: function(data){
                        swal("نجاح", 'تم التسجيل بنجاح!', "success");
                        location.reload();
                    },
                    error: function (data) {
                        response = data.responseJSON;
                        var arr = response.errors;
                        var string = '';
                        for (var key in arr) {
                            string += arr[key] + '\n'
                        }
                        swal("خطأ", string, "error")
                    }
                });
            })
            $('#login-form').keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    $loginForm.submit();
                    return false;    //<---- Add this line
                }
            });
            $('#register-form').keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    $registerForm.submit();
                    return false;    //<---- Add this line
                }
            });
        });
    </script>

@endpush