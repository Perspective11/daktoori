<div class="header-top fl-wrap rtl">
    <div class="container">
        <div class="logo-holder">
            <a href="/"><img src="/images/logo.png" alt=""></a>
        </div>
        @if (Auth::guest())
            <div class="modal-open add-doctor">دخـول <i class="fa fa-sign-in"></i></div>
        @endif
        @if (Auth::check())
            <div class="header-user-menu">
                <div class="header-user-name">
                    @if (Auth::user()->isDoctor())
                        <span><img src="{{ Auth::user()->doctor->getPicture() }}" class="img-responsive" alt=""></span>
                    @endif
                    مرحبا، {{ Auth::user()->childOrUserFirstName() }}
                </div>
                <ul>
                    @if (Auth::user()->isDoctor())
                        <li><a href="/profile">بيانات الحساب </a></li>
                        <li><a href="/bookings">حجوزاتي </a></li>
                    @endif
                    @if (Auth::user()->isCustomer())
                        <li><a href="/user/bookings">حجوزاتي </a></li>
                    @endif
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">تسجيل الخروج</a></li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </div>
           
        @endif

    </div>
</div>