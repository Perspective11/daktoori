<section class="hero-section" data-scrollax-parent="true" id="sec1">
    <div class="hero-parallax">
        <div class="slideshow-container" data-scrollax="properties: { translateY: '200px' }" >
            <!-- slideshow-item -->
            <div class="slideshow-item">
                <div class="bg"  data-bg="{{ $images['home_slider_1']->image_path }}"></div>
            </div>
            <!--  slideshow-item end  -->
            <!-- slideshow-item -->
            <div class="slideshow-item">
                <div class="bg"  data-bg="{{ $images['home_slider_2']->image_path }}"></div>
            </div>
            <!--  slideshow-item end  -->
            <!-- slideshow-item -->
            <div class="slideshow-item">
                <div class="bg"  data-bg="{{ $images['home_slider_3']->image_path }}"></div>
            </div>
            <!--  slideshow-item end  -->
        </div>
        <div class="overlay op7"></div>
    </div>
    <div class="hero-section-wrap fl-wrap">
<br>
        <div class="container">
            <div class="home-intro">
                <img src="images/logo2.png"><span></span>

                <div class="section-title-separator"><span></span></div>
                <h2>{{ $contents["home_header_title4"] }} </h2>
                <span class="section-separator"></span>
                <h3>{{ $contents["home_header_subtitle"] }} </h3>
            </div>
            <div class="main-search-input-wrap">
                <form class="main-search-input fl-wrap" method="get" action="/listings">
                    <div class="main-search-input-item location">
                        <span class="inpt_dec"><i class="fal fa-stethoscope"></i></span>
                        <select id="select-major" data-placeholder="التخصص" name="major">
                            <option value=""></option>
                            @foreach($global_majors->where('level', 1) as $major)
                                <option value="{{ $major->name_ar }}">{{ $major->name_ar }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="main-search-input-item" id="main-search-city">
                        <span class="inpt_dec"><i class="fal fa-map-marker-alt"></i></span>
                        <select id="select-city" data-placeholder="المحافظة" name="city">
                            <option value=""></option>
                            @foreach(\App\City::has('offices')->get() as $city)
                                <option value="{{ $city->key }}">{{ $city->name_ar }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="main-search-input-item" id="main-search-region">
                        <span class="inpt_dec"><i class="fal fa-map-marker-alt"></i></span>
                        <select id="select-region" data-placeholder="المنطقة" name="region">
                            <option value=""></option>
                            @foreach(\App\Region::has('offices')->get() as $region)
                                <option value="{{ $region->key }}">{{ $region->name_ar }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="main-search-input-item">
                        <span class="inpt_dec"><i class="fal fa-user-md"></i></span>
                        <input type="text" placeholder="ابحث عن اسم الدكتور" name="doctor" value=""/>
                    </div>
                    <button class="main-search-button color2-bg" type="submit">بـحـث <i class="fal fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
    <div class="header-sec-link">
        <div class="container"><a href="#sec2" class="custom-scroll-link color-bg"><i class="fal fa-angle-double-down"></i></a></div>
    </div>
    <div class="reserve-now" style="top:33px"> {{ $contents["home_header_title1"] }} <br><a href="tel:{{$appSettings['mobile_phone']}}">{{ $appSettings['mobile_phone'] }}</a> {{ $contents["home_header_title2"] }} <a
    href="tel:{{$appSettings['land_line']}}">{{ $appSettings['land_line'] }}</a> {{ $contents["home_header_title3"] }}</div>

    @if (filled($appSettings['notification_text']))
    <div class="col-12 col-sm-12" style="position: absolute;width: -webkit-fill-available; font-weight: 500;top: -7px;z-index: 10;border-radius: 10px 0 0 10px;padding: 7px;font-size:14px;background-color:{{$appSettings['notification_color']}};color: #fff;text-align: center">

        <div class="alert alert-primary" ; role="alert">
              <i class="fal fa-bell"></i>  {{$appSettings['notification_text']}} !
         </div>
    </div>   
     @endif   

</section>