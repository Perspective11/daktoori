<!--footer -->
<footer class="main-footer rtl">
    <!--subscribe-wrap-->
    <div class="subscribe-wrap color-bg  fl-wrap">
        <div class="container">
            <div class="sp-bg"> </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="subscribe-header">
                        <h3>{{ $contents["home_footer_title1"] }}</h3>
                        <p>{{ $contents["home_footer_subtitle1"] }}</p>
                        <h3><a href="tel:{{$appSettings['mobile_phone']}}">{{ $appSettings['mobile_phone'] }}</a> أو <a href="tel:{{$appSettings['land_line']}}">{{ $appSettings['land_line'] }}</a></h3>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="subscribe-header">
                        <div class="footer-widget fl-wrap">
                            <h3>{{ $contents["home_footer_title"] }}</h3>
                            <a href="/about" class="block btn color3-bg float-btn">{{ $contents["home_footer_btn1"] }}<i class="fal fa-exclamation"></i></a>
                            <div class="clearfix"></div>
                            <a href="/faqs" class="block btn color3-bg float-btn">{{ $contents["home_footer_btn2"] }}<i class="fal fa-question"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wave-bg"></div>
    </div>
    <!--subscribe-wrap end -->
    <!--footer-inner-->
    <div class="footer-inner">
        <div class="container">
            <div class="row">
                <!--footer-widget -->
                <div class="col-md-3">
                    <div class="footer-widget fl-wrap">
                        <h3>{{ $contents["home_footer_title5"] }}</h3>
                        <div class="footer-contacts-widget fl-wrap">
                         <p> {!! $contents["home_footer_subtitle5"] !!} </p>
</p>

                        </div>
                    </div>
                </div>
                <!--footer-widget end-->
                <!--footer-widget -->
                <div class="col-md-3">

                    <div class="footer-widget fl-wrap">
                        <h3>{{ $contents["home_footer_title4"] }}</h3>
                        <ul class="footer-contacts fl-wrap">
                            <li><span><i class="fal fa-envelope"></i> </span><a href="mailto:{{$appSettings['email']}}" target="_blank">{{ $appSettings['email'] }}</a></li>
                            <li> <span><i class="fal fa-map-marker-alt"></i> </span><a class="empty-link" href="javascript:void(0);">{{ $appSettings['location'] }}</a></li>
                            <li><span><i class="fal fa-mobile"></i> </span><a class="ltr" href="tel:{{$appSettings['mobile_phone']}}">{{ $appSettings['mobile_phone'] }}</a></li>
                            <li><span><i class="fal fa-phone"></i> </span><a class="ltr" href="tel:{{$appSettings['land_line']}}">{{ $appSettings['land_line'] }}</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!--footer-widget end-->
                <!--footer-widget -->
                <div class="col-md-3">
                    <div class="footer-widget fl-wrap">
                        <h3>{{ $contents["home_footer_title1"] }}</h3>
                        <div class="footer-social">
                            <ul>
                            @foreach (\App\Setting::getLinks() as $link)
                                @if (filled($link->value))
                                <li><a href="{{ $link->value }}" target="_blank"><i class="fab {{ $link->icon }}"></i></a></li>
                                @endif
                            @endforeach
                 
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="footer-whatsapp">
                            <a href="whatsapp://send?text=HelloDaktoori&phone=+967{{$appSettings['whatsaap_phone']}}">
                                <h4>{{ $contents["home_footer_subtitle3"] }}</h4>
                                <i class="fab fa-whatsapp"></i> <span>{{ $appSettings['whatsaap_phone'] }}</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--footer-widget end-->
                <!--footer-widget -->
                <div class="col-md-3">
                    <div class="footer-widget fl-wrap">
                        <h3>{{ $contents["home_footer_title2"] }}</h3>
                        <div class="footer-majors fl-wrap">
                            <ul>
                               <li><a href="/majors">التخصصات</a></li>
                               <li><a href="/services">الخدمات</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--footer-widget end-->
            </div>
            <div class="clearfix"></div>
            <!--footer-widget -->

            <!--footer-widget end -->
        </div>
    </div>
    <!--footer-inner end -->
    <div class="footer-bg">
    </div>
    <!--sub-footer-->
    <div class="sub-footer">
        <div class="container">
            <p class="pull-right otek-p">
                تطوير وتصميم <a target="_blank" href="https://otekit.com/">شركة اوتِك</a>
                <a class="oteklink" target="_blank" href="https://otekit.com/">
                    <img id="oteklogo" src="/images/logo.svg" alt="">
                </a>
            </p>
            <div class="subfooter-nav">
                <ul>
                    <li><a href="/faqs">الأسئلة الأكثر شيوعاً</a></li>
                    <li><a href="/privacy-policy">اتفاقية الخصوصية</a></li>
                    <li><a href="/blog">المدونة</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--sub-footer end -->
</footer>
<!--footer end -->