<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-152943292-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-152943292-1');
</script>
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '196585765279318');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=196585765279318&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name') . ' - ' . 'احجز بسهولة من افضل الدكاترة في اليمن' }}</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="author" content="OteK">
<meta name="description" content="دكتوري دوت كوم. أسهل طريقة للحجز عند افضل الدكاترة في اليمن">
<meta property="og:title" content="دكتوري دوت كوم">
<meta property="og:description" content="دكتوري دوت كوم. أسهل طريقة للحجز عند افضل الدكاترة في اليمن {{ env('APP_URL')}}">
<meta property="og:image" content="{{ env('APP_URL') . '/images/site_intro_ar.jpg'  }}"/>
<meta property="og:image:url" content="{{ env('APP_URL') . '/images/site_intro_ar.jpg'  }}"/>
<meta property="og:image:secure_url" content="{{ env('APP_URL') . '/images/site_intro_ar.jpg'  }}"/>
<meta property="og:image:type" content="image/jpeg"/>
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:title" content="دكتوري دوت كوم">
<meta property="twitter:description" content="دكتوري دوت كوم. أسهل طريقة للحجز عند افضل الدكاترة في اليمن">
<meta property="twitter:image" content="{{ env('APP_URL') . '/images/site_intro_ar.jpg'  }}">

<meta name="language" content="ar">

<!--=============== css  ===============-->
<link type="text/css" rel="stylesheet" href="/css/plugins.css">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">

<link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
<script src="/plugins/sweetalert/sweetalert.min.js"></script>

