<header class="main-header">
    <!-- header-top-->
    @include('partials.nav-top')
    <!-- header-top end-->
    <!-- header-inner-->
    @include('partials.nav-middle')
    <!-- header-inner end-->
    <!-- header-search -->
    @include('partials.nav-search')
    <!-- header-search  end -->
</header>
<!--  header end -->