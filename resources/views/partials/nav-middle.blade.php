<div class="header-inner fl-wrap rtl">
    <div class="container">
        @if (false)
            <div class="show-search-button"><span>بحث</span> <i class="fas fa-search"></i> </div>
        @endif
        <div class="show-search-button" data-toggle="tooltip" title="نسخة تجريبية"><span>نسخة تجريبية</span> <i class="fas fa-code-branch"></i> </div>
        @if (Auth::check() && false)
            <div class="wishlist-link hidden"><i class="fal fa-bell"></i><span class="wl_counter">3</span></div>
        @endif

        <div class="home-btn"><a href="/"><i class="fas fa-home"></i></a></div>
        <!-- nav-button-wrap-->
        <div class="nav-button-wrap color-bg">
            <div class="nav-button">
                <span></span><span></span><span></span>
            </div>
        </div>
        <!-- nav-button-wrap end-->
        <!--  navigation -->
        <div class="nav-holder main-menu">
            <nav>
                <ul>
                    <li>
                        <a href="/listings">الاطباء </a>
                    </li>
                    <li>
                        <a href="/blog">المدونة </a>
                    </li>
                    <li>
                        <a href="#">عنا <i class="fas fa-caret-down"></i></a>
                        <!--second level -->
                        <ul>
                            <li><a href="/about">نبذة عن دكتوري</a></li>
                            <li><a href="/faqs">الأسئلة الأكثر شيوعاً</a></li>
                            {{--<li><a href="/team">فريق دكتوري</a></li>--}}
                        </ul>
                        <!--second level end-->
                    </li>
                </ul>
            </nav>
        </div>
        <!-- navigation  end -->
    </div>
</div>