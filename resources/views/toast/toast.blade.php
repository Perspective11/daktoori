@if (Session::has('toast'))
    @php
        $toast = Session::get('toast');
    @endphp
    <script>
        $(document).ready(function () {
            iziToast.settings({
                rtl: true
            });
            @if(is_array($toast))
            @php
                $message = $toast[0];
                $class = isset($toast[1])? $toast[1] : 'success';
                $title = isset($toast[2])? $toast[2] : ucfirst($class);
                $important = isset($toast[3])? $toast[3] : false;
            @endphp
            iziToast.{{ $class }}({
                title: '{{ $title }}',
                message: '{{ $message }}',
                @if($important)
                    overlay: 'true',
                    position: 'center',
                    titleSize: '24px',
                    messageSize: '22px',
                    titleLineHeight: '2',
                    messageLineHeight: '2',
                @endif
            });
            @else
            iziToast.info({
                title: '@lang('toast.success')',
                message: '{{ $toast }}'
            });
            @endif
        });

    </script>
@endif


