@php
    if (isset($error_message_bag)){
        $errorList = $errors->getBag($error_message_bag);
    }
    else
        $errorList = $errors;
@endphp

@if(count($errorList))
    <div id="errors-list" class="form-group text-body float-wrap">
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach($errorList->all() as $error)
                    <li class="list-group-item">&bull; {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif