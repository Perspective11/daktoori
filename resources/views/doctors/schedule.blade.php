@extends('layouts.app')
@section('content')
    @include('doctors.header')
    <!--  section  end-->
    <section class="middle-padding">
        <div class="container">
            <!--dashboard-wrap-->
            <div class="dashboard-wrap">
                <!-- dashboard-content-->
                <form id="schedule-form" action="/schedule" method="post" class="dashboard-content fl-wrap rtl rtl-col">
                    {{ csrf_field() }}
                    <div class="box-widget-item-header">
                        <h3> الجدول الاسبوعي</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container">
                        <div class="custom-form">
                            @foreach(\App\Day::all() as $day)
                                @php
                                    $dayChecked = '';
                                    $eveningChecked = '';
                                    foreach ($officeTimes as $officeTime){

                                        if ($officeTime->day_id === $day->id && $officeTime->time_id === $times['day']){
                                            $dayChecked = 'checked';
                                        }
                                        if ($officeTime->day_id === $day->id && $officeTime->time_id === $times['evening']){
                                            $eveningChecked = 'checked';
                                        }
                                    }


                                @endphp
                                <div class="checkbox-ul">
                                    <label>{{ $day->name_ar }} </label>
                                    <div class="clearfix"></div>
                                    <!--col -->
                                    <!-- Checkboxes -->
                                    <ul class="filter-tags ">
                                        <li>
                                            <input id="day{{ $day->id }}" type="checkbox" value="1" name="days[{{ $day->id }}]" {{ $dayChecked }}>
                                            <label for="day{{ $day->id }}">صباح</label>
                                        </li>
                                        <li>
                                            <input id="evening{{ $day->id }}" type="checkbox" value="1" name="evenings[{{ $day->id }}]" {{ $eveningChecked }}>
                                            <label for="evening{{ $day->id }}">مساء</label>
                                        </li>
                                    </ul>
                                    <!-- Checkboxes end -->
                                    <!--col end-->
                                </div>

                            @endforeach
                        </div>
                    </div>

                    @include('errors.errors')

                    <button type="submit" class="btn color2-bg float-btn">ادراج<i class="fal fa-calendar-check"></i></button>
                </form>
                <!-- dashboard-list-box end-->
            </div>
            <!-- dashboard-wrap end-->
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection

@section('bottom-ex')
    <script>
        $(function() {
            $scheduleForm = $('#schedule-form');
            $scheduleForm.on('submit', function (e) {
                e.preventDefault();
                var formData = $scheduleForm.serialize();

                $.ajax({
                    url: '/schedule',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    success:function(data){
                        swal("نجاح!", 'تم تعديل الجدول بنجاح', "success")
                    },
                    error: function (data) {
                        response = data.responseJSON;
                        var arr = response.errors;
                        var string = '';
                        for (var key in arr) {
                            string += arr[key] + '\n'
                        }
                        swal("خطأ", string, "error")
                    }
                });
            });
        });
    </script>

@endsection