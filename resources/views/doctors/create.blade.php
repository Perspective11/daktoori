@extends('layouts.app')
@section('top-ex')

@endsection
@section('content')
    <!--  section  -->
    <section class="parallax-section single-par" data-scrollax-parent="true">
        <div class="bg par-elem "  data-bg="/images/bg/5.jpg" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="section-title center-align big-title">
                <div class="section-title-separator"><span></span></div>
                <h2><span>انضم الى طاقم أطبائنا</span></h2>
                <span class="section-separator"></span>
                <h4>ادخل معلوماتك بدقة ومصداقية وسوف يتم مراجعة البيانات والموافقه على تفعيل حسابك.</h4>
            </div>
        </div>
        <div class="header-sec-link">
            <div class="container"><a href="#sec1" class="custom-scroll-link color-bg"><i class="fal fa-angle-double-down"></i></a></div>
        </div>
    </section>
    <!--  section  end-->
    <section class="middle-padding">
        <div class="container">
            <!--dashboard-wrap-->
            <div class="col-md-8 col-md-offset-2">
                <!-- dashboard-content-->
                <form action="/doctors/create" method="post" class="dashboard-content fl-wrap rtl rtl-col">
                    {{ csrf_field() }}
                    <input type="hidden" name="temp" value="{{ $temp_token }}">
                    <div class="box-widget-item-header">
                        <h3> معلومات الحساب</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container">
                        <div class="custom-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>اسم المستخدم <i class="fa fa-user-circle"></i></label>
                                    <input class="{{  $errors->has('username') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('username') }}" name="username"/>
                                </div>
                                <div class="col-md-6">
                                    <label>البريد الالكتروني <i class="fa fa-envelope"></i></label>
                                    <input class="{{  $errors->has('email') ? ' has-error' : ''  }}"
                                           type="email" value="{{ old('email') }}" name="email"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>كلمة المرور <i class="fa fa-lock"></i></label>
                                    <input class="{{  $errors->has('password') ? ' has-error' : ''  }}"
                                           type="password" value="" name="password"/>
                                </div>
                                <div class="col-md-6">
                                    <label>تأكيد كلمة المرور <i class="fa fa-lock"></i></label>
                                    <input class="{{  $errors->has('password_confirmation') ? ' has-error' : ''  }}"
                                           type="password" value="" name="password_confirmation"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-widget-item-header">
                        <h3> المعلومات الاساسية</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container">
                        <div class="custom-form">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>الأسم <i class="fa fa-user"></i></label>
                                    <input class="{{  $errors->has('fname_ar') ? ' has-error' : ''  }}
                                            " type="text" value="{{ old('fname_ar') }}" name="fname_ar"/>
                                </div>
                                <div class="col-md-3">
                                    <label>اسم الأب <i class="fa fa-user"></i></label>
                                    <input class="{{  $errors->has('sname_ar') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('sname_ar') }}" name="sname_ar"/>
                                </div>
                                <div class="col-md-3">
                                    <label>اللقب <i class="fa fa-user"></i></label>
                                    <input class="{{  $errors->has('lname_ar') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('lname_ar') }}" name="lname_ar"/>
                                </div>
                                <div class="col-md-3">
                                    <label>بادئة الاسم <i class="fa fa-user"></i></label>
                                    <input class="{{  $errors->has('prefix') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('prefix') }}" name="prefix"/>
                                </div>
                            </div>
                            <label>العنوان الوظيفي <i class="fal fa-medkit"></i></label>
                            <input class="{{  $errors->has('title_ar') ? ' has-error' : ''  }}"
                                   placeholder="العنوان الوظيفي للطبيب" type="text" name="title_ar" value="{{ old('title_ar') }}"/>
                            <label>اسم المكتب <i class="fal fa-hospital"></i></label>
                            <input class="{{  $errors->has('office_name') ? ' has-error' : ''  }}"
                                   placeholder="اسم المكتب او العيادة" type="text" name="office_name" value="{{ old('office_name') }}"/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>تاريخ الميلاد <i class="fa fa-birthday-cake"></i></label>
                                    <input placeholder="اختر التاريخ"
                                           type="text"
                                           value="{{ old('dob') }}"
                                           name="dob"
                                           id="dob"
                                           class="{{  $errors->has('dob') ? ' has-error' : ''  }} form-control date"
                                           data-date-end-date="-18y"
                                           data-date-start-date="-100y">
                                </div>
                                <div class="col-md-6">
                                    <label>الجنس <i class="fa fa-venus-mars"></i></label>
                                    <!--col -->
                                    <div class="{{  $errors->has('gender') ? ' has-error' : ''  }} add-list-media-header radio-container">
                                        @php
                                            $checked = '';
                                            if (old('gender')){
                                                $checked = old('gender');
                                            }
                                        @endphp
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="radio inline">
                                                    <input type="radio" name="gender" value="1" {{ $checked === '1'? 'checked' : ''  }}>
                                                    <span>ذكر</span>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="radio inline">
                                                    <input type="radio" name="gender" value="2" {{ $checked === '2'? 'checked' : ''  }}>
                                                    <span>انثى</span>
                                                </label>
                                            </div>
                                        </div>
                                        <!--col end-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- profile-edit-container end-->
                    <div class="box-widget-item-header">
                        <h3> معلومات التواصل</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container">
                        <div class="custom-form">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>رقم المحمول <i class="fa fa-mobile-alt"></i></label>
                                    <input class="{{  $errors->has('mobile') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('mobile') }}" name="mobile"/>
                                </div>
                                <div class="col-md-4">
                                    <label>رقم الواتس اب <i class="fab fa-whatsapp"></i></label>
                                    <input class="{{  $errors->has('whatsapp') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('whatsapp') }}" name="whatsapp"/>
                                </div>
                                <div class="col-md-4">
                                    <label>رقم الهاتف الارضي <i class="fa fa-phone"></i></label>
                                    <input class="{{  $errors->has('phone') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('phone') }}" name="phone"/>
                                </div>
                            </div>
                            <label>العنوان <i class="fal fa-map-marker"></i></label>
                            <input class="{{  $errors->has('address_ar') ? ' has-error' : ''  }}"
                                   type="text" value="{{ old('address_ar') }}" name="address_ar"/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>المدينة</label>
                                    <div class="listsearch-input-item">
                                        <select data-placeholder="اختر المدينة..." data-display="اختر المدينة..." class="{{  $errors->has('city') ? ' has-error' : ''  }} chosen-select" name="city">
                                            @foreach(\App\City::shown()->get() as $city)
                                                @php
                                                    $selected = '';
                                                    if (old('city')){
                                                        if (old('city') == $city->id)
                                                        $selected = true;
                                                    }
                                                    else $selected = false;
                                                @endphp
                                                <option value="{{ $city->id }}" {{ $selected? 'selected' : '' }}>{{ $city->name_ar }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>المنطقة</label>
                                    <div class="listsearch-input-item">
                                        <select data-placeholder="اختر المنطقة..." data-display="اختر المنطقة..." class="{{  $errors->has('region') ? ' has-error' : ''  }} chosen-select" name="region">
                                            @foreach(\App\Region::shown()->get() as $region)
                                                @php
                                                    $selected = '';
                                                    if (old('region')){
                                                        if (old('region') == $region->id)
                                                        $selected = true;
                                                    }
                                                    else $selected = false;
                                                @endphp
                                                <option value="{{ $region->id }}" {{ $selected? 'selected' : '' }}>{{ $region->name_ar }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="box-widget-item-header mat-top">
                        <h3>معلومات التعليم</h3>
                    </div>
                    <div class="profile-edit-container">
                        <div class="custom-form">
                            <div class="row">
                                <div class="col-md-5">
                                    <label>جامعة التخرج <i class="fa fa-university"></i></label>
                                    <input class="{{  $errors->has('grad_university') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('grad_university') }}" name="grad_university"/>
                                </div>
                                <div class="col-md-4">
                                    <label>دولة التخرج <i class="fas fa-globe-americas"></i></label>
                                    <input class="{{  $errors->has('grad_country') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('grad_country') }}" name="grad_country"/>
                                </div>
                                <div class="col-md-3">
                                    <label>سنة التخرج <i class="fa fa-user-graduate"></i></label>
                                    <input class="{{  $errors->has('grad_year') ? ' has-error' : ''  }}"
                                           type="text" value="{{ old('grad_year') }}" name="grad_year"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-widget-item-header mat-top">
                        <h3>تفاصيل اخرى</h3>
                    </div>
                    <!-- profile-edit-container-->
                    <div class="profile-edit-container">
                        <div class="custom-form">
                            <label>التخصص الرئيسي</label>
                            <div class="listsearch-input-item">
                                <select data-placeholder="اختر التخصص..." data-display="اختر التخصص..." class="{{  $errors->has('major') ? ' has-error' : ''  }} chosen-select" name="major">
                                    <option>اختر التخصص</option>
                                    @foreach($majors as $major)
                                        @php
                                            $selected = '';
                                            if (old('major')){
                                                if (old('major') == $major->id)
                                                $selected = true;
                                            }
                                            else $selected = false;
                                        @endphp
                                        <option value="{{ $major->id }}" {{ $selected? 'selected' : '' }}>
                                            {{ $major->name_en . ' - ' . $major->name_ar }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="secondaryMajors">التخصصات <i class="fal fa-stethoscope"></i></label>
                            <select class="{{  $errors->has('secondaryMajors') ? ' has-error' : ''  }} select-secondary-majors form-control" name="secondaryMajors[]" multiple="multiple">
                                @foreach($secondaryMajors as $secondaryMajor)
                                    @php
                                        $selected = '';
                                        if (old('secondaryMajors')){
                                            if (in_array($secondaryMajor->id, old('secondaryMajors')))
                                            $selected = true;
                                        }
                                        else $selected = false;
                                    @endphp
                                    <option {{ $selected ? 'selected': ''}} value="{{ $secondaryMajor->id }}">{{ $secondaryMajor->name_ar }} </option>
                                @endforeach
                            </select>
                            <label for="services">الخدمات الموفرة <i class="fal fa-band-aid"></i></label>
                            <select class="{{  $errors->has('services') ? ' has-error' : ''  }} select-services form-control" name="services[]" multiple="multiple">
                                @foreach($services as $service)
                                    @php
                                        $selected = '';
                                        if (old('services')){
                                            if (in_array($service->id, old('services')))
                                            $selected = true;
                                        }
                                        else $selected = false;
                                    @endphp
                                    <option {{ $selected ? 'selected': ''}} value="{{ $service->id }}">{{ $service->name_ar }} </option>
                                @endforeach
                            </select>

                            <label for="insurances">التأمينات <i class="fal fa-shield"></i></label>
                            <select class="{{  $errors->has('insurances') ? ' has-error' : ''  }} select-insurances form-control" name="insurances[]" multiple="multiple">
                                @foreach($insurances as $insurance)
                                    @php
                                        $selected = '';
                                        if (old('insurances')){
                                            if (in_array($insurance->id, old('insurances')))
                                            $selected = true;
                                        }
                                        else $selected = false;
                                    @endphp
                                    <option {{ $selected ? 'selected': ''}} value="{{ $insurance->id }}">{{ $insurance->name_ar }} </option>
                                @endforeach
                            </select>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>سعر الكشف بالريال <i class="fal fa-dollar-sign"></i></label>
                                    <input class="{{  $errors->has('fees') ? ' has-error' : ''  }}"
                                           type="text" placeholder="" name="fees" value="{{ old('fees') }}"/>
                                </div>
                                <div class="col-md-6">
                                    <label>معدل وقت الانتظار بالدقائق <i class="fal fa-clock"></i></label>
                                    <input class="{{  $errors->has('average_waiting_time') ? ' has-error' : ''  }}"
                                           type="text" placeholder="" name="average_waiting_time" value="{{ old('average_waiting_time') }}"/>
                                </div>
                            </div>
                            <label>وصف الدكتور <i class="fal fa-list-ul"></i></label>
                            <textarea class="{{  $errors->has('about_ar') ? ' has-error' : ''  }}"
                                      cols="40" rows="3" name="about_ar">{{ old('about_ar') }}</textarea>
                            
                            <label>ملاحظات <i class="fal fa-sticky-note"></i></label>
                            <textarea class="{{  $errors->has('notes_ar') ? ' has-error' : ''  }}"
                                      cols="40" rows="3" name="notes_ar">{{ old('notes_ar') }}</textarea>
                        </div>
                    </div>
                    <!-- profile-edit-container end-->
                    <div class="box-widget-item-header mat-top">
                        <h3>صورة شخصية رسمية</h3>
                    </div>
                    <div class="profile-edit-container">
                        <div class="custom-form">
                            @include('doctors.upload-store')
                        </div>
                    </div>
                    <!-- profile-edit-container end-->
                    <div class="box-widget-item-header mat-top">
                        <h3>الموافقة على الشروط والأحكام</h3>
                    </div>
                    <div class="profile-edit-container">
                        <div class="custom-form">
                            <div class="agreement">
                                <div class="agreement-text">
                                    {!! $contents["doctor_agreement"]["value_ar"] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('errors.errors')
                    <p>&bull; بالضغط على زر التسجيل. أوافق على جميع الشروط والأحكام المذكورة أعلاه.</p>
                    <p>&bull; سوف يتم مراجعة ملفك قبل قبولك في المشاركة في الموقع.</p>
                    <button type="submit" class="btn color2-bg block">تسجيل<i class="fal fa-user-plus"></i></button>
                </form>
                <!-- dashboard-list-box end-->
            </div>
            <!-- dashboard-wrap end-->
        </div>
    </section>
@endsection
@section('bottom-ex')
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
    <script type="text/javascript" src="/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        $(function() {
            $('.date').datepicker({
                format: 'yyyy-mm-dd',
                startView: 'decade'
            });
            $(".select-secondary-majors").select2({
                placeholder: "اختر التخصصات الفرعية",
                allowClear: true,
                maximumSelectionLength: 5,
                width: '100%',
                dir: "rtl"
            });
            $(".select-insurances").select2({
                placeholder: "اختر شركات التأمين المعتمدة لديك",
                allowClear: true,
                maximumSelectionLength: 20,
                width: '100%',
                dir: "rtl"
            });
            $(".select-services").select2({
                placeholder: "اختر الخدمات التي توفرها عيادتك",
                allowClear: true,
                maximumSelectionLength: 20,
                width: '100%',
                dir: "rtl"
            });
        });
    </script>
@endsection