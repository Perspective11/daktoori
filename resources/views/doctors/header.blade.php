<!-- section-->
<section class="flat-header color-bg adm-header">
    <div class="wave-bg wave-bg2"></div>
    <div class="container">
        <div class="dashboard-wrap fl-wrap">
            <!--dashboard-sidebar-->
            <div class="dashboard-sidebar rtl">
                <div class="dashboard-sidebar-content fl-wrap">
                    <div class="dashboard-avatar">
                        <img src="{{ $doctor->getPicture() }}" alt="">
                    </div>
                    <div class="dashboard-sidebar-item fl-wrap">
                        <h3>
                            <span>مرحبا </span>
                            {{ $doctor->getName() }}
                        </h3>
                    </div>
                    <a href="/schedule" class="ed-btn">ضبط المواعيد</a>
                    <div class="user-stats fl-wrap">
                        <ul>
                            <li>
                                الكشوفات
                                <span>4</span>
                            </li>
                            <li>
                                الحجوزات
                                <span>{{ $office->bookings()->count() }}</span>
                            </li>
                            <li>
                                المرضى
                                <span>9</span>
                            </li>
                        </ul>
                    </div>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="log-out-btn color-bg">تسجيل الخروج <i class="far fa-sign-out"></i></a>
                </div>
            </div>
            <!--dashboard-sidebar end-->
            <!-- dashboard-menu-->
            <div class="dashboard-menu">
                <div class="dashboard-menu-btn color3-bg">القائمة <i class="fal fa-bars"></i></div>
                <ul class="dashboard-menu-wrap">
                    @php
                        $path = request()->getPathInfo();
                    @endphp
                    @if (false)
                        <li>
                            <a href="/dashboard" class="{{ MyHelper::urlActive('/dashboard', $path)}}"><i class="far fa-tachometer-alt"></i>الرئيسية</a>
                        </li>
                    @endif

                    <li>
                        <a href="/profile" class="{{ MyHelper::urlActive('/profile', $path)}}"><i class="far fa-user"></i>البروفايل</a>
                    </li>
                    <li>
                        <a href="/bookings" class="{{ MyHelper::urlActive('/bookings', $path)}}"><i class="far fa-calendar-check"></i> الحجوزات <span>{{ $office->bookings()->count() }}</span></a>
                    </li>
                    <li>
                        <a href="/schedule" class="{{ MyHelper::urlActive('/schedule', $path)}}"><i class="far fa-calendar"></i> مواعيدي  </a>
                    </li>
                </ul>
            </div>
            <!--dashboard-menu end-->
            @if (! $doctor->is_approved)
                <div class="approval">لم يتم قبولك للمشاركة في الموقع بعد.</div>
            @endif
        </div>
    </div>
</section>
<!-- section end-->