@extends('layouts.app')
@section('content')
    <!--  section  -->
    <section class="color-bg no-padding">
        <div class="wave-bg wave-bg2"></div>
        <div class="container">
            <div class="flat-title-wrap text-body-center">
                <h2><span>{{ $doctor->getPrefix() }}: <strong>{{ $doctor->getName() }}</strong></span></h2>
                <span class="section-separator"></span>
                <h4>{{ $doctor->title_ar }}</h4>
            </div>
            @if ($customer_can_rate)
                <button type="button" id="review-button" data-doctor-id="{{ $doctor->id }}" class="btn rate-button color2-bg float-btn">قيّم الدكتور<i class="fa fa-star-half-alt"></i></button>
            @endif
        </div>
    </section>
    <section  id="sec1" class="middle-padding grey-blue-bg">
        <div class="container">
            <div class="row rtl-col">
                <div class="list-single-main-item fl-wrap visible-sm visible-xs">
                    @if (Session::has('confirmed'))
                        @php
                            $confirmed = Session::get('confirmed');
                        @endphp
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>لقد قمت بالحجز</h3>
                        </div>
                        <h3>سيتم التواصل معك لتأكيد الحجز:</h3>
                        <br>
                        <div class="box-widget-list">
                            <ul>
                                <li>
                                    <span> اليوم :</span>
                                    <a class="empty-link" href="javascript:void(0);">{{ $confirmed["day"] }}</a>
                                </li>
                                <li>
                                    <span> الوقت :</span>
                                    <a class="empty-link" href="javascript:void(0);">{{ $confirmed["time"] }}</a>
                                </li>
                                <li>
                                    <span> التاريخ :</span>
                                    <a class="empty-link" href="javascript:void(0);">{{ $confirmed["date"] }}</a>
                                </li>
                            </ul>
                        </div>
                    @else
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>اوقات الحجز   <p class="text-muted small" style="display: inline; font-size: 10px">اضغط على الحجز المناسب لك</p></h3>
                        </div>
                        @if ($office->officeTimes()->count())

                            <div class="timetable-container">
                                @foreach ($sortedDays as $day)
                                    @php
                                        $dayHidden = 'hidden';
                                        $morningHidden = 'hidden';
                                        $eveningHidden = 'hidden';
                                        foreach ($officeTimes as $officeTime){
                                            if ($officeTime->day_id == $day->id){
                                                $dayHidden = '';
                                                if ($officeTime->time_id == $times['day']){
                                                $morningHidden = '';
                                                }
                                                if ($officeTime->time_id == $times['evening']){
                                                    $eveningHidden = '';
                                                }
                                            }
                                        }
                                    @endphp
                                    <div class="day {{ $dayHidden }}">
                                        <label>{{ $day->string }}</label>
                                        <a class="button-time button-day {{ $morningHidden }}" data-office="{{ $office->id }}" data-day="{{ $day->id }}" data-time="{{ $times['day'] }}" data-date="{{ $day->date }}" data-toggle="tooltip" data-placement="bottom" title="انقر للحجز">صباح</a>
                                        <a class="button-time button-evening {{ $eveningHidden }}" data-office="{{ $office->id }}" data-day="{{ $day->id }}" data-time="{{ $times['evening'] }}" data-date="{{ $day->date }}" data-toggle="tooltip" data-placement="bottom" title="انقر للحجز">مساء</a>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <h3>لا توجد اوقات حجز لهذا الطبيب</h3>
                        @endif
                    @endif


                </div>
                <div class="col-md-4">
                    <!-- box-widget-item-->
                    <div class="box-widget-item fl-wrap rtl">
                        <div class="box-widget">
                            <div class="box-widget-content">
                                <div class="box-widget-item-header">
                                    <h3>{{ $doctor->getPrefix() }}</h3>
                                    @if ($doctor->getRatingsCount() >= \App\Review::$REVIEW_THRESHOLD)
                                        <div class="doctor-star-rating star-rating" title="{{ $doctor->getRating(true) }}%">
                                            <div class="back-stars">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>

                                                <div class="front-stars" style="width: {{ $doctor->getRating(true) }}%">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p>{{ $doctor->getRatingsCount() }} تقييمات</p>
                                        </div>
                                    @endif
                                </div>
                                <div class="box-widget-author fl-wrap">
                                    <div class="box-widget-author-title fl-wrap">
                                        <div class="box-widget-author-title-img">
                                            <img src="{{ $doctor->getPicture() }}" alt="">
                                        </div>
                                        <a href="{{ $doctor->path() }}">{{ $doctor->getName() }}</a>
                                        <span>{{ $doctor->title_ar }}</span>
                                    </div>
                                </div>
                                <div class="box-widget-list">
                                    <ul>
                                        <li>
                                            <span><i class="fal fa-stethoscope"></i> التخصص :</span>
                                            <a class="empty-link" href="javascript:void(0);">{{ $doctor->majorsString() }}</a>
                                        </li>
                                        @if ($office->name_ar)
                                            <li>
                                                <span><i class="fal fa-hospital-alt"></i> المكتب :</span>
                                                <a class="empty-link" href="javascript:void(0);">{{ $office->name_ar }}</a>
                                            </li>
                                        @endif
                                        @if ($doctor->region)
                                            <li>
                                                <span><i class="fal fa-map-marker-alt"></i> المنطقة :</span>
                                                <a class="empty-link" href="javascript:void(0);">{{ $office->region->name_ar }} - {{ $office->city->name_ar }}</a>
                                            </li>
                                        @endif
                                        @if ($doctor->mobile)
                                            <li>
                                                <span><i class="fal fa-mobile-alt"></i> المحمول :</span>
                                                <a class="empty-link" href="tel:+967771366000">771366000</a>
                                            </li>
                                        @endif
                                        @if ($office->phone)
                                            <li>
                                                <span><i class="fal fa-phone"></i> الهاتف :</span>
                                                <a class="empty-link" href="tel:01441702">01441702</a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- box-widget-item end-->
                    <!-- box-widget-item-->
                    <div class="box-widget-item fl-wrap rtl">
                        <div class="box-widget">
                            <div class="box-widget-content">
                                <div class="box-widget-item-header">
                                    <h3>خدمات العيادة</h3>
                                </div>
                                <div class="services-links rtl-right">
                                    @foreach ($office->services as $service)
                                        <a href="/listings?service={{ $service->name_ar }}" class="service-link">{{ $service->name_ar }}</a>
                                    @endforeach
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- box-widget-item end-->
                </div>
                <div class="col-md-8 hidden-sm hidden-xs">
                    <div class="list-single-main-item fl-wrap">
                        @if (Session::has('confirmed'))
                            @php
                                $confirmed = Session::get('confirmed');
                            @endphp
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>لقد قمت بالحجز</h3>
                            </div>
                            <h3>سيتم التواصل معك لتأكيد الحجز:</h3>
                            <br>
                            <div class="box-widget-list">
                                <ul>
                                    <li>
                                        <span> اليوم :</span>
                                        <a class="empty-link" href="javascript:void(0);">{{ $confirmed["day"] }}</a>
                                    </li>
                                    <li>
                                        <span> الوقت :</span>
                                        <a class="empty-link" href="javascript:void(0);">{{ $confirmed["time"] }}</a>
                                    </li>
                                    <li>
                                        <span> التاريخ :</span>
                                        <a class="empty-link" href="javascript:void(0);">{{ $confirmed["date"] }}</a>
                                    </li>
                                </ul>
                            </div>
                        @else
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>اوقات الحجز   <p class="text-muted small" style="display: inline; font-size: 10px">اضغط على الحجز المناسب لك</p></h3>
                            </div>
                            @if ($office->officeTimes()->count())

                                <div class="timetable-container">
                                    @foreach ($sortedDays as $day)
                                        @php
                                            $dayHidden = 'hidden';
                                            $morningHidden = 'hidden';
                                            $eveningHidden = 'hidden';
                                            foreach ($officeTimes as $officeTime){
                                                if ($officeTime->day_id == $day->id){
                                                    $dayHidden = '';
                                                    if ($officeTime->time_id == $times['day']){
                                                    $morningHidden = '';
                                                    }
                                                    if ($officeTime->time_id == $times['evening']){
                                                        $eveningHidden = '';
                                                    }
                                                }
                                            }
                                        @endphp
                                        <div class="day {{ $dayHidden }}">
                                            <label>{{ $day->string }}</label>
                                            <a class="button-time button-day {{ $morningHidden }}" data-office="{{ $office->id }}" data-day="{{ $day->id }}" data-time="{{ $times['day'] }}" data-date="{{ $day->date }}" data-toggle="tooltip" data-placement="bottom" title="انقر للحجز">صباح</a>
                                            <a class="button-time button-evening {{ $eveningHidden }}" data-office="{{ $office->id }}" data-day="{{ $day->id }}" data-time="{{ $times['evening'] }}" data-date="{{ $day->date }}" data-toggle="tooltip" data-placement="bottom" title="انقر للحجز">مساء</a>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                <h3>لا توجد اوقات حجز لهذا الطبيب</h3>
                            @endif
                        @endif


                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <!-- box-widget-item-->
                            <div class="box-widget-item fl-wrap rtl">
                                <div class="box-widget">
                                    <div class="box-widget-content">
                                        <div class="box-widget-item-header">
                                            <h3>معلومات الحجز</h3>
                                        </div>
                                        <div class="box-widget-list">
                                            <ul>
                                                @if ($doctor->average_waiting_time)
                                                    <li>
                                                        <span><i class="fal fa-clock"></i> معدل الانتظار :</span>
                                                        <a class="empty-link" href="javascript:void(0);">{{ $doctor->average_waiting_time }} دقائق</a>
                                                    </li>
                                                @endif
                                                @if ($office->fees)
                                                    <li>
                                                        <span><i class="fal fa-dollar-sign"></i> سعر الكشف :</span>
                                                        <a class="empty-link" href="javascript:void(0);">{{ $office->fees }} ريال</a>
                                                    </li>
                                                @endif
                                                <li>
                                                    <span><i class="fal fa-user-check"></i> متواجد :</span>
                                                    <a class="empty-link" href="javascript:void(0);">{{ $doctor->is_available ? 'نعم' : 'لا' }}</a>
                                                </li>
                                                @if ($office->insurances()->count())
                                                    <li>
                                                        <span><i class="fal fa-shield"></i> شركات التأمين :</span>
                                                        <a class="empty-link" href="javascript:void(0);">{{ $office->insurancesString() }}</a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- box-widget-item end-->
                        </div>
                        @if ($doctor->about_ar)
                            <div class="col-md-6">
                                <div class="box-widget-item fl-wrap rtl">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3>عن الدكتور</h3>
                                            </div>
                                            <div class="rtl-right">
                                                {{ $doctor->about_ar }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    @if ($doctor->getRatingsCountHasComment())
                        <!-- list-single-main-item -->
                            <div class="list-single-main-item fl-wrap" id="sec5">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>تقييم الدكتور - <span> {{ $doctor->getRatingsCountHasComment() }} تعليقات</span></h3>
                                </div>
                                <div class="reviews-comments-wrap">
                                @foreach ($reviews as $review)
                                    <!-- reviews-comments-item -->
                                        <div class="reviews-comments-item">
                                            <div class="reviews-comments-item-text">
                                                <h4><a class="empty-link" href="javascript:void(0);">{{ $review->customer? $review->customer->getName() : $review->patient_name }}</a></h4>
                                                <div class="review-score-user star-rating" title="{{ $review->getRating(true) }}%">
                                                    <div class="back-stars">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>

                                                        <div class="front-stars" style="width: {{ $review->getRating(true) }}%">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <p>{{ $review->review }}</p>
                                                <div class="reviews-comments-item-date rtl"><span><i class="far fa-calendar-check"></i>{{ $review->created_at->diffForHumans() }}</span></div>
                                            </div>
                                        </div>
                                        <!--reviews-comments-item end-->
                                    @endforeach

                                </div>
                            </div>
                            <!-- list-single-main-item end -->
                    @endif

                </div>
            </div>
        </div>
        <div class="section-decor"></div>
    </section>
    <!-- section end -->
@endsection
@section('bottom-ex')
    <script>
        $(function() {
            $buttonTime = $('.button-time');
            $buttonTime.on('click', function (e) {
                e.preventDefault();
                buttonData = $(this)[0].dataset;
                $form = $('<form />', {action: '/reservation', method: 'GET' }).append(
                    $('<input />', { name: 'office', value: buttonData.office} ),
                    $('<input />', { name: 'day', value: buttonData.day} ),
                    $('<input />', { name: 'time', value: buttonData.time} ),
                    $('<input />', { name: 'date', value: buttonData.date} ),
                );
                $('body').append($form);
                $form.submit();
            });
        });
    </script>

@endsection