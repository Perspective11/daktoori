
    <div id="uploadDoctorImages" class="uploadDoctorImages"></div>

@push('bottom-stack')
    <link href="/plugins/fine-uploader/fine-uploader-new.min.css" rel="stylesheet">
    <script src="/plugins/fine-uploader/fine-uploader.min.js"></script>
    @include('partials.fineuploader-template')
    <script type="text/javascript">
        var uploadDoctorImages = new qq.FineUploader({
            element: document.getElementById('uploadDoctorImages'),
            debug: {{ env('APP_DEBUG') ? 'true' : 'false'  }},
            request: {
                inputName: 'image',
                endpoint: '{{ url('/doctors/uploadImagesStore') }}',
                customHeaders: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                params: {
                    temp: '{{ isset($temp_token)? $temp_token : "" }}'
                }
            },
            deleteFile: {
                enabled: true,
                endpoint: '{{ url('/doctors/deleteImage') }}',
                customHeaders: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                method: 'POST',
                params: {
                    temp: '{{ isset($temp_token)? $temp_token : "" }}',
                }
            },
            multiple: true,
            showMessage: function(message) { swal(message); },
            validation: {
                acceptFiles: 'image/jpeg, image/png',
                sizeLimit: 2000000,
                allowedExtensions: ['jpg', 'jpeg', 'png'],
                itemLimit: {{ \App\Doctor::$image_limit }}
            },
            callbacks: {
                onError: function(id, name, errorReason, response) {
                    var obj = JSON.parse(response['response']);
                    var errorList = obj;
                    if (typeof(errorList) === 'object'){
                        var errorString = errorList.message;
                    }
                    else{
                        var errorString = qq.format("Error on file {}.\n\n{}", name, errorList);
                    }
                    swal("Failed!", errorString , "error");
                },
                onComplete: function(id, name, responseJSON, xhr){
                    if(responseJSON['success'])
                        swal(responseJSON['message'], '', responseJSON['class']);
                },
                onDeleteComplete: function(id, xhr, isError) {
                }
            }

        });

    </script>
@endpush
