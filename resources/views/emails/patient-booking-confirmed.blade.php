@component('mail::message')
# مرحباً
تم تأكيد حجزك مع {{ $doctor->getPrefix() . ' ' . $doctor->getName() }}.
رقم الحجز هو {{ $booking->id }}

مع خالص الشكر،<br>
فريق دكتوري
@endcomponent
