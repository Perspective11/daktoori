@component('mail::message')
# مرحباً
المريض {{ $booking->patient_name }} قام بحجز موعد مع {{ $doctor->getPrefix() . ' ' . $doctor->getName()}}. الحجز بانتظار موافقة فريق دكتوري

<br>
# تفاصيل الحجز
- <strong>رقم الحجز:</strong> &nbsp;   &nbsp;{{ $booking->id }}
- <strong>{{ $doctor->getPrefix() }}:</strong> &nbsp;   &nbsp;{{  $doctor->getName()}}
- <strong>المريض:</strong> &nbsp;   &nbsp;{{ $booking->patient_name }}
- <strong>الموعد:</strong> &nbsp;   &nbsp;{{ $time->name_ar . ' - ' .  $booking->appointment_date->format('Y/m/d') }}
- <strong>هاتف الدكتور:</strong> &nbsp;   &nbsp;{{ $doctor->mobile }}
- <strong>الهاتف الأرضي:</strong> &nbsp;   &nbsp;{{ $booking->office->phone }}
- <strong>هاتف المريض:</strong> &nbsp;   &nbsp;{{ $booking->mobile }}
- <strong>وقت وتاريخ السجل:</strong> &nbsp;   &nbsp;{{  $booking->created_at->toDateTimeString()}}


@component('mail::button', ['url' => env('APP_URL') . '/admin/bookings'])
الذهاب الى لوحة التحكم
@endcomponent

@endcomponent
