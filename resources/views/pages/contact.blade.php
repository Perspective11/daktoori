@extends('layouts.app')
@section('content')
    <!-- map-view-wrap -->
    <div class="map-view-wrap">
        <div class="container">
            <div class="map-view-wrap_item">
                <div class="list-single-main-item-title fl-wrap">
                    <h3>تواصل معنا</h3>
                </div>
                <div class="box-widget-list mar-top">
                    <ul>
                        <li>
                            <span><i class="fal fa-map-marker"></i> </span> <a class="empty-link" href="javascript:void(0);">حده - عمارة النزيلي</a>
                        </li>
                        <li>
                            <span><i class="fal fa-phone"></i> </span> <a class="" href="tel:+967771366000">+967 771366000</a>
                        </li>
                        <li>
                            <span><i class="fab fa-whatsapp"></i> </span> <a class="" href="https://api.whatsapp.com/send?phone=+967771366000">+967 771366000</a>
                        </li>
                        <li>
                            <span><i class="fal fa-envelope"></i></span> <a href="mailto:info@daktoori.com">info@daktoori.com</a>
                        </li>
                    </ul>
                </div>
                <div class="list-widget-social">
                    <ul>
                        <li><a href="https://www.facebook.com/Daktoori/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCkR5nBr_Snm0vdzkbOFtV7Q" target="_blank"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--map-view-wrap end -->
    <!-- Map -->
    <div class="map-container fw-map2">
        <div id="singleMap" data-latitude="15.321842" data-longitude="44.198611"></div>
    </div>
    <section id="sec1" class="grey-b lue-bg middle-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="list-single-main-item fl-wrap">
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>ارسل طلبك</h3>
                        </div>
                        <div id="contact-form">
                            <form class="custom-form rtl" action="/contact" method="POST">
                                {{ csrf_field() }}
                                <label>الاسم <i class="fal fa-user"></i></label>
                                <input class="{{  $errors->has('name') ? ' has-error' : ''  }}"
                                       placeholder="" type="text" name="name" value="{{ old('name') }}"/>
                                <label>البريد الالكتروني <i class="fal fa-envelope"></i></label>
                                <input class="{{  $errors->has('email') ? ' has-error' : ''  }}"
                                       placeholder="" type="text" name="email" value="{{ old('email') }}"/>
                                <label>الموضوع <i class="fal fa-comment-dots"></i></label>
                                <input class="{{  $errors->has('subject') ? ' has-error' : ''  }}"
                                       placeholder="" type="text" name="subject" value="{{ old('subject') }}"/>
                                <label>الرسالة <i class="fal fa-align-right"></i></label>
                                <textarea class="{{  $errors->has('message') ? ' has-error' : ''  }}"
                                          cols="40" rows="3" name="message">{{ old('message') }}</textarea>
                                <div class="fw-separator"></div>
                                @include('errors.errors')
                                <button type="submit" class="btn color2-bg float-btn">ارسال<i class="fal fa-paper-plane"></i></button>
                            </form>
                        </div>
                        <!-- contact form  end-->
                    </div>
                </div>

            </div>
        </div>
        <div class="section-decor"></div>
    </section>
    <!-- section end -->
@endsection
@section('bottom-ex')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}" type="text/javascript"></script>
    <script type="text/javascript" src="/js/map-single.js"></script>
@endsection