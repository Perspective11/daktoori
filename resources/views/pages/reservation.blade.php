@extends('layouts.app')
@section('content')
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '545369795960236',
                cookie     : true,
                xfbml      : true,
                version    : 'v3.2'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/ar_SA/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <section class="color-bg no-padding">
        <div class="wave-bg wave-bg2"></div>
        <div class="container">
            <div class="flat-title-wrap text-body-center">
                <h2><span>تأكيد الحجز</span></h2>
            </div>
        </div>
    </section>

    <section id="reserve" class="middle-padding grey-blue-bg">
        <div class="container">
            <div class="row rtl-col">
                <div class="col-md-4">
                    <!-- box-widget-item-->
                    <div class="box-widget-item fl-wrap rtl">
                        <div class="box-widget">
                            <div class="box-widget-content">
                                <div class="box-widget-item-header">
                                    <h3>الدكتور</h3>
                                </div>
                                <div class="box-widget-author fl-wrap">
                                    <div class="box-widget-author-title fl-wrap">
                                        <div class="box-widget-author-title-img">
                                            <img src="{{ $doctor->getPicture() }}" alt="">
                                        </div>
                                        <a href="{{ $doctor->path() }}">{{ $doctor->getName() }}</a>
                                        <span>{{ $doctor->title_ar }}</span>
                                    </div>
                                </div>
                                <div class="box-widget-list">
                                    <ul>
                                        <li>
                                            <span><i class="fal fa-stethoscope"></i> التخصص :</span>
                                            <a class="empty-link" href="javascript:void(0);">{{ $doctor->majorsString() }}</a>
                                        </li>
                                        @if ($office->name_ar)
                                            <li>
                                                <span><i class="fal fa-hospital-alt"></i> المكتب :</span>
                                                <a class="empty-link" href="javascript:void(0);">{{ $office->name_ar }}</a>
                                            </li>
                                        @endif
                                        @if ($doctor->city)
                                            <li>
                                                <span><i class="fal fa-map-marker-alt"></i> المدينة :</span>
                                                <a class="empty-link" href="javascript:void(0);">{{ $doctor->city->name_ar }}</a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- box-widget-item end-->
                </div>
                <div class="col-md-4">
                    <div class="list-single-main-item fl-wrap">
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>تفاصيل الحجز</h3>
                        </div>
                        <!-- dashboard-content-->
                        <form action="/reserve" method="post" class="dashboard-content fl-wrap rtl rtl-col">
                            {{ csrf_field() }}
                            <fb:login-button class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="false"
                                             scope="public_profile,email"
                                             onlogin="checkLoginState();"
                                             id="fbbutton"
                                            style="float:right">

                            </fb:login-button>
                            <!-- profile-edit-container-->
                            <input type="hidden" name="day" value="{{ $day->id }}">
                            <input type="hidden" name="time" value="{{ $time->id }}">
                            <input type="hidden" name="date" value="{{ $date->toDateString() }}">
                            <input type="hidden" name="office" value="{{ $office->id }}">
                            <div class="profile-edit-container">
                                <div class="custom-form">
                                    <label>الاسم الكامل للمريض <i class="fal fa-user-circle"></i>  <span style="color:orangered">*</span></label>
                                    <input class="{{  $errors->has('patient_name') ? ' has-error' : ''  }}"
                                           placeholder="" type="text" name="patient_name" id="patient_name"
                                           value="{{ old('patient_name') ?: optional($customer)->getName() }}"/>
                                    <label>رقم الهاتف <i class="fal fa-mobile-alt"></i>  <span style="color:orangered">*</span></label>
                                    <input class="{{  $errors->has('mobile') ? ' has-error' : ''  }}"
                                           placeholder="" type="text" name="mobile"
                                           value="{{ old('mobile') ?: optional($customer)->mobile }}"/>
                                    <label>البريد الالكتروني <i class="fal fa-envelope"></i>  <span style="color:orangered">*</span></label>
                                    <input class="{{  $errors->has('email') ? ' has-error' : ''  }}"
                                           placeholder="" type="text" name="email" id="email"
                                           value="{{ old('email') ?: optional($customer->user)->email }}"/>
                                    @if ($insurances->count())
                                        <label>التأمين  <span style="color:orangered">*</span></label>
                                        <div class="listsearch-input-item">
                                            <select data-placeholder="اختر التأمين..." data-display="اختر التأمين..." class="{{  $errors->has('insurance') ? ' has-error' : ''  }} chosen-select" name="insurance">
                                                <option value="">بدون تأمين</option>
                                                @foreach($insurances as $insurance)
                                                    @php
                                                        $selected = '';
                                                        if (old('insurance')){
                                                            if (old('insurance') == $insurance->id)
                                                            $selected = true;
                                                        }
                                                        else $selected = false;
                                                    @endphp
                                                    <option value="{{ $insurance->id }}" {{ $selected? 'selected' : '' }}>
                                                        {{ $insurance->name_ar }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif

                                </div>
                            </div>
                            @include('errors.errors')
                            <div class="filter-tags">
                                <input id="accept_term" name="accept_term" value="1" type="checkbox" required="required">
                                <label for="accept_term">أوافق على  <a href="/privacy-policy">سياسة الخصوصية</a> لموقع دكتوري</label>
                            </div>
                            <button type="submit" class="btn color2-bg float-btn">
                                استكمال الحجز<i class="fal fa-calendar-check"></i>
                            </button>
                            <a href="/listings" class="btn color-dark-grey float-btn">الغاء الحجز</a>
                        </form>
                        <!-- dashboard-list-box end-->
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- box-widget-item-->
                    <div class="box-widget-item fl-wrap rtl">
                        <div class="box-widget">
                            <div class="box-widget-content">
                                <div class="box-widget-item-header">
                                    <h3>معلومات الحجز</h3>
                                </div>
                                <div class="box-widget-list">
                                    <ul>
                                        <li>
                                            <span><i class="fal fa-calendar-alt"></i> تاريخ الحجز :</span>
                                            <a class="empty-link" href="javascript:void(0);">{{ $day->name_ar . ' - ' .  $date->format('Y/m/d') }}</a>
                                        </li>
                                        <li>
                                            <span><i class="fal fa-clock"></i> الوقت :</span>
                                            <a class="empty-link" href="javascript:void(0);">{{ $time->name_ar }}</a>
                                        </li>
                                        @if ($doctor->average_waiting_time)
                                            <li>
                                                <span><i class="fal fa-user-clock"></i> معدل الانتظار :</span>
                                                <a class="empty-link" href="javascript:void(0);">{{ $doctor->average_waiting_time }}
                                                    دقائق</a>
                                            </li>
                                        @endif
                                        @if ($office->fees)
                                            <li>
                                                <span><i class="fal fa-dollar-sign"></i> سعر الكشف :</span>
                                                <a class="empty-link" href="javascript:void(0);">{{ $office->fees }} ريال</a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- box-widget-item end-->
                </div>
            </div>
        </div>
        <div class="section-decor"></div>
    </section>
    <!-- section end -->
@endsection
@section('bottom-ex')
    <script>
        function checkLoginState() {
            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });
        }
        function statusChangeCallback(data) {
            console.log(data);
            if (data.status == 'connected'){
                var response = data.authResponse;
                $.ajax({
                    'url' : 'https://graph.facebook.com/' + response.userID + '?fields=name,email',
                    'type' : 'GET',
                    'dataType': 'json',
                    'data' : {
                        'access_token' : response.accessToken
                    },
                    'success' : function(data) {
                        $('#patient_name').val(data.name);
                        $('#email').val(data.email);
                        $('#fbbutton').remove();
                    }
                });
            }
        }
    </script>
@endsection