@extends('layouts.app')
@section('content')
    <section id="sec1" class="middle-padding grey-blue-bg">
        <div class="container">
            <div class="row rtl-col">
                <div class="col-md-12">
                    <!-- box-widget-item-->
                    <div class="box-widget-item fl-wrap rtl">
                        <div class="box-widget">
                            <div class="box-widget-content">
                                <div class="box-widget-item-header">
                                    <h3>الخدمات</h3>
                                </div>
                                <div class="rtl-right">
                                    <div class="services-search-input custom-form pull-right inline-block">
                                        <label>ابحث عن الخدمة <i class="fas fa-search"></i></label>
                                        <input class="majors-search-input-item" type="text" id="search-input"
                                               placeholder="اسم الخدمة" title="ابحث عن الخدمة">
                                    </div>
                                    <ul id="services-links" class="services-links">
                                        @foreach ($services as $service)
                                            <li class="search-list-item">
                                                <a href="/listings?service={{ $service->name_ar }}"
                                                   class="service-link">{{ $service->name_ar }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- box-widget-item end-->
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('#search-input').on('keyup', function (e) {
                var input, filter, ul, li, a, i, txtValue;
                input = document.getElementById("search-input");
                filter = input.value.toUpperCase();
                ul = document.getElementById("services-links");
                li = ul.getElementsByClassName("search-list-item");
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    txtValue = a.textContent || a.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            })
        });
    </script>
@endsection