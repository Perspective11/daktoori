@extends('layouts.app')
@section('content')
    <section id="sec1" class="middle-padding grey-blue-bg">
        <div class="container">
            <div class="row rtl-col">
                <div class="col-md-12">
                    <!-- box-widget-item-->
                    <div class="box-widget-item fl-wrap rtl">
                        <div class="box-widget">
                            <div class="box-widget-content">
                                <div class="box-widget-item-header">
                                    <h3>التخصصات</h3>
                                </div>
                                <div id="search-list" class="majors-container rtl-right">
                                    <div class="majors-search-input custom-form pull-right inline-block">
                                        <label>ابحث عن التخصص <i class="fas fa-stethoscope"></i></label>
                                        <input class="majors-search-input-item" type="text" id="search-input" placeholder="اسم التخصص" title="ابحث عن التخصص">
                                    </div>
                                    <ul id="accordion-dropdown" class="accordion-dropdown list">
                                        @foreach($global_majors->load('children')->where('level', 1) as $primaryMajor)
                                            @php($MajorChildren = $primaryMajor->children->where('is_hidden', 0)->where('doctors_count', ">",0))
                                            <li class="search-list-item fl-wrap">
                                                <div class="link {{ $MajorChildren->count() ? "droppable" : "" }}">
                                                    @if ($MajorChildren->count())
                                                        <i class="fa fa-chevron-down"></i>
                                                    @endif
                                                    <a href="/listings?major={{ $primaryMajor->name_ar }}">{{ $primaryMajor->name_ar }}</a>
                                                </div>
                                                @if ($MajorChildren->count())
                                                    <ul class="submenu">
                                                        @foreach($MajorChildren as $secondaryMajor)
                                                            @if($secondaryMajor->has('doctors') && ! $secondaryMajor->isHidden)
                                                                <li class="search-list-item"><a href="/listings?major={{ $secondaryMajor->name_ar }}">{{ $secondaryMajor->name_ar }}</a></li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- box-widget-item end-->
@endsection
@section('bottom-ex')
    <script>
        $(function() {
            var Accordion = function(el, multiple) {
                this.el = el || {};
                this.multiple = multiple || true;

                // Variables privadas
                var links = this.el.find('.link.droppable');
                // Evento
                links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
            }

            Accordion.prototype.dropdown = function(e) {
                var $el = e.data.el;
                $this = $(this),
                    $next = $this.next();

                $next.slideToggle();
                $this.parent().toggleClass('open');

                if (!e.data.multiple) {
                    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
                };
            }

            var accordion = new Accordion($('#accordion-dropdown'), false);

            $('#search-input').on('keyup', function (e) {
                var input, filter, ul, li, a, i, txtValue;
                input = document.getElementById("search-input");
                filter = input.value.toUpperCase();
                ul = document.getElementById("accordion-dropdown");
                li = ul.getElementsByClassName("search-list-item");
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    txtValue = a.textContent || a.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
                $('#accordion-dropdown').find('.submenu').slideDown().parent().removeClass('open').addClass('open');
            })
        });

    </script>
@endsection