@extends('layouts.app')
@section('content')

    <!--  section  -->
    <section class="parallax-section single-par" data-scrollax-parent="true">
        <div class="bg par-elem " data-bg="images/bg/4.jpg" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="section-title center-align big-title">
                <div class="section-title-separator"><span></span></div>
                <h2><span>نبذة عن فريق دكتوري</span></h2>
                <span class="section-separator"></span>
                <h4></h4>
            </div>
        </div>
        <div class="header-sec-link">
            <div class="container"><a href="#sec1" class="custom-scroll-link color-bg"><i
                            class="fal fa-angle-double-down"></i></a></div>
        </div>
    </section>
    <!--  section  end-->

    <section id="sec1" class="middle-padding">
        <div class="container">
            <!--about-wrap -->
            <div class="about-wrap">
                <div class="row">
                    <div class="col-md-6">
                        <div class="video-box fl-wrap">
                            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FDaktoori%2Fvideos%2F704299850339798%2F&show_text=0&width=560"
                                    width="560" height="315" style="border:none;overflow:hidden" scrolling="no"
                                    frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>من نحن</h3>
                        </div>
                        <p>دكتوري دوت كوم يمكنك من العثور على أفضل طبيب وفقاً لإحتياجك مجاناً . حجز موعد مباشر مع الطبيب الاقرب الى موقعك , و يتيح لك التصفح من خلال قاعدة بيانات محدثة من الأطباء والعيادات والمستشفيات . </p>

                        <p>يمكنك أيضا معرفة المزيد عن التجربة الطبية ورؤية خبرات الطبيب قبل حجز موعده المباشر.كما يمكنك أيضا معرفة ما إذا كان الطبيب أو العيادة أو المستشفى يقبل التأمين الخاص بك!</p>
{{--                        <a href="#sec2" class="btn  color-bg float-btn custom-scroll-link">تعرف على فريق العمل <i--}}
{{--                                    class="fal fa-users"></i></a>--}}
                    </div>
                </div>
            </div>
            <!-- about-wrap end  -->

        </div>
    </section>
    <!-- section end -->

@endsection