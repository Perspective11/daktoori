@extends('layouts.app')
@section('top-ex')
    <style>

    </style>
@endsection
@section('content')
    <!--  section  -->
    <section class="parallax-section single-par" data-scrollax-parent="true">
        <div class="bg par-elem "  data-bg="/images/bg/5.jpg" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="section-title center-align big-title">
                <div class="section-title-separator"><span></span></div>
                <h2><span>الأسئلة الأكثر شيوعاً</span></h2>
                <span class="section-separator"></span>
                <h4>هنا تجد الأسئلة التي طرحت بكثرة من قبل عملائنا وأطبائنا</h4>
            </div>
        </div>
        <div class="header-sec-link">
            <div class="container"><a href="#sec1" class="custom-scroll-link color-bg"><i class="fal fa-angle-double-down"></i></a></div>
        </div>
    </section>
    <!--  section  end-->
    <section id="sec1" class="middle-padding grey-blue-bg">
    <div class="accordion-container">
        @foreach($faqs as $faq)
            <div id="accordion-2">
            <div class="head">
                <p>{{ $faq->title_ar }}</p>
                <i class="arrow"></i>
            </div>
            <div class="accordion-content">
                <p>{{ $faq->body_ar }}</p>
            </div>
        </div>
        @endforeach
    </div>

                <!-- dashboard-list-box end-->
        <!--dashboard-wrap-->
        <!-- dashboard-content-->

        <!-- dashboard-wrap end-->
    </section>
@endsection
@section('bottom-ex')
    <script>
        $(function() {
            $('.head').click(function(){
                $(this).toggleClass('active');
                $(this).parent().find('.arrow').toggleClass('arrow-animate');
                $(this).parent().find('.accordion-content').slideToggle(280);
            });
        });
    </script>
@endsection