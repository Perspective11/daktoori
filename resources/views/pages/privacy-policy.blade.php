@extends('layouts.app')
@section('content')
    <!--  section  -->
    <section class="color-bg no-padding">
        <div class="wave-bg wave-bg2"></div>
        <div class="container">
            <div class="flat-title-wrap text-body-center">
                <h2>سياسة الخصوصية</h2>
                <span class="section-separator"></span>
                <h4>وشروط الاستخدام</h4>
            </div>
        </div>
    </section>
    <section  id="sec1" class="middle-padding grey-blue-bg">
        <div class="container">
            {!! $contents['privacy_policy']['value_ar'] !!}
        </div>
    </section>
@endsection