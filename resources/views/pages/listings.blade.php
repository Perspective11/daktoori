@extends('layouts.app')
@section('content')
    <section class="listings-header" data-bg="/images/bg/steth.jpg" style="background-image: url('/images/bg/steth.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-search-input-wrap">
                        <form class="main-search-input fl-wrap" method="get" action="/listings">
                            <div class="main-search-input-item location">
                                <span class="inpt_dec"><i class="fal fa-stethoscope"></i></span>
                                <select id="select-major" data-placeholder="التخصص" name="major">
                                    <option value=""></option>
                                    @foreach($global_majors as $major)
                                        <option value="{{ $major->name_ar }}">{{ $major->name_ar }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="main-search-input-item" id="main-search-city">
                                <span class="inpt_dec"><i class="fal fa-map-marker-alt"></i></span>
                                <select id="select-city" data-placeholder="المحافظة" name="city">
                                    <option value=""></option>
                                    @foreach(\App\City::has('offices')->get() as $city)
                                        <option value="{{ $city->key }}">{{ $city->name_ar }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="main-search-input-item" id="main-search-region">
                                <span class="inpt_dec"><i class="fal fa-map-marker-alt"></i></span>
                                <select id="select-region" data-placeholder="المنطقة" name="region">
                                    <option value=""></option>
                                    @foreach(\App\Region::has('offices')->get() as $region)
                                        <option value="{{ $region->key }}">{{ $region->name_ar }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="main-search-input-item">
                                <span class="inpt_dec"><i class="fal fa-user-md"></i></span>
                                <input type="text" placeholder="ابحث عن اسم الدكتور" name="doctor" value=""/>
                            </div>
                            <button class="main-search-button color2-bg" type="submit">بـحـث <i class="fal fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="reserve-now">احجز موعدك الآن اونلاين مع دكتوري <br><a href="tel:+967771366000">771366000</a> او اتصل برقمنا الثابت <a
                    href="tel:01441702">01441702</a> او رقم التلفون </div>
    </section>
    <!--  section-->
    <section class="grey-blue-bg small-padding" id="sec1">
        <div class="container">
            <div class="row rtl-row">
                <!--filter sidebar -->
                <div class="col-md-3 filters-col clearfix">
                    <div class="mobile-list-controls fl-wrap">
                        <div class="mlc show-list-wrap-search fl-wrap"><i class="fal fa-filter"></i> بحث متقدم</div>
                    </div>
                    <div class="fl-wrap filter-sidebar_item fixed-bar">
                        <form class="filter-sidebar fl-wrap lws_mobile" method="get" action="/listings">
                            <!--col-list-search-input-item -->
                            <div class="col-list-search-input-item in-loc-dec fl-wrap not-vis-arrow">
                                <label>التخصص</label>
                                <div class="listsearch-input-item">
                                    <select data-placeholder="اختر التخصص..." class="chosen-select" name="major">
                                        <option value="">جميع التخصصات</option>
                                        @foreach($global_majors as $major)
                                            <option value="{{ $major->name_ar }}">{{ $major->name_ar }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--col-list-search-input-item end-->
                            <!--col-list-search-input-item -->
                            <div class="col-list-search-input-item in-loc-dec fl-wrap not-vis-arrow">
                                <label>التأمين</label>
                                <div class="listsearch-input-item">
                                    <select data-placeholder="اختر التأمين..." class="chosen-select" name="insurance">
                                        <option value="">جميع التأمينات</option>
                                        @foreach(\App\Insurance::has('offices')->get() as $insurance)
                                            <option value="{{ $insurance->name_ar }}">{{ $insurance->name_ar }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--col-list-search-input-item end-->
                            <!--col-list-search-input-item -->
                            <div class="col-list-search-input-item in-loc-dec fl-wrap not-vis-arrow">
                                <label>المدينة</label>
                                <div class="listsearch-input-item">
                                    <select data-placeholder="اختر المدينة..." class="chosen-select" name="city">
                                        <option value="">جميع المدن</option>
                                        @foreach(\App\City::has('offices')->get() as $city)
                                            <option value="{{ $city->key }}">{{ $city->name_ar }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--col-list-search-input-item end-->
                            <!--col-list-search-input-item -->
                            <div class="col-list-search-input-item in-loc-dec fl-wrap not-vis-arrow">
                                <label>المنطقة</label>
                                <div class="listsearch-input-item">
                                    <select data-placeholder="اختر المنطقة..." class="chosen-select" name="region">
                                        <option value="">جميع المناطق</option>
                                        @foreach(\App\Region::has('offices')->get() as $region)
                                            <option value="{{ $region->key }}">{{ $region->name_ar }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--col-list-search-input-item end-->
                            <!--col-list-search-input-item end-->
                            <div class="col-list-search-input-item fl-wrap">
                                <label>اسم الطبيب</label>
                                <span class="header-search-input-item-icon"><i class="fal fa-user-md"></i></span>
                                <input type="text" placeholder="ابحث عن اسم الدكتور" name="doctor" value=""/>
                            </div>
                            <!--col-list-search-input-item end-->
                            <!--col-list-search-input-item-->
                            <div class="col-list-search-input-item fl-wrap">
                                <label>اسم المكتب</label>
                                <span class="header-search-input-item-icon"><i class="fal fa-hospital"></i></span>
                                <input type="text" placeholder="ابحث عن اسم المكتب" name="office" value=""/>
                            </div>
                            <!--col-list-search-input-item end-->
                            <!--col-list-search-input-item -->
                            <div class="col-list-search-input-item fl-wrap">
                                <div class="range-slider-title">حدود السعر - ريال يمني</div>
                                <div class="range-slider-wrap fl-wrap">
                                    <input class="range-slider" data-from="500" data-to="20000" data-step="500" data-min="500" data-max="20000" name="price">
                                </div>
                            </div>
                            <!--col-list-search-input-item end-->
                            <!--col-list-search-input-item  -->
                            <div class="col-list-search-input-item fl-wrap">
                                <button class="header-search-button" type="submit">بـحـث <i class="far fa-search"></i></button>
                            </div>
                            <!--col-list-search-input-item end-->
                        </form>
                    </div>
                </div>
                <!--filter sidebar end-->
                <!--listing -->
                <div class="col-md-9">
                    <!--col-list-wrap -->
                    <div class="col-list-wrap fw-col-list-wrap post-container">
                        <!-- list-main-wrap-->
                        <div class="list-main-wrap fl-wrap card-listing">
                            <!-- list-main-wrap-opt-->
                            <div class="list-main-wrap-opt fl-wrap">
                                @if (request()->keys())
                                    <div class="list-main-wrap-title fl-wrap col-title rtl">
                                        <h2>نتائج لـ :</h2>
                                        @if (request()->filled('major'))
                                            <span> {{ request('major')}}</span>
                                        @endif
                                        @if (request()->filled('service'))
                                            <span> {{ request('service')}}</span>
                                        @endif
                                        @if (request()->filled('city'))
                                            <span> {{ request('city')}}</span>
                                        @endif
                                        @if (request()->filled('region'))
                                            <span> {{ request('region')}}</span>
                                        @endif
                                        @if (request()->filled('doctor'))
                                            <span> {{ request('doctor')}}</span>
                                        @endif
                                        @if (request()->filled('office'))
                                            <span> {{ request('office')}}</span>
                                        @endif
                                        @if (request()->filled('insurance'))
                                            <span> {{ request('insurance')}}</span>
                                        @endif
                                        @if (request()->filled('price'))
                                            <span> {{ request('price')}} ريال</span>
                                        @endif
                                    </div>
                                @endif
                            </div>
                            <!-- list-main-wrap-opt end-->
                            <!-- listing-item-container -->
                            <div class="listing-item-container init-grid-items fl-wrap">

                                @foreach ($doctors as $doctor)
                                    @php
                                        $office = $doctor->getOffice()
                                    @endphp
                                        <!-- listing-item  -->
                                        <div class="listing-item has_one_column">
                                            <article class="geodir-category-listing fl-wrap">
                                                <div class="geodir-category-img">
                                                    <a href="{{ $doctor->path() }}"><img src="{{ $doctor->getPicture() }}" alt=""></a>
                                                    <div class="sale-window hidden-xs">{{ $doctor->getMajorName() }}</div>
                                                </div>
                                                <div class="geodir-category-content fl-wrap title-sin_item">
                                                    <div class="geodir-category-content-title fl-wrap">
                                                        <div class="geodir-category-content-title-item">
                                                            <h3 class="title-sin_map"><a href="{{ $doctor->path() }}"><span class="title-prefix">{{ $doctor->getPrefix() }}</span> {{ $doctor->getName() }}</a></h3>
                                                            <div class="geodir-category-location fl-wrap"><span>{{ $doctor->title_ar }}</span></div>
                                                        </div>
                                                    </div>
                                                    <ul class="facilities-list fl-wrap rtl-right">
                                                        <li class="block"><i class="fal fa-stethoscope"></i> {{ $doctor->majorsString() }}</li>
                                                        @if (isset($doctor->average_waiting_time))
                                                            <li><i class="fal fa-clock"></i> الانتظار: {{ $doctor->average_waiting_time }} دقائق</li>
                                                        @endif
                                                        <li><i class="fal fa-map-marker-alt"></i> المنطقة: {{ $office->city->name_ar }} - {{ $office->region->name_ar }}</li>
                                                        @if (isset($doctor->mobile))
                                                            <li><i class="fal fa-phone"></i> الهاتف: <a href="tel:01441702">01441702</a></li>
                                                        @endif
                                                    </ul>
                                                    <div class="geodir-category-footer fl-wrap">
                                                        <div class="geodir-category-price">الكشف بـ <span>{{ $office->fees }}</span> ريال</div>
                                                        <a href="{{ $doctor->path() }}" class="make-appointment">احجز الان</a>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-timetable rtl">
                                                    <div class="timetable-container">
                                                        @foreach ($sortedDays as $day)
                                                            @php
                                                                $unavailable = 'shown';
                                                                $morningHidden = 'hidden';
                                                                $eveningHidden = 'hidden';

                                                                foreach ($office->officeTimes as $officeTime){
                                                                    if ($officeTime->day_id == $day->id){
                                                                        $unavailable = '';
                                                                        if ($officeTime->time_id == $times['day']){
                                                                            $morningHidden = '';
                                                                        }
                                                                        if ($officeTime->time_id == $times['evening']){
                                                                            $eveningHidden = '';
                                                                        }
                                                                    }
                                                                }
                                                            @endphp
                                                            <div class="slide-item">
                                                                <div class="day">
                                                                    <label>{{ $day->string }}</label>
                                                                    <a class="button-time button-day {{ $morningHidden }}" data-office="{{ $office->id }}" data-day="{{ $day->id }}" data-time="{{ $times['day'] }}" data-date="{{ $day->date }}" data-toggle="tooltip" data-placement="bottom" title="انقر للحجز">صباح</a>
                                                                    <a class="button-time button-evening {{ $eveningHidden }}" data-office="{{ $office->id }}" data-day="{{ $day->id }}" data-time="{{ $times['evening'] }}" data-date="{{ $day->date }}" data-toggle="tooltip" data-placement="bottom" title="انقر للحجز">مساء</a>
                                                                    <h3 class="unavailable {{ $unavailable }}">غير متوفر</h3>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <p class="booking-note">الدخول بميعاد محدد</p>
                                                </div>
                                                @if ($doctor->getRatingsCount() >= \App\Review::$REVIEW_THRESHOLD)
                                                    <div class="listing-star-rating star-rating" title="{{ $doctor->getRating(true) }}%">
                                                        <div class="back-stars">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>

                                                            <div class="front-stars" style="width: {{ $doctor->getRating(true) }}%">
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                        <p>{{ $doctor->getRatingsCount() }} تقييمات</p>
                                                    </div>
                                                @endif
                                                <div class="listing-footer fl-wrap"><span class="office-name">{{ $office->name_ar }}</span></div>
                                            </article>
                                        </div>
                                        <!-- listing-item end -->
                                @endforeach
                            </div>
                            <!-- listing-item-container end-->
                            <div class="pagination-wrapper row text-center">
                                <div class="col-xs-12">
                                    {{ $doctors->appends(request()->input())->links() }}
                                </div>
                            </div>

                        </div>
                        <!-- list-main-wrap end-->
                    </div>
                    <!--col-list-wrap end -->
                </div>
                <!--listing  end-->
            </div>
            <!--row end-->
        </div>
        <div class="limit-box fl-wrap"></div>
    </section>
@endsection
@section('bottom-ex')
    <script>
        $(function() {
            $('.timetable-container').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                rtl: true,
                prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-right"></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-left"></i></button>',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                ]
            });
            $buttonTime = $('.button-time');
            $buttonTime.on('click', function (e) {
                e.preventDefault();
                buttonData = $(this)[0].dataset;
                $form = $('<form />', {action: '/reservation', method: 'GET' }).append(
                    $('<input />', { name: 'office', value: buttonData.office} ),
                    $('<input />', { name: 'day', value: buttonData.day} ),
                    $('<input />', { name: 'time', value: buttonData.time} ),
                    $('<input />', { name: 'date', value: buttonData.date} ),
                );
                $('body').append($form);
                $form.submit();
            });
        });
    </script>

@endsection