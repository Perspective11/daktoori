@extends('layouts.app')
@section('content')
    <!--  section  -->
    <section class="parallax-section single-par" data-scrollax-parent="true">
        <div class="bg par-elem "  data-bg="/images/bg/team.jpg" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="section-title center-align big-title">
                <div class="section-title-separator"><span></span></div>
                <h2><span>فريق دكتوري</span></h2>
                <span class="section-separator"></span>
                <h4>لمحة سريعة عن الفريق المسؤول عن نجاح دكتوري</h4>
            </div>
        </div>
        <div class="header-sec-link">
            <div class="container"><a href="#sec1" class="custom-scroll-link color-bg"><i class="fal fa-angle-double-down"></i></a></div>
        </div>
    </section>
    <!--  section  end-->
    <!--section -->
    <section id="sec2">
        <div class="container">
            <div class="team-holder section-team fl-wrap">
                <!-- team-item -->
                <div class="team-box">
                    <div class="team-photo">
                        <img src="/images/team/aiman.jpg" alt="" class="respimg">
                    </div>
                    <div class="team-info">
                        <div class="team-dec"><i class="fal fa-laptop"></i></div>
                        <h3><a href="https://www.linkedin.com/in/aiman-noman-750b74120/">أيمن نعمان</a></h3>
                        <h4>الرئيس التقني</h4>
                        <div class="team-social">
                            <ul>
                                <li><a href="https://www.facebook.com/aiman.noman" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com/AimanNoman11" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.linkedin.com/in/aiman-noman-750b74120/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                            </ul>
                            <a class="team-contact_link" href="mailto:aiman.noman11@gmail.com"><i class="fal fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
                <!-- team-item  end-->
                <!-- team-item -->
                <div class="team-box">
                    <div class="team-photo">
                        <img src="/images/team/moe.jpeg" alt="" class="respimg">
                    </div>
                    <div class="team-info">
                        <div class="team-dec"><i class="fal fa-glasses"></i></div>
                        <h3><a href="#">محمد العمودي</a></h3>
                        <h4>الرئيس التنفيذي</h4>
                        <div class="team-social">
                            <ul>
                                <li><a href="https://www.facebook.com/mohamed.alamudi.3" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.linkedin.com/in/mohammed-alamoudi-3697a79b/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                            </ul>
                            <a class="team-contact_link" href="mailto:muhamedalamoudi@gmail.com"><i class="fal fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
                <!-- team-item end  -->
                <!-- team-item -->
                <div class="team-box">
                    <div class="team-photo">
                        <img src="images/team/wala.jpg" alt="" class="respimg">
                    </div>
                    <div class="team-info">
                        <div class="team-dec"><i class="fal fa-chart-line"></i></div>
                        <h3><a href="#">ولاء صالح</a></h3>
                        <h4>مديرة التسويق والموارد البشرية</h4>
                        <div class="team-social">
                            <ul>
                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <a class="team-contact_link" href="#"><i class="fal fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
                <!-- team-item end  -->
            </div>
        </div>
    </section>
@endsection