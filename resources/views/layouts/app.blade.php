<!DOCTYPE html>
<html lang="ar">
<head>
    @include('partials.head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/css/iziToast.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/js/iziToast.min.js"></script>
    @yield('top-ex')
    @stack('top-stack')
</head>
<body class="ar">
    <!--loader-->
    <div class="loader-wrap">
        <div class="pin">
            <div class="pulse"></div>
        </div>
    </div>

    <div id="main">
        @include('partials.nav')
        <div id="wrapper">
            <!--content-->
            <div class="content">
                @yield('content')
            </div>
            <!-- content end -->
        </div>
        @include('partials.footer')
        @include('partials.map-modal')
        @include('partials.register-modal')
        <a class="to-top"><i class="fas fa-caret-up"></i></a>
    </div>
    <!--loader end-->

    <!-- wrapper -->
    <!-- wrapper end -->
    @include('partials.foot')
    @yield('bottom-ex')
    @stack('bottom-stack')
    @include('toast.toast')
</body>
</html>
