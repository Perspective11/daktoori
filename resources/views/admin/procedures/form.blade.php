@section('top-ex')
@endsection
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $procedure->name_ar }}">
        </div>
    </div>
</div>
@section('bottom-ex')
    <script type="text/javascript">
    </script>
@endsection