@extends('adminlte::page')
@section('top-ex')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

@endsection
@section('content')
    <form action="{{ url('/admin/settings/') }}" method="post">
        {{ csrf_field() }}
        <div class="row justify-content-center">
            <div class="col-md-10 col-md-offset-1">
               @foreach($groupedSettings as $key => $settings)
                    <div class="card card-gray-dark">
                        <div class="card-header with-border">
                            <h3 class="card-title">{{ $key }}</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            @foreach($settings as $setting)
                                <div class="form-inputs">
                                    <div class="input-group {{ $errors->has($setting->key) ? ' has-error' : '' }}">
                                        <label for="{{ $setting->key }}" class="control-label col-2">{{ $setting->name_en }}</label>
                                            @if ($setting->icon)
                                                <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        style="{{ $setting->color?  'color: ' . $setting->color : ''}}"
                                                        class="fa {{ $setting->icon }} icon"></i></span>
                                                </div>
                                            @endif
                                        @if ($setting->category != "notification_color")
                                            <input type="text" class="form-control"
                                                   placeholder="{{ $setting->placeholder }}"
                                                   name="{{ $setting->key }}"
                                                   value="{{ old($setting->key) ?: $setting->value }}">
                                        <label for="{{ $setting->key }}" class="control-label control-label-ar col-2">{{ $setting->name_ar }}</label>
                                        @endif
                                        @if ($setting->category == "notification_color")
                                        <input type="text" class="form-control colorpicker"
                                                   placeholder="{{ $setting->placeholder }}"
                                                   name="{{ $setting->key }}"
                                                   value="{{ old($setting->key) ?: $setting->value }}">
                                        <label for="{{ $setting->key }}" class="control-label control-label-ar col-2">{{ $setting->name_ar }}</label>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                @endforeach
              
                
</div> 
              
             
            </div>
       
        <div class="row">
            <div class="col-xs-12">
                @include('errors.errors')
            </div>
        </div>
        <section class="FAB">
            <div class="FAB__action-button">
                <button class="btn-link"><i class="action-button__icon bg-orange fa fa-edit"></i></button>
                <p class="action-button__text--hide">Submit Settings</p>
            </div>
        </section>
    </form>
   
@endsection
@section('bottom-ex')

    <script>
        $('.FAB__action-button').hover(function () {
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function () {
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.toggle-button'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html);
        });

    </script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
    <script>
        $('.colorpicker').colorpicker();
    </script>

@endsection
