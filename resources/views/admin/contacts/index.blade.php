@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('content_header')
    <h3>Contact Messages</h3>
@endsection
@section('content')
    @include('admin.contacts.filters')
    <table class="table table-striped table-bordered order-column table-hover jdatatable display smaller rtl" id="contacts-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>الاسم</th>
            <th>الايميل</th>
            <th>الموضوع</th>
            <th>الرسالة</th>
            <th>تاريخ</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('#contacts-table tbody').on('click','.model-link', function (e) {
                e.stopPropagation()
            })

            var $contactsTable = $('#contacts-table').DataTable({
                "order": [[ 4, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/contacts/getData")}}',
                    data: function (d) {
                        d.daterange = '{{ request('daterange')? request('daterange') : ''}}'
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'subject', name: 'subject'},
                    {data: 'message', name: 'message', width: "50%"},
                    {data: 'created_at', name: 'created_at'},
                ]
            });


            var $modal = $('.action-modal');
            $('#contacts-table tbody').on('click', 'tr', function () {

                var data = $contactsTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-contact-name').text(data['name']);
                $modal.find('.modal-contact-email').text(data['email']);
                $modal.find('.modal-contact-subject').text(data['subject']);
                $modal.find('.modal-contact-message').text(data['message']);
                $modal.find('.modal-contact-created').text(data['created_at']);
                $modal.find('.contact-reply').attr('href', 'mailto:' + data['email']);
                $modal.find('.delete-contact').attr('action' , '/admin/contacts/' + data['id']);

            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this contact message?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="modal-field">
                                <strong>Name:</strong> <span class="modal-contact-name"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Email:</strong> <span class="modal-contact-email"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Subject:</strong> <span class="modal-contact-subject"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Created At:</strong> <span class="modal-contact-created"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="modal-field">
                                <strong>Message:</strong> <span class="modal-contact-message"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item contact-reply">Reply to Message</a>
                        <form class="list-group-item list-group-item-danger delete-contact" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Message</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection