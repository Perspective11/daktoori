@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>

    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Customers</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/users/create') }}"><i class="action-button__icon fa fa-plus bg-red"></i></a>
            <p class="action-button__text--hide">Add Customer</p>
        </div>
    </section>
    <div class="container-fluid">
        @include('admin.customers.filters')
        <table class="table table-striped table-bordered order-column table-hover jdatatable display smaller rtl" id="customers-table" style="background-color: #fff;">
            <thead>
            <tr>
                <th>id</th>
                <th>الاسم</th>
                <th>الجنس</th>
                <th>العمر</th>
                <th>العنوان</th>
                <th>المحمول</th>
                <th>الواتس اب</th>
                <th>ملاحظات</th>
                <th>المدينة</th>
                <th>المنطقة</th>
                <th>الحجوزات</th>
                <th>تاريخ</th>
                <th>الخيارات</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $customersTable = $('#customers-table').DataTable({
                "order": [[ 3, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/customers/getData")}}',
                    data: function (d) {
                        d.name = $.urlParam('name');
                        d.city = $.urlParam('city');
                        d.region = $.urlParam('region');
                        d.recordrange = $.urlParam('recordrange');
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'id', name: 'id', visible: false},
                    {data: 'full_name', name: 'full_name', sortable: 'no', orderable: 'no', searchable: 'no' },
                    {data: 'gender', name: 'gender', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'age', name: 'age', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'address_ar', name: 'address_ar'},
                    {data: 'mobile', name: 'mobile', visible: false},
                    {data: 'whatsapp', name: 'whatsapp', visible: false},
                    {data: 'note', name: 'note', visible: false},
                    {data: 'city', name: 'city', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'region', name: 'region', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'bookings', name: 'bookings', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', sortable: 'no', orderable: 'no', searchable: 'no'},
                ],
            });

            $('#customers-table tbody').on('click', '.btn', function (e) {
                e.stopPropagation();
            });

            $('#customers-table tbody').on('click', '.customer-delete', function (e) {
                e.preventDefault();
                $row = $(this).closest('tr');
                var data = $customersTable.row($row).data();
                deleteCustomer(data['user']['id']);
            });

            var $modal = $('.action-modal');
            $('#customers-table tbody').on('click', 'tr', function () {
                var data = $customersTable.row(this).data();
                $('.action-modal').modal();
                $modal.find('.delete-customer').attr('action' , '/admin/customers/' + data['id']);

            });

            function deleteCustomer(userId) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this customer record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: "/admin/users/" + userId,
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("Deleted!", "The record was successfully deleted!", "success");
                                $('#customers-table').DataTable().ajax.reload();
                            },
                            error: function (data) {
                                swal("Oops", data.responseText, "error")
                            }
                        }
                    )
                });
            }


        });
    </script>
@endsection

@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection