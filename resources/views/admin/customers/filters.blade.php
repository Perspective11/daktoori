<form id="customer-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Customer Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-1">
                @php
                    $comboCity = '';
                    $comboCity = request('city');
                    $cities = \App\City::whereHas('customers')->get();
                @endphp
                <h5><strong>City:</strong></h5>
                <select name="city" class="form-control" id="city">
                    <option {{ $comboCity== ''? 'selected': '' }} value="">All</option>
                    @foreach ($cities as $city)
                        <option {{ $comboCity == $city->key ? 'selected': '' }} value="{{ $city->key }}">{{$city->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                @php
                    $comboCity = '';
                    $comboCity = request('region');
                    $cities = \App\Region::whereHas('customers')->get();
                @endphp
                <h5><strong>Region:</strong></h5>
                <select name="region" class="form-control" id="region">
                    <option {{ $comboCity== ''? 'selected': '' }} value="">All</option>
                    @foreach ($cities as $region)
                        <option {{ $comboCity == $region->key ? 'selected': '' }} value="{{ $region->key }}">{{$region->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <h5><strong>Name:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="name"
                           value="{{ request('name')? request('name') : '' }}">
                </div>
            </div>
            <div class="col-md-3">
                <h5><strong>Record Date Range:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="recordrange" id="recordrange"
                           value="{{ request('recordrange')? request('recordrange') : '' }}">
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $('#clear-fields').on('click', function () {
        var strippedUrl = stripQuery(window.location.href);
        window.location.replace(strippedUrl);
    })

    function stripQuery(url) {
        return url.split("?")[0].split("#")[0];
    }

    $('#search-submit').on('click', function (e) {
        e.preventDefault();
        putQueryString();
        $('#customers-table').DataTable().ajax.reload();
    })

    function putQueryString() {
        var strippedUrl = stripQuery(window.location.href);
        var newQueryString = $('#customer-filter').serialize();
        var url = strippedUrl + '?' + newQueryString;
        history.replaceState('', 'Customers', url);
    }

    var start = moment().subtract(29, 'days');
    var end = moment();


    $('#recordrange').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
    $('#recordrange').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
    });

    $('#recordrange').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

</script>

