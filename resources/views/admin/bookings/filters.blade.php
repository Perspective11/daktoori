<form id="booking-filter" target="_blank" action="{{ url('/admin/bookings/export') }}" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Booking Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-2">
                @php
                    $comboBookingStatus = '';
                    $comboBookingStatus = request('bookingstatus');
                    $bookingStatuses = \App\BookingStatus::whereHas('bookings')->get();
                @endphp
                <h6><strong>Booking Status:</strong></h6>
                <select name="bookingstatus" class="form-control" id="bookingstatus">
                    <option {{ $comboBookingStatus== ''? 'selected': '' }} value="">All</option>
                    @foreach ($bookingStatuses as $bookingStatus)
                        <option {{ $comboBookingStatus == $bookingStatus->key ? 'selected': '' }} value="{{ $bookingStatus->key }}">{{$bookingStatus->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                @php
                    $comboVisitStatus = '';
                    $comboVisitStatus = request('visitstatus');
                    $visitStatuses = \App\VisitStatus::whereHas('bookings')->get();
                @endphp
                <h6><strong>Visit Status:</strong></h6>
                <select name="visitstatus" class="form-control" id="visitstatus">
                    <option {{ $comboVisitStatus== ''? 'selected': '' }} value="">All</option>
                    @foreach ($visitStatuses as $visitStatus)
                        <option {{ $comboVisitStatus == $visitStatus->key ? 'selected': '' }} value="{{ $visitStatus->key }}">{{$visitStatus->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                @php
                    $comboTime = '';
                    $comboTime = request('time');
                    $times = \App\Time::whereHas('bookings')->get();
                @endphp
                <h6><strong>Time:</strong></h6>
                <select name="time" class="form-control" id="time">
                    <option {{ $comboTime== ''? 'selected': '' }} value="">All</option>
                    @foreach ($times as $time)
                        <option {{ $comboTime == $time->key ? 'selected': '' }} value="{{ $time->key }}">{{$time->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                @php
                    $comboRegion = '';
                    $comboRegion = request('region');
                    $regions = \App\Region::whereHas('offices')->get();
                @endphp
                <h6><strong>Region:</strong></h6>
                <select name="region" class="form-control" id="region">
                    <option {{ $comboRegion== ''? 'selected': '' }} value="">All</option>
                    @foreach ($regions as $region)
                        <option {{ $comboRegion == $region->key ? 'selected': '' }} value="{{ $region->key }}">{{$region->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <h6><strong>Doctors:</strong></h6>
                <div class="form-group">
                    <input type="text" class="form-control" name="user" value="{{ request('user')? request('user') : '' }}">
                </div>
            </div>
            <div class="col-md-3">
                <h6><strong>Patients:</strong></h6>
                <div class="form-group">
                    <input type="text" class="form-control" name="patient" value="{{ request('patient')? request('patient') : '' }}">
                </div>
            </div>
            <div class="col-md-3">
                <h6><strong>Patient Phone:</strong></h6>
                <div class="form-group">
                    <input type="text" class="form-control" name="patient_phone" value="{{ request('patient_phone')? request('patient_phone') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                <h6><strong>Office:</strong></h6>
                <div class="form-group">
                    <input type="text" class="form-control" name="office" value="{{ request('office')? request('office') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                @php
                    $comboInsurances = '';
                    $comboInsurances = request('insurance');
                    $insurances = \App\Insurance::whereHas('bookings')->get();
                @endphp
                <h6><strong>Insurance:</strong></h6>
                <select name="insurance" class="form-control" id="insurance">
                    <option {{ $comboInsurances== ''? 'selected': '' }} value="">All</option>
                    @foreach ($insurances as $insurance)
                        <option {{ $comboInsurances == $insurance->name_ar ? 'selected': '' }} value="{{ $insurance->name_ar }}">{{$insurance->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <h6><strong>Fee Range (YR):</strong></h6>
                <div class="form-group range-form-group">
                    <input type="number" class="form-control" name="feestart" value="{{ request('feestart')? request('feestart') : '' }}"> to
                    <input type="number" class="form-control" name="feeend" value="{{ request('feeend')? request('feeend') : '' }}">
                </div>
            </div>
            <div class="col-md-3">
                <h6><strong>Appoi. Date Range:</strong></h6>
                <div class="form-group">
                    <input type="text" class="form-control" name="appoinrange" id="appoinrange"
                           value="{{ request('appoinrange')? request('appoinrange') : '' }}">
                </div>
            </div>
            <div class="col-md-3">
                <h6><strong>Record Date Range:</strong></h6>
                <div class="form-group">
                    <input type="text" class="form-control" name="recordrange" id="recordrange"
                           value="{{ request('recordrange')? request('recordrange') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                <h6><strong>When:</strong></h6>
                @php
                    $radioWhen = '';
                    $radioWhen = request('when');
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="when" id="radioUpcoming"
                               value="upcoming" {{ $radioWhen == 'upcoming'? 'checked': '' }}>
                        Upcoming
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="when" id="radioPast"
                               value="past" {{ $radioWhen == 'past'? 'checked': ''  }}>
                        Past
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="when" id="radioAll"
                               value="" {{ $radioWhen== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <h6><strong>Invoice Status:</strong></h6>
                @php
                    $radioInvoice = '';
                    $radioInvoice = request('invoice');
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="invoice" id="radioActive"
                               value="1" {{ $radioInvoice == '1'? 'checked': '' }}>
                        تمت الفوترة
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="invoice" id="radioInactive"
                               value="0" {{ $radioInvoice == '0'? 'checked': ''  }}>
                        لم تتم الفوترة
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="invoice" id="radioAll"
                               value="" {{ $radioInvoice == ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
            <button type="submit" id="export" class="btn btn-warning">Export to Excel</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $('#clear-fields').on('click', function () {
        var strippedUrl = stripQuery(window.location.href);
        window.location.replace(strippedUrl);
    })
    function stripQuery(url) {
        return url.split("?")[0].split("#")[0];
    }

    $('#search-submit').on('click', function (e) {
        e.preventDefault();
        putQueryString();
        $('#bookings-table').DataTable().ajax.reload();
    })
    function putQueryString() {
        var strippedUrl = stripQuery(window.location.href);
        var newQueryString = $('#booking-filter').serialize();
        var url = strippedUrl + '?' + newQueryString;
        history.replaceState('', 'Bookings', url);
    }

    var start = moment().subtract(29, 'days');
    var end = moment();


    $('#appoinrange, #recordrange').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
    $('#appoinrange, #recordrange').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
    });

    $('#appoinrange, #recordrange').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

</script>

