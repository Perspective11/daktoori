@push('top-stack')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js">
    </script>    <script type="text/javascript" src="/plugins/datepicker/bootstrap-datepicker.js"></script>
@endpush
<div class="row">
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('patient_confirmed') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('patient_confirmed')){
                    $checked = old('patient_confirmed');
                }
                else
                    $checked = $booking->patient_confirmed . '';
            @endphp
            <label for="patient_confirmed">أكد المريض</label>
            <div class="">
                <input type="radio" name="patient_confirmed" id="patient_confirmed-1" class="patient_confirmed-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                نعم <br>
                <input type="radio" name="patient_confirmed" id="patient_confirmed-0" class="patient_confirmed-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                لا
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('is_invoiced') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('is_invoiced')){
                    $checked = old('is_invoiced');
                }
                else
                    $checked = $booking->is_invoiced . '';
            @endphp
            <label for="is_invoiced">تمت الفوترة</label>
            <div class="">
                <input type="radio" name="is_invoiced" id="is_invoiced-1" class="is_invoiced-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                نعم <br>
                <input type="radio" name="is_invoiced" id="is_invoiced-0" class="is_invoiced-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                لا
            </div>
        </div>
    </div>
    <div class='col-sm-3'>
        <div class="form-group {{  $errors->has('scheduled_datetime') ? ' has-error' : ''  }}">
            <label for="bookingStatus">الوقت والتاريخ المتفق عليهم</label>
            <div class='input-group ltr text-body-center datetime' id='scheduled-datetime'>
                <input type="text" name="scheduled_datetime" class="form-control" value="{{ old('scheduled_datetime') ?: optional($booking->scheduled_datetime)->format('Y-m-d H:i a') }}">
                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
        </div>
    </div>
    <div class='col-sm-3'>
        <div class="form-group{{  $errors->has('actual_datetime') ? ' has-error' : ''  }}">
            <label for="bookingStatus">وقت وتاريخ حدوث الحجز</label>
            <div class='input-group ltr text-body-center datetime' id='actual-datetime'>
                <input type="text" name="actual_datetime" class="form-control" value="{{ old('actual_datetime') ?: optional($booking->actual_datetime)->format('Y-m-d H:i a') }}">
                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('bookingStatus') ? ' has-error' : ''  }}">
            <label for="bookingStatus">حالة الحجز</label>
            <select name="bookingStatus" class="form-control select-booking-status" id="bookingStatus">
                <option value=""  {{ $booking->booking_status_id? 'selected' : '' }}></option>
                @foreach($bookingStatuses as $singleBookingStatus)
                    @php
                        $selected = '';
                        if (old('bookingStatus')){
                            if (old('bookingStatus') == $singleBookingStatus->id)
                            $selected = true;
                        }
                        elseif ($booking->booking_status_id == $singleBookingStatus->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $singleBookingStatus->id }}">
                        {{ $singleBookingStatus->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('visitStatus') ? ' has-error' : ''  }}">
            <label for="visitStatus">حالة الزيارة</label>
            <select name="visitStatus" class="form-control select-visit-status" id="visitStatus">
                <option value=""  {{ $booking->visit_status_id? 'selected' : '' }}></option>
                @foreach($visitStatuses as $singleVisitStatus)
                    @php
                        $selected = '';
                        if (old('visitStatus')){
                            if (old('visitStatus') == $singleVisitStatus->id)
                            $selected = true;
                        }
                        elseif ($booking->visit_status_id == $singleVisitStatus->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $singleVisitStatus->id }}">
                        {{ $singleVisitStatus->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('procedure') ? ' has-error' : ''  }}">
            <label for="procedure">نوع الاجراء</label>
            <select name="procedure" class="form-control select-procedure" id="procedure">
                <option value=""  {{ $booking->procedure_id? 'selected' : '' }}></option>
                @foreach($procedures as $singleProcedure)
                    @php
                        $selected = '';
                        if (old('procedure')){
                            if (old('procedure') == $singleProcedure->id)
                            $selected = true;
                        }
                        elseif ($booking->procedure_id == $singleProcedure->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $singleProcedure->id }}">
                        {{ $singleProcedure->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class='col-sm-2'>
        <div class="form-group{{  $errors->has('cost') ? ' has-error' : ''  }}">
            <label for="cost">مبلغ الاجراء</label>
            <div class='input-group' id='cost'>
                <input type="text" name="cost" class="form-control" value="{{ old('cost') ?: $booking->cost }}">
                <span class="input-group-addon">
                        ريال
                    </span>
            </div>
        </div>
    </div>
    <div class='col-sm-2'>
        <div class="form-group{{  $errors->has('patient_review') ? ' has-error' : ''  }}">
            <label for="patient_review">تقييم المريض</label>
            <input type="text" name="patient_review" class="form-control" value="{{ old('patient_review') ?: $booking->patient_review }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <div class="form-group {{  $errors->has('note') ? ' has-error' : ''  }}">
            <label for="note">الملاحظات:</label>
            <textarea class="form-control" name="note" id="note" rows="5">{{ old('note') ?: $booking->note }}</textarea>
        </div>
        
    </div>
</div>
@push('bottom-stack')
    <script type="text/javascript">
        $(".select-booking-status").select2({
            placeholder: "اختر حالة الحجز",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-visit-status").select2({
            placeholder: "اختر حالة الزيارة",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-procedure").select2({
            placeholder: "اختر نوع الاجراء",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-city").select2({
            placeholder: "اختر مدينة",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-region").select2({
            placeholder: "اختر منطقة",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-insurances").select2({
            placeholder: "اختر التأمينات المعتمدة عند الدكتور",
            allowClear: true,
            maximumSelectionLength: 20,
            width: '100%',
            dir: "rtl"
        });
        $('#scheduled-datetime').datetimepicker({
            format: 'YYYY-MM-DD hh:mm a'
        });
        $('#actual-datetime').datetimepicker({
            format: 'YYYY-MM-DD hh:mm a'
        });
    </script>
@endpush