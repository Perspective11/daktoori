@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Bookings</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/bookings/create') }}"><i class="action-button__icon fa fa-plus bg-red"></i></a>
            <p class="action-button__text--hide">Add Booking</p>
        </div>
    </section>
    <div class="container-fluid">
        @include('admin.bookings.filters')
        <table class="table table-striped table-bordered order-column table-hover jdatatable display smaller rtl" id="bookings-table" style="background-color: #fff;">
            <thead>
            <tr>
                <th>id</th>
                <th>اسم المريض</th>
                <th>الدكتور</th>
                <th>هاتف المريض</th>
                <th>التاريخ</th>
                <th>الوقت</th>
                <th>مبلغ الاجراء</th>
                <th>تقييم المريض</th>
                <th>حساب العميل</th>
                <th>المكتب</th>
                <th>التامين</th>
                <th>هاتف الدكتور</th>
                <th>بريد المريض</th>
                <th>متى</th>
                <th>الكشف</th>
                <th>ملاحظة</th>
                <th>المنطقة</th>
                <th>السجل</th>
                <th>الخيارات</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $bookingsTable = $('#bookings-table').DataTable({
                "order": [[ 0, 'desc' ]],
                pageLength: 25,
                "stateSave": true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/bookings/getData")}}',
                    data: function (d) {
                        d.bookingstatus = $.urlParam('bookingstatus');
                        d.visitstatus = $.urlParam('visitstatus');
                        d.when = $.urlParam('when');
                        d.recordrange = $.urlParam('recordrange');
                        d.appoinrange = $.urlParam('appoinrange');
                        d.region = $.urlParam('region');
                        d.feestart = $.urlParam('feestart');
                        d.feeend = $.urlParam('feeend');
                        d.office = $.urlParam('office');
                        d.insurance = $.urlParam('insurance');
                        d.time = $.urlParam('time');
                        d.user = $.urlParam('user');
                        d.patient = $.urlParam('patient');
                        d.patient_phone = $.urlParam('patient_phone');
                        d.invoice = $.urlParam('invoice');

                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'patient_name', name: 'patient_name' },
                    {data: 'doctor', name: 'doctor', sortable: 'no', orderable: 'no', searchable: 'no' },
                    {data: 'mobile', name: 'mobile'},
                    {data: 'appointment_date', name: 'appointment_date'},
                    {data: 'time', name: 'time', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'cost', name: 'cost', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'patient_review', name: 'patient_review', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'customer', name: 'customer', sortable: 'no', orderable: 'no', searchable: 'no', visible:false },
                    {data: 'office', name: 'office', sortable: 'no', orderable: 'no', searchable: 'no', visible:false},
                    {data: 'insurance', name: 'insurance', sortable: 'no', orderable: 'no', searchable: 'no', visible:false},
                    {data: 'doctor_phone', name: 'doctor_phone', sortable: 'no', orderable: 'no', searchable: 'no', visible:false},
                    {data: 'email', name: 'email', visible:false},
                    {data: 'when', name: 'when', sortable: 'no', orderable: 'no', searchable: 'no', visible: false},
                    {data: 'fees', name: 'fee', sortable: 'no', orderable: 'no', searchable: 'no', visible: false},
                    {data: 'note', name: 'note', visible: false},
                    {data: 'region', name: 'region', sortable: 'no', orderable: 'no', searchable: 'no', visible: false},
                    {data: 'created_at', name: 'created_at', visible: false},
                    {data: 'actions', name: 'actions', sortable: 'no', orderable: 'no', searchable: 'no', className: "options-column"},
                ],
            });



            $('#bookings-table tbody').on('click', '.booking-delete', function (e) {
                e.preventDefault();
                var $url = $(this).attr('href');
                deleteBooking($url);
            });

            $('#bookings-table tbody').on('click', '.change-status', function (e) {
                e.preventDefault();
                var $url = $(this).attr('href');
                changeStatus($url);
            });
            $('#bookings-table tbody').on('click', '.change-visit-status', function (e) {
                e.preventDefault();
                var $url = $(this).attr('href');
                changeVisitStatus($url);
            });
            $('#bookings-table tbody').on('click', '.change-procedure', function (e) {
                e.preventDefault();
                var $url = $(this).attr('href');
                changeProcedure($url);
            });

            function deleteBooking($url) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this booking record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: $url,
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("Deleted!", "The record was successfully deleted!", "success");
                                location.reload();
                            },
                            error: function (data) {
                                swal("Oops", data.responseText, "error");
                            }
                        }
                    )
                });
            }

            function changeStatus($url) {
                $.ajax(
                    {
                        type: "post",
                        url: $url,
                        data: {_method: 'post', _token : '{{ csrf_token() }}' },
                        dataType: 'json',
                        success: function(data){
                            swal("Changed!", "The booking status was successfully changed!", "success");
                            location.reload();
                        },
                        error: function (data) {
                            swal("Oops", data.responseText, "error");
                        }
                    }
                )
            }
            function changeVisitStatus($url) {
                $.ajax(
                    {
                        type: "post",
                        url: $url,
                        data: {_method: 'post', _token : '{{ csrf_token() }}' },
                        dataType: 'json',
                        success: function(data){
                            swal("Changed!", "The visit status was successfully changed!", "success");
                            location.reload();
                        },
                        error: function (data) {
                            swal("Oops", data.responseText, "error");
                        }
                    }
                )
            }
            function changeProcedure($url) {
                $.ajax(
                    {
                        type: "post",
                        url: $url,
                        data: {_method: 'post', _token : '{{ csrf_token() }}' },
                        dataType: 'json',
                        success: function(data){
                            swal("Changed!", "The booking procedure was successfully changed!", "success");
                            location.reload();
                        },
                        error: function (data) {
                            swal("Oops", data.responseText, "error");
                        }
                    }
                )
            }
        });
    </script>
@endsection
