@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>Booking Details</h3>
    <div class="">
        <form class="form-inline inline" method="post" action="{{ $booking->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i
                        class="fa fa-times"></i> Delete
            </button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row rtl">
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">بيانات الحجز</h3>
                </div><!-- /.box-header -->
                <div class="box-body col-rtl">
                    <div class="form-group">
                        <label>حالة الحجز:</label>
                        <p>{{optional($bookingStatus)->name_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label>حالة الزيارة:</label>
                        <p>{{optional($visitStatus)->name_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label>تم التأكيد من المريض:</label>
                        <p>{{ $booking->patient_confirmed ? "نعم" : "لا" }}</p>
                    </div>
                    <div class="form-group">
                        <label>تمت الفوترة:</label>
                        <p>{{ $booking->is_invoiced ? "نعم" : "لا" }}</p>
                    </div>
                    <div class="form-group">
                        <label>نوع الاجراء:</label>
                        <p>{{optional($bookingProcedure)->name_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label>مبلغ الاجراء:</label>
                        <p>{{$booking->cost}}</p>
                    </div>
                    <div class="form-group">
                        <label>تقييم المريض:</label>
                        <p>{{$booking->patient_review}}</p>
                    </div>
                    <div class="form-group">
                        <label>الملاحظات:</label>
                        <p>{{$booking->note}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">تاريخ ووقت</h3>
                </div><!-- /.box-header -->
                <div class="box-body col-rtl">
                    <div class="form-group">
                        <label>تاريخ الحجز:</label>
                        <p>{{optional($booking->appointment_date)->toFormattedDateString()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="company_ar">وقت الحجز:</label>
                        <p id="company_ar">{{optional($booking->time)->name_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label>الوقت والتاريخ المتفق عليهم:</label>
                        <p>{{optional($booking->scheduled_datetime)->toDateTimeString()}}</p>
                    </div>
                    <div class="form-group">
                        <label>وقت وتاريخ حدوث الحجز:</label>
                        <p>{{optional($booking->actual_datetime)->toDateTimeString()}}</p>
                    </div>


                    <div class="form-group">
                        <label for="created_at">انشاء السجل:</label>
                        <p id="created_at">{{$booking->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">تعديل السجل:</label>
                        <p id="updated_at">{{$booking->updated_at->diffForHumans()}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">العيادة والدكتور</h3>
                </div><!-- /.box-header -->
                <div class="box-body col-rtl">
                    @if ($office)
                        <div class="form-group">
                            <label>اسم العيادة:</label>
                            <p>{{$office->name_ar}}</p>
                        </div>
                        <div class="form-group">
                            <label>هاتف العيادة:</label>
                            <p>{{ $office->phone }}</p>
                        </div>
                        <div class="form-group">
                            <label for="company_ar">اسم الدكتور:</label>
                            <p id="company_ar">{{ optional($office->doctor)->full_name_ar}}</p>
                        </div>
                        <div class="form-group">
                            <label for="company_ar">هاتف الدكتور:</label>
                            <p id="company_ar">{{ optional($office->doctor)->mobile }}</p>
                        </div>
                    @endif
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">المريض</h3>
                </div><!-- /.box-header -->
                <div class="box-body col-rtl">
                    <div class="form-group">
                        <label>اسم المريض:</label>
                        <p>{{$booking->patient_name}}</p>
                    </div>
                    <div class="form-group">
                        <label>هاتف المريض:</label>
                        <p>{{$booking->mobile}}</p>
                    </div>
                    <div class="form-group">
                        <label>ايميل المريض:</label>
                        <p>{{$booking->email}}</p>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>

    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ $booking->path(true) }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="box box-danger">
                <div class="box-header with-border rtl">
                    <h3 class="box-title">تعديل بيانات الحجز</h3>
                </div><!-- /.box-header -->
                <div class="box-body rtl col-rtl">
                    @include('admin.bookings.form')
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-warning pull-right">تعديل بيانات الحجز</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
    
    <hr>
@endsection

@section('bottom-ex')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this booking',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection