@extends('adminlte::page')
@push('top-stack')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
    <script type="text/javascript" src="/plugins/datepicker/bootstrap-datepicker.js"></script>
@endpush
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ $user->path(true)  }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <section class="FAB">
                <div class="FAB__action-button">
                    <button class="btn-link " type="submit"><i class="action-button__icon bg-orange-active fa fa-floppy-o"></i></button>
                    <p class="action-button__text--hide">Save Changes</p>
                </div>
            </section>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit User</h3>
                    <div class="box-tools text-muted">
                        <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('admin.users.user-form')
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            @if ($user->isDoctor())
                <div class="box box-success doctor-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Doctor Info</h3>
                        <div class="box-tools text-muted">
                            <p>
                                <small>Fields with <span style="color:orangered">*</span> are required</small>
                            </p>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body rtl col-rtl">
                        @include('admin.users.doctor-form')
                        <div class="row">
                            <div class="col-md-12">
                                <label for="doctor_images" class="control-label">صورة الدكتور</label>
                                @include('admin.users.upload-update')
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            @endif
            @if ($user->isCustomer())
                <div class="box box-success customer-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Customer Info</h3>
                        <div class="box-tools text-muted">
                            <p>
                                <small>Fields with <span style="color:orangered">*</span> are required</small>
                            </p>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body rtl col-rtl">
                        @include('admin.users.customer-form')
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            @endif
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
