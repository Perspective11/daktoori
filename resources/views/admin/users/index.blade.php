@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Users</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/users/create') }}"><i class="action-button__icon fa fa-user-plus bg-red"></i></a>
            <p class="action-button__text--hide">Add User</p>
        </div>
    </section>
    <div class="container-fluid">
        @include('admin.users.filters')
        <table class="table table-striped table-bordered order-column table-hover jdatatable display rtl" id="users-table" style="background-color: #fff;">
            <thead>
            <tr>
                <th>Id</th>
                <th>اسم المستخدم</th>
                <th>الاسم</th>
                <th>النوع</th>
                <th>الايميل</th>
                <th>الحالة</th>
                <th>انضم في</th>
                <th>خيارات</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $usersTable = $('#users-table').DataTable({
                "order": [[ 6, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/users/getData")}}',
                    data: function (d) {
                        d.status = $.urlParam('status');

                        d.role = $.urlParam('role');

                        d.user = $.urlParam('user');
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'id', name: 'id', visible: false},
                    {data: 'name', name: 'name'},
                    {data: 'child_name', name: 'child_name', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'role', name: 'role', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'action', sortable: 'no', orderable: 'no', searchable: 'no'},
                ]
            });


            var $modal = $('.action-modal');
            $('#users-table tbody').on('click', 'tr', function () {

                var data = $usersTable.row(this).data();
                $('.action-modal').modal();

            });
            $('#users-table tbody').on('click', '.user-delete', function (e) {
                e.preventDefault();
                $row = $(this).closest('tr');
                var data = $usersTable.row($row).data();
                deleteUser(data['id']);
            });
            function deleteUser(userId) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this user record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: "/admin/users/" + userId,
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("Deleted!", "The record was successfully deleted!", "success");
                                $('#users-table').DataTable().ajax.reload();
                            },
                            error: function (data) {
                                var response = data.responseJSON;
                                swal("Oops", response.msg, "error");
                            }
                        }
                    )
                });
            }


        });
    </script>
@endsection

@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection