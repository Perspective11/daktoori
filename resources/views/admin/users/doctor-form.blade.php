@push('top-stack')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
    <script type="text/javascript" src="/plugins/datepicker/bootstrap-datepicker.js"></script>
@endpush
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('fname_ar') ? ' has-error' : ''  }}">
            <label for="fname_ar">الاسم:</label>
            <input type="text" name="fname_ar" class="form-control" id="fname_ar" value="{{ old('fname_ar') ?: $doctor->fname_ar }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('sname_ar') ? ' has-error' : ''  }}">
            <label for="sname_ar">اسم الأب:</label>
            <input type="text" name="sname_ar" class="form-control" id="sname_ar" value="{{ old('sname_ar') ?: $doctor->sname_ar }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('lname_ar') ? ' has-error' : ''  }}">
            <label for="lname_ar">اللقب:</label>
            <input type="text" name="lname_ar" class="form-control" id="lname_ar" value="{{ old('lname_ar') ?: $doctor->lname_ar }}">
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group {{  $errors->has('title_ar') ? ' has-error' : ''  }}">
            <label for="title_ar">الوصف الوظيفي:</label>
            <input type="text" name="title_ar" class="form-control" id="title_ar" value="{{ old('title_ar') ?: $doctor->title_ar }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('prefix') ? ' has-error' : ''  }}">
            <label for="prefix">بادئة الاسم:</label>
            <input type="text" name="prefix" class="form-control" id="prefix" value="{{ old('prefix') ?: $doctor->prefix }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('office_name') ? ' has-error' : ''  }}">
            <label for="office_name">اسم المكتب:</label>
            <input type="text" name="office_name" class="form-control" id="office_name" value="{{ old('office_name') ?: $office->name_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('mobile') ? ' has-error' : ''  }}">
            <label for="mobile">المحمول:</label>
            <input type="text" name="mobile" class="form-control" id="mobile" value="{{ old('mobile') ?: $doctor->mobile }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('phone') ? ' has-error' : ''  }}">
            <label for="phone">الهاتف:</label>
            <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone') ?: $office->phone }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('whatsapp') ? ' has-error' : ''  }}">
            <label for="whatsapp">الواتس اب:</label>
            <input type="text" name="whatsapp" class="form-control" id="whatsapp" value="{{ old('whatsapp') ?: $doctor->whatsapp }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="form-group {{  $errors->has('grad_university') ? ' has-error' : ''  }}">
            <label for="grad_university">تخرج من جامعة:</label>
            <input type="text" name="grad_university" class="form-control" id="grad_university" value="{{ old('grad_university') ?: $doctor->grad_university }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('grad_country') ? ' has-error' : ''  }}">
            <label for="grad_country">تخرج من دولة:</label>
            <input type="text" name="grad_country" class="form-control" id="grad_country" value="{{ old('grad_country') ?: $doctor->grad_country }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('grad_year') ? ' has-error' : ''  }}">
            <label for="grad_year">تخرج في سنة:</label>
            <input type="text" name="grad_year" class="form-control" id="grad_year" value="{{ old('grad_year') ?: $doctor->grad_year }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group {{ $errors->has('is_approved') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('is_approved')){
                    $checked = old('is_approved');
                }
                else
                    $checked = $doctor->is_approved . '';
            @endphp
            <label for="is_approved">مسموح:</label>
            <div class="">
                <input type="radio" name="is_approved" id="is_approved-1" class="is_approved-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                نعم <br>
                <input type="radio" name="is_approved" id="is_approved-0" class="is_approved-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                لا
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{ $errors->has('is_available') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('is_available')){
                    $checked = old('is_available');
                }
                else
                    $checked = $doctor->is_available . '';
            @endphp
            <label for="is_available">متواجد</label>
            <div class="">
                <input type="radio" name="is_available" id="is_available-1" class="is_available-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                نعم <br>
                <input type="radio" name="is_available" id="is_available-0" class="is_available-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                لا
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('average_waiting_time') ? ' has-error' : ''  }}">
            <label for="average_waiting_time">متوسط وقت الانتظار:</label>
            <input type="number" name="average_waiting_time" class="form-control" id="average_waiting_time" value="{{ old('average_waiting_time') ?: $doctor->average_waiting_time }}"> دقائق
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('fees') ? ' has-error' : ''  }}">
            <label for="fees">الكشف</label>
            <input type="number" name="fees" class="form-control" id="fees" value="{{ old('fees') ?: $office->fees }}"> ريال
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('dob') ? ' has-error' : ''  }}">
            <label for="dob">تاريخ الميلاد:</label>
            <div class="input-group date" data-date-end-date="-18y" data-date-start-date="-100y">
                <input placeholder="اختر التاريخ"
                       type="text"
                       value="{{ old('dob') ?? ($doctor->dob? $doctor->dob->format('Y-m-d') : '') }}"
                       name="dob"
                       id="dob"
                       class="form-control">
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('gender')){
                    $checked = old('gender');
                }
                else
                    $checked = $doctor->gender . '';
            @endphp
            <label for="gender">الجنس:</label>  <span style="color:orangered"></span>
            <div class="">
                <input type="radio" name="gender" id="gender-male" class="gender-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                ذكر <br>
                <input type="radio" name="gender" id="gender-female" class="gender-radio"
                       value="2" {{ $checked === '2'? 'checked' : ''  }}>
                انثى
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('address_ar') ? ' has-error' : ''  }}">
            <label for="address_ar">العنوان:</label>
            <input type="text" name="address_ar" class="form-control" id="address_ar" value="{{ old('address_ar') ?: $doctor->address_ar }}">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group {{  $errors->has('about_ar') ? ' has-error' : ''  }}">
            <label for="about_ar">عن الدكتور:</label>
            <textarea class="form-control" name="about_ar" id="about_ar" rows="5">{{ old('about_ar') ?: $doctor->about_ar }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('city') ? ' has-error' : ''  }}">
            <label for="city">المدينة</label>
            <select name="city" class="form-control select-city" id="city">
                <option value=""  {{ $office->city_id? 'selected' : '' }}></option>
                @foreach($cities as $city)
                    @php
                        $selected = '';
                        if (old('city')){
                            if (old('city') == $city->id)
                            $selected = true;
                        }
                        elseif ($office->city && $office->city->id == $city->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $city->id }}">
                        {{ $city->name_en . ' - ' . $city->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('region') ? ' has-error' : ''  }}">
            <label for="region">المنطقة</label>
            <select name="region" class="form-control select-region" id="region">
                <option value=""  {{ $office->region_id? 'selected' : '' }}></option>
                @foreach($regions as $region)
                    @php
                        $selected = '';
                        if (old('region')){
                            if (old('region') == $region->id)
                            $selected = true;
                        }
                        elseif ($office->region && $office->region->id == $region->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $region->id }}">
                        {{ $region->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group {{  $errors->has('major') ? ' has-error' : ''  }}">
            <label for="major">التخصص الرئيسي</label>
            <select name="major" class="form-control select-major" id="major">
                <option value=""  {{ $doctor->major_id? 'selected' : '' }}></option>
                @foreach($majors as $major)
                    @php
                        $selected = '';
                        if (old('major')){
                            if (old('major') == $major->id)
                            $selected = true;
                        }
                        elseif (optional($doctor->getMajor())->id == $major->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $major->id }}">
                        {{ $major->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('rank') ? ' has-error' : ''  }}">
            <label for="rank">الترتيب</label>
            <div class="">
                <input type="number" name="rank" class="form-control" id="rank" value="{{ old('rank') ?: $doctor->rank ?: 0}}">
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('secondaryMajors') ? ' has-error' : '' }}">
            <label for="secondaryMajors">التخصصات</label>
            <select class="select-secondary-majors form-control" name="secondaryMajors[]" multiple="multiple">
                @foreach($secondaryMajors as $secondaryMajor)
                    @php
                        $selected = '';
                        if (old('secondaryMajors')){
                            if (in_array($secondaryMajor->id, old('secondaryMajors')))
                            $selected = true;
                        }
                        elseif ($doctor->majors->pluck('id')->contains($secondaryMajor->id))
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected ? 'selected': ''}} value="{{ $secondaryMajor->id }}">{{ $secondaryMajor->name_ar }} </option>
                    {{-- TODO: create secondaryMajors on the fly --}}
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('services') ? ' has-error' : '' }}">
            <label for="services">الخدمات</label>
            <select class="select-services form-control" name="services[]" multiple="multiple">
                @foreach($services as $service)
                    @php
                        $selected = '';
                        if (old('services')){
                            if (in_array($service->id, old('services')))
                            $selected = true;
                        }
                        elseif ($office->services->pluck('id')->contains($service->id))
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected ? 'selected': ''}} value="{{ $service->id }}">{{ $service->name_ar }} </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('insurances') ? ' has-error' : '' }}">
            <label for="insurances">التأمينات</label>
            <select class="select-insurances form-control" name="insurances[]" multiple="multiple">
                @foreach($insurances as $insurance)
                    @php
                        $selected = '';
                        if (old('insurances')){
                            if (in_array($insurance->id, old('insurances')))
                            $selected = true;
                        }
                        elseif ($office->insurances->pluck('id')->contains($insurance->id))
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected ? 'selected': ''}} value="{{ $insurance->id }}">{{ $insurance->name_ar }} </option>
                @endforeach
            </select>
        </div>
    </div>
</div>


@push('bottom-stack')
    <script type="text/javascript">
        $(".select-major").select2({
            placeholder: "اختر تخصص",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-city").select2({
            placeholder: "اختر مدينة",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-region").select2({
            placeholder: "اختر منطقة",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-secondary-majors").select2({
            placeholder: "اختر التخصصات الفرعية",
            allowClear: true,
            maximumSelectionLength: 5,
            width: '100%',
            dir: "rtl"
        });
        $(".select-insurances").select2({
            placeholder: "اختر التأمينات المعتمدة عند الدكتور",
            allowClear: true,
            maximumSelectionLength: 20,
            width: '100%',
            dir: "rtl"
        });
        $(".select-services").select2({
            placeholder: "اختر الخدمات التي توفرها عيادة الدكتور",
            allowClear: true,
            maximumSelectionLength: 20,
            width: '100%',
            dir: "rtl",
            tags: true,
            tokenSeparators: [',','-']
        });


    </script>
@endpush