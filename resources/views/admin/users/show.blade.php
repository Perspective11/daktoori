@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>User Details</h3>
    <div class="">
        <a href="{{ $user->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $user->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i class="fa fa-times"></i> Delete</button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        @if($user->isDoctor() && $doctor->getPicture())
            <div class="col-md-3">
                <div class="form-group">
                    <label for="image">Image:</label>
                    <img src="{{$doctor->getPicture()}}"
                         class="img-responsive img-rounded" alt="">
                </div>
            </div>
        @endif
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">User</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Username:</label>
                        <p id="name">{{$user->name}}</p>
                    </div>
                    @if($user->childName())
                        <div class="form-group">
                            <label for="childName">Name:</label>
                            <p id="childName">{{ $user->childName() }}</p>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <p id="email">{{$user->email}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="role">Role:</label>
                        <p id="role">{{$user->getRoleName()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="created_at">Joined At:</label>
                        <p id="created_at">{{$user->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$user->updated_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        @php
                            $class = $user->status ? 'success' : 'danger';
                            $status = $user->status ? 'Deactivate' : 'Activate';
                        @endphp
                        <label for="status">User Status:</label><br>
                        <a href="{{ $user->path(true) . '/activation' }}"
                           class="btn btn-xs post-status btn-{{ $class}}">{{ $status }}</a>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <hr>
    @if($user->isDoctor())
        @include('admin.users.doctor-info')
    @elseif($user->isCustomer())
        @include('admin.users.customer-info')
    @endif
@endsection
@section('bottom-ex')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this user?',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection