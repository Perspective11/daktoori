<h4>Doctor Details</h4>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Doctor Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="full_name">Full Name:</label>
                    <p id="full_name">{{$doctor->getName()}}</p>
                </div>
                <div class="form-group">
                    <label for="office_name">Office Name:</label>
                    <p id="office_name">{{$office->name_ar}}</p>
                </div>
                <div class="form-group">
                    <label for="title_ar">Title:</label>
                    <p id="title_ar">{{$doctor->title_ar}}</p>
                </div>
                <div class="form-group">
                    <label for="major">Major:</label>
                    <p id="major">{{$doctor->majorsString()}}
                    </p>
                </div>
                <div class="form-group">
                    <label for="service">Services:</label>
                    <p id="service">{{$office->servicesString()}}
                    </p>
                </div>
                <div class="form-group">
                    <label for="grad_country">Graduation Country:</label>
                    <p id="grad_country">
                        <span class="label label-info">{{ $doctor->grad_country }}</span>
                    </p>
                </div>
                <div class="form-group">
                    <label for="grad_university">Graduation University:</label>
                    <p id="grad_university">
                        <span class="label label-info">{{ $doctor->grad_university }}</span>
                    </p>
                </div>
                <div class="form-group">
                    <label for="grad_year">Graduation Year:</label>
                    <p id="grad_year">
                        <span class="label label-info">{{ $doctor->grad_year }}</span>
                    </p>
                </div>
                <div class="form-group">
                    <label for="user_status">Approved:</label><br>
                    <span class="label doctor-status label-warning">{{ $doctor->is_approved ? 'Yes' : 'No'}}</span>
                </div>
                <div class="form-group">
                    <label for="available">Available:</label><br>
                    <span class="label doctor-available label-warning">{{ $doctor->is_available ? 'Yes' : 'No'}}</span>
                </div>
                @if ($office->insurances()->count())
                    <div class="form-group">
                        <label for="insurances">Insurances:</label>
                        <p id="insurances">{{$office->insurancesString()}}
                        </p>
                    </div>
                @endif
                @if($office->fees)
                    <div class="form-group">
                        <label for="mobile">Fees:</label>
                        <p id="fees">{{$office->fees}}</p>
                    </div>
                @endif
                @if($doctor->average_waiting_time)
                    <div class="form-group">
                        <label for="mobile">Average Waiting Time:</label>
                        <p id="average_waiting_time">{{$doctor->getOffice()->average_waiting_time}}</p>
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Personal Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if($doctor->mobile)
                    <div class="form-group">
                        <label for="mobile">Mobile No:</label>
                        <p id="mobile">{{$doctor->mobile}}</p>
                    </div>
                @endif
                @if($office->phone)
                    <div class="form-group">
                        <label for="phone">Phone No:</label>
                        <p id="phone">{{$office->phone}}</p>
                    </div>
                @endif
                @if($doctor->whatsapp)
                    <div class="form-group">
                        <label for="whatsapp">Whatsapp No:</label>
                        <p id="whatsapp">{{$doctor->whatsapp}}</p>
                    </div>
                @endif
                @if($doctor->dob)
                    <div class="form-group">
                        <label for="age">Age:</label>
                        <p id="age">
                            <span class='label label-warning label-lg'>{{$doctor->age()}}</span>
                        </p>
                    </div>
                @endif
                @if($doctor->gender !== null)
                    <div class="form-group">
                        @php
                            $gender = $doctor->gender == 1 ? 'male': 'female';
                            $color = $doctor->gender == 1 ? 'lightblue': 'pink';
                        @endphp
                        <label for="gender">Gender:</label>
                        <p id="gender">
                            <span style="color:{{$color}}" class="fa-2x fa fa-{{$gender}}"></span>
                        </p>
                    </div>
                @endif
                @if($doctor->address_ar)
                    <div class="form-group">
                        <label for="address_ar">Address:</label>
                        <p id="address_ar">{{$doctor->address_ar}}</p>
                    </div>
                @endif
                @if($doctor->about_ar)
                    <div class="form-group">
                        <label for="about_ar">About:</label>
                        <p id="about_ar">{{$doctor->about_ar}}</p>
                    </div>
                @endif
                @if($doctor->city)
                    <div class="form-group">
                        <label for="city">City:</label>
                        <p id="city">{{$office->city->name_ar}}</p>
                    </div>
                @endif
                @if($doctor->region)
                    <div class="form-group">
                        <label for="region">Region:</label>
                        <p id="region">{{$office->region->name_ar}}</p>
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Activity</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="list-group">
                    <a href="{{ url('/admin/posts?user=' . $user->name) }}" class="list-group-item">
                        <span class="badge">{{ $user->posts()->count() }}</span>
                        Posts by this user
                    </a>
                    <a href="{{ url('/admin/users?user=' . $doctor->getName()) }}" class="list-group-item">
                        <span class="badge">{{ $doctor->getCustomers()->count() }}</span>
                        Customers by this doctor
                    </a>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
</div>
</div>
