<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('customer_fname_ar') ? ' has-error' : ''  }}">
            <label for="customer_fname_ar">الاسم:</label>
            <input type="text" name="customer_fname_ar" class="form-control" id="customer_fname_ar" value="{{ old('customer_fname_ar') ?: $customer->fname_ar }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('customer_sname_ar') ? ' has-error' : ''  }}">
            <label for="customer_sname_ar">اسم الأب:</label>
            <input type="text" name="customer_sname_ar" class="form-control" id="customer_sname_ar" value="{{ old('customer_sname_ar') ?: $customer->sname_ar }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('customer_lname_ar') ? ' has-error' : ''  }}">
            <label for="customer_lname_ar">اللقب:</label>
            <input type="text" name="customer_lname_ar" class="form-control" id="customer_lname_ar" value="{{ old('customer_lname_ar') ?: $customer->lname_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('customer_mobile') ? ' has-error' : ''  }}">
            <label for="customer_mobile">المحمول:</label>
            <input type="text" name="customer_mobile" class="form-control" id="customer_mobile" value="{{ old('customer_mobile') ?: $customer->mobile }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('customer_phone') ? ' has-error' : ''  }}">
            <label for="customer_phone">الهاتف:</label>
            <input type="text" name="customer_phone" class="form-control" id="customer_phone" value="{{ old('customer_phone') ?: $customer->phone }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('customer_dob') ? ' has-error' : ''  }}">
            <label for="customer_dob">تاريخ الميلاد:</label>
            <div class="input-group date" data-date-end-date="-18y" data-date-start-date="-100y">
                <input placeholder="اختر التاريخ"
                       type="text"
                       value="{{ old('customer_dob') ?? ($customer->dob? $customer->dob->format('Y-m-d') : '') }}"
                       name="customer_dob"
                       id="customer_dob"
                       class="form-control">
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{ $errors->has('customer_gender') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('customer_gender')){
                    $checked = old('customer_gender');
                }
                else
                    $checked = $customer->gender . '';
            @endphp
            <label for="customer_gender">الجنس:</label>  <span style="color:orangered">*</span>
            <div class="">
                <input type="radio" name="customer_gender" id="customer_gender-male" class="customer_gender-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                ذكر <br>
                <input type="radio" name="customer_gender" id="customer_gender-female" class="customer_gender-radio"
                       value="2" {{ $checked === '0'? 'checked' : ''  }}>
                انثى
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('customer_address_ar') ? ' has-error' : ''  }}">
            <label for="customer_address_ar">العنوان:</label>
            <input type="text" name="customer_address_ar" class="form-control" id="customer_address_ar" value="{{ old('customer_address_ar') ?: $customer->address_ar }}">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group {{  $errors->has('note') ? ' has-error' : ''  }}">
            <label for="note">ملاحظات:</label>
            <textarea class="form-control" name="note" id="note" rows="5">{{ old('note') ?: $customer->note }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('customer_city') ? ' has-error' : ''  }}">
            <label for="customer_city">المدينة</label>
            <select name="customer_city" class="form-control select-city-customer" id="customer_city">
                <option value=""  {{ $customer->city_id? 'selected' : '' }}></option>
                @foreach($cities as $city)
                    @php
                        $selected = '';
                        if (old('customer_city')){
                            if (old('customer_city') == $city->id)
                            $selected = true;
                        }
                        elseif ($customer->city && $customer->city->id == $city->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $city->id }}">
                        {{ $city->name_en . ' - ' . $city->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('customer_region') ? ' has-error' : ''  }}">
            <label for="customer_region">المنطقة</label>
            <select name="customer_region" class="form-control select-region-customer" id="customer_region">
                <option value=""  {{ $customer->region_id? 'selected' : '' }}></option>
                @foreach($cities as $region)
                    @php
                        $selected = '';
                        if (old('customer_region')){
                            if (old('customer_region') == $region->id)
                            $selected = true;
                        }
                        elseif ($customer->region && $customer->region->id == $region->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $region->id }}">
                        {{ $region->name_en . ' - ' . $region->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
@push('bottom-stack')
    <script type="text/javascript">
        $(".select-city-customer").select2({
            placeholder: "اختر مدينة",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
        $(".select-region-customer").select2({
            placeholder: "اختر منطقة",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });

    </script>
@endpush

