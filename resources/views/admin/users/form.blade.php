<input type="hidden" name="id" value="{{ $user->id }}">
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name') ? ' has-error' : ''  }}">
            <label for="name">Username:</label> <span style="color:orangered">*</span>
            <input type="text" name="name" class="form-control" id="name" value="{{ old('name') ?: $user->name }}"
                   required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('email') ? ' has-error' : ''  }}">
            <label for="email">Email Address:</label> <span style="color:orangered">*</span>
            <input type="email" name="email" class="form-control" id="email" value="{{ old('email') ?: $user->email }}"
                   required>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-md-6">
            <div class="form-group {{  $errors->has('password') ? ' has-error' : ''  }}">
                <label for="password">Password:</label> <span style="color:orangered">*</span>
                <input type="password" name="password" class="form-control" id="password" placeholder="{{ $user->password? '(Unchanged)' : '' }}" >
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group {{  $errors->has('password_confirmation') ? ' has-error' : ''  }}">
                <label for="password_confirmation">Confirm Password:</label> <span style="color:orangered">*</span>
                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="{{ $user->password? '(Unchanged)' : '' }}">
            </div>
        </div>
</div>
