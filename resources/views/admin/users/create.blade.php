@extends('adminlte::page')
@push('top-stack')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
    <script type="text/javascript" src="/plugins/datepicker/bootstrap-datepicker.js"></script>
@endpush
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ url('admin/users') }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="temp" value="{{ $temp_token }}">
            <section class="FAB">
                <div class="FAB__action-button">
                    <button class="btn-link" type="submit"><i class="action-button__icon bg-green fa fa-user-plus"></i>
                    </button>
                    <p class="action-button__text--hide">Create User</p>
                </div>
            </section>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Create User</h3>
                    <div class="box-tools text-muted">
                        <p>
                            <small>Fields with <span style="color:orangered">*</span> are required</small>
                        </p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('admin.users.user-form',[
                    'user' => new App\User,
                    'doctor' => new App\Doctor,
                    'customer' => new App\Customer
                    ])
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box box-success doctor-box" style="display: none">
                <div class="box-header with-border">
                    <h3 class="box-title">Doctor Info</h3>
                    <div class="box-tools text-muted">
                        <p>
                            <small>Fields with <span style="color:orangered">*</span> are required</small>
                        </p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body rtl col-rtl">
                    @include('admin.users.doctor-form',[
                    'user' => new App\User,
                    'doctor' => new App\Doctor,
                    'customer' => new App\Customer,
                    'office' => new App\Office
                    ])
                    <div class="row">
                        <div class="col-md-12">
                            <label for="doctor_images" class="control-label">صورة الدكتور</label>
                            @include('admin.users.upload-store')
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box box-success customer-box"style="display: none">
                <div class="box-header with-border">
                    <h3 class="box-title">Customer Info</h3>
                    <div class="box-tools text-muted">
                        <p>
                            <small>Fields with <span style="color:orangered">*</span> are required</small>
                        </p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body rtl col-rtl">
                    @include('admin.users.customer-form',[
                    'user' => new App\User,
                    'doctor' => new App\Doctor,
                    'customer' => new App\Customer
                    ])
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection

@push('bottom-stack')
    <script>
        $('.FAB__action-button').hover(function () {
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function () {
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });

        if ($('#user-type-doctor').is(':checked')) {
            $('.doctor-box').show();
            $('.customer-box').hide();
        } else if ($('#user-type-customer').is(':checked')) {
            $('.doctor-box').hide();
            $('.customer-box').show();
        }

        $('#user-type-doctor').click(function () {
            $('.doctor-box').slideDown();
            $('.customer-box').slideUp();
        });

        $('#user-type-customer').click(function () {
            $('.customer-box').slideDown();
            $('.doctor-box').slideUp();
        });
        $('#user-type-admin').click(function () {
            $('.customer-box').slideUp();
            $('.doctor-box').slideUp();
        });
    </script>
@endpush