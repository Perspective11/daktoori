<h4>Customer Details</h4>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Customer Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="full_name">Full Name:</label>
                    <p id="full_name">{{$customer->getName()}}</p>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Personal Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if($customer->mobile)
                    <div class="form-group">
                        <label for="mobile">Mobile No:</label>
                        <p id="mobile">{{$customer->mobile}}</p>
                    </div>
                @endif
                @if($customer->whatsapp)
                    <div class="form-group">
                        <label for="whatsapp">Whatsapp No:</label>
                        <p id="whatsapp">{{$customer->whatsapp}}</p>
                    </div>
                @endif
                @if($customer->dob)
                    <div class="form-group">
                        <label for="age">Age:</label>
                        <p id="age">
                            <span class='label label-warning label-lg'>{{$customer->age()}}</span>
                        </p>
                    </div>
                @endif
                @if($customer->gender !== null)
                    <div class="form-group">
                        @php
                            $gender = $customer->gender === 1 ? 'male': 'female';
                            $color = $customer->gender === 1 ? 'lightblue': 'pink';
                        @endphp
                        <label for="gender">Gender:</label>
                        <p id="gender">
                            <span style="color:{{$color}}" class="fa-2x fa fa-{{$gender}}"></span>
                        </p>
                    </div>
                @endif
                @if($customer->address_ar)
                    <div class="form-group">
                        <label for="address_ar">Address:</label>
                        <p id="address_ar">{{$customer->address_ar}}</p>
                    </div>
                @endif
                @if($customer->city)
                    <div class="form-group">
                        <label for="city">City:</label>
                        <p id="city">{{$customer->city->name_ar}}</p>
                    </div>
                @endif
                @if($customer->region)
                    <div class="form-group">
                        <label for="region">Region:</label>
                        <p id="region">{{$customer->region->name_ar}}</p>
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Activity</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="list-group">
                    <a href="{{ url('/admin/posts?user=' . $customer->getName()) }}" class="list-group-item">
                        <span class="badge">{{ $user->posts()->count() }}</span>
                        Posts by this user
                    </a>
                    <a href="{{ url('/admin/users?user=' . $customer->getName()) }}" class="list-group-item">
                        <span class="badge">{{ $customer->getDoctors()->count() }}</span>
                        Doctors by this customer
                    </a>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
</div>
</div>
