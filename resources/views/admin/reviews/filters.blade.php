<form id="reviews-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Review Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-2">
                <h5><strong>Review Status:</strong></h5>
                @php
                    $radioStatus = '';
                    if (request()->has('approval')){
                        $radioStatus = request('approval');
                    }
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="approval" id="radioActive"
                               value="approved" {{ $radioStatus == 'approved'? 'checked': '' }}>
                        Approved
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="approval" id="radioInactive"
                               value="disapproved" {{ $radioStatus == 'disapproved'? 'checked': ''  }}>
                        Disapproved
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="approval" id="radioAll"
                               value="" {{ $radioStatus== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <h5><strong>Review Type:</strong></h5>
                @php
                    $radioType = '';
                    if (request()->has('type')){
                        $radioType = request('type');
                    }
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="type" id="radioActive"
                               value="featured" {{ $radioType == 'featured'? 'checked': '' }}>
                        Featured
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="type" id="radioInactive"
                               value="unfeatured" {{ $radioType == 'unfeatured'? 'checked': ''  }}>
                        Unfeatured
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="type" id="radioAll"
                               value="" {{ $radioType== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <h5><strong>Date Range:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="daterange" id="reportrange"
                           value="{{ request('daterange')? request('daterange') : '' }}">
                </div>
            </div>
            <div class="col-md-3">
                <h5><strong>Doctors and Customers:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="user" value="{{ request('user')? request('user') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                <h5><strong>Rating Range:</strong></h5>
                <div class="form-group range-form-group">
                    <input type="number" class="form-control" name="ratingstart"
                           value="{{ request('ratingstart')? request('ratingstart') : '' }}"> to
                    <input type="number" class="form-control" name="ratingend"
                           value="{{ request('ratingend')? request('ratingend') : '' }}">
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $(function () {

        $('#clear-fields').on('click', function () {
            var strippedUrl = stripQuery(window.location.href);
            window.location.replace(strippedUrl);
        });

        function stripQuery(url) {
            return url.split("?")[0].split("#")[0];
        }

        $('#search-submit').on('click', function (e) {
            e.preventDefault();
            putQueryString();
            $('#reviews-table').DataTable().ajax.reload();
        })
        function putQueryString() {
            var strippedUrl = stripQuery(window.location.href);
            var newQueryString = $('#reviews-filter').serialize();
            var url = strippedUrl + '?' + newQueryString;
            history.replaceState('', 'Reviews', url);
        }


        var start = moment().subtract(29, 'days');
        var end = moment();


        $('#reportrange').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            },
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
        });

        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
    });

</script>