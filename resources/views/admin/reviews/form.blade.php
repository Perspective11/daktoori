@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection
<div class="row">
    <div class="col-md-12 rtl">
        <div class="form-group {{  $errors->has('doctor') ? ' has-error' : ''  }}">
            <label for="doctor">الدكتور: <span style="color:orangered">*</span></label>
            <select name="doctor" class="form-control select-doctor" id="doctor">
                <option value=""  {{ $doctor_id? 'selected' : '' }}></option>
                @foreach($doctors as $doctor)
                    @php
                        $selected = '';
                        if (old('doctor')){
                            if (old('doctor') == $doctor->id)
                            $selected = true;
                        }
                        elseif ($doctor_id == $doctor->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $doctor->id }}">
                        {{ $doctor->full_name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-12 rtl">
        <div class="form-group {{  $errors->has('patient_name') ? ' has-error' : ''  }}">
            <label for="patient_name">اسم المريض: <span style="color:orangered">*</span></label>
            <input type="text" name="patient_name" class="form-control" id="patient_name" value="{{ old('patient_name') ?: $review->patient_name }}">
        </div>
    </div>
    <div class="col-md-12 rtl">
        <div class="form-group {{  $errors->has('rating') ? ' has-error' : ''  }}">
            <label for="rating">التقييم (من 1 الى 5): <span style="color:orangered">*</span></label>
            <input type="text" name="rating" class="form-control" id="rating" value="{{ old('rating') ?: $review->rating }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 rtl">
        <div class="form-group{{  $errors->has('review') ? ' has-error' : ''  }}">
            <label for="review">نص التقييم:</label>
            <textarea class="form-control" name="review" id="review" rows="5">{{ old('review') ?: $review->review }}</textarea>
        </div>
    </div>
    
</div>
@section('bottom-ex')
    <script type="text/javascript">
        $(".select-doctor").select2({
            placeholder: "اختر الدكتور",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
    </script>
@endsection