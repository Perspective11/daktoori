@extends('adminlte::page')
@section('top-ex')
    <!-- DataTables -->
    @include('links.datatables')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('content_header')
    <h3>Manage reviews</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/reviews/create') }}"><i class="action-button__icon fa bg-red fa-plus"></i></a>
            <p class="action-button__text--hide">Add review</p>
        </div>
    </section>
    @include('admin.reviews.filters')
    <table class="table table-striped table-bordered order-column table-hover jdatatable display rtl" id="reviews-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>ID</th>
            <th>الدكتور</th>
            <th>المريض</th>
            <th>التقييم</th>
            <th>التعليق</th>
            <th>التاريخ</th>
            <th>خيارات</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $reviewsTable = $('#reviews-table').DataTable({
                "order": [[ 5, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/reviews/getData")}}',
                    data: function (d) {
                        d.daterange = $.urlParam('daterange');
                        d.type = $.urlParam('type');
                        d.approval = $.urlParam('approval');
                        d.ratingstart = $.urlParam('ratingstart');
                        d.ratingend = $.urlParam('ratingend');
                        d.user = $.urlParam('user');
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],

                columns: [
                    {data: 'id', name: 'id', visible: false},
                    {data: 'doctor', name: 'doctor'},
                    {data: 'customer', name: 'customer'},
                    {data: 'rating', name: 'rating'},
                    {data: 'review', name: 'review', 'visible': false},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', sortable: 'no', orderable: 'no', searchable: 'no'}
                ],
            });


            $('#reviews-table tbody').on('click', 'a.btn', function (e) {
                e.stopPropagation();
            });
            $('#reviews-table tbody').on('click', '.review-delete', function (e) {
                e.preventDefault();
                $row = $(this).closest('tr');
                var data = $reviewsTable.row($row).data();
                deleteReview(data['id']);
            });

            function deleteReview(reviewid) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this review record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: "/admin/reviews/" + reviewid,
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("Deleted!", "The record was successfully deleted!", "success");
                                $('#reviews-table').DataTable().ajax.reload();
                            },
                            error: function (data) {
                                swal("Oops", data.responseText, "error");
                            }
                        }
                    )
                });
            }

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this review?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection