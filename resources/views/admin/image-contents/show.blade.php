
@extends('adminlte::page')
@section('content_header')
    <h3>View Image Content</h3>
@endsection
@section('content')

    <ul class="nav nav-tabs justify-content-around">
        @foreach ($imageContents->pluck('image_category')->unique() as $category)
            <li class="nav-item"><a class="nav-link" href="{{ '#' . $category }}">{{ $category }}</a></li>
        @endforeach
    </ul>
    <form action="{{ url('/admin/image-contents/') }}" method="post" class="edit-image-contents" enctype="multipart/form-data">
        {{ csrf_field() }}
        @php
        $counter = 0;
        @endphp
        @foreach($imageContents->groupBy('image_category') as $groupKey => $groupedImageContents)
        <hr>
        <div id="{{ $groupKey }}" class="text-center"><h2>{{ $groupKey }}</h2></div>
    @foreach ($groupedImageContents as $imageContent)
        <div class="row">
            <div class="col-md-12">
                <div class="card card-danger">
                    <div class="card-header with-border">
                        <h3 class="card-title">{{ $imageContent->name }}</h3>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <img src="{{ $imageContent->image_path }}" alt="" class="img-fluid image-preview">
                            </div>
                            <div class="col-md-2 image-info">
                                <h3>Info</h3>
                                @if ($imageContent->description)
                                    <div class="form-group">
                                        <label for="description">Description:</label>
                                        <p id="description">{{ $imageContent->description }}</p>
                                    </div>
                                @endif
                                @if ($imageContent->resolution_x || $imageContent->resolution_y)
                                    <div class="form-group">
                                        <label for="resolution">Preferred Size:</label>
                                        <p id="resolution">{{ $imageContent->resolution_x . ' X ' . $imageContent->resolution_y }}</p>
                                    </div>
                                @endif
                                @if ($imageContent->aspect_ratio)
                                    <div class="form-group">
                                        <label for="aspect_ratio">Preferred Aspect Ratio:</label>
                                        <p id="aspect_ratio">{{ $imageContent->aspect_ratio }}</p>
                                    </div>
                                @endif
                                @if ($imageContent->type)
                                    <div class="form-group">
                                        <label for="type">Type:</label>
                                        <p id="type">{{ $imageContent->type }}</p>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="image_path">Update Image:</label>
                                    <input type="file" name="updated_path[{{ $counter }}]" class="content-upload">
                                    <input type="hidden" name="key[{{ $counter ++  }}]" value="{{ $imageContent->key }}" class="content-upload">
                                </div>
                            </div>
                        </div>
                    </div><!-- /.card-body -->
                </div><!-- /.card -->
            </div>
        </div>
    @endforeach
    @endforeach
        @include('errors.errors')
        <section class="FAB">
            <div class="FAB__action-button">
                <button type="submit" class="btn-link"><i class="action-button__icon fa fa-edit"></i></button>
                <p class="action-button__text--hide">Submit Images</p>
            </div>
        </section>
    </form>

@endsection
@section('bottom-ex')
    <script>
        $('.FAB__action-button').hover(function(){
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function(){
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });

        function readURL(input, Jinput) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    Jinput.closest('.card-body').find('.image-preview').first().attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".content-upload").change(function() {
            readURL(this , $(this));
        });
    </script>
@endsection
