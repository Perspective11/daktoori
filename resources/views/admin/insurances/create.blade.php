@extends('adminlte::page')
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ url('admin/insurances') }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Insurance</h3>
                    <div class="box-tools text-muted">
                        <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body rtl col-rtl">
                    @include('admin.insurances.form',[
                    'insurance' => new App\Insurance
                    ])
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Create Insurance</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
