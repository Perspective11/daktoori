@section('top-ex')
@endsection
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $insurance->name_ar }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('company_ar') ? ' has-error' : ''  }}">
            <label for="company_ar">اسم الشركة:</label>
            <input type="text" name="company_ar" class="form-control" id="company_ar" value="{{ old('company_ar') ?: $insurance->company_ar }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('address_ar') ? ' has-error' : ''  }}">
            <label for="address_ar">العنوان:</label>
            <input type="text" name="address_ar" class="form-control" id="address_ar" value="{{ old('address_ar') ?: $insurance->address_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('phone') ? ' has-error' : ''  }}">
            <label for="phone">الهاتف:</label>
            <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone') ?: $insurance->phone }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group {{  $errors->has('description_ar') ? ' has-error' : ''  }}">
            <label for="description_ar">الوصف:</label>
            <textarea class="form-control" name="description_ar" id="description_ar" rows="5">{{ old('description_ar') ?: $insurance->description_ar }}</textarea>
        </div>
    </div>
</div>
@section('bottom-ex')
    <script type="text/javascript">
    </script>
@endsection