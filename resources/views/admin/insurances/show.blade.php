@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>Insurance Details</h3>
    <div class="">
        <a href="{{ $insurance->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i>
            Edit</a>
        <form class="form-inline inline" method="post" action="{{ $insurance->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i
                        class="fa fa-times"></i> Delete
            </button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="created_at">Created At At:</label>
                        <p id="created_at">{{$insurance->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$insurance->updated_at->diffForHumans()}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Insurance</h3>
                </div><!-- /.box-header -->
                <div class="box-body rtl col-rtl">
                    <div class="form-group">
                        <label for="name_ar">الاسم:</label>
                        <p id="name_ar">{{$insurance->name_ar}}</p>
                    </div>
                    @if ($insurance->company_ar)
                        <div class="form-group">
                            <label for="company_ar">الشركة:</label>
                            <p id="company_ar">{{$insurance->company_ar}}</p>
                        </div>
                    @endif
                    @if ($insurance->address_ar)
                        <div class="form-group">
                            <label for="address_ar">العنوان:</label>
                            <p id="address_ar">{{$insurance->address_ar}}</p>
                        </div>
                    @endif
                    @if ($insurance->phone)
                        <div class="form-group">
                            <label for="phone">الهاتف:</label>
                            <p id="phone">{{$insurance->phone}}</p>
                        </div>
                    @endif
                    @if ($insurance->description)
                        <div class="form-group">
                            <label for="phone">الوصف:</label>
                            <p id="phone">{{$insurance->description}}</p>
                        </div>
                    @endif
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>
    <hr>
@endsection

@section('bottom-ex')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this insurance',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection