@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Insurances</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/insurances/create') }}"><i class="action-button__icon fa fa-plus bg-red"></i></a>
            <p class="action-button__text--hide">Add Insurance</p>
        </div>
    </section>
    <div class="container-fluid">

        <table class="table table-striped table-bordered order-column table-hover jdatatable display smaller rtl" id="insurances-table" style="background-color: #fff;">
            <thead>
            <tr>
                <th>id</th>
                <th>الاسم</th>
                <th>الشركة</th>
                <th>العنوان</th>
                <th>الهاتف</th>
                <th>الوصف</th>
                <th>الاطباء</th>
                <th>تاريخ</th>
                <th>الخيارات</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $insurancesTable = $('#insurances-table').DataTable({
                "order": [[ 3, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/insurances/getData")}}',
                    data: function (d) {
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'id', name: 'id', visible: false},
                    {data: 'name_ar', name: 'name_ar'},
                    {data: 'company_ar', name: 'company_ar'},
                    {data: 'address_ar', name: 'address_ar'},
                    {data: 'phone', name: 'phone', visible: false},
                    {data: 'description_ar', name: 'description_ar', visible: false},
                    {data: 'doctors_count', name: 'doctors_count', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', sortable: 'no', orderable: 'no', searchable: 'no'},
                ],
            });

            $('#insurances-table tbody').on('click', '.btn', function (e) {
                e.stopPropagation();
            });

            $('#insurances-table tbody').on('click', '.insurance-delete', function (e) {
                e.preventDefault();
                $row = $(this).closest('tr');
                var data = $insurancesTable.row($row).data();
                console.log($row);
                deleteInsurance(data['id']);
            });

            var $modal = $('.action-modal');
            $('#insurances-table tbody').on('click', 'tr', function () {
                var data = $insurancesTable.row(this).data();
                $('.action-modal').modal();
                $modal.find('.delete-insurance').attr('action' , '/admin/insurances/' + data['id']);

            });

            function deleteInsurance(insuranceid) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this insurance record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: "/admin/insurances/" + insuranceid,
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("Deleted!", "The record was successfully deleted!", "success");
                                $('#insurances-table').DataTable().ajax.reload();
                            },
                            error: function (data) {
                                var response = data.responseJSON;
                                swal("Oops", response.msg, "error");
                            }
                        }
                    )
                });
            }


        });
    </script>
@endsection

@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection