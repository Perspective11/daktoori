<form id="user-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">User Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <h5><strong>User Status:</strong></h5>
                @php
                    $radioStatus = '';
                    $radioStatus = request('status');
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioActive"
                               value="active" {{ $radioStatus == 'active'? 'checked': '' }}>
                        Activated
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioInactive"
                               value="inactive" {{ $radioStatus == 'inactive'? 'checked': ''  }}>
                        Deactivated
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioAll"
                               value="" {{ $radioStatus== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                @php
                    $comboRole = '';
                    $comboRole = request('role');
                    $roles = \App\Role::whereHas('users')->get();
                @endphp
                <h5><strong>User Role:</strong></h5>
                <select name="role" class="form-control" id="role">
                    <option {{ $comboRole== ''? 'selected': '' }} value="">All</option>
                    @foreach ($roles as $role)
                        <option {{ $comboRole == $role->key ? 'selected': '' }} value="{{ $role->key }}">{{$role->name_en}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <h5><strong>User:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="user" value="{{ request('user')? request('user') : '' }}">
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $('#clear-fields').on('click', function () {
        var strippedUrl = stripQuery(window.location.href);
        window.location.replace(strippedUrl);
    })
    function stripQuery(url) {
        return url.split("?")[0].split("#")[0];
    }

    $('#search-submit').on('click', function (e) {
        e.preventDefault();
        putQueryString();
        $('#users-table').DataTable().ajax.reload();
    })
    function putQueryString() {
        var strippedUrl = stripQuery(window.location.href);
        var newQueryString = $('#user-filter').serialize();
        var url = strippedUrl + '?' + newQueryString;
        history.replaceState('', 'Users', url);
    }
</script>

