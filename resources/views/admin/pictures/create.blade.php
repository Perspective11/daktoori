@extends('adminlte::page')
@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endsection
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" id="pictures-form" action="{{ url('admin/pictures') }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Picture</h3>
                    <div class="box-tools text-muted">
                        <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{  $errors->has('name') ? ' has-error' : ''  }}">
                                <label for="name">Name:</label> <span style="color:orangered">*</span>
                                <input type="text" name="name" class="form-control" id="name" value="{{ old('name') ?: $picture->name }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{  $errors->has('tag') ? ' has-error' : ''  }}">
                                <label for="tag">Tag</label>
                                <select name="tag" class="form-control select-tag" id="tag">
                                    @foreach($tags as $tag)
                                        @php
                                            $selected = '';
                                            if (old('tag')){
                                                if (old('tag') == $tag->id)
                                                $selected = true;
                                            }
                                            elseif ($picture->tags &&  $tag->id  == $picture->getTagId())
                                                $selected = true;
                                            elseif (request()->filled('cat') &&  $tag->id  == request('cat'))
                                                $selected = true;
                                            else $selected = false;
                                        @endphp
                                        <option value=""></option>
                                        <option {{ $selected? 'selected' : '' }}
                                                value="{{ $tag->id }}">
                                            {{ $tag->name_en . ' - ' . $tag->name_ar }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="event_images" class="control-label">Event Images</label>
                            @include('admin.pictures.upload')
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" id="submit" class="btn btn-info pull-right">Update Info</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
@section('bottom-ex')
    <script type="text/javascript">
        $(".select-tag").select2({
            allowClear: true,
            placeholder: "Select a Tag",
        });

    </script>
@endsection
