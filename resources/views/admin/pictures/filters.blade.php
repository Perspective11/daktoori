<form id="user-filter" action="" method="get">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Picture Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-2">
                @php
                    $combotag = '';
                    $tagsArray =  \App\Tag::has('pictures')->pluck('name_en')->toArray();
                    if (request()->has('tag')){
                        if (in_array(request('tag'), $tagsArray))
                            $combotag = request('tag');
                    }
                @endphp
                <h5><strong>Tag:</strong></h5>
                <select name="tag" class="form-control" id="tag">
                    <option {{ $combotag== ''? 'selected': '' }} value="">All</option>
                    @foreach($tagsArray as $tag)
                        <option {{ $combotag== $tag? 'selected': '' }} value="{{ $tag }}">{{ $tag }}</option>
                    @endforeach
                </select>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-info">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $('#clear-fields').on('click', function () {
        var strippedUrl = stripQuery(window.location.href);
        window.location.replace(strippedUrl);
    })
    function stripQuery(url) {
        return url.split("?")[0].split("#")[0];
    }
</script>