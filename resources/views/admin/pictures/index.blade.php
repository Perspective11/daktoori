@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Manage Pictures</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/pictures/create') }}"><i class="action-button__icon fa bg-red fa-plus"></i></a>
            <p class="action-button__text--hide">Add Pictures</p>
        </div>
    </section>
    @include('admin.pictures.filters')
    <table class="table table-striped table-bordered order-column table-hover jdatatable display small" id="pictures-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Image</th>
            <th>Name</th>
            <th>Tag</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $picturesTable = $('#pictures-table').DataTable({
                "order": [[ 3, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/pictures/getData")}}',
                    data: function (d) {
                        d.tag = '{{ request('tag')? request('tag') : ''}}'
                    }
                },

                columns: [
                    {data: 'picture', name: 'picture', sortable: 'no', orderable: 'no', searchable: 'no', width: "40%"},
                    {data: 'name', name: 'name'},
                    {data: 'tag', name: 'tag', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'},
                ],

            });

            var $modal = $('.action-modal');
            $('#pictures-table tbody').on('click', 'tr', function () {

                var data = $picturesTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-picture-picture').html(data['picture']);
                $modal.find('.modal-picture-name').text(data['name']);
                $modal.find('.modal-picture-tag').text(data['tag']);
                $modal.find('.modal-picture-postedAt').text(data['created_at']);
                $modal.find('.view-picture-details').attr('href' , '/admin/pictures/' + data['id']);
                $modal.find('.edit-picture').attr('href' , '/admin/pictures/create?picture=' + data['id']);
                $modal.find('.delete-picture').attr('action' , '/admin/pictures/' + data['id']);
                if (data['tags'].length) {
                    $modal.find('.view-pictures-by-tag').show().attr('href' , '/admin/pictures/create?cat=' + data['tags'][0].id);
                }else {
                    $modal.find('.view-pictures-by-tag').hide();
                }
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this picture?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="modal-picture-picture"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Name:</strong> <span class="modal-picture-name"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Created At:</strong> <span class="modal-picture-postedAt"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Tag:</strong> <span class="modal-picture-tag"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item view-picture-details">View picture details</a>
                        <a href="#" class="list-group-item edit-picture">Edit Picture</a>
                        <a href="#" class="list-group-item view-pictures-by-tag">View Pictures By This Tag</a>
                        <form class="list-group-item list-group-item-danger delete-picture" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Picture</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection