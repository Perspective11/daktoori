@extends('adminlte::page')
@section('content_header')
    <h3>Edit Text Content</h3>
@endsection
@section('top-ex')
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
@endsection
@section('content')
    <ul class="nav nav-tabs justify-content-around">
        @foreach ($contents->pluck('content_category')->unique() as $category)
            <li class="nav-item"><a class="nav-link" href="{{ '#' . $category }}">{{ $category }}</a></li>
        @endforeach
    </ul>
    <a href="{{ url('/admin/contents/') }}" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back</a>
    <br>
    <br>
    <form action="{{ url('/admin/contents/') }}" method="post" class="edit-contents">
        {{ csrf_field() }}
        @foreach($contents->groupBy('content_category') as $groupKey => $groupedContents)
            <hr>
            <div id="{{ $groupKey }}" class="text-center"><h2>{{ $groupKey }}</h2></div>
            @foreach ($groupedContents as $content)
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-default">
                        <div class="card-header with-border bg-light">
                            <h3 class="card-title">{{ $content->name_en }}</h3>
                            <h3 class="card-title pull-right">{{ $content->name_ar }}</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has($content->key . '[0]') ? ' has-error' : '' }}">
                                        <textarea name="{{$content->key . '[]'}}"
                                                  id="{{$content->key . '_ar'}}"
                                                  title="{{ $content->name_ar }}"
                                                  class="textarea_ar rtl {{ $content->type === 0 ?: 'editor' }}"
                                                  maxlength="{{ $content->max_char }}"
                                                  rows="10">{!! old($content->key . '[1]') ?: $content->value_ar  !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div>
            </div>
        @endforeach
        @endforeach
        <section class="FAB">
            <div class="FAB__action-button">
                <button type="submit" class="btn-link"><i class="action-button__icon fa fa-edit"></i></button>
                <p class="action-button__text--hide">Submit Content</p>
            </div>
        </section>
    </form>
@endsection
@section('bottom-ex')

    <script>
        $('.FAB__action-button').hover(function () {
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function () {
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });


        $(function(){
            $('.textarea_ar').each(function(e){
                CKEDITOR.replace( this.id, {
                    removePlugins: 'image',
                    height: 400,
                    contentsLangDirection : 'rtl',
                    enterMode : CKEDITOR.ENTER_BR,
                    shiftEnterMode: CKEDITOR.ENTER_P
                });
            });
        });

    </script>

@endsection
