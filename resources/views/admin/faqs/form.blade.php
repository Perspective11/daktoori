@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection
<div class="row">
    <div class="col-md-12 rtl">
        <div class="form-group {{  $errors->has('title_ar') ? ' has-error' : ''  }}">
            <label for="title_ar">العنوان:</label>
            <input type="text" name="title_ar" class="form-control" id="title_ar" value="{{ old('title_ar') ?: $faq->title_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 rtl">
        <div class="form-group{{  $errors->has('body_ar') ? ' has-error' : ''  }}">
            <label for="body_ar">النص:</label>
            <textarea class="form-control" name="body_ar" id="body_ar" rows="5">{{ old('body_ar') ?: $faq->body_ar }}</textarea>
        </div>
    </div>
</div>
