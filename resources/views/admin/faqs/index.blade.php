@extends('adminlte::page')
@section('top-ex')
    <!-- DataTables -->
    @include('links.datatables')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('content_header')
    <h3>Manage faqs</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/faqs/create') }}"><i class="action-button__icon fa bg-red fa-plus"></i></a>
            <p class="action-button__text--hide">Add faq</p>
        </div>
    </section>
    <table class="table table-striped table-bordered order-column table-hover jdatatable display rtl" id="faqs-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>العنوان</th>
            <th>النص</th>
            <th>انشئ في</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $faqsTable = $('#faqs-table').DataTable({
                "order": [[ 2, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/faqs/getData")}}',
                    data: function (d) {
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],

                columns: [
                    {data: 'title_ar', name: 'title_ar', width: '20%'},
                    {data: 'body_ar', name: 'body_ar'},
                    {data: 'created_at', name: 'created_at'}
                ],
            });

            var $modal = $('.action-modal');
            $('#faqs-table tbody').on('click', 'tr', function () {

                var data = $faqsTable.row(this).data();
                $('.action-modal').modal();
                $modal.find('.view-faq-details').attr('href' , '/admin/faqs/' + data['id']);
                $modal.find('.edit-faq').attr('href' , '/admin/faqs/' + data['id'] + '/edit');
                $modal.find('.delete-faq').attr('action' , '/admin/faqs/' + data['id']);
            });

            $('#faqs-table tbody').on('click', 'a.btn', function (e) {
                e.stopPropagation();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this faq?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                        <a href="#" class="list-group-item edit-faq">Edit faq</a>
                        <a href="#" class="list-group-item view-faq-details">View faq details</a>
                        <form class="list-group-item list-group-item-danger delete-faq" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete faq</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection