<form id="doctor-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Doctor Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-2">
                <h5><strong>Approved:</strong></h5>
                @php
                    $radioStatus = '';
                    $radioStatus = request('status');
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioApproved"
                               value="approved" {{ $radioStatus == 'approved'? 'checked': '' }}>
                        Approved
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioUnapproved"
                               value="unapproved" {{ $radioStatus == 'unapproved'? 'checked': ''  }}>
                        Unapproved
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioAll"
                               value="" {{ $radioStatus== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <h5><strong>Available:</strong></h5>
                @php
                    $radioStatus = '';
                    $radioStatus = request('availability');
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="availability" id="radioAvailable"
                               value="available" {{ $radioStatus == 'available' ? 'checked' : '' }}>
                        Available
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="availability" id="radioUnavailable"
                               value="unavailable" {{ $radioStatus == 'unavailable' ? 'checked' : ''  }}>
                        Unavailable
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="availability" id="radioAll"
                               value="" {{ $radioStatus== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                @php
                    $comboMajor = '';
                    $comboMajor = request('major');
                    $majors = \App\Major::whereHas('doctors')->get();
                @endphp
                <h5><strong>Major:</strong></h5>
                <select name="major" class="form-control" id="major">
                    <option {{ $comboMajor == ''? 'selected': '' }} value="">All</option>
                    @foreach ($majors as $major)
                        <option {{ $comboMajor == $major->name_ar ? 'selected': '' }} value="{{ $major->name_ar }}">{{$major->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                @php
                    $comboServices = '';
                    $comboServices = request('service');
                    $services = \App\Service::whereHas('offices')->get();
                @endphp
                <h5><strong>Service:</strong></h5>
                <select name="service" class="form-control" id="service">
                    <option {{ $comboServices== ''? 'selected': '' }} value="">All</option>
                    @foreach ($services as $service)
                        <option {{ $comboServices == $service->name_ar ? 'selected': '' }} value="{{ $service->name_ar }}">{{$service->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                @php
                    $comboCity = '';
                    $comboCity = request('city');
                    $cities = \App\City::whereHas('offices')->get();
                @endphp
                <h5><strong>City:</strong></h5>
                <select name="city" class="form-control" id="city">
                    <option {{ $comboCity== ''? 'selected': '' }} value="">All</option>
                    @foreach ($cities as $city)
                        <option {{ $comboCity == $city->key ? 'selected': '' }} value="{{ $city->key }}">{{$city->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                @php
                    $comboCity = '';
                    $comboCity = request('region');
                    $cities = \App\Region::whereHas('offices')->get();
                @endphp
                <h5><strong>Region:</strong></h5>
                <select name="region" class="form-control" id="region">
                    <option {{ $comboCity== ''? 'selected': '' }} value="">All</option>
                    @foreach ($cities as $region)
                        <option {{ $comboCity == $region->key ? 'selected': '' }} value="{{ $region->key }}">{{$region->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                @php
                    $comboInsurances = '';
                    $comboInsurances = request('insurance');
                    $insurances = \App\Insurance::whereHas('offices')->get();
                @endphp
                <h5><strong>Insurance:</strong></h5>
                <select name="insurance" class="form-control" id="insurance">
                    <option {{ $comboInsurances== ''? 'selected': '' }} value="">All</option>
                    @foreach ($insurances as $insurance)
                        <option {{ $comboInsurances == $insurance->name_ar ? 'selected': '' }} value="{{ $insurance->name_ar }}">{{$insurance->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <h5><strong>Office:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="office"
                           value="{{ request('office')? request('office') : '' }}">
                </div>
            </div>
            <div class="col-md-3">
                <h5><strong>Record Date Range:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="recordrange" id="recordrange"
                           value="{{ request('recordrange')? request('recordrange') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                <h5><strong>Fee Range (YR):</strong></h5>
                <div class="form-group range-form-group">
                    <input type="number" class="form-control" name="feestart"
                           value="{{ request('feestart')? request('feestart') : '' }}"> to
                    <input type="number" class="form-control" name="feeend"
                           value="{{ request('feeend')? request('feeend') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                <h5><strong>Rating Range:</strong></h5>
                <div class="form-group range-form-group">
                    <input type="number" class="form-control" name="ratingstart"
                           value="{{ request('ratingstart')? request('ratingstart') : '' }}"> to
                    <input type="number" class="form-control" name="ratingend"
                           value="{{ request('ratingend')? request('ratingend') : '' }}">
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $('#clear-fields').on('click', function () {
        var strippedUrl = stripQuery(window.location.href);
        window.location.replace(strippedUrl);
    })

    function stripQuery(url) {
        return url.split("?")[0].split("#")[0];
    }

    $('#search-submit').on('click', function (e) {
        e.preventDefault();
        putQueryString();
        $('#doctors-table').DataTable().ajax.reload();
    })

    function putQueryString() {
        var strippedUrl = stripQuery(window.location.href);
        var newQueryString = $('#doctor-filter').serialize();
        var url = strippedUrl + '?' + newQueryString;
        history.replaceState('', 'Doctors', url);
    }
    var start = moment().subtract(29, 'days');
    var end = moment();


    $('#recordrange').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
    $('#recordrange').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
    });

    $('#recordrange').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

</script>

