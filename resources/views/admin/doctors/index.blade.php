@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Doctors</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/users/create') }}"><i class="action-button__icon fa fa-plus bg-red"></i></a>
            <p class="action-button__text--hide">Add Doctor</p>
        </div>
    </section>
    <div class="container-fluid">
        @include('admin.doctors.filters')
        <table class="table table-striped table-bordered order-column table-hover jdatatable display smallest rtl" id="doctors-table" style="background-color: #fff;">
            <thead>
            <tr>
                <th>id</th>
                <th>الصورة</th>
                <th>الاسم</th>
                <th>اللقب الوظيفي</th>
                <th>متواجد</th>
                <th>الجنس</th>
                <th>التلفون</th>
                <th>الوتس</th>
                <th>الكشف</th>
                <th>الانتظار</th>
                <th>وصف</th>
                <th>عنوان</th>
                <th>التخصصات</th>
                <th>الخدمات</th>
                <th>المكاتب</th>
                <th>التأمينات</th>
                <th>المدينة</th>
                <th>المنطقة</th>
                <th>الحجوزات</th>
                <th>التقييم</th>
                <th>الترتيب</th>
                <th>التاريخ</th>
                <th>خيارات</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $doctorsTable = $('#doctors-table').DataTable({
                "order": [[ 21, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/doctors/getData")}}',
                    data: function (d) {
                        d.availability = $.urlParam('availability');
                        d.name = $.urlParam('name');
                        d.major = $.urlParam('major');
                        d.service = $.urlParam('service');
                        d.city = $.urlParam('city');
                        d.region = $.urlParam('region');
                        d.feestart = $.urlParam('feestart');
                        d.feeend = $.urlParam('feeend');
                        d.ratingstart = $.urlParam('ratingstart');
                        d.ratingend = $.urlParam('ratingend');
                        d.office = $.urlParam('office');
                        d.insurance = $.urlParam('insurance');
                        d.status = $.urlParam('status');
                        d.recordrange = $.urlParam('recordrange');
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'id', name: 'id', visible: false},
                    {data: 'picture', name: 'picture', sortable: 'no', orderable: 'no', searchable: 'no', visible: false},
                    {data: 'full_name_ar', name: 'full_name_ar'},
                    {data: 'title_ar', name: 'title_ar'},
                    {data: 'is_available', name: 'is_available', sortable: 'no', orderable: 'no', searchable: 'no', visible: false},
                    {data: 'gender', name: 'gender', visible: false},
                    {data: 'mobile', name: 'mobile', visible: false},
                    {data: 'whatsapp', name: 'whatsapp', visible: false},
                    {data: 'fees', name: 'fees', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'average_waiting_time', name: 'average_waiting_time', visible: false},
                    {data: 'about_ar', name: 'about_ar', visible: false},
                    {data: 'address_ar', name: 'address_ar', visible: false},
                    {data: 'majors', name: 'majors', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'services', name: 'services', sortable: 'no', orderable: 'no', searchable: 'no', visible: false},
                    {data: 'offices', name: 'offices', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'insurances', name: 'insurances', sortable: 'no', orderable: 'no', searchable: 'no', visible: false},
                    {data: 'city', name: 'city', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'region', name: 'region', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'bookings', name: 'bookings', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'rating', name: 'rating', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'rank', name: 'rank'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', sortable: 'no', orderable: 'no', searchable: 'no'},
                ],
            });

            $('#doctors-table tbody').on('click', '.btn', function (e) {
                e.stopPropagation();
            });

            $('#doctors-table tbody').on('click', '.doctor-delete', function (e) {
                e.preventDefault();
                $row = $(this).closest('tr');
                var data = $doctorsTable.row($row).data();
                deleteDoctor(data['user']['id']);
            });

            var $modal = $('.action-modal');
            $('#doctors-table tbody').on('click', 'tr', function () {
                var data = $doctorsTable.row(this).data();
                window.location.href = '/admin/users/' + data['user_id'];
            });

            function deleteDoctor(userId) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this doctor record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: "/admin/users/" + userId,
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("Deleted!", "The record was successfully deleted!", "success");
                                $('#doctors-table').DataTable().ajax.reload();
                            },
                            error: function (data) {
                                swal("Oops", data.responseText, "error");
                            }
                        }
                    )
                });
            }


        });
    </script>
@endsection

@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection