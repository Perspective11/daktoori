@extends('adminlte::page')
@section('top-ex')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
@endsection
@section('title', 'Daktoori')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-purple"><i class="fa fa-user-circle"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Users</span>
                    <span class="info-box-number">{{ number_format(\App\User::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Users --}}

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-teal"><i class="fa fa-shield"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Insurances</span>
                    <span class="info-box-number">{{ number_format(\App\Insurance::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Insurances --}}
    </div>
    <hr>
    <div class="row filled-info-boxes">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange">
                <span class="info-box-icon"><i class="fa fa-user-md"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Doctors</span>
                    <span class="info-box-number">{{ number_format(\App\Doctor::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Doctor::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Doctor::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Doctors --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-files-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Bookings</span>
                    <span class="info-box-number">{{ number_format(\App\Booking::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Booking::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Booking::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Bookings --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Customers</span>
                    <span class="info-box-number">{{ number_format(\App\Customer::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Customer::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Customer::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Customers --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-maroon">
                <span class="info-box-icon"><i class="fa fa-file-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Posts</span>
                    <span class="info-box-number">{{ number_format(\App\Post::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Post::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Post::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Posts --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-purple">
                <span class="info-box-icon"><i class="fa fa-envelope-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Contact Messages</span>
                    <span class="info-box-number">{{ number_format(\App\Contact::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Contact::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Contact::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Contact Messages --}}
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daktoori Website Activity</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <p class="text-center">
                                <strong>Site Activity ({{ Carbon\Carbon::now()->year -1 . ' - ' . Carbon\Carbon::now()->year }})</strong>
                            </p>

                            <div class="chart" style="height: 250px">
                                <!-- Sales Chart Canvas -->
                                <canvas id="activityChart" style="height: 180px;"></canvas>
                            </div>
                            <!-- /.chart-responsive -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>Some Numbers Describing The Past Month</strong>
                            </p>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $usersStatus = \App\User::activatedPercentage();
                                @endphp
                                <span class="progress-text">Activated Users</span>
                                <span class="progress-number"><b>{{ $usersStatus['activatedCount'] }}</b>/{{ $usersStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-blue" style="width: {{ $usersStatus['count']? $usersStatus['activatedCount'] /  $usersStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $doctorsStatus = \App\Doctor::approvedPercentage();
                                @endphp
                                <span class="progress-text">Approved Doctors</span>
                                <span class="progress-number"><b>{{ $doctorsStatus['approvedCount'] }}</b>/{{ $doctorsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-info" style="width: {{ $doctorsStatus['count']? $doctorsStatus['approvedCount'] /  $doctorsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $bookingsStatus = \App\Booking::approvedPercentage();
                                @endphp
                                <span class="progress-text">Approved Bookings</span>
                                <span class="progress-number"><b>{{ $bookingsStatus['approvedCount'] }}</b>/{{ $bookingsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-warning" style="width: {{ $bookingsStatus['count']? $bookingsStatus['approvedCount'] /  $bookingsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $doctorsStatus = \App\Doctor::availablePercentage();
                                @endphp
                                <span class="progress-text">Available Doctors</span>
                                <span class="progress-number"><b>{{ $doctorsStatus['availableCount'] }}</b>/{{ $doctorsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red" style="width: {{ $doctorsStatus['count']? $doctorsStatus['availableCount'] /  $doctorsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <hr>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Unapproved Bookings</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box rtl-list">
                        @forelse ($unapprovedBookings as $booking)
                            <li class="item">
                                @if ($booking->office)
                                    <div class="product-img">
                                        <img src="{{ $booking->doctor->getPicture() }}" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <span class="product-title">
                                            <a href="{{ $booking->doctor->userPath(true) }}">اسم الدكتور: {{ $booking->doctor->getName() }}</a> | <a href="{{ $booking->customer ? $booking->customer->userPath() : 'javascript:void(0):' }}">اسم المريض: {{ $booking->patient_name }}</a>
                                            <span class="label label-warning label-timestamp">
                                                {{ $booking->created_at->diffForHumans() }}
                                            </span>
                                            <span class="label label-info label-timestamp">
                                                {{ $booking->office->city->name_ar . ' - ' . optional($booking->office->region)->name_ar }}
                                            </span>
                                        </span>
                                        <span class="product-description">
                                            {{ $booking->doctor->majorsString()}}
                                            <span class="label label-warning">
                                                {{ 'رقم المريض: ' . $booking->mobile . ' | رقم الدكتور: ' . optional($booking->office)->doctor->mobile }}
                                            </span>
                                            <span class="label label-info label-timestamp">
                                                {{ $booking->time->name_ar . ' - ' . $booking->appointment_date->format('Y/m/d') }}
                                            </span>
                                            <a data-toggle="tooltip" title="Click to Approve Booking" data-entity="booking" data-id="{{ $booking->id }}" data-action="approve" style="color: lightgreen" class="list-action-button booking-approve label-timestamp"><i class="fa fa-2x fa-check"></i></a>
                                            <a data-toggle="tooltip" title="Click to Reject Booking" data-entity="booking" data-id="{{ $booking->id }}" data-action="reject" style="color: lightcoral" class="list-action-button booking-reject label-timestamp"><i class="fa fa-2x fa-times"></i></a>
                                        </span>
                                    </div>
                                @else
                                    <p>الدكتور محذوف من قاعدة البيانات</p>
                                @endif

                            </li>
                        @empty
                            <p>No Records Available</p>
                        @endforelse
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/bookings') }}" class="uppercase">View All Bookings</a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Unapproved Doctors</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box rtl-list">
                        @forelse ($unapprovedDoctors as $doctor)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $doctor->getPicture() }}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="{{ $doctor->userPath(true) }}"
                                       class="product-title">{{ str_limit($doctor->getName() . ' - ' . $doctor->title_ar , 200) }}
                                        <span class="label label-warning label-timestamp">
                                            {{ $doctor->created_at->diffForHumans() }}
                                        </span>
                                        <span class="label label-info label-timestamp">
                                            {{ $doctor->getOffice()->city->name_ar . ' - ' . optional($doctor->getOffice()->region)->name_ar }}
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {{ $doctor->majorsString()}}
                                        <span class="label label-info">
                                            {{ $doctor->mobile }}
                                        </span>
                                        <a data-toggle="tooltip" title="Click to Approve Doctor" data-entity="doctor" data-id="{{ $doctor->id }}" data-action="approve" style="color: lightgreen" class="list-action-button doctor-approve label-timestamp"><i class="fa fa-2x fa-unlock"></i></a>
                                        <a data-toggle="tooltip" title="Click to Hide This" data-entity="doctor" data-id="{{ $doctor->id }}" data-action="reject" style="color: lightcoral" class="list-action-button doctor-reject label-timestamp"><i class="fa fa-2x fa-times"></i></a>
                                    </span>
                                </div>
                            </li>
                        @empty
                            <p>No Records Available</p>
                        @endforelse
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/doctors?status=0') }}" class="uppercase">View All Unapproved Doctors</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            var ctx = document.getElementById("activityChart").getContext('2d');
            var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: MONTHS,
                    datasets: [{
                        label: "Doctors",
                        backgroundColor: 'rgba(0, 192, 240, 0.5)',
                        borderColor: 'rgba(0, 192, 240, 0.9)',
                        borderWidth: 1,
                        fill: false,
                        hoverBorderColor: 'rgba(0, 192, 240, 1)',
                        hoverBackgroundColor: 'rgba(0, 192, 240, 0.7)',
                        data: [
                            {{ \App\Doctor::increaseDuringMonth(1) }},
                            {{ \App\Doctor::increaseDuringMonth(2) }},
                            {{ \App\Doctor::increaseDuringMonth(3) }},
                            {{ \App\Doctor::increaseDuringMonth(4) }},
                            {{ \App\Doctor::increaseDuringMonth(5) }},
                            {{ \App\Doctor::increaseDuringMonth(6) }},
                            {{ \App\Doctor::increaseDuringMonth(7) }},
                            {{ \App\Doctor::increaseDuringMonth(8) }},
                            {{ \App\Doctor::increaseDuringMonth(9) }},
                            {{ \App\Doctor::increaseDuringMonth(10) }},
                            {{ \App\Doctor::increaseDuringMonth(11) }},
                            {{ \App\Doctor::increaseDuringMonth(12) }}
                        ],
                    }, {
                        label: "Customers",
                        fill: false,
                        borderWidth: 1,
                        backgroundColor: 'rgba(221, 75, 57, 0.5)',
                        borderColor: 'rgba(221, 75, 57, 0.9)',
                        hoverBorderColor: 'rgba(221, 75, 57, 1)',
                        hoverBackgroundColor: 'rgba(221, 75, 57, 0.7)',
                        data: [
                            {{ \App\Customer::increaseDuringMonth(1) }},
                            {{ \App\Customer::increaseDuringMonth(2) }},
                            {{ \App\Customer::increaseDuringMonth(3) }},
                            {{ \App\Customer::increaseDuringMonth(4) }},
                            {{ \App\Customer::increaseDuringMonth(5) }},
                            {{ \App\Customer::increaseDuringMonth(6) }},
                            {{ \App\Customer::increaseDuringMonth(7) }},
                            {{ \App\Customer::increaseDuringMonth(8) }},
                            {{ \App\Customer::increaseDuringMonth(9) }},
                            {{ \App\Customer::increaseDuringMonth(10) }},
                            {{ \App\Customer::increaseDuringMonth(11) }},
                            {{ \App\Customer::increaseDuringMonth(12) }}
                        ],
                    }, {
                        label: "Bookings",
                        fill: false,
                        borderWidth: 1,
                        backgroundColor: 'rgba(245, 162, 32, 0.5)',
                        borderColor: 'rgba(245, 162, 32, 0.9)',
                        hoverBorderColor: 'rgba(245, 162, 32, 1)',
                        hoverBackgroundColor: 'rgba(245, 162, 32, 0.7)',
                        data: [
                            {{ \App\Booking::increaseDuringMonth(1) }},
                            {{ \App\Booking::increaseDuringMonth(2) }},
                            {{ \App\Booking::increaseDuringMonth(3) }},
                            {{ \App\Booking::increaseDuringMonth(4) }},
                            {{ \App\Booking::increaseDuringMonth(5) }},
                            {{ \App\Booking::increaseDuringMonth(6) }},
                            {{ \App\Booking::increaseDuringMonth(7) }},
                            {{ \App\Booking::increaseDuringMonth(8) }},
                            {{ \App\Booking::increaseDuringMonth(9) }},
                            {{ \App\Booking::increaseDuringMonth(10) }},
                            {{ \App\Booking::increaseDuringMonth(11) }},
                            {{ \App\Booking::increaseDuringMonth(12) }}
                        ],
                    },
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                }

            });

            $('.list-action-button').on('click', function (e) {
                e.preventDefault();
                var $button = $(this);
                var buttonData = $(this)[0].dataset;
                $.ajax({
                    url: '/admin/' + buttonData.entity + 's/' + buttonData.id + '/activation/' + buttonData.action,
                    type: 'GET',
                    dataType: 'json',
                    success:function(data){
                        swal("Success!", data.msg, "success");
                        $button.closest('li.item').fadeOut();
                    },
                    error: function (data) {
                        response = data.responseJSON;
                        var arr = response.errors;
                        var string = '';
                        for (var key in arr) {
                            string += arr[key] + '\n'
                        }
                        swal("خطأ", string, "error")
                    }
                });
            });
        });
    </script>
@endsection