@extends('adminlte::page')
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ $category->path(true) }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Category</h3>
                    <div class="box-tools text-muted">
                        <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body rtl col-rtl">
                    @include('admin.categories.form')
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-warning pull-right">Edit Category</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
