@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Categories</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/categories/create') }}"><i class="action-button__icon fa fa-plus bg-red"></i></a>
            <p class="action-button__text--hide">Add Category</p>
        </div>
    </section>
    <div class="container-fluid">

        <table class="table table-striped table-bordered order-column table-hover jdatatable display rtl" id="categories-table" style="background-color: #fff;">
            <thead>
            <tr>
                <th>id</th>
                <th>الاسم</th>
                <th>عدد المنشورات</th>
                <th>تاريخ</th>
                <th>الخيارات</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $categoriesTable = $('#categories-table').DataTable({
                "order": [[ 3, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/categories/getData")}}',
                    data: function (d) {
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'id', name: 'id', visible: false},
                    {data: 'name_ar', name: 'name_ar'},
                    {data: 'posts_count', name: 'posts_count'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', sortable: 'no', orderable: 'no', searchable: 'no'},
                ],
            });

            $('#categories-table tbody').on('click', '.btn', function (e) {
                e.stopPropagation();
            });

            $('#categories-table tbody').on('click', '.category-delete', function (e) {
                e.preventDefault();
                $row = $(this).closest('tr');
                var data = $categoriesTable.row($row).data();
                deleteCategory(data['id']);
            });

            var $modal = $('.action-modal');
            $('#categories-table tbody').on('click', 'tr', function () {
                var data = $categoriesTable.row(this).data();
                $('.action-modal').modal();
                $modal.find('.delete-category').attr('action' , '/admin/categories/' + data['id']);

            });

            function deleteCategory(categoryid) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this category record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: "/admin/categories/" + categoryid,
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("Deleted!", "The record was successfully deleted!", "success");
                                $('#categories-table').DataTable().ajax.reload();
                            },
                            error: function (data) {
                                var response = data.responseJSON;
                                swal("Oops", response.msg, "error");
                            }
                        }
                    )
                });
            }


        });
    </script>
@endsection

@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection