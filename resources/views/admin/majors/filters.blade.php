<form id="major-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Major Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                <h5><strong>Level:</strong></h5>
                @php
                    $radioLevel = '';
                    $radioLevel = request('level');
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="level" id="radioLevel"
                               value="1" {{ $radioLevel == '1' ? 'checked' : '' }}>
                        Primary Majors
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="level" id="radioLevel"
                               value="2" {{ $radioLevel == '2' ? 'checked' : ''  }}>
                        Secondary Majors
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="level" id="radioAll"
                               value="" {{ $radioLevel== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $(function () {

        $('#clear-fields').on('click', function () {
            var strippedUrl = stripQuery(window.location.href);
            window.location.replace(strippedUrl);
        })

        function stripQuery(url) {
            return url.split("?")[0].split("#")[0];
        }

        $('#search-submit').on('click', function (e) {
            e.preventDefault();
            putQueryString();
            $('#majors-table').DataTable().ajax.reload();
        })

        function putQueryString() {
            var strippedUrl = stripQuery(window.location.href);
            var newQueryString = $('#major-filter').serialize();
            var url = strippedUrl + '?' + newQueryString;
            history.replaceState('', 'Major', url);
        }

    });

</script>