@push('top-stack')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endpush
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $major->name_ar }}">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('level') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('level')){
                    $checked = old('level');
                }
                else
                    $checked = $major->level . '';
            @endphp
            <label for="level">المستوى:</label>  <span style="color:orangered">*</span>
            <div class="">
                <input type="radio" name="level" id="level-main" class="level-radio"
                       value="1" {{ $checked == '1'? 'checked' : ''  }}
                        {{ ($major->level == '1' && $major->doctors->count()) ? 'disabled' : '' }}>
                رئيسي <br>
                <input type="radio" name="level" id="level-sub" class="level-radio"
                       value="2" {{ $checked == '2'? 'checked' : ''  }}
                        {{ ($major->level == '1' && $major->doctors->count()) ? 'disabled' : '' }}>
                فرعي
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div id="parent-majors-box" class="form-group {{  $errors->has('parent-major') ? ' has-error' : ''  }}">
            <label for="parent-major">تابع للتخصص الرئيسي</label>
            <select name="parent_major" class="form-control select-parent-major" id="parent-major">
                <option value=""  {{ $major->id? 'selected' : '' }}></option>
                @foreach($majors as $singleMajor)
                    @php
                        $selected = '';
                        if (old('parent_major')){
                            if (old('parent_major') == $singleMajor->id)
                            $selected = true;
                        }
                        elseif (optional($major->parent)->id == $singleMajor->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $singleMajor->id }}">
                        {{ $singleMajor->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('rank') ? ' has-error' : ''  }}">
            <label for="rank">الترتيب</label>
            <div class="">
                <input type="number" name="rank" class="form-control" id="rank"
                       value="{{ old('rank') ?: $major->rank ?: 0}}">
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('is_hidden') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('is_hidden')){
                    $checked = old('is_hidden');
                }
                else
                    $checked = $major->is_hidden . '';
            @endphp
            <label for="is_hidden">مخفي:</label>
            <div class="">
                <input type="radio" name="is_hidden" id="is_hidden-1" class="is_hidden-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                نعم <br>
                <input type="radio" name="is_hidden" id="is_hidden-0" class="is_hidden-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                لا
            </div>
        </div>
    </div>
</div>
@section('bottom-ex')
    <script type="text/javascript">
        $(".select-parent-major").select2({
            placeholder: "اختر تخصص",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });

        if ($('#level-main').is(':checked')) {
            $('#parent-majors-box').hide();
        } else if ($('#level-sub').is(':checked')) {
            $('#parent-majors-box').show();
        }

        $('#level-main').click(function () {
            $('#parent-majors-box').slideUp();
        });

        $('#level-sub').click(function () {
            $('#parent-majors-box').slideDown();
        });
    </script>
@endsection