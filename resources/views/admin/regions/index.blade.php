@extends('adminlte::page')
@section('top-ex')
    @include('links.datatables')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Regions</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/regions/create') }}"><i class="action-button__icon fa fa-plus bg-red"></i></a>
            <p class="action-button__text--hide">Add Region</p>
        </div>
    </section>
    <div class="container-fluid">

        <table class="table table-striped table-bordered order-column table-hover jdatatable display smaller rtl" id="regions-table" style="background-color: #fff;">
            <thead>
            <tr>
                <th>id</th>
                <th>الاسم</th>
                <th>المفتاح</th>
                <th>الظهور</th>
                <th>الاطباء</th>
                <th>تاريخ</th>
                <th>الخيارات</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $regionsTable = $('#regions-table').DataTable({
                "order": [[ 5, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/regions/getData")}}',
                    data: function (d) {
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'id', name: 'id', visible: false},
                    {data: 'name_ar', name: 'name_ar'},
                    {data: 'key', name: 'key'},
                    {data: 'is_shown', name: 'is_shown', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'doctors_count', name: 'doctors_count', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', sortable: 'no', orderable: 'no', searchable: 'no'},
                ],
            });

            $('#regions-table tbody').on('click', '.btn', function (e) {
                e.stopPropagation();
            });

            $('#regions-table tbody').on('click', '.region-delete', function (e) {
                e.preventDefault();
                $row = $(this).closest('tr');
                var data = $regionsTable.row($row).data();
                console.log($row);
                deleteRegion(data['id']);
            });


            function deleteRegion(regionid) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this region record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax(
                        {
                            type: "post",
                            url: "/admin/regions/" + regionid,
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            dataType: 'json',
                            success: function(data){
                                swal("Deleted!", "The record was successfully deleted!", "success");
                                $('#regions-table').DataTable().ajax.reload();
                            },
                            error: function (data) {
                                var response = data.responseJSON;
                                swal("Oops", response.msg, "error");
                            }
                        }
                    )
                });
            }


        });
    </script>
@endsection
