@section('top-ex')
@endsection
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">اسم المنطقة:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $region->name_ar }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('key') ? ' has-error' : ''  }}">
            <label for="key">الكلمة المفتاحية (بالانجليزي):</label>
            <input type="text" name="key" class="form-control" id="key" value="{{ old('key') ?: $region->key }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('is_shown') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('is_shown')){
                    $checked = old('is_shown');
                }
                else
                    $checked = $region->is_shown . '';
            @endphp
            <label for="is_shown">الظهور:</label>
            <div class="">
                <input type="radio" name="is_shown" id="is_shown-1" class="is_shown-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                نعم <br>
                <input type="radio" name="is_shown" id="is_shown-0" class="is_shown-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                لا
            </div>
        </div>
    </div>
</div>
@section('bottom-ex')
    <script type="text/javascript">
    </script>
@endsection