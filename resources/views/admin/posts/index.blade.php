@extends('adminlte::page')
@section('top-ex')
    <!-- DataTables -->
    @include('links.datatables')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('content_header')
    <h3>Manage posts</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/posts/create') }}"><i class="action-button__icon fa bg-red fa-plus"></i></a>
            <p class="action-button__text--hide">Add post</p>
        </div>
    </section>
    @include('admin.posts.filters')
    <table class="table table-striped table-bordered order-column table-hover jdatatable display smallest rtl" id="posts-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>الصورة</th>
            <th>العنوان</th>
            <th>النص</th>
            <th>التصنيف</th>
            <th>خيارات</th>
            <th>انشئ في</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $postsTable = $('#posts-table').DataTable({
                "order": [[ 4, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/posts/getData")}}',
                    data: function (d) {
                        d.category = $.urlParam('category');
                        d.daterange = $.urlParam('daterange');
                        d.type = $.urlParam('type');
                        d.status = $.urlParam('status');
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],

                columns: [
                    {data: 'picture', name: 'picture', sortable: 'no', orderable: 'no', searchable: 'no', width: "10%"},
                    {data: 'title_ar', name: 'title_ar'},
                    {data: 'body_ar', name: 'body_ar', 'visible': false},
                    {data: 'category', name: 'category', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'status', name: 'status', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'}
                ],
            });

            var $modal = $('.action-modal');
            $('#posts-table tbody').on('click', 'tr', function () {

                var data = $postsTable.row(this).data();
                $('.action-modal').modal();
                $modal.find('.view-post-details').attr('href' , '/admin/posts/' + data['id']);
                $modal.find('.edit-post').attr('href' , '/admin/posts/' + data['id'] + '/edit');
                $modal.find('.delete-post').attr('action' , '/admin/posts/' + data['id']);
                $feature_post = $modal.find('.feature-post');
                $feature_post.hide();
                if (! data['featured']){
                    $modal.find('.feature-post').attr('href' , '/admin/posts/' + data['id'] + '/featured');
                    $feature_post.show();
                }
            });

            $('#posts-table tbody').on('click', 'a.btn', function (e) {
                e.stopPropagation();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this post?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                        <a href="#" class="list-group-item edit-post">Edit post</a>
                        <a href="#" class="list-group-item view-post-details">View post details</a>
                        <a href="#" class="list-group-item feature-post">Feature Post</a>
                        <form class="list-group-item list-group-item-danger delete-post" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete post</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection