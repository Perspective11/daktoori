@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection
<div class="row">
    <div class="col-md-12 rtl">
        <div class="form-group {{  $errors->has('title_ar') ? ' has-error' : ''  }}">
            <label for="title_ar">العنوان:</label>
            <input type="text" name="title_ar" class="form-control" id="title_ar" value="{{ old('title_ar') ?: $post->title_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 rtl">
        <div class="form-group{{  $errors->has('body_ar') ? ' has-error' : ''  }}">
            <label for="body_ar">النص:</label>
            <textarea class="form-control" name="body_ar" id="body_ar" rows="5">{{ old('body_ar') ?: $post->body_ar }}</textarea>
        </div>
    </div>
    <div class="col-md-12 rtl">
        <div class="form-group {{  $errors->has('category') ? ' has-error' : ''  }}">
            <label for="category">التصنيف:</label>
            <select name="category" class="form-control select-category" id="category">
                <option value=""  {{ $post->category_id? 'selected' : '' }}></option>
                @foreach($categories as $category)
                    @php
                        $selected = '';
                        if (old('category')){
                            if (old('category') == $category->id)
                            $selected = true;
                        }
                        elseif ($post->category && $post->category->id == $category->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $category->id }}">
                        {{ $category->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
            <label for="picture" class="control-label">Picture</label>
            @if($post->picture)
                <div class="alert">
                    <p>This post already contains an image</p>
                    <a class="btn btn-xs btn-warning change-image-picture">Change Image</a>
                </div>
            @else
                <input type="file" name="picture" id="picture">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
    @if ($post->picture)
        <div class="col-md-6">
            <img class="img-responsive" src="{{ $post->picture }}" alt="{{  $post->title }}">
        </div>
    @endif
</div>
@section('bottom-ex')
    <script type="text/javascript">
        $('.change-image-picture').on('click', function (e) {
            e.preventDefault();
            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        });
        CKEDITOR.replace('body_ar', {
            removePlugins: 'image',
            height: 500,
            contentsLangDirection : 'rtl',
        });
        $(".select-category").select2({
            placeholder: "اختر التصنيف",
            allowClear: true,
            width: '100%',
            dir: "rtl"
        });
    </script>
@endsection