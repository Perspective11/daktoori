<form id="posts-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Post Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-2">
                <h5><strong>Post Status:</strong></h5>
                @php
                    $radioStatus = '';
                    if (request()->has('status')){
                        if (request('status') == 'published')
                            $radioStatus = request('status');
                        else if (request('status') == 'unpublished')
                            $radioStatus = request('status');
                    }
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioActive"
                               value="published" {{ $radioStatus == 'published'? 'checked': '' }}>
                        Published
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioInactive"
                               value="unpublished" {{ $radioStatus == 'unpublished'? 'checked': ''  }}>
                        Unpublished
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioAll"
                               value="" {{ $radioStatus== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <h5><strong>Post Type:</strong></h5>
                @php
                    $radioType = '';
                    if (request()->has('type')){
                        $radioType = request('type');
                    }
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="type" id="radioActive"
                               value="featured" {{ $radioType == 'featured'? 'checked': '' }}>
                        Featured
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="type" id="radioInactive"
                               value="unfeatured" {{ $radioType == 'unfeatured'? 'checked': ''  }}>
                        Unfeatured
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="type" id="radioAll"
                               value="" {{ $radioType== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <h5><strong>Date Range:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="daterange" id="reportrange"
                           value="{{ request('daterange')? request('daterange') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                @php
                    $comboCategory = '';
                    $comboCategory = request('category');
                    $categories = \App\Category::whereHas('posts')->get();
                @endphp
                <h5><strong>Category:</strong></h5>
                <select name="category" class="form-control" id="category">
                    <option {{ $comboCategory== ''? 'selected': '' }} value="">All</option>
                    @foreach ($categories as $category)
                        <option {{ $comboCategory == $category->name_ar ? 'selected': '' }} value="{{ $category->name_ar }}">{{$category->name_ar}}</option>
                    @endforeach
                </select>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $(function () {

        $('#clear-fields').on('click', function () {
            var strippedUrl = stripQuery(window.location.href);
            window.location.replace(strippedUrl);
        });

        function stripQuery(url) {
            return url.split("?")[0].split("#")[0];
        }

        $('#search-submit').on('click', function (e) {
            e.preventDefault();
            putQueryString();
            $('#posts-table').DataTable().ajax.reload();
        })
        function putQueryString() {
            var strippedUrl = stripQuery(window.location.href);
            var newQueryString = $('#posts-filter').serialize();
            var url = strippedUrl + '?' + newQueryString;
            history.replaceState('', 'Posts', url);
        }


        var start = moment().subtract(29, 'days');
        var end = moment();


        $('#reportrange').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            },
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
        });

        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
    });

</script>