<!--box-widget-wrap -->
<div class="box-widget-wrap fl-wrap fixed-bar">
    <!--box-widget-item -->
    <div class="box-widget-item fl-wrap">
        <div class="box-widget">
            <div class="box-widget-content">
                <div class="box-widget-item-header">
                    <h3> ابحث في المدونة </h3>
                </div>
                <div class="search-widget">
                    <form action="/blog" class="fl-wrap">
                        <input name="search" id="se" type="text" class="search" placeholder="بـحـث.." value="{{ request('search') }}" />
                        <button type="submit" class="search-submit color2-bg" id="submit_btn"><i class="fal fa-search transition"></i> </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--box-widget-item end -->
    <!--box-widget-item -->
    <div class="box-widget-item fl-wrap">
        <div class="box-widget widget-posts">
            <div class="box-widget-content">
                <div class="box-widget-item-header">
                    <h3>المنشورات المميزة</h3>
                </div>
                @foreach ($featuredPosts as $post)
                    <!--box-image-widget-->
                        <div class="box-image-widget">
                            <div class="box-image-widget-media"><img src="{{ $post->picture }}" alt="">
                                <a href="{{ $post->path() }}" class="color-bg">تفاصيل</a>
                            </div>
                            <div class="box-image-widget-details">
                                <h4>{{ str_limit($post->title_ar, 40) }}</h4>
                                <div class="post-body rtl-right-body">{{ $post->body_ar }}</div>
                                <span class="widget-posts-date"><i class="fal fa-calendar"></i> {{ $post->created_at->diffForHumans() }} </span>
                            </div>
                        </div>
                        <!--box-image-widget end -->
                @endforeach
            </div>
        </div>
    </div>
    <!--box-widget-item end -->
    <!--box-widget-item -->
    <div class="box-widget-item fl-wrap">
        <div class="box-widget">
            <div class="box-widget-content">
                <div class="box-widget-item-header">
                    <h3> التصنيفات</h3>
                </div>
                <ul class="cat-item">
                    @foreach ($categories as $category)
                        <li><a href="{{ '/blog?' . http_build_query(['category' => $category->name_ar]) }}">{{ $category->name_ar }}</a> <span>{{ $category->posts_count }}</span></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <!--box-widget-item end -->
</div>
<!--box-widget-wrap end -->