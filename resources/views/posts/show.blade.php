@extends('layouts.app')
@section('content')
    <!--  section  -->
    <section class="color-bg middle-padding ">
        <div class="wave-bg wave-bg2"></div>
        <div class="container">
            <div class="flat-title-wrap text-body-center">
                <h2><span>{{ $singlePost->title_ar }}</span></h2>
            </div>
        </div>
    </section>
    <section  id="sec1" class="middle-padding grey-blue-bg rtl">
        <div class="container">
            <div class="row">
                <!--blog content -->
                <div class="col-md-8">
                    <!--post-container -->
                    <div class="post-container fl-wrap">
                        <!-- article> -->
                        <article class="post-article">
                            <div class="list-single-main-media fl-wrap">
                                <div class="single-slider-wrapper fl-wrap">
                                    <div class="single-slider fl-wrap"  >
                                        <div class="slick-slide-item"><img src="{{ $singlePost->picture }}" alt=""></div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>{{ $singlePost->title_ar }}</h3>
                                </div>
                                <div class="post-body rtl-right-body">
                                    {!! $singlePost->body_ar !!}
                                </div>
                                <div class="post-opt">
                                    <ul>
                                        <li><i class="fal fa-clock"></i> <span>{{ $singlePost->created_at->diffForHumans() }}</span></li>
                                        <li><i class="fal fa-tags"></i> <span>{{ $singlePost->getCategoryName() }}</span></li>
                                    </ul>
                                </div>
                                <span class="fw-separator"></span>
                                <div class="post-nav fl-wrap">
                                    @if ($nextPost)
                                        <a href="{{ $nextPost->path() }}" class="post-link prev-post-link">
                                            <i class="fal fa-angle-left"></i>التالي<span class="clearfix">{{ $nextPost->title_ar }}</span>
                                        </a>
                                    @endif
                                    @if ($prevPost)
                                        <a href="{{ $prevPost->path() }}" class="post-link next-post-link">
                                            <i class="fal fa-angle-right"></i>السابق <span class="clearfix">{{ $prevPost->title_ar }}</span>
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <!-- list-single-main-item -->
                        </article>
                        <!-- article end -->
                    </div>
                    <!--post-container end -->
                </div>
                <!-- blog content end -->
                <!--   sidebar  -->
                <div class="col-md-4">
                    @include('posts.nav')
                </div>
                <!--   sidebar end  -->
            </div>
        </div>
        <div class="limit-box fl-wrap"></div>
    </section>
    <!-- section end -->
@endsection