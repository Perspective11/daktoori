@extends('layouts.app')
@section('content')
    <!--  section  -->
    <section class="color-bg middle-padding ">
        <div class="wave-bg wave-bg2"></div>
        <div class="container">
            <div class="flat-title-wrap text-body-center">
                <h2><span>مدونة دكتوري</span></h2>
                <span class="section-separator"></span>
                <h4>هنا تعرض مقالاتنا واعلاناتنا وكل ما يتعلق بموقع دكتوري.</h4>
            </div>
        </div>
    </section>
    <section  id="sec1" class="middle-padding grey-blue-bg rtl">
        <div class="container">
            <div class="row">
                <!--blog content -->
                <div class="col-md-8">
                    <!--post-container -->
                    <div class="post-container fl-wrap">
                        @foreach ($posts as $post)
                            <!-- article> -->
                            <article class="post-article">
                                    <div class="list-single-main-media fl-wrap">
                                        <div class="single-slider-wrapper fl-wrap">
                                            <div class="single-slider fl-wrap"  >
                                                <div class="slick-slide-item"><img src="{{ $post->picture }}" alt=""></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3><a href="{{ $post->path() }}">{{ $post->title_ar }}</a></h3>
                                        </div>
                                        <div class="post-body rtl-right-body">
                                            {!! $post->body_ar !!}
                                        </div>
                                        <div class="post-opt">
                                            <ul>
                                                <li><i class="fal fa-clock"></i> <span>{{ $post->created_at->diffForHumans() }}</span></li>
                                                <li><i class="fal fa-tags"></i> <span>{{ $post->getCategoryName() }}</span></li>
                                            </ul>
                                        </div>
                                        <span class="fw-separator"></span>
                                        <a href="{{ $post->path() }}" class="btn float-btn color-bg">اقرأ المزيد<i class="fal fa-angle-left"></i></a>
                                    </div>
                                </article>
                            <!-- article end -->
                        @endforeach

                        <div class="pagination-wrapper row text-center">
                            <div class="col-xs-12">
                                {{ $posts->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                    <!--post-container end -->
                </div>
                <!-- blog content end -->
                <!--   sidebar  -->
                <div class="col-md-4">
                    @include('posts.nav')
                </div>
                <!--   sidebar end  -->
            </div>
        </div>
        <div class="limit-box fl-wrap"></div>
    </section>
    <!-- section end -->
@endsection