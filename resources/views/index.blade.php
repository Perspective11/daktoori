@extends('layouts.app')
@section('content')
    @include('partials.main-search')
    <!--section -->
    <section id="sec2">
        <div class="container">
            <div class="section-title">
                <div class="section-title-separator"><span></span></div>
                <h2>{{ $contents["home_ads_title"] }}</h2>
                <span class="section-separator"></span>
                <p>{{ $contents["home_ads_subtitle"] }}</p>
            </div>
            <!-- -->
            <div class="row">
                <div class="col-md-4">
                    <!-- process-item-->
                    <div class="process-item big-pad-pr-item nodecpre">
                        <span class="process-count"> </span>
                        <div class="time-line-icon"><i class="fal  fa-calendar "></i></div>
                        <h4><a href="/listings">{{ $contents["home_ads_title3"] }}</a></h4>
                        <p>{{ $contents["home_ads_subtitle3"] }}</p>
                    </div>
                    <!-- process-item end -->
                </div>

                <div class="col-md-4">
                    <!-- process-item-->
                    <div class="process-item big-pad-pr-item">
                        <span class="process-count"> </span>
                        <div class="time-line-icon"><i class="fal fa-user-md"></i></div>
                        <h4> <a href="/listings">{{ $contents["home_ads_title2"] }}</a></h4>
                        <p>{{ $contents["home_ads_subtitle2"] }} </p>
                    </div>
                    <!-- process-item end -->
                </div>
                <div class="col-md-4">
                    <!-- process-item-->
                    <div class="process-item big-pad-pr-item">
                        <span class="process-count"> </span>
                        <div class="time-line-icon"><i class="fal fa-search"></i></div>
                        <h4><a href="/listings"> {{ $contents["home_ads_title1"] }} </a></h4>
                        <p>{{ $contents["home_ads_subtitle1"] }} </p>
                    </div>
                    <!-- process-item end -->
                </div>

            </div>
            <!--process-wrap   end-->




        </div>
    </section>
    <!-- section end -->
    <section  class="parallax-section rtl-col" style="display:{{ $appSettings['show_page'] }}" data-scrollax-parent="true">
        <div class="bg"  data-bg="{{ $images['home_rudoctor_section']->image_path }}" data-scrollax="properties: { translateY: '100px' }"></div>
        <div class="overlay"></div>
        <!--container-->
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <!-- column text-->
                    <div class="colomn-text fl-wrap">
                        <div class="colomn-text-title">
                            <h3>{{ $contents["home_ads_title4"] }}</h3>
                            <p>{{ $contents["home_ads_subtitle4"] }} </p>
                            <a href="/doctors/create" class="btn  color-bg float-btn">{{ $contents["home_btn"] }}<i class="fal fa-plus"></i></a>
                        </div>
                    </div>
                    <!--column text   end-->
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
@endsection
