<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    dd(\Carbon\Carbon::now()->format('Y-m-d H:i a'));
});

Route::get('/', function () {
    return view('welcome');
});

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/', 'PagesController@index');
Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'PagesController@storeContact');
Route::get('/about', 'PagesController@about');
Route::get('/blog', 'PostsController@index');
Route::get('/blog/{id}', 'PostsController@show');
Route::get('/privacy-policy', 'PagesController@privacyPolicy');
Route::get('/credits', 'PagesController@credits');
Route::get('/services', 'PagesController@services');
Route::get('/majors', 'PagesController@majors');
Route::get('/privacy-policy', 'PagesController@privacyPolicy');
Route::get('/faqs', 'PagesController@faqs');

//Route::get('/team', 'PagesController@team');

Route::get('/doctors/create', 'DoctorsController@create')->middleware(['throttle:10', 'guest']);
Route::post('/doctors/create', 'DoctorsController@store')->middleware(['throttle:10', 'guest']);
Route::post('/doctors/uploadImagesStore', 'DoctorsController@uploadImagesStore')->middleware(['throttle:10', 'guest']);
Route::get('/doctors/listDoctorImages', 'DoctorsController@listDoctorImages')->middleware(['auth', 'roles:doctor']);
Route::post('/doctors/uploadImagesUpdate', 'DoctorsController@uploadImagesUpdate')->middleware(['auth', 'roles:doctor']);
Route::delete('/doctors/deleteImage', 'DoctorsController@DeleteImage');

Route::get('/listings', 'ListingsController@index');
Route::get('/reservation', 'ListingsController@reservation');
Route::post('/reserve', 'ListingsController@reserve')->middleware(['throttle:3']);
Route::get('/doctors/{doctor}', 'DoctorsController@show');


Route::post('/user/bookings/cancel/{booking}', 'CustomersController@cancel')->middleware(['auth', 'roles:customer']);
Route::post('/user/review/{doctor}', 'CustomersController@review')->middleware(['auth', 'roles:customer']);
Route::get('/user/bookings', 'CustomersController@bookings')->middleware(['auth', 'roles:customer']);

Route::group(['middleware' => ['auth', 'roles:doctor']], function(){
    Route::get('/schedule', 'DoctorsController@schedule');
    Route::post('/schedule', 'DoctorsController@scheduleStore');
    Route::get('/profile', 'DoctorsController@profile');
    Route::post('/profile', 'DoctorsController@update');
    Route::get('/bookings', 'DoctorsController@bookings');
    Route::post('/bookings/cancel/{booking}', 'DoctorsController@cancel');
    Route::get('/dashboard', 'DoctorsController@dashboard');
    Route::get('/change-password', 'DoctorsController@change-password');
});



Route::group(['prefix' => 'admin','middleware' => ['auth', 'roles:admin'], 'as' => 'admin.'], function() {

    Route::get('/', function () {
        return redirect('/admin/home');
    });

    Route::get('/home', 'admin\HomeController@index')->name('home');

    //  admin users routes
    Route::get('/users/getData', 'admin\UsersController@getUsersData');
    Route::get('/change-password','admin\UsersController@changePassword');
    Route::post('/change-password','admin\UsersController@changePasswordStore');
    Route::get('/users/listDoctorImages', 'admin\UsersController@listDoctorImages');
    Route::post('/users/uploadImagesStore', 'admin\UsersController@uploadImagesStore');
    Route::post('/users/uploadImagesUpdate', 'admin\UsersController@uploadImagesUpdate');
    Route::delete('/users/deleteImage', 'admin\UsersController@DeleteImage');
    Route::get('users/{user}/activation','admin\UsersController@activation');
    Route::resource('users','admin\UsersController');

    Route::get('/doctors/getData', 'admin\DoctorsController@getDoctorsData');
    Route::delete('/doctors/deleteImage', 'admin\DoctorsController@DeleteImage');
    Route::get('/doctors/listEventImages', 'admin\DoctorsController@listDoctorImages');
    Route::post('/doctors/uploadImagesStore', 'admin\DoctorsController@uploadImagesStore');
    Route::post('/doctors/uploadImagesUpdate', 'admin\DoctorsController@uploadImagesUpdate');
    Route::get('/doctors/{doctor}/activation/{action?}','admin\DoctorsController@approved');
    Route::resource('doctors','admin\DoctorsController');

    Route::get('/offices/getData', 'admin\OfficesController@getOfficesData');
    Route::resource('offices','admin\OfficesController');

    Route::get('/customers/getData', 'admin\CustomersController@getCustomersData');
    Route::resource('customers','admin\CustomersController');

    Route::get('/bookings/export','admin\BookingsController@export');
    Route::get('/bookings/getData', 'admin\BookingsController@getBookingsData');
    Route::get('/bookings/{booking}/activation/{action?}','admin\BookingsController@approved');
    Route::post('/bookings/{booking}/changeStatus/{change_to}','admin\BookingsController@changeStatus');
    Route::post('/bookings/{booking}/changeVisitStatus/{change_to}','admin\BookingsController@changeVisitStatus');
    Route::post('/bookings/{booking}/changeProcedure/{change_to}','admin\BookingsController@changeProcedure');

    Route::post('/bookings/{booking}/confirmPatient/{change_to}','admin\BookingsController@confirmPatient');
    Route::post('/bookings/{booking}/confirmInvoiced/{change_to}','admin\BookingsController@confirmInvoiced');
    Route::resource('bookings','admin\BookingsController');

    Route::get('/contacts/getData', 'admin\ContactsController@getContactsData');
    Route::delete('/contacts/{contact}', 'admin\ContactsController@destroy');
    Route::get('/contacts', 'admin\ContactsController@index');

    Route::get('/posts/getData', 'admin\PostsController@getPostsData');
    Route::resource('/posts', 'admin\PostsController');
    Route::get('posts/{post}/activation', 'admin\PostsController@activation');
    Route::get('posts/{post}/featured', 'admin\PostsController@featured');

    //    admin categories routes
    Route::get('/categories/getData', 'admin\CategoriesController@getCategoriesData');
    Route::resource('categories','admin\CategoriesController', ['except' => [
        'show'
    ]]);

    Route::get('/majors/getData', 'admin\MajorsController@getMajorsData');
    Route::resource('/majors', 'admin\MajorsController');
    Route::get('/majors/{major}/visibility/{action?}','admin\MajorsController@visibility');


    Route::get('/services/getData', 'admin\ServicesController@getServicesData');
    Route::resource('/services', 'admin\ServicesController');

    Route::get('/faqs/getData', 'admin\FaqsController@getFaqsData');
    Route::resource('/faqs', 'admin\FaqsController');

    Route::get('/insurances/getData', 'admin\InsurancesController@getInsurancesData');
    Route::resource('/insurances', 'admin\InsurancesController');

    Route::get('/procedures/getData', 'admin\ProceduresController@getProceduresData');
    Route::resource('/procedures', 'admin\ProceduresController');

    Route::get('/regions/getData', 'admin\RegionsController@getRegionsData');
    Route::resource('/regions', 'admin\RegionsController');
    Route::get('regions/{region}/toggleShown', 'admin\RegionsController@toggleShown');


    Route::get('/reviews/getData', 'admin\ReviewsController@getReviewsData');
    Route::resource('/reviews', 'admin\ReviewsController', ['only', ['index', 'destroy']]);
    Route::get('/reviews/{review}/change-status/{action?}/{value?}','admin\ReviewsController@changeStatus');

    Route::get('/pictures/listPicturesByCat', 'admin\PicturesController@listPicturesByCat');
    Route::get('/pictures/listPictureById', 'admin\PicturesController@listPictureById');
    Route::post('/pictures/uploadedImagesUpdateInfo', 'admin\PicturesController@uploadedImagesUpdateInfo');
    Route::delete('/pictures/deleteImage', 'admin\PicturesController@DeleteImage');
    Route::get('/pictures/getData', 'admin\PicturesController@getPicturesData');
    Route::resource('/pictures', 'admin\PicturesController')->only([
        'index', 'show', 'create', 'destroy'
    ]);;
    Route::post('/pictures/uploadImagesStore', 'admin\PicturesController@uploadImagesStore');


    Route::get('/contents', 'admin\ContentsController@show');
    Route::get('/contents/edit', 'admin\ContentsController@edit');
    Route::post('/contents/', 'admin\ContentsController@update');

    Route::get('/image-contents', 'admin\ImageContentsController@show');
    Route::post('/image-contents/', 'admin\ImageContentsController@update');

    Route::get('/settings', 'admin\SettingsController@edit');
    Route::post('/settings/', 'admin\SettingsController@update');


});


