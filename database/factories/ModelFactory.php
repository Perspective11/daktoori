<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
function autoIncrement($init = 0)
{
    for ($i = $init; $i < 1000; $i++) {
        yield $i;
    }
}

$factory->define(App\User::class, function (\Faker\Generator $faker) {
    $faker = \Faker\Factory::create('ar_SA');
    $faker_en = \Faker\Factory::create('en_US');

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->email,
        'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'status'         => ((int) $faker->boolean(80)),
        'approved'       => ((int) $faker->boolean(80)),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Doctor::class, function (\Faker\Generator $faker) {
    $faker = \Faker\Factory::create('ar_SA');
    $faker_en = \Faker\Factory::create('en_US');
    $fname = $faker->firstName;
    $sname = $faker->optional()->firstName;
    $lname = $faker->lastName;

    if ($sname){
        $fullName = $fname . ' ' . $sname . ' ' . $lname;
    }
    else
        $fullName = $fname . ' ' . $lname;

    return [
        'city_id'              => 1,
        'region_id'            => $faker->numberBetween(1, 30),
        'fname_ar'             => $fname,
        'sname_ar'             => $sname,
        'lname_ar'             => $lname,
        'title_ar'             => $faker->realText(40),
        'gender'               => ((int) $faker->boolean(10)) + 1,
        'dob'                  => $faker->optional(0.8)->dateTimeThisCentury('18 years ago'), // secret
        'mobile'               => $faker->optional(0.8)->phoneNumber,
        'phone'                => $faker->optional(0.1)->phoneNumber,
        'whatsapp'             => $faker->optional(0.3)->phoneNumber,
        'grad_year'            => $faker->optional(0.7)->year(),
        'grad_university'      => $faker_en->optional(0.3)->company,
        'grad_country'         => $faker->optional(0.7)->country,
        'fees'                 => $faker->numberBetween(500, 10000),
        'is_available'         => ((int) $faker->boolean(90)),
        'is_approved'          => ((int) $faker->boolean(70)),
        'average_waiting_time' => $faker->optional(0.8)->numberBetween(5, 180),
        'about_ar'             => $faker->optional(0.3)->realText(1000),
        'address_ar'           => $faker->optional(0.1)->address,
        'full_name_ar'         => $fullName,
    ];
});

$factory->define(App\Office::class, function (\Faker\Generator $faker) {
    $faker = \Faker\Factory::create('ar_SA');
    $faker_en = \Faker\Factory::create('en_US');

    return [
        'city_id'    => 1,
        'region_id'  => $faker->numberBetween(1, 30),
        'name_ar'    => $faker->company,
        'mobile'     => $faker->optional(0.1)->phoneNumber,
        'phone'      => $faker->optional(0.9)->phoneNumber,
        'address_ar' => $faker->optional(0.9)->address,
        'fees'       => $faker->numberBetween(500, 10000),
        'longitude'  => $faker->optional(0.1)->longitude,
        'latitude'   => $faker->optional(0.1)->latitude,
    ];
});

$factory->define(App\Customer::class, function (\Faker\Generator $faker) {
    $faker = \Faker\Factory::create('ar_SA');
    $faker_en = \Faker\Factory::create('en_US');

    return [
        'city_id'    => 1,
        'region_id'  => $faker->numberBetween(1, 30),
        'fname_ar'   => $faker->firstName,
        'sname_ar'   => $faker->optional()->firstName,
        'lname_ar'   => $faker->lastName,
        'gender'     => ((int) $faker->optional(0.3)->boolean(50)) + 1,
        'dob'        => $faker->optional(0.8)->dateTimeThisCentury('18 years ago'), // secret
        'mobile'     => $faker->optional(0.8)->phoneNumber,
        'whatsapp'   => $faker->optional(0.3)->phoneNumber,
        'address_ar' => $faker->optional(0.1)->address,
    ];
});

$factory->define(App\Insurance::class, function (\Faker\Generator $faker) {
    $faker = \Faker\Factory::create('ar_SA');
    $faker_en = \Faker\Factory::create('en_US');

    return [
        'name_ar'        => $faker->company,
        'company_ar'     => $faker->company,
        'address_ar'     => $faker->optional(0.9)->address,
        'description_ar' => $faker->optional(0.9)->realText(190),
        'longitude'      => $faker->optional(0.1)->longitude,
        'latitude'       => $faker->optional(0.1)->latitude,
    ];
});

$factory->define(App\Contact::class, function (\Faker\Generator $faker) {
    return [
        'name'       => $faker->name,
        'email'      => $faker->email,
        'subject'    => $faker->optional()->words(3, true),
        'message'    => $faker->paragraphs(2, true),
        'created_at' => $faker->dateTimeThisMonth,
    ];
});

$postIncrement = autoIncrement(4);
$factory->define(App\Post::class, function (\Faker\Generator $faker) use ($postIncrement) {
    $faker = \Faker\Factory::create('en_US');
    $faker_ar = \Faker\Factory::create('ar_SA');
    $postIncrement->next();

    return [
        'user_id'    => 1,
        'title_ar'   => $faker_ar->realText(100),
        'body_ar'    => $faker_ar->realText(500),
        'status'     => (int) $faker->boolean(90),
        'type'       => $faker->numberBetween(0, 4),
        'created_at' => $faker->dateTimeBetween('-6 months'),
        'picture'    => '/images/posts/post_'.$postIncrement->current().'.jpg',
        'category_id' => $faker->randomElement(\App\Category::pluck('id')->toArray()),
    ];
});
$pictureIncrement = autoIncrement();

$factory->define(App\Picture::class, function (\Faker\Generator $faker) use ($pictureIncrement) {
    $faker = \Faker\Factory::create('en_US');
    $faker_ar = \Faker\Factory::create('ar_SA');
    $pictureIncrement->next();

    return [
        'name'       => $faker->words(3, true),
        'image_path' => '/images/pictures/picture_'.$pictureIncrement->current().'.jpg',
        'thumb_path' => '/images/pictures/picture_'.$pictureIncrement->current().'.jpg',
        'created_at' => $faker->dateTimeThisMonth,
        'temp_uuid'  => $faker->uuid,
    ];
});

$factory->define(App\Review::class, function (\Faker\Generator $faker) {
    $faker = \Faker\Factory::create('en_US');
    $faker_ar = \Faker\Factory::create('ar_SA');

    return [
        'customer_id'     => $faker->randomElement(\App\Customer::pluck('id')->toArray()),
        'doctor_id'       => $faker->randomElement(\App\Doctor::pluck('id')->toArray()),
        'rating'          => rand(1,5),
        'review'          => $faker_ar->realText(100),
        'rating_approved' => (int) $faker->boolean(90),
        'review_approved' => (int) $faker->boolean(90),
        'review_hidden'   => (int) $faker->boolean(10),
        'featured'        => 0,
    ];
});