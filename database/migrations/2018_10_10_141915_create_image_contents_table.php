<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->unique()->index();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('resolution_x')->nullable();
            $table->string('resolution_y')->nullable();
            $table->string('aspect_ratio')->nullable();
            $table->string('position')->nullable()->default('center center');;
            $table->string('size')->nullable()->default('cover');
            $table->string('repeat')->nullable()->default('no-repeat');
            $table->string('attachment')->nullable()->default('scroll');
            $table->string('color')->nullable()->default('white');
            $table->string('image_path');
            $table->string('thumb_path')->nullable();
            $table->string('type')->nullable();
            $table->string('image_category')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_contents');
    }
}
