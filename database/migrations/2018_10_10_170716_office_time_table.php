<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfficeTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_time', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id')->unsigned()->index();
            $table->integer('month_id')->unsigned()->index()->nullable();
            $table->integer('day_id')->unsigned()->index();
            $table->integer('time_id')->unsigned()->index();
            $table->integer('status')->unsigned()->default(1);
            $table->integer('waiting_minutes')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_time');
    }
}
