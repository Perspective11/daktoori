<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('city_id')->index();
            $table->unsignedInteger('region_id')->index();
            $table->string('fname_ar');
            $table->string('sname_ar')->nullable();
            $table->string('lname_ar');
            $table->string('fname_en')->nullable();
            $table->string('sname_en')->nullable();
            $table->string('lname_en')->nullable();
            $table->string('prefix')->nullable();
            $table->string('title_ar');
            $table->string('title_en')->nullable();
            $table->integer('gender')->nullable(); // 1 for male 2 for female
            $table->date('dob')->nullable();
            $table->string('mobile')->nullable();
            $table->string('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('grad_year')->nullable();
            $table->string('grad_university')->nullable();
            $table->string('grad_country')->nullable();
            $table->string('fees')->nullable();
            $table->integer('is_available')->default(0);
            $table->integer('is_approved')->default(0);
            $table->integer('average_waiting_time')->nullable();
            $table->text('about_ar')->nullable();
            $table->text('about_en')->nullable();
            $table->string('address_ar')->nullable();
            $table->string('address_en')->nullable();
            $table->integer('total_ratings')->nullable();
            $table->decimal('rating_percentage')->nullable();
            $table->text('links')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
