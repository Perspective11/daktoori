<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMajorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('majors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable()->index()->unsigned();
            $table->string('name_ar');
            $table->string('name_en')->nullable();
            $table->string('adj_ar')->nullable();
            $table->string('adj_en')->nullable();
            $table->integer('level')->default(1);
            $table->integer('is_hidden')->default(0);
            $table->integer('rank')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('majors');
    }
}
