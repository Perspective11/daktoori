<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id')->unsigned()->index();
            $table->integer('time_id')->unsigned()->index();
            $table->integer('customer_id')->unsigned()->index()->nullable(); //user doesn't have to be signed in
            $table->integer('insurance_id')->unsigned()->index()->nullable();
            $table->integer('procedure_id')->unsigned()->index()->nullable();
            $table->string('patient_name');
            $table->string('mobile');
            $table->string('email')->nullable();
            $table->integer('booking_status_id')->unsigned()->index();
            $table->integer('visit_status_id')->nullable()->unsigned()->index();
            $table->date('appointment_date');
            $table->dateTime('scheduled_datetime')->nullable();
            $table->dateTime('actual_datetime')->nullable();
            $table->string('cost')->nullable();
            $table->decimal('patient_rating')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
