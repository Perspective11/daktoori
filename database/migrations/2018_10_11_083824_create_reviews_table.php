<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->index()->nullable();
            $table->string('patient_name')->nullable();
            $table->integer('doctor_id')->unsigned()->index();
            $table->integer('rating')->unsigned();
            $table->text('review')->nullable();
            $table->text('description')->nullable();
            $table->integer('rating_approved')->nullable();
            $table->integer('review_approved')->nullable();
            $table->integer('review_hidden')->nullable();
            $table->integer('featured')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
