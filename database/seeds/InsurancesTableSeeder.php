<?php

use Illuminate\Database\Seeder;

class InsurancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ar_SA');

        if (env('DEV_SEEDS', false)){
            factory(App\Insurance::class, 10)->create();
            $office_ids = \App\Office::pluck('id')->toArray();
            foreach (\App\Insurance::all() as $insurance){
                $insurance->offices()->attach($faker->randomElements($office_ids, rand(0, 10)));
            }
            $this->command->info("Insurances table seeded!");
        }

    }
}
