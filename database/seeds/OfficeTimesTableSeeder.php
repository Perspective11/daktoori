<?php

use Illuminate\Database\Seeder;

class OfficeTimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DEV_SEEDS', false)) {
            $faker = \Faker\Factory::create('ar_SA');
            $times = App\Time::where('is_shown', 1)->pluck('id', 'key')->toArray();
            $days = App\Day::pluck('id')->toArray();
            $offices = App\Office::all();
            foreach ($offices as $office) {
                foreach ($days as $day) {
                    $boolean = $faker->boolean(60);
                    if ($boolean) {
                        $office->officeTimes()->create([
                            'day_id'  => $day,
                            'time_id' => $times['day'],
                            'status'  => 1,
                        ]);
                    }
                    $boolean = $faker->boolean(60);
                    if ($boolean) {
                        $office->officeTimes()->create([
                            'day_id'  => $day,
                            'time_id' => $times['evening'],
                            'status'  => 1,
                        ]);
                    }
                }
            }
            $this->command->info("Office Times table seeded!");
        }
    }
}
