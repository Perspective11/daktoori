<?php

use App\Doctor;
use App\Service;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');

        $service = Service::create([
            'name_ar' => 'خدمات جلدية',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات اسنان',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات عظام',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات نساء وتوليد',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات انف واذن وحنجرة',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات قلب واوعية دموية',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات عيون',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات مسالك بولية',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات جراحة تجميل',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات اورام',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات تخسيس وتغذية',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات جراحة عامة',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات جراحة مخ واعصاب',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات جراحة سمنة ومناظير',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات جراحة قلب وصدر',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات جهاز هضمي ومناظير',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات حقن مجهري واطفال انابيب',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات كلى',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات جراحة عمود فقري',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات امراض دم',
        ]);
        $service = Service::create([
            'name_ar' => 'خدمات علاج الآلام',
        ]);


        if (env('DEV_SEEDS', false))
        {
            $services = \App\Service::pluck('id')->toArray();

            $doctors = \App\Office::each(function ($d) use ($faker, $services) {
                $d->services()->attach($faker->randomElements($services, rand(1, 5)));
            });
        }

        $this->command->info("Services with Doctors table seeded!");
    }
}
