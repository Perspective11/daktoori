<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');
        $faker_ar = \Faker\Factory::create('ar_SA');
        $categories = \App\Category::pluck('id');
        Post::create([
            'user_id'     => 1,
            'title_ar'    => 'موقع دكتوري يعلن افتتاحه!',
            'body_ar'     => 'ما زال الموقع ضمن الفترة التجريبية. وخلال هذا الوقت تفضلوا في مراسلتنا في حال واجهتكم أي صعوبة.',
            'status'      => 1,
            'type'        => Post::$TYPE_FEATURED_POST,
            'picture'     => '/images/posts/post_1.jpg',
            'category_id' => $faker->randomElement($categories),
        ]);
        Post::create([
            'user_id'     => 1,
            'title_ar'    => 'نحن نرحب باللأطباء للاشتراك معنا!',
            'body_ar'     => 'جميع الأطباء في الجمهورية اليمنية مرحبين في الدخول وتسجيل بياناتهم للمشاركة والتواصل. لمزيد من المعلومات تواصلوا معنا.',
            'status'      => 1,
            'type'        => Post::$TYPE_FEATURED_POST,
            'picture'     => '/images/posts/post_2.jpg',
            'category_id' => $faker->randomElement($categories),

        ]);
        Post::create([
            'user_id'     => 1,
            'title_ar'    => 'هل تواجه صعوبة في الاختيار والحجز؟ تعال هنا!',
            'body_ar'     => 'اول خطوة هي ان تتوجه لصفحة الأطباء حيث يمكنك البحث بجميع طرق التصفية. اختر المعايير التي تريدها ثم اضغط بحث. بعد ذلك اضغط على الموعد المناسب لك امام معلومات الطبيب. ثم احجز في ذاك الموعد؟',
            'status'      => 1,
            'type'        => Post::$TYPE_FEATURED_POST,
            'picture'     => '/images/posts/post_3.jpg',
            'category_id' => $faker->randomElement($categories),
        ]);
        if (env('DEV_SEEDS', false)) {
            factory(App\Post::class, 10)->create();
        }
        $this->command->info("Posts table seeded!");
    }
}
