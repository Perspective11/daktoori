<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DEV_SEEDS', false)) {
            factory(App\Review::class, 100)->create();
            \App\Review::applyRatings();
        }
        $this->command->info("Reviews table seeded!");
    }
}
