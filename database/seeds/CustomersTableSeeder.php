<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::pluck('id', 'key')->toArray();

        if (env('DEV_SEEDS', false)) {
            factory(App\User::class, 50)->create()->each(function ($u) use ($roles){  //only works when the user id is nullable
                $u->customer()->save(factory(App\Customer::class)->make());
                $u->roles()->attach($roles['customer']);
            });
            $user = User::create([
                'name'     => 'اسامة علي نعمان',
                'email'    => 'osama@otek.com',
                'password' => bcrypt('secret'),
                'status'   => 1,
                'approved' => 1,
            ]);
            $customer = $user->customer()->create([
                'fname_ar'  => 'أسامة',
                'sname_ar'  => 'علي',
                'lname_ar'  => 'نعمان',
                'city_id'   => 1,
                'region_id' => 1,
            ]);
            $user->roles()->attach($roles['customer']);
        }

        $this->command->info("Customers table seeded!");

    }
}
