<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            "id"      => 1,
            "key"     => "admin",
            "name_ar" => "مسؤول",
            "name_en" => "Admin",
        ]);
        Role::create([
            "id"      => 2,
            "key"     => "doctor",
            "name_ar" => "طبيب",
            "name_en" => "Doctor",
        ]);
        Role::create([
            "id"      => 3,
            "key"     => "customer",
            "name_ar" => "عميل",
            "name_en" => "Customer",
        ]);
        Role::create([
            "id"      => 4,
            "key"     => "superadmin",
            "name_ar" => "مسؤول شامل",
            "name_en" => "Super Admin",
        ]);
        $this->command->info("Roles table seeded!");
    }
}
