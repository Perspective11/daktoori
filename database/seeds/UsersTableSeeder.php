<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'     => 'Aiman Ali',
            'email'    => 'aiman@gmail.com',
            'password' => bcrypt('123'),
            'status'   => 1,
            'approved' => 1,
        ]);
        $user2 = User::create([
            'name'     => 'Moe Al-Amodi',
            'email'    => 'moe@gmail.com',
            'password' => bcrypt('PassCode'),
            'status'   => 1,
            'approved' => 1,
        ]);
        $user->roles()->attach(1);
        $user2->roles()->attach(1);

        $this->command->info("Users table seeded!");
    }
}
