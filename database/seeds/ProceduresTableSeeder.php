<?php

use Illuminate\Database\Seeder;
use App\Procedure;
class ProceduresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $procedure = Procedure::create([
            'name_ar' => 'عملية',
            'name_en' => 'Operation',
        ]);
        $procedure = Procedure::create([
            'name_ar' => 'فحص',
            'name_en' => 'Test',
        ]);
        $procedure = Procedure::create([
            'name_ar' => 'استشارة',
            'name_en' => 'Consultation',
        ]);
        $procedure = Procedure::create([
            'name_ar' => 'عودة',
            'name_en' => 'Return',
        ]);
        $procedure = Procedure::create([
            'name_ar' => 'جلسة',
            'name_en' => 'Session',
        ]);              

    }
}
