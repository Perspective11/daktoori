<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::pluck('id', 'key')->toArray();

        if (env('DEV_SEEDS', false)) {
            factory(App\User::class, 20)->create()->each(function ($u) use ($roles) {  //only works when the user id is nullable
                $d = $u->doctor()->save(factory(App\Doctor::class)->make());
                $d->offices()->save(factory(App\Office::class)->make());
                $u->roles()->attach($roles['doctor']);
            });
            $user = User::create([
                'name'     => 'ايمن علي نعمان',
                'email'    => 'aiman@otek.com',
                'password' => bcrypt('secret'),
                'status'   => 1,
                'approved' => 1,
            ]);
            $doctor = $user->doctor()->create([
                'fname_ar'             => 'ايمن',
                'sname_ar'             => 'علي',
                'lname_ar'             => 'نعمان',
                'title_ar'             => 'دكتور حاصل على شهادة ماسترز في مجال البرمجة',
                'dob'                  => '1993-12-12',
                'mobile'               => '773393905',
                'whatsapp'             => '773393905',
                'grad_year'            => '2018',
                'grad_university'      => 'Lebanese International University',
                'grad_country'         => 'Yemen',
                'address_ar'           => 'حدة شارع الخميسن',
                'gender'               => 1,
                'is_available'         => 1,
                'is_approved'          => 1,
                'average_waiting_time' => 30,
                'city_id'              => 1,
                'region_id'            => 1,
                'full_name_ar'         => 'أيمن علي نعمان',
            ]);
            $office = $doctor->offices()->create([
                'name_ar'    => 'شركة اوتك',
                'address_ar' => 'حدة شارع الخمسين',
                'fees'       => 2500,
                'phone'      => '773393905',
                'city_id'    => 1,
                'region_id'  => 1,
            ]);
            $user->roles()->attach($roles['doctor']);
        }

        $this->command->info("Doctors and Offices table seeded!");
    }
}
