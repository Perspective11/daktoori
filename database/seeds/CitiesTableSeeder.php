<?php

use App\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = City::create([
            'key'      => 'sanaa',
            'name_ar'  => 'صنعاء',
            'name_en'  => 'Sana\'a',
            'is_shown' => 1,
        ]);
        $city = City::create([
            'key'      => 'aden',
            'name_ar'  => 'عدن',
            'is_shown' => 0,
        ]);
        $city = City::create([
            'key'      => 'maarib',
            'name_ar'  => 'مأرب',
            'is_shown' => 0,
        ]);
        $this->command->info("Cities table seeded!");
    }
}
