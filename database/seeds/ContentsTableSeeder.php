<?php

use App\Content;
use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $TYPE_NORMAL = Content::TYPE_NORMAL;
        $TYPE_EDITOR = Content::TYPE_EDITOR;

        ////////////////////////////////////
        /// Slider Start
        ////////////////////////////////////
        Content::create([
            "key"              => "doctor_agreement",
            "name_en"          => "Doctor Agreement",
            "name_ar"          => "اتفاقية الدكتور",
            "value_en"         => " ",
            "value_ar"         => view('seeds.doctor-agreement')->render(),
            "type"             => $TYPE_EDITOR,
            "content_category" => "Agreements",
        ]);

        Content::create([
            "key"              => "privacy_policy",
            "name_en"          => "Privacy Policy Description",
            "name_ar"          => "لوائح الخصوصية",
            "value_en"         => '',
            "value_ar"         => view('seeds.privacy-policy')->render(),
            "type"             => $TYPE_EDITOR,
            "content_category" => "Pages",
        ]);

        Content::create([
            "key"              => "home_header_title1",
            "name_en"          => "home header title1",
            "name_ar"          => "عنوان الرئيسية الاول",
            "value_en"         => '',
            "value_ar"         => 'احجز موعدك الآن اونلاين مع دكتوري',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_header_title2",
            "name_en"          => "home header title2",
            "name_ar"          => "عنوان الرئيسية الثاني",
            "value_en"         => '',
            "value_ar"         => 'او اتصل برقمنا الثابت',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_header_title3",
            "name_en"          => "home header title3",
            "name_ar"          => "عنوان الرئيسية الثالث",
            "value_en"         => '',
            "value_ar"         => 'و رقم التلفون',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_header_title4",
            "name_en"          => "home header title4",
            "name_ar"          => "عنوان الرئيسية الرابع",
            "value_en"         => '',
            "value_ar"         => 'أسهل طريقة للحجز عند افضل الدكاترة في اليمن',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_header_subtitle",
            "name_en"          => "home header subtitle",
            "name_ar"          => "عنوان الفرعي الرئيسية ",
            "value_en"         => '',
            "value_ar"         => 'احجز موعدك الآن مع دكتوري',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_title",
            "name_en"          => "home ads title",
            "name_ar"          => "العنوان التسويق ",
            "value_en"         => '',
            "value_ar"         => 'كيف تحجز موعد مع دكتوري',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_subtitle",
            "name_en"          => "home ads subtitle",
            "name_ar"          => "العنوان الفرعي التسويق الاول",
            "value_en"         => '',
            "value_ar"         => 'احجز موعدك مع دكتوري',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_title1",
            "name_en"          => "home ads title1",
            "name_ar"          => "العنوان التسويق الاول",
            "value_en"         => '',
            "value_ar"         => 'دور على الدكتور',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_subtitle1",
            "name_en"          => "home ads subtitle1",
            "name_ar"          => "العنوان الفرعي التسويق الاول",
            "value_en"         => '',
            "value_ar"         => 'بالتخصص و الاسعار و المكان',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_title2",
            "name_en"          => "home ads title2",
            "name_ar"          => "العنوان التسويق الثاني",
            "value_en"         => '',
            "value_ar"         => 'قارن و اختار',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_subtitle2",
            "name_en"          => "home ads subtitle2",
            "name_ar"          => "العنوان الفرعي التسويق الثاني",
            "value_en"         => '',
            "value_ar"         => 'اختار بحسب اقرب مكان لك او السعر اللي يناسبك',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_title3",
            "name_en"          => "home ads title3",
            "name_ar"          => "العنوان التسويق الثالث",
            "value_en"         => '',
            "value_ar"         => 'احجز موعدك',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_subtitle3",
            "name_en"          => "home ads subtitle3",
            "name_ar"          => "العنوان الفرعي التسويق الثالث",
            "value_en"         => '',
            "value_ar"         => 'بضغطة زر من غير وجع راس احجز موعدك واوصل على دورك',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_title4",
            "name_en"          => "home ads title4",
            "name_ar"          => "العنوان التسويق الرابع",
            "value_en"         => '',
            "value_ar"         => 'هل انت طبيب ؟',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_ads_subtitle4",
            "name_en"          => "home ads subtitle4",
            "name_ar"          => "العنوان الفرعي التسويق الرابع",
            "value_en"         => '',
            "value_ar"         => 'انظم الان الى قائمة الأطباء الان عن طريق التسجيل في الموقع',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_btn",
            "name_en"          => "home btn",
            "name_ar"          => "زر الرئيسية",
            "value_en"         => '',
            "value_ar"         => 'انظم الان',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_title",
            "name_en"          => "home footer title",
            "name_ar"          => "عنوان التذبيلة الرئيسية",
            "value_en"         => '',
            "value_ar"         => 'اعرف المزيد عنا',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_btn1",
            "name_en"          => "home footer btn1",
            "name_ar"          => " زر التذبيلة الاولى",
            "value_en"         => '',
            "value_ar"         => 'عن دكتوري',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_btn2",
            "name_en"          => "home footer btn2",
            "name_ar"          => "زر التذبيلة الثانية",
            "value_en"         => '',
            "value_ar"         => 'الأسئلة الأكثر شيوعاً',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_title1",
            "name_en"          => "home footer title1",
            "name_ar"          => " عنوان التذبيلة الاولى",
            "value_en"         => '',
            "value_ar"         => 'تواصل معنا الان',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_subtitle1",
            "name_en"          => "home footer subtitle1",
            "name_ar"          => " عنوان الفرعي التذبيلةالاولى",
            "value_en"         => '',
            "value_ar"         => 'يمكنك الحجز الان فقط عن طريق التواصل معنا عبر الخط المباشر و سوف نقوم بالبقية',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_title2",
            "name_en"          => "home footer title2",
            "name_ar"          => " عنوان التذبيلة الثاني",
            "value_en"         => '',
            "value_ar"         => 'ابحث عن طريق',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_title3",
            "name_en"          => "home footer title3",
            "name_ar"          => " عنوان التذبيلة الثالث",
            "value_en"         => '',
            "value_ar"         => 'تواصل معنا',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_subtitle3",
            "name_en"          => "home footer subtitle3",
            "name_ar"          => " عنوان الفرعي التذبيلة الثالث",
            "value_en"         => '',
            "value_ar"         => 'تواصل معنا مباشرة عبر الواتس اب:',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_title4",
            "name_en"          => "home footer title4",
            "name_ar"          => " عنوان التذبيلة الرابعه",
            "value_en"         => '',
            "value_ar"         => 'نحن هنا',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_title5",
            "name_en"          => "home footer title5",
            "name_ar"          => " عنوان التذبيلة الخامسه",
            "value_en"         => '',
            "value_ar"         => 'من نحن',
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_footer_subtitle5",
            "name_en"          => "home footer subtitle5",
            "name_ar"          => "عنوان الفرعي التذبيلة الخامسه",
            "value_en"         => '',
            "value_ar"         => 'دكتوري دوت كوم يمكنك من العثور على أفضل طبيب وفقاً لإحتياجك مجاناً . حجز موعد مباشر مع الطبيب الاقرب الى موقعك , و يتيح لك التصفح من خلال قاعدة بيانات محدثة من الأطباء والعيادات والمستشفيات .

            يمكنك أيضا معرفة المزيد عن التجربة الطبية ورؤية خبرات الطبيب قبل حجز موعده المباشر.كما يمكنك أيضا معرفة ما إذا كان الطبيب أو العيادة أو المستشفى يقبل التأمين الخاص بك!',
            "type"             => $TYPE_EDITOR,
            "content_category" => "Home",
        ]);
        $this->command->info("Contents table seeded!");
    }
}
