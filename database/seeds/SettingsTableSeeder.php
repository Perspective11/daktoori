<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'key'      => 'facebook_link',
            'name_en'  => 'Facebook Link',
            'name_ar'  => 'رابط الفيسبوك',
            'value'    => 'https://www.facebook.com/Daktoori/',
            'icon'     => 'fab fa-facebook-f',
            'color'    => '#3b5998',
            'category' => 'Social Links',
            'type'     => 'social_links',
            'rules'    => 'url|nullable',
        ]);
        Setting::create([
            'key'      => 'twitter_link',
            'name_en'  => 'Twitter Link',
            'name_ar'  => 'رابط التويتر',
            'value'    => 'https://twitter.com/daktoori',
            'icon'     => 'fab fa-twitter',
            'color'    => '#1da1f2',
            'category' => 'Social Links',
            'type'     => 'social_links',
            'rules'    => 'url|nullable',
        ]);
        Setting::create([
            'key'      => 'instagram_link',
            'name_en'  => 'Instagram Link',
            'name_ar'  => 'رابط الانستجرام',
            'value'    => 'https://www.instagram.com/daktoori/',
            'icon'     => 'fab fa-instagram',
            'color'    => '#c13584',
            'category' => 'Social Links',
            'type'     => 'social_links',
            'rules'    => 'url|nullable',
        ]);
        Setting::create([
            'key'      => 'linkedin_link',
            'name_en'  => 'Linkedin Link',
            'name_ar'  => 'رابط اللينكد ان',
            'value'    => 'https://www.linkedin.com/company/capyemen/',
            'icon'     => 'fab fa-linkedin-in',
            'color'    => '#0077b5',
            'category' => 'Social Links',
            'type'     => 'social_links',
            'rules'    => 'url|nullable',
        ]);
        Setting::create([
            'key'      => 'youtube_link',
            'value'    => 'https://www.youtube.com/channel/UCkR5nBr_Snm0vdzkbOFtV7Q',
            'name_en'  => 'Youtube Link',
            'name_ar'  => 'رابط اليوتيوب',
            'icon'     => 'fab fa-youtube',
            'color'    => '#c4302b',
            'category' => 'Social Links',
            'type'     => 'social_links',
            'rules'    => 'url|nullable',
        ]);

        Setting::create([
            'key'      => 'mobile_phone',
            'value'    => '+967 771366000 ',
            'name_en'  => 'Mobile Phone',
            'name_ar'  => 'رقم الهاتف',
            'icon'     => 'fas fa-mobile-alt',
            'category' => 'Site Info',
            'type'     => 'text',
            'rules'    => 'required|between:6,20',

        ]);

        Setting::create([
            'key'      => 'whatsaap_phone',
            'value'    => '+967 771366000 ',
            'name_en'  => 'Whatsaap Phone',
            'name_ar'  => 'رقم الوتس',
            'icon'     => 'fas fa-whatsapp',
            'category' => 'Site Info',
            'type'     => 'text',
            'rules'    => 'required|between:6,20',

        ]);
        Setting::create([
            'key'      => 'email',
            'value'    => 'info@daktoori.org',
            'name_en'  => 'Site Email',
            'name_ar'  => 'ايميل الموقع',
            'icon'     => 'fa fa-envelope',
            'category' => 'Site Info',
            'type'     => 'text',
            'rules'    => 'required|email',

        ]);
        Setting::create([
            'key'      => 'land_line',
            'value'    => ' 01441702',
            'name_en'  => 'Land Line',
            'name_ar'  => 'الهاتف الارضي',
            'icon'     => 'fa fa-phone',
            'category' => 'Site Info',
            'rules'    => 'required|between:6,20',
        ]);
        Setting::create([
            'key'      => 'location',
            'value'    => 'حده مقابل عمارة النزيلي',
            'name_en'  => 'location',
            'name_ar'  => 'الموقع',
            'icon'     => 'fa fa-map-marker',
            'category' => 'Site Info',
            'rules'    => 'required|between:6,30',
        ]);
        Setting::create([
            'key'      => 'show_page',
            'value'    => 'visible',
            'name_en'  => 'choose visible to show or none to hide',
            'name_ar'  => 'لاظهار او اخفاء قسم هل انت طبيب؟',
            'icon'     => 'fa fa-edit',
            'category' => 'Site Info',
            'rules'    => 'required',
        ]);
        Setting::create([
            'key'      => 'notification_text',
            'value'    => 'لا نستطيع استقبال اي حجز اليوم',
            'name_en'  => 'write what notification to display',
            'name_ar'  => 'اكتب ايش يظهر من تنبية',
            'icon'     => 'fa fa-bell',
            'category' => 'notification',
            'rules'    => 'nullable',
        ]);
        Setting::create([
            'key'      => 'notification_color',
            'value'    => 'rgba(0,0,0,.2)',
            'name_en'  => 'choose the background color of the notification',
            'name_ar'  => 'اختر لون خلفية الشعار',
            'icon'     => 'fa fa-bell',
            'category' => 'notification_color',
            'rules'    => 'nullable',
        ]);
        $appSettings = Setting::pluck('value', 'key')->toArray();
        \Cache::forever('appSettings', $appSettings);
        $this->command->info("Settings table seeded!");
    }
}

