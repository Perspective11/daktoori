<?php

use Illuminate\Database\Seeder;

class BookingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ar_SA');

        $customers = \App\Customer::with('offices');
        $customers_ids = \App\Customer::pluck('id');
        $times_ids = \App\Time::where('is_shown', 1)->pluck('id');
        $statuses_ids = \App\BookingStatus::pluck('id');
        $insurances_ids = \App\Insurance::pluck('id')->toArray();

        $offices = \App\Office::with('doctor', 'customers')->get();

        foreach ($offices as $office) {
            $boolean = $faker->boolean(30);
            if ($boolean) {
                $office->customers()->attach($faker->randomElements($customers_ids, rand(0, 10)), [
                        'appointment_date'  => '2012-02-02',
                        'time_id'           => 1,
                        'booking_status_id' => 1,
                        'patient_name'      => $faker->name,
                        'mobile'            => $faker->phoneNumber,
                    ]);
            }
        }
        foreach ($offices as $office) {
            $boolean = $faker->boolean(30);
            if ($boolean) {
                $office->customers()->attach($faker->randomElements($customers_ids, rand(0, 10)), [
                        'appointment_date'  => '2012-02-02',
                        'time_id'           => 1,
                        'booking_status_id' => 1,
                        'patient_name'      => $faker->name,
                        'mobile'            => $faker->phoneNumber,
                    ]);
            }
        }

        foreach ($offices as $office) {
            $boolean = $faker->boolean(30);
            if ($boolean) {
                $office->customers()->attach($faker->randomElements($customers_ids, rand(0, 10)), [
                        'appointment_date'  => '2012-02-02',
                        'time_id'           => 1,
                        'booking_status_id' => 1,
                        'patient_name'      => $faker->name,
                        'mobile'            => $faker->phoneNumber,
                    ]);
            }
        }

        foreach (\App\Booking::all() as $booking) {
            $booking->update([
                'time_id'           => $faker->randomElement($times_ids),
                'booking_status_id' => $faker->randomElement($statuses_ids),
                'appointment_date'  => $faker->dateTimeBetween('-3 days', '+1 month'),
                'email'             => $faker->email,
                'customer_id'       => $faker->boolean() ? $booking->customer_id : null,
                'insurance_id'      => $faker->boolean() ? $faker->randomElement($insurances_ids) : null,
            ]);
        }

        $this->command->info("Bookings table seeded!");
    }
}
