<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '-1');
        $this->call(CitiesTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
        $this->call(TimesTableSeeder::class);
        $this->call(DaysTableSeeder::class);
        $this->call(MonthsTableSeeder::class);
        $this->call(BookingStatusesTableSeeder::class);
        $this->call(VisitStatusesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DoctorsTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(BookingsTableSeeder::class);
        $this->call(InsurancesTableSeeder::class);
        $this->call(MajorsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(PicturesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(OfficeTimesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(ReviewsTableSeeder::class);
        //$this->call(ContentsTableSeeder::class);
        //$this->call(ImageContentsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        ini_set('memory_limit','256M');
    }
}
