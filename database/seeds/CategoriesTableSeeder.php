<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(["name_ar" => "جراحة"]);
        Category::create(["name_ar" => "استشارة"]);
        Category::create(["name_ar" => "اعلان"]);
        Category::create(["name_ar" => "معرفة"]);
        Category::create(["name_ar" => "دليل"]);
        Category::create(["name_ar" => "مساعدة"]);
        Category::create(["name_ar" => "مقال"]);
        Category::create(["name_ar" => "ترحيب"]);
        Category::create(["name_ar" => "تنويه"]);
        $this->command->info("Categories table seeded!");

    }
}
