<?php

use App\ImageContent;
use Illuminate\Database\Seeder;

class ImageContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImageContent::create([
            "key"            => "home_slider_1",
            "name"           => "Home Slider 1",
            "image_category" => "Home",
            "image_path"     => "/media/image_contents/home_slide_1.jpg",
        ]);
        ImageContent::create([
            "key"            => "home_slider_2",
            "name"           => "Home Slider 2",
            "image_category" => "Home",
            "image_path"     => "/media/image_contents/home_slide_2.jpg",
        ]);
        ImageContent::create([
            "key"            => "home_slider_3",
            "name"           => "Home Slider 3",
            "image_category" => "Home",
            "image_path"     => "/media/image_contents/home_slide_3.jpg",
        ]);

        ImageContent::create([
            "key"            => "home_rudoctor_section",
            "name"           => "Home are you a doctor Bg",
            "image_category" => "Home",
            "image_path"     => "/media/image_contents/home_latest_events_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "home_joining_section",
            "name"           => "Home Join Us Section Bg",
            "image_category" => "Home",
            "image_path"     => "/media/image_contents/home_joining_section.jpg",
        ]);



        $this->command->info("Image Contents table seeded!");

    }
}
