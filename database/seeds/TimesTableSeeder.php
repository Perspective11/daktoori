<?php

use App\Time;
use Illuminate\Database\Seeder;

class TimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = Time::create([
            'key'     => 'day',
            'name_ar'  => 'النهار',
            'name_en'  => 'Day',
            'is_shown' => 1,
        ]);
        $time = Time::create([
            'key'     => 'evening',
            'name_ar'  => 'المساء',
            'name_en'  => 'Evening',
            'is_shown' => 1,
        ]);
        $time = Time::create([
            'key'     => '7:00AM',
            'name_ar'  => '7:00 صباحا',
            'name_en'  => '7:00 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '7:30AM',
            'name_ar'  => '7:30 صباحا',
            'name_en'  => '7:30 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '8:00AM',
            'name_ar'  => '8:00 صباحا',
            'name_en'  => '8:00 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '8:30AM',
            'name_ar'  => '8:30 صباحا',
            'name_en'  => '8:30 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '9:00AM',
            'name_ar'  => '9:00 صباحا',
            'name_en'  => '9:00 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '9:30AM',
            'name_ar'  => '9:30 صباحا',
            'name_en'  => '9:30 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '10:00AM',
            'name_ar'  => '10:00 صباحا',
            'name_en'  => '10:00 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '10:30AM',
            'name_ar'  => '10:30 صباحا',
            'name_en'  => '10:30 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '11:00AM',
            'name_ar'  => '11:00 صباحا',
            'name_en'  => '11:00 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '11:30AM',
            'name_ar'  => '11:30 صباحا',
            'name_en'  => '11:30 AM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '12:00PM',
            'name_ar'  => '12:00 ظهرا',
            'name_en'  => '12:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '12:30PM',
            'name_ar'  => '12:30 ظهرا',
            'name_en'  => '12:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '1:00PM',
            'name_ar'  => '1:00 ظهرا',
            'name_en'  => '1:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '1:30PM',
            'name_ar'  => '1:30 ظهرا',
            'name_en'  => '1:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '2:00PM',
            'name_ar'  => '2:00 ظهرا',
            'name_en'  => '2:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '2:30PM',
            'name_ar'  => '2:30 ظهرا',
            'name_en'  => '2:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '3:00PM',
            'name_ar'  => '3:00 مساءً',
            'name_en'  => '3:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '3:30PM',
            'name_ar'  => '3:30 مساءً',
            'name_en'  => '3:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '4:00PM',
            'name_ar'  => '4:00 مساءً',
            'name_en'  => '4:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '4:30PM',
            'name_ar'  => '4:30 مساءً',
            'name_en'  => '4:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '5:00PM',
            'name_ar'  => '5:00 مساءً',
            'name_en'  => '5:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '5:30PM',
            'name_ar'  => '5:30 مساءً',
            'name_en'  => '5:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '6:00PM',
            'name_ar'  => '6:00 مساءً',
            'name_en'  => '6:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '6:30PM',
            'name_ar'  => '6:30 مساءً',
            'name_en'  => '6:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '7:00PM',
            'name_ar'  => '7:00 مساءً',
            'name_en'  => '7:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '7:30PM',
            'name_ar'  => '7:30 مساءً',
            'name_en'  => '7:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '8:00PM',
            'name_ar'  => '8:00 مساءً',
            'name_en'  => '8:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '8:30PM',
            'name_ar'  => '8:30 مساءً',
            'name_en'  => '8:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '9:00PM',
            'name_ar'  => '9:00 مساءً',
            'name_en'  => '9:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '9:30PM',
            'name_ar'  => '9:30 مساءً',
            'name_en'  => '9:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '10:00PM',
            'name_ar'  => '10:00 مساءً',
            'name_en'  => '10:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '10:30PM',
            'name_ar'  => '10:30 مساءً',
            'name_en'  => '10:30 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '11:00PM',
            'name_ar'  => '11:00 مساءً',
            'name_en'  => '11:00 PM',
            'is_shown' => 0,
        ]);
        $time = Time::create([
            'key'     => '11:30PM',
            'name_ar'  => '11:30 مساءً',
            'name_en'  => '11:30 PM',
            'is_shown' => 0,
        ]);

        $this->command->info("Times table seeded!");
    }
}
