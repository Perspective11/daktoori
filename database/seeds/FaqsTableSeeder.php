<?php

use App\Faq;
use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Faq::create([
            "title_ar" => 'ماهو دكتوري',
            "body_ar"  => 'دكتوري دوت كوم يمكنك من العثور على أفضل طبيب وفقاً لإحتياجك مجاناً . حجز موعد مباشر مع الطبيب الاقرب الى موقعك , و يتيح لك التصفح من خلال قاعدة بيانات محدثة من الأطباء والعيادات والمستشفيات .',
        ]);
        Faq::create([
            "title_ar" => 'كيف يمكننى الاستفادة من خدمة دكتوري؟',
            "body_ar"  => 'يمكنك أيضا معرفة المزيد عن التجربة الطبية ورؤية خبرات الطبيب قبل حجز موعده المباشر.كما يمكنك أيضا معرفة ما إذا كان الطبيب أو العيادة أو المستشفى يقبل التأمين الخاص بك!',
        ]);
        $this->command->info("Faqs table seeded!");
    }
}
