<?php

use App\Doctor;
use App\Major;
use Illuminate\Database\Seeder;

class MajorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');

        $major = Major::create([
            'name_ar' => 'اطفال',
            'name_en' => 'Pediatrician',
        ]);
        $major = Major::create([
            'name_ar' => 'نساء و ولادة',
            'name_en' => 'OB/GYN',
        ]);
        $major = Major::create([
            'name_ar' => 'جراحة',
            'name_en' => 'Surgeon',
        ]);
        $major = Major::create([
            'name_ar' => 'نفساني',
            'name_en' => 'Psychiatrist',
        ]);
        $major = Major::create([
            'name_ar' => 'قلب',
            'name_en' => 'Cardiologist',
        ]);
        $major = Major::create([
            'name_ar' => 'جلدية',
            'name_en' => 'Dermatologist',
        ]);
        $major = Major::create([
            'name_ar' => 'هرمونات',
            'name_en' => 'Endocrinologist',
        ]);
        $major = Major::create([
            'name_ar' => 'باطنية',
            'name_en' => 'Gastroenterologist',
        ]);
        $major = Major::create([
            'name_ar' => 'أمراض معدية',
            'name_en' => 'Infectious Disease Physician',
        ]);
        $major = Major::create([
            'name_ar' => 'الكلى',
            'name_en' => 'Nephrologist',
        ]);
        $major = Major::create([
            'name_ar' => 'عيون',
            'name_en' => 'Ophthalmologist',
        ]);
        $major = Major::create([
            'name_ar' => 'أذن و أنف و حنجرة',
            'name_en' => 'Otolaryngologist',
        ]);
        $major = Major::create([
            'name_ar' => 'الجهاز التنفسي',
            'name_en' => 'Pulmonologist',
        ]);
        $major = Major::create([
            'name_ar' => 'مخ و أعصاب',
            'name_en' => 'Neurologist',
        ]);
        $major = Major::create([
            'name_ar' => 'أورام',
            'name_en' => 'Oncologist',
        ]);
        $major = Major::create([
            'name_ar' => 'أسنان',
            'name_en' => 'Dentist',
        ]);


        $major = Major::create([
            'name_ar' => 'أمراض دم',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'تخسيس وتغذية',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'جراحة اطفال',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'جراحة اورام',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'جراحة اوعية دموية',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'جراحة تجميل',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'حساسية ومناعة',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'ذكورة وعقم',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'سكر وغدد صماء',
            'level'   => 2,
        ]);
        $major = Major::create([
            'name_ar' => 'سمعيات',
            'level'   => 2,
        ]);

        $firstMajors = \App\Major::where('level', 1)->pluck('id')->toArray();
        $secondMajors = \App\Major::where('level', 2)->pluck('id')->toArray();

        $doctors = Doctor::each(function ($d) use ($faker, $firstMajors, $secondMajors) {
            $d->majors()->attach($faker->randomElement($firstMajors));
            $d->majors()->attach($faker->randomElements($secondMajors, rand(1, 3)));
        });

        $this->command->info("Majors with Doctors table seeded!");
    }
}
