<?php

use App\VisitStatus;
use Illuminate\Database\Seeder;

class VisitStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $visit_status = VisitStatus::create([
            'key'         => 'no_visit',
            'name_ar'     => 'لم تتم الزيارة',
            'name_en'     => 'No visit',
            'is_approved' => 2,
        ]);
        $visit_status = VisitStatus::create([
            'key'         => 'done',
            'name_ar'     => 'تمت الزيارة',
            'name_en'     => 'Visit done',
            'is_approved' => 1,
        ]);
        $visit_status = VisitStatus::create([
            'key'         => 'no_answer',
            'name_ar'     => 'لم يرد المريض',
            'name_en'     => 'No answer from patient',
            'is_approved' => 2,
        ]);
        $visit_status = VisitStatus::create([
            'key'         => 'abscent_doctor',
            'name_ar'     => 'الطبيب غير مداوم',
            'name_en'     => 'Abscent Doctor',
            'is_approved' => 2,
        ]);
        $visit_status = VisitStatus::create([
            'key'         => 'done_free_return',
            'name_ar'     => 'تمت الزيارة عودة مجانية',
            'name_en'     => 'Visit done free return',
            'is_approved' => 1,
        ]);
        $visit_status = VisitStatus::create([
            'key'         => 'done_paid_return',
            'name_ar'     => 'تمت الزيارة عودة مدفوعة',
            'name_en'     => 'Visit done paid return',
            'is_approved' => 1,
        ]);
        $visit_status = VisitStatus::create([
            'key'         => 'doctor_case',
            'name_ar'     => 'حالة الطبيب',
            'name_en'     => 'Doctor case',
            'is_approved' => 0,
        ]);
        $visit_status = VisitStatus::create([
            'key'         => 'operation_perscribed',
            'name_ar'     => 'تم تقرير عملية',
            'name_en'     => 'Operation was perscribed',
            'is_approved' => 1,
        ]);
        $visit_status = VisitStatus::create([
            'key'         => 'not_reachable',
            'name_ar'     => 'تعذر الوصول للمريض',
            'name_en'     => 'Patient not reachable',
            'is_approved' => 2,
        ]);
        $this->command->info("Visit Statuses table seeded!");
    }
}
