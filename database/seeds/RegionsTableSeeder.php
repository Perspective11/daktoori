<?php

use App\Region;
use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = App\City::pluck('id', 'key')->toArray();

        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'hadda',
            'name_ar'  => 'حدة',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'bayt-bows',
            'name_ar'  => 'بيت بوس',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'safia',
            'name_ar'  => 'الصافية',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'tahreer',
            'name_ar'  => 'التحرير',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'bab-alyemen',
            'name_ar'  => 'باب اليمن',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'jamal',
            'name_ar'  => 'شارع جمال',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'sunainah',
            'name_ar'  => 'السنينة',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'seteen-north',
            'name_ar'  => 'الستين الشمالي',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'seteen-south',
            'name_ar'  => 'الستين الجنوبي',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'taiz-street',
            'name_ar'  => 'شارع تعز',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'shumailah',
            'name_ar'  => 'شميلة',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'shoub',
            'name_ar'  => 'شعوب',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'hasabah',
            'name_ar'  => 'الحصبة',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'alqasr-street',
            'name_ar'  => 'شارع القصر',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'sakhr-street',
            'name_ar'  => 'شارع صخر',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'saawan',
            'name_ar'  => 'سعوان',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'sana',
            'name_ar'  => 'سنع',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'aser',
            'name_ar'  => 'عصر',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'mathbah',
            'name_ar'  => 'مذبح',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'almatar',
            'name_ar'  => 'شارع المطار',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'oshash',
            'name_ar'  => 'العشاش',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'asbahi',
            'name_ar'  => 'الأصبحي',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'dar-salm',
            'name_ar'  => 'دار سلم',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => '70-street',
            'name_ar'  => 'شارع السبعين',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => '50-street',
            'name_ar'  => 'شارع الخمسين',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => '45-street',
            'name_ar'  => 'شارع 45',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => '30-street',
            'name_ar'  => 'شارع الثلاثين',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'allakamah',
            'name_ar'  => 'اللكمة',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'hail-street',
            'name_ar'  => 'شارع هايل',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'sanhan',
            'name_ar'  => 'سنحان',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'bani-matar',
            'name_ar'  => 'بني مطر',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'ziraa-street',
            'name_ar'  => 'شارع الزراعة',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'television-street',
            'name_ar'  => 'شارع التلفزيون',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'zubairi',
            'name_ar'  => 'شارع الزبيري',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'qiadah',
            'name_ar'  => 'شارع القيادة',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'kamaran',
            'name_ar'  => 'شارع كمران',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'marib_cross',
            'name_ar'  => 'جولة مأرب',
            'is_shown' => 1,
        ]);
        $region = Region::create([
            'city_id'  => $cities['sanaa'],
            'key'      => 'dairy_street',
            'name_ar'  => 'شارع الدائري',
            'is_shown' => 1,
        ]);

    }
}
