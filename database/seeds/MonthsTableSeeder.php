<?php

use App\Month;
use Illuminate\Database\Seeder;

class MonthsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $months = Month::create([
            'id'      => 1,
            'name_ar' => 'يناير',
            'name_en' => 'January',
        ]);
        $months = Month::create([
            'id'      => 2,
            'name_ar' => 'فبراير',
            'name_en' => 'February',
        ]);
        $months = Month::create([
            'id'      => 3,
            'name_ar' => 'مارس',
            'name_en' => 'March',
        ]);
        $months = Month::create([
            'id'      => 4,
            'name_ar' => 'ابريل',
            'name_en' => 'April',
        ]);
        $months = Month::create([
            'id'      => 5,
            'name_ar' => 'مايو',
            'name_en' => 'May',
        ]);
        $months = Month::create([
            'id'      => 6,
            'name_ar' => 'يونيو',
            'name_en' => 'June',
        ]);
        $months = Month::create([
            'id'      => 7,
            'name_ar' => 'يوليو',
            'name_en' => 'July',
        ]);
        $months = Month::create([
            'id'      => 8,
            'name_ar' => 'اغسطس',
            'name_en' => 'August',
        ]);
        $months = Month::create([
            'id'      => 9,
            'name_ar' => 'سبتمبر',
            'name_en' => 'September',
        ]);
        $months = Month::create([
            'id'      => 10,
            'name_ar' => 'اكتوبر',
            'name_en' => 'October',
        ]);
        $months = Month::create([
            'id'      => 11,
            'name_ar' => 'نوفمبر',
            'name_en' => 'November',
        ]);
        $months = Month::create([
            'id'      => 12,
            'name_ar' => 'ديسمبر',
            'name_en' => 'December',
        ]);
        $this->command->info("Months table seeded!");

    }
}
