<?php

use App\BookingStatus;
use Illuminate\Database\Seeder;

class BookingStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $booking_status = BookingStatus::create([
            'key'         => 'reviewing',
            'name_ar'     => 'قيد المراجعة',
            'name_en'     => 'Under review',
            'is_approved' => 0,
        ]);
        $booking_status = BookingStatus::create([
            'key'         => 'approving',
            'name_ar'     => 'بانتظار الموافقة',
            'name_en'     => 'Waiting for approval',
            'is_approved' => 0,
        ]);
        $booking_status = BookingStatus::create([
            'key'         => 'confirmed',
            'name_ar'     => 'تم التأكيد',
            'name_en'     => 'Confirmed',
            'is_approved' => 1,
        ]);
        $booking_status = BookingStatus::create([
            'key'         => 'rejected',
            'name_ar'     => 'تم الرفض',
            'name_en'     => 'Rejected',
            'is_approved' => 2,
        ]);
        $booking_status = BookingStatus::create([
            'key'         => 'passed',
            'name_ar'     => 'انتهى بدون موافقة',
            'name_en'     => 'Passed Without Approval',
            'is_approved' => 2,
        ]);
        $booking_status = BookingStatus::create([
            'key'         => 'canceled_by_customer',
            'name_ar'     => 'الغي من قبل المريض',
            'name_en'     => 'Canceled By Patient',
            'is_approved' => 2,
        ]);
        $booking_status = BookingStatus::create([
            'key'         => 'canceled_by_doctor',
            'name_ar'     => 'الغي من قبل الدكتور',
            'name_en'     => 'Canceled By Doctor',
            'is_approved' => 2,
        ]);
        $this->command->info("Booking Statuses table seeded!");
    }
}
