<?php

use App\Day;
use Illuminate\Database\Seeder;

class DaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $day = Day::create([
            'id'      => 1,
            'name_ar' => 'السبت',
            'name_en' => 'Saturday',
        ]);
        $day = Day::create([
            'id'      => 2,
            'name_ar' => 'الأحد',
            'name_en' => 'Sunday',
        ]);
        $day = Day::create([
            'id'      => 3,
            'name_ar' => 'الإثنين',
            'name_en' => 'Monday',
        ]);
        $day = Day::create([
            'id'      => 4,
            'name_ar' => 'الثلاثاء',
            'name_en' => 'Tuesday',
        ]);
        $day = Day::create([
            'id'      => 5,
            'name_ar' => 'الأربعاء',
            'name_en' => 'Wednesday',
        ]);
        $day = Day::create([
            'id'      => 6,
            'name_ar' => 'الخميس',
            'name_en' => 'Thursday',
        ]);
        $day = Day::create([
            'id'      => 7,
            'name_ar' => 'الجمعة',
            'name_en' => 'Friday',
        ]);

        $this->command->info("Days table seeded!");
    }
}
